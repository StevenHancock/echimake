/*######################################################################################*/
/*#These are functions for echiMake                                                    #*/
/*#They are mainly concerned with creating a voxel map                                 #*/
/*# 15th February 2007              Steven Hancock, Geography, UCL                     #*/
/*######################################################################################*/
#include "echiMake.h"
#include "nrutil.h"
#include "nrutil.c" 

/*######################################################################################################*/
/* The big one, create a voxel representation out of all the files, prepared in prepareVoxels*/

#define MATN 2  /*the number of materials in the canopy*/
#define mapHEAD 7       /*length of map file header*/
#define mapFLOA 4       /*length of float map header*/
#define voxHEAD 7       /*length of voxel file integer header*/
#define voxFLOA 4       /*length of voxel file float header*/
#define ANGLERES 100    /*the resolution for eccetricity to omega array*/

/*some functions needed by all these subfunctions*/
float *meanLAD(RatControl *,RatImage *,int,int,int,float *);
float meanPsi(RatControl *,RatImage *,int,int,int);
float *meanThetaHat(RatControl *,RatImage *,float *,float *,int,int);

void marquardtLAD(float, float *, float *, float *, int,int); /*function for levnbger marquardt*/ 


RatImage *createVoxels(RatControl *ratPoint,RatImage *dimage)
{
  int i=0,mat=0;                 /*loop controls*/
  int Vplace=0;                  /*voxel label*/
  int numb=0,Lplace=0,band=0;    /*lidar beam labels*/
  int inter=0;                   /*a running total for averages along the way*/
  RatImage *prepareVoxels(RatControl *,RatImage *);
  RatImage *paintVoxels(RatImage *,RatControl *);
  RatImage *compareMaterials(RatImage *,RatControl *);
  RatImage *vrmlUp(RatImage *,RatControl *);
  RatImage *writeVoxels(RatImage *,RatControl *,int);
  RatImage *clearAngles(RatImage *);
  RatImage *solveLAD(RatImage *,RatControl *,int);
  RatImage *asciiVoxels(RatImage *,RatControl *,char[100]);
  RatImage *clumpingCorrection(RatImage *,RatControl *);
  RatImage *initialPsi(RatImage *,RatControl *);
  RatImage *countZeniths(RatImage *,RatControl *);
  RatImage *resizeGaps(RatImage *,RatControl *);
  RatImage *resizeGapsWithin(RatImage *,RatControl *);
  RatImage *initialAreas(RatImage *,RatControl *);
  RatImage *calculatePsi(RatImage *,RatControl *,int);
  RatImage *calculateArea(RatImage *,RatControl *,int);
  RatImage *clearVoxels(RatImage *);
  RatImage *linearVoxel(RatImage *,RatControl *,int);
  void resizeLinearGapsWithin(RatImage *,RatControl *);
  void initialLinearAreas(RatImage *,RatControl *);
  void resizeLinearGaps(RatImage *,RatControl *);
  void clearTheRest(RatImage *,RatControl *,float **,float **);
  void clearBeamVoxel(RatControl *,RatImage *);
  void flattenVoxels(RatImage *,RatControl *);
  void readPhaseLUT(RatImage *);
  void linearRadianceError(RatControl *,RatImage *,float **);
  float **radianceError(RatControl *,RatImage *,float **);
  float **refErr=NULL,**oldErr=NULL;  /*one for each waveband, but each file could have different nBands*/
  double mu=0;                        /*angle for LAD*/
  float guessGap(RatControl *,RatImage *,float,int,int,int);
  float oldTotalErr=0,newTotalErr=0;
  float oldAverageErr=0,newAverageErr=0;
  /*int Langle=0;  */
  char namen[200];

  /*create the maps*/
  dimage=prepareVoxels(ratPoint,dimage);

  if(dimage->linearVoxels)readPhaseLUT(dimage);
  
  mu=0.955655;  /*54.755 degrees in radians*/ 
  dimage->mu=mu;
  dimage->cos2mu=cos(2*mu);

  dimage=initialPsi(dimage,ratPoint);


  if(!dimage->linearVoxels)dimage=initialAreas(dimage,ratPoint);
  else                     initialLinearAreas(dimage,ratPoint);

  fprintf(stdout,"Resizing gaps...\n");
  if(!dimage->linearVoxels)dimage=resizeGaps(dimage,ratPoint);
  else                     resizeLinearGaps(dimage,ratPoint);

  ratPoint=clearAllArrays(ratPoint,dimage);

  /*once that has been done begin with the iterations*/
  if(!(refErr=(float **)calloc(dimage->scanN,sizeof(float *)))){
    fprintf(stderr,"error in radiance error allocation.\n");
    exit(1);
  }
  if(!(oldErr=(float **)calloc(dimage->scanN,sizeof(float *)))){
    fprintf(stderr,"error in radiance error allocation.\n");
    exit(1);
  }
  for(i=0;i<dimage->scanN;i++){
    refErr[i]=falloc(dimage->nBands,"radiance error",i);
    oldErr[i]=falloc(dimage->nBands,"previous radiance error",i);
  }
  fprintf(stdout,"Calculating initial radiant flux error...\n");
  if(!dimage->linearVoxels)refErr=radianceError(ratPoint,dimage,refErr);
  else                     linearRadianceError(ratPoint,dimage,refErr);

  /*step over beams passing through to see if we have multiple zeniths*/
  dimage=countZeniths(dimage,ratPoint);

  printf("Having a go at iterating...\n");

  /*########the iterations##################*/
  do{
    for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
      if(dimage->voxel[Vplace].contN>0){  /*otherwise we can't solve except by the gap*/

        dimage=calculatePsi(dimage,ratPoint,Vplace);

        if(!dimage->linearVoxels){   /*do it the long, non-linear way*/
          dimage=solveLAD(dimage,ratPoint,Vplace);
          dimage=calculateArea(dimage,ratPoint,Vplace);
        }else{   /*otherwise use the really simple linear model*/
          dimage=linearVoxel(dimage,ratPoint,Vplace);
        }

        for(i=0;i<dimage->voxel[Vplace].contN;i++){
          numb=dimage->voxel[Vplace].rayMap[3*i];
          Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
          ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
        }

      }else{ /*Any intersections*/
        for(mat=0;mat<dimage->matN;mat++){
          dimage->voxel[Vplace].Ae[mat]=-1.0;
        }
      }
      /*dimage=clearAngles(dimage);*/
    } /*end of voxel step ######end of parameter calculation within#######*/


    /*step through all beams and make sure none are being prematurley blocked*/
    if(dimage->anyAdjustable){
      printf("Checking gaps %d...\n",inter);
      if(!dimage->linearVoxels)dimage=resizeGapsWithin(dimage,ratPoint);
      else                     resizeLinearGapsWithin(dimage,ratPoint);
    }

    /*ratPoint=clearAllArrays(ratPoint,dimage);*/ /*just in case I have forgotten*/

    oldAverageErr=refErr[0][0]-oldErr[0][0];

    /*now we have adjusted all the voxels caluclate the error*/
    oldTotalErr=0;
    for(numb=0;numb<dimage->scanN;numb++){
      for(i=0;i<dimage->nBands;i++){
        band=dimage->band[numb][i];
        oldErr[numb][band]=refErr[numb][band];
        oldTotalErr+=refErr[numb][band]/(float)(dimage->nBands*dimage->scanN);
      }
    }
    oldTotalErr/=dimage->scanN;
    if(inter>7)refErr[0][1]=100;
    printf("Iteration %d\n",inter);
    if(!dimage->linearVoxels)refErr=radianceError(ratPoint,dimage,refErr);
    else                     linearRadianceError(ratPoint,dimage,refErr);
    newTotalErr=0;
    for(numb=0;numb<dimage->scanN;numb++){
      for(i=0;i<dimage->nBands;i++){
        band=dimage->band[numb][i];
        newTotalErr+=refErr[numb][band]/(float)(dimage->nBands*dimage->scanN);
      }
    }
    newTotalErr/=dimage->scanN;
    newAverageErr=refErr[0][1]-oldErr[0][1];
    printf("total error %f\n",newTotalErr);
    if((newTotalErr-oldTotalErr)*(newTotalErr-oldTotalErr)<0.000001*0.000001){
      printf("That's as close as you'll get of the firt kind\n");
      break;
    }else if(refErr[0][1]==oldErr[0][1]){
      printf("That's as close as you'll get of the second kind\n");
      break;
    }else if((inter>2)&&(newAverageErr==oldAverageErr)){
      printf("We are probably yoyoing\n");
      break;
    }
    if(dimage->Vlog){
      /*print out the estimate so far*/
      sprintf(namen,"estimatedValues.%d",inter);
      if(dimage->Vascii)dimage=asciiVoxels(dimage,ratPoint,namen);
      dimage=writeVoxels(dimage,ratPoint,inter);
    }
    inter++;

    ratPoint=clearAllArrays(ratPoint,dimage);
  }while(newTotalErr>dimage->errThresh&&inter<20);   /*end of the iterative process #########################*/

  if(inter>19)printf("We gave up, it was probably yoyoing.\n");

  /*print out the results to a file*/
  sprintf(namen,"%s.finalValues",dimage->output);
  dimage=asciiVoxels(dimage,ratPoint,namen);
  dimage=writeVoxels(dimage,ratPoint,-1);

  if(dimage->vzoom&&(!dimage->Vflat))dimage=paintVoxels(dimage,ratPoint);
  else if(dimage->vzoom&&dimage->Vflat)flattenVoxels(dimage,ratPoint);
/*  if(dimage->vrml)dimage=vrmlUp(dimage,ratPoint); */
/*  dimage=compareMaterials(dimage,ratPoint);  */
/*  dimage=clumpingCorrection(dimage,ratPoint); */

  printf("The voxel representation has been finished.\n");

  dimage=clearVoxels(dimage);
  clearBeamVoxel(ratPoint,dimage);

  ratPoint=clearAllArrays(ratPoint,dimage);

  clearTheRest(dimage,ratPoint,refErr,oldErr);

  return(dimage);
} /*##main voxel function##*/

/*######################################################################################################*/
/*prepares the voxel arrays and creates the map*/

RatImage *prepareVoxels(RatControl *ratPoint,RatImage *dimage)
{
  int i=0,j=0,k=0,m=0;       /*loop controls*/
  int place=0,zen=0,az=0;    /*zenith and azimuth elements of "place"*/
  int numb=0,Lplace=0,bin=0; /*lidar beam labels*/
  int maxbins=0;             /*maximum number of bins in a scans*/
  int binStart=0,binEnd=0;
  int placeX=0,placeY=0,placeZ=0;
  int leftMost=0,rightMost=0;
  int n=0;
  int **binSep=NULL;           /*separation in terms of bins*/
  float a=0;                  /*half length of a voxel*/
  float sep=0;
  float x=0,y=0,z=0;         /*bin coordinates*/
  /*float vtheta=0,vthata=0;*/   /*direction from scan to voxel*/
  /*float thethresh=0,thathresh=0;*//*angle to look either side of each beam*/
  float dx=0,dy=0,dz=0;      /*scanner to voxel vectors*/
  int *mapTransf=NULL;       /*temporary array for beam and voxel maps*/
  float dvox=0;                 /*temporary number for intersection*/
  float *maxSepSq=NULL,maxSep=0,sepSq=0; /*separations and squares for intersection test*/
  float theta=0,thata=0;                 /*beam azimuth for upside down*/
  float corn[8][3];                      /*corners of the voxel space*/
  float cornBin[8][3];                   /*corners of the range bin*/
  float cornVox[8][3];                   /*corners of the voxel*/
  float vect[4][3],vecttocentre[3];      /* vectors for corner sorting*/
  float maxAz=0,minAz=0,angle=0;
  float thataLeft=0,thataRight=0;
  float thataStart=0,thataEnd=0;
  float sinTheta=0,cosTheta=0,sinThata=0; /*for rotation matrices*/
  float cosThata=0,sinBeta=0,cosBeta=0;   /*for rotation matrices*/
  float tanDiv=0;
  float ddashX=0,ddashY=0,ddashZ=0;       /*for rotation matrices*/
  float Rx=0,Ry=0,Rz=0;                   /*rotation matrix elements*/
  int *azStart=NULL,*azEnd=NULL,nAz=0;
  int *zStart=NULL,*zEnd=NULL; 
  float PI_2=0;
  char intersect=0;                       /*intersect indicator*/
  /*char doazi=0;    */                      /*intersection test pointer*/
  RatImage *omegaEccentricity(RatImage *);/*to create a look up table for conversion*/
  RatImage *clearAngles(RatImage *);
  RatImage *readBinaryMaps(RatImage *,RatControl *);
  RatImage *writeBinaryMaps(RatImage *,RatControl *);
  RatControl *labelAlbedo(RatImage *,RatControl *);
  void proportionOverlap(RatControl *,RatImage *);
  void asciiMaps(RatImage *,RatControl *);

  a=dimage->voxRes/2.0;    /*set the half length of a voxel side*/

  /*set up the zeniths for angle of incidence with a spheroid*/
  for(numb=0;numb<dimage->scanN;numb++){
    if(!(ratPoint[numb].Etheta=(float *)calloc(ratPoint[numb].NZscans,sizeof(float)))){
      fprintf(stderr,"error in zenith allocation 234.76 to the power of pi.\n");
      exit(1);
    }
    for(i=0;i<ratPoint[numb].NZscans;i++){
      Lplace=i*ratPoint[numb].NAscans;
   /*   ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,2); this is redundant now*/
      if(ratPoint[numb].theta[Lplace]>=0&&ratPoint[numb].theta[Lplace]<=M_PI/2.0)\
        ratPoint[numb].Etheta[i]=ratPoint[numb].theta[Lplace];
      else if(ratPoint[numb].theta[Lplace]<=0&&ratPoint[numb].theta[Lplace]>=M_PI/-2.0)\
        ratPoint[numb].Etheta[i]=-1*ratPoint[numb].theta[Lplace];
      else if(ratPoint[numb].theta[Lplace]>M_PI/2&&ratPoint[numb].theta[Lplace]<=M_PI)\
        ratPoint[numb].Etheta[i]=M_PI-ratPoint[numb].theta[Lplace];
      else if(ratPoint[numb].theta[Lplace]<M_PI/-2&&ratPoint[numb].theta[Lplace]>=-1*M_PI)\
        ratPoint[numb].Etheta[i]=M_PI+ratPoint[numb].theta[Lplace];
      /*dimage=clearAngles(dimage);*/
    }
  }  /*end of relative ellipsoid zenith labelling loop*/

  ratPoint=labelAlbedo(dimage,ratPoint);

  /*set up the voxel arrays*/
  if(!(dimage->ends=(float *)calloc(6,sizeof(float)))){
    fprintf(stderr,"error in  boundary allocation.\n");
    exit(1);
  }
  for(i=0;i<3;i++){
    dimage->voxNumb[i]=(int)(dimage->voxScene[i]/dimage->voxRes+0.5);
    dimage->ends[2*i]=dimage->voxCentre[i]-(float)(dimage->voxNumb[i]+1)*a;    /*I have added one here to prevent rounding*/
    dimage->ends[2*i+1]=dimage->voxCentre[i]+(float)(dimage->voxNumb[i]+1)*a;  /*a little inelegeant and should probably be refined*/
  }
  dimage->totalVoxels=dimage->voxNumb[0]*dimage->voxNumb[1]*dimage->voxNumb[2];
  if(dimage->totalVoxels<1){
    fprintf(stderr,"You have chosen %d voxels, are you quite sure?\n",dimage->totalVoxels);
    exit(1);
  }

  /*allocate voxel space*/
  if(!(dimage->voxel=(Voxel *)calloc(dimage->totalVoxels,sizeof(Voxel)))){
    fprintf(stderr,"error in voxel data allocation for %d voxels.\n",dimage->totalVoxels);
    exit(1);
  }
  for(i=0;i<dimage->totalVoxels;i++){
    if(!(dimage->voxel[i].LAD=(float *)calloc(dimage->matN,sizeof(float)))){
      fprintf(stderr,"error in LAD data allocation.\n");
      exit(1);
    }
    if(!(dimage->voxel[i].Ae=(float *)calloc(dimage->matN,sizeof(float)))){
      fprintf(stderr,"error in element area data allocation.\n");
      exit(1);
    }
    /*set some initial values*/
    if(!dimage->linearVoxels){
      dimage->voxel[i].LAD[0]=M_PI/4.0;
      dimage->voxel[i].LAD[1]=M_PI/4.0; /*index for leaf 0, and bark 1*/
    }else{
      dimage->voxel[i].LAD[0]=0.0;  /*flat line to make spherical*/
      dimage->voxel[i].LAD[1]=0.0;  /*same for wood*/
    }
    dimage->voxel[i].Ae[0]=-1.0;
    dimage->voxel[i].Ae[1]=-1.0;
    dimage->voxel[i].psi=0.5;
  } /*end of voxel allocation*/

  dimage=omegaEccentricity(dimage);  /*create a look up table*/


  dimage=readBinaryMaps(dimage,ratPoint);

  /*dimage->createMap=1;*/
  if(!dimage->createMap){
    TIDY(dimage->ends);
    proportionOverlap(ratPoint,dimage);
    return(dimage);  /*then we have read the map from elsewhere*/
  }

  /*From here on it is just creating the maps*/

  /*calculate maximum number of bins to use for array allocation and things*/
  for(i=0;i<dimage->scanN;i++)if(ratPoint[i].bins>maxbins)maxbins=ratPoint[i].bins;
  maxSepSq=falloc(dimage->scanN*maxbins,"maximum separation",0);
  if(!(binSep=(int **)calloc(dimage->scanN,sizeof(int *)))){
    fprintf(stderr,"error in maximum separation allocation.\n");
    exit(1);
  }
  for(i=0;i<numb;i++)binSep[i]=ialloc(maxbins,"bin separation",numb);


  /*allocate space for beam labels and set up bin dimensions*/
  dvox=sqrt(3)*a;
  for(i=0;i<dimage->scanN;i++){
    tanDiv=tan(ratPoint[i].div/2.0);
    for(j=0;j<ratPoint[i].NZscans*ratPoint[i].NAscans;j++){
      if(!(ratPoint[i].ratRes[j].voxN=(int *)calloc(ratPoint[i].bins,sizeof(int)))){
        fprintf(stderr,"error in beam map allocation.\n");
        exit(1);
      }
      if(!(ratPoint[i].ratRes[j].voxMap=(int **)calloc(ratPoint[i].bins,sizeof(int *)))){
        fprintf(stderr,"error in beam map allocation 2.\n");
        exit(1);
      }
      /*set all bins to have no voxels within them for now*/
      for(k=0;k<ratPoint[i].bins;k++)ratPoint[i].ratRes[j].voxN[k]=0;
    }
    for(j=0;j<ratPoint[i].bins;j++){
      x=((float)j+1.0)*ratPoint[i].bin_L*tanDiv;
      sep=sqrt(x*x+pow(ratPoint[i].bin_L/2.0,2.0));
      binSep[i][j]=(int)(sep/dimage->voxRes)+1;

      maxSepSq[i*maxbins+j]=dvox+sqrt(((double)(bin+1)*ratPoint[i].bin_L/2.0+ratPoint[i].min_R)*\
        ((double)(bin+1)*ratPoint[i].bin_L/2.0+ratPoint[i].min_R)*tan(ratPoint[i].div/2)*\
        tan(ratPoint[i].div/2.0)+ratPoint[i].bin_L*ratPoint[i].bin_L/16.0);
      maxSepSq[i*maxbins+j]*=maxSepSq[i*maxbins+j];
    }
  }/*scan step, beam label allocation*/

  /*start off by mapping out the datasets for speed later on*/
  fprintf(stdout,"\nCreating a voxel to beam map ...\n");

/*#these were added here to check the memory mapping, which is fudged*/
/*  if((opoo=fopen("bloodyRAM","w"))==NULL){
#    printf("Error opening results file %s\n","voxMap");
#    exit(1);
#  }
#  if((owoo=fopen("flaminRAM","w"))==NULL){
#    printf("Error opening results file %s\n","voxMap");
#    exit(1);
#  }
*/



  /*The new mapping loops*/
  fprintf(stdout,"There will be %d voxels\n",dimage->totalVoxels);
  for(i=0;i<dimage->voxNumb[0];i++){     /*step over x*/
    for(j=0;j<dimage->voxNumb[1];j++){   /*step over y*/
      for(k=0;k<dimage->voxNumb[2];k++){ /*step over z*/
        place=i*dimage->voxNumb[1]*dimage->voxNumb[2]+j*dimage->voxNumb[2]+k;
        /*printf("Dealing with voxel %d of %d\n",place+1,dimage->totalVoxels);
        fflush(stdout);*/
        /*calculate the central cartesian coordinate of each voxel*/
        dimage->voxel[place].pos[0]=dimage->voxCentre[0]+((float)i-(float)dimage->voxNumb[0]/2.0)*dimage->voxRes;
        dimage->voxel[place].pos[1]=dimage->voxCentre[1]+((float)j-(float)dimage->voxNumb[1]/2.0)*dimage->voxRes;
        dimage->voxel[place].pos[2]=dimage->voxCentre[2]+((float)k-(float)dimage->voxNumb[2]/2.0)*dimage->voxRes;
      }
    }
  }

  /* determine the four corners of the whole scene, could become eight for the zenith range*/
  place=0;
  corn[0][0]=dimage->voxel[place].pos[0]-dimage->voxRes/2.0;
  corn[0][1]=dimage->voxel[place].pos[1]-dimage->voxRes/2.0;
  corn[0][2]=dimage->voxel[place].pos[2]-dimage->voxRes/2.0;

  place=(dimage->voxNumb[0]-1)*dimage->voxNumb[1]*dimage->voxNumb[2];
  corn[1][0]=dimage->voxel[place].pos[0]+dimage->voxRes/2.0;
  corn[1][1]=dimage->voxel[place].pos[1]-dimage->voxRes/2.0;
  corn[1][2]=dimage->voxel[place].pos[2]-dimage->voxRes/2.0;

  place=(dimage->voxNumb[1]-1)*dimage->voxNumb[2];
  corn[2][0]=dimage->voxel[place].pos[0]-dimage->voxRes/2.0;
  corn[2][1]=dimage->voxel[place].pos[1]+dimage->voxRes/2.0;
  corn[2][2]=dimage->voxel[place].pos[2]-dimage->voxRes/2.0;

  place=(dimage->voxNumb[0]-1)*dimage->voxNumb[1]*dimage->voxNumb[2]+(dimage->voxNumb[1]-1)*dimage->voxNumb[2];
  corn[3][0]=dimage->voxel[place].pos[0]+dimage->voxRes/2.0;
  corn[3][1]=dimage->voxel[place].pos[1]+dimage->voxRes/2.0;
  corn[3][2]=dimage->voxel[place].pos[2]-dimage->voxRes/2.0;




  for(numb=0;numb<dimage->scanN;numb++){
    /*determine the azimuth range of interest. It may be multiple (ie positive and negative, mind the zenith sigh*/
    for(i=0;i<3;i++)vecttocentre[i]=dimage->voxCentre[i]-ratPoint[numb].from[i]; /*from scan centre to the scene centre*/
    maxAz=-10.0;
    minAz=561.0;
    for(j=0;j<4;j++){  /*loop over the four corners*/
      for(i=0;i<3;i++)vect[j][i]=corn[j][i]-ratPoint[numb].from[i];
      angle=acos((vect[j][0]*vecttocentre[0]+vect[j][1]*vecttocentre[1])/(sqrt(vect[j][0]*vect[j][0]+vect[j][1]*vect[j][1])*sqrt(vecttocentre[0]*vecttocentre[0]+vecttocentre[1]*vecttocentre[1])));
      /*detemine whether it's positive or negative*/
      if((vect[j][1]*vecttocentre[0]-vect[j][0]*vecttocentre[1])<0.0)angle*=-1.0;
      if(angle>maxAz){
        maxAz=angle;
        leftMost=j;
      }
      if(angle<minAz){
        minAz=angle;
        rightMost=j;
      }
    }/*four corner loop*/
    /*and now determine where the start and finish of the azimuth is, in terms of indices*/

    PI_2=2.0*M_PI;

    thataLeft=decimalMod(atan2(vect[leftMost][1],vect[leftMost][0]),PI_2);
    thataRight=decimalMod(atan2(vect[rightMost][1],vect[rightMost][0]),PI_2);

    //if(thataLeft<thataRight){ /*if we have is split over the quarter spheres*/
      /*if(nAz==1){
        fprintf(stderr,"Aaarrgghh! I know too much and need to gouge my own ears out with a spoon!\nA SPOON!!!\n");
        exit(1);
      }
      thata=thataLeft;
      thataLeft=thataRight;
      thataRight=thata;
    }*/

    /*the scan start and end*/
    thataStart=decimalMod(ratPoint[numb].thata[0],PI_2);
    thataEnd=decimalMod(ratPoint[numb].thata[ratPoint[numb].NAscans*ratPoint[numb].NZscans-1],PI_2);

    if((ratPoint[numb].theta[0]<0.0)&&(ratPoint[numb].NAscans>1)&&(ratPoint[numb].NZscans>1)){
      if((thataRight<=thataEnd)&&(thataRight>=thataStart)){  /*start on the positive zenith side*/
        if((thataLeft<=thataEnd)&&(thataLeft>=thataStart)){
          nAz=1;
          azStart=ialloc(nAz,"azimuth start for map",numb);
          azStart[0]=(int)((thataRight-thataStart)/ratPoint[numb].divSt);
          azEnd=ialloc(nAz,"azimuth start for map",numb);
          azEnd[0]=(int)((thataLeft-thataStart)/ratPoint[numb].divSt)+1;
          zStart=ialloc(nAz,"zenith start for map",numb);
          zStart[0]=0;  /*I believe this should be ratPoint[numb].NZscans/2 but it seems rto work, how odd*/
          zEnd=ialloc(nAz,"zenith end for map",numb);
          zEnd[0]=ratPoint[numb].NZscans;
        }else{  /*the start and end are split over the two sides*/
          nAz=2;
          azStart=ialloc(nAz,"azimuth start for map",numb);
          azStart[0]=(int)((thataRight-thataStart)/ratPoint[numb].divSt);
          azStart[1]=0;
          azEnd=ialloc(nAz,"azimuth end for map",numb);
          azEnd[0]=ratPoint[numb].NAscans;
          azEnd[1]=(int)((thataLeft-thataStart-M_PI)/ratPoint[numb].divSt)+1;
          zStart=ialloc(nAz,"zenith start for map",numb);
          zStart[0]=ratPoint[numb].NZscans/2;
          zStart[1]=0;
          zEnd=ialloc(nAz,"zenith end for map",numb);
          zEnd[0]=ratPoint[numb].NZscans;
          zEnd[1]=ratPoint[numb].NZscans/2+1;
        }
      }else{  /*start on the negative zenith side*/
        if(thataLeft>=(M_PI+ratPoint[numb].thata[0])){
          nAz=1;
          azStart=ialloc(nAz,"azimuth start for map",numb);
          azStart[0]=(int)((thataRight-thataStart-M_PI)/ratPoint[numb].divSt);
          azEnd=ialloc(nAz,"azimuth end for map",numb);
          azEnd[0]=(int)((thataLeft-thataStart-M_PI)/ratPoint[numb].divSt)+1;
          zStart=ialloc(nAz,"zenith start for map",numb);
          zStart[0]=0;
          zEnd=ialloc(nAz,"zenith end for map",numb);
          zEnd[0]=ratPoint[numb].NZscans/2+1;
        }else{ /*the start and end are split over the two sides*/
          nAz=2;
          azStart=ialloc(nAz,"azimuth start for map",numb);
          azStart[0]=(int)((thataRight-thataStart-M_PI)/ratPoint[numb].divSt);
          azStart[1]=0;
          azEnd=ialloc(nAz,"azimuth end for map",numb);
          azEnd[0]=ratPoint[numb].NAscans;
          azEnd[1]=(int)((thataLeft-thataStart)/ratPoint[numb].divSt)+1;
          zStart=ialloc(nAz,"zenith start for map",numb);
          zStart[0]=0;
          zStart[1]=ratPoint[numb].NZscans/2;
          zEnd=ialloc(nAz,"zenith end for map",numb);
          zEnd[0]=ratPoint[numb].NZscans/2+1;
          zEnd[1]=ratPoint[numb].NZscans;
        }
      }
    }else if((ratPoint[numb].NAscans==1)&&(ratPoint[numb].NZscans==1)){
      nAz=1;
      azStart=ialloc(nAz,"azimuth start for map",numb);
      azEnd=ialloc(nAz,"azimuth end for map",numb);
      azStart[0]=0;
      azEnd[0]=ratPoint[numb].NAscans;
      zStart=ialloc(nAz,"zenith start for map",numb);
      zEnd=ialloc(nAz,"zenith end for map",numb);
      zStart[0]=0;
      zEnd[0]=ratPoint[numb].NZscans;
    }else{
      fprintf(stderr,"What are you trying to pull here? Not sure how to deal with files that start at %f\n",ratPoint[numb].theta[0]*180.0/M_PI);
      fprintf(stderr,"This error is part of the fancy voxel mapping\n");
      exit(1);
    }    



    for(n=0;n<nAz;n++){

      for(zen=zStart[n];zen<zEnd[n];zen++){
        if((zen<0)||(zen>=ratPoint[numb].NZscans)){
          fprintf(stderr,"The sky is aound your ankles, the trousers went long ago. For the zenith this time. %d with zen %d of %d bound %d %d\n",n,zen,ratPoint[numb].NZscans,azStart[n],azEnd[n]);
          exit(1);
        }
 
        for(az=azStart[n];az<azEnd[n];az++){
          fprintf(stdout,"Scan %d of %d azimuth %f\n",numb+1,dimage->scanN,ratPoint[numb].thata[az]*180.0/M_PI);
          fflush(stdout);
          if((az<0)||(az>=ratPoint[numb].NAscans)){
            fprintf(stderr,"The sky is arond your ankles, the trousers went long ago. %d with az %d of %d\n",n,az,ratPoint[numb].NAscans);
            exit(1);
          }
 

	  Lplace=zen*ratPoint[numb].NAscans+az;
          thata=ratPoint[numb].thata[Lplace];
          theta=ratPoint[numb].theta[Lplace];


          /*check that it's not pointing too high, eventually*/

          /*could cleverly decide when to start*/


          binStart=0;
          binEnd=ratPoint[numb].bins;
          for(bin=binStart;bin<binEnd;bin++){

            /*determine which voxel the bin is centred*/
            x=ratPoint[numb].from[0]+(ratPoint[numb].bin_L/2.0*((double)bin+0.5)+ratPoint[numb].min_R)*sin(theta)*cos(thata);
            y=ratPoint[numb].from[1]+(ratPoint[numb].bin_L/2.0*((double)bin+0.5)+ratPoint[numb].min_R)*sin(theta)*sin(thata);
            z=ratPoint[numb].from[2]+(ratPoint[numb].bin_L/2.0*((double)bin+0.5)+ratPoint[numb].min_R)*cos(theta);

//printf("%f %f %f %f %f\n",theta*180.0/M_PI,thata*180.0/M_PI,x,y,z);
            /*break if we've left the back*/
            /*see if we are within the scene*/
            if(((ratPoint[numb].from[0]>dimage->ends[0])&&(x<dimage->ends[0]))||\
               ((ratPoint[numb].from[0]<dimage->ends[1])&&(x>dimage->ends[1]))||\
               ((ratPoint[numb].from[1]>dimage->ends[2])&&(y<dimage->ends[2]))||\
               ((ratPoint[numb].from[1]<dimage->ends[3])&&(y>dimage->ends[3]))||\
               ((ratPoint[numb].from[2]>dimage->ends[4])&&(z<dimage->ends[4]))||\
               ((ratPoint[numb].from[2]<dimage->ends[5])&&(z>dimage->ends[5]))){
              ratPoint[numb].ratRes[Lplace].voxN[bin]=-1;
              break;
            }

            /*otherwise mark down this bin*/
            placeX=(int)((x-dimage->voxel[0].pos[0])/dimage->voxRes-0.5+0.5);
            placeY=(int)((y-dimage->voxel[0].pos[1])/dimage->voxRes);
            placeZ=(int)((z-dimage->voxel[0].pos[2])/dimage->voxRes);
            if((placeX>=0)&&(placeX<dimage->voxNumb[0])&&(placeY>=0)&&(placeY<dimage->voxNumb[1])&&(placeZ>=0)&&(placeZ<dimage->voxNumb[2])){
              place=placeX*dimage->voxNumb[1]*dimage->voxNumb[2]+placeY*dimage->voxNumb[2]+placeZ;

/*fprintf(stdout,"%f %f %f %f %f %f\n",x,y,z,dimage->voxel[place].pos[0],dimage->voxel[place].pos[1],dimage->voxel[place].pos[2]);
fflush(stdout);*/

              /* mark the relevant maps*/
              mapTransf=NULL;
              mapTransf=ialloc(ratPoint[numb].ratRes[Lplace].voxN[bin],"map transfer",1);
              for(m=0;m<ratPoint[numb].ratRes[Lplace].voxN[bin];m++){
                mapTransf[m]=ratPoint[numb].ratRes[Lplace].voxMap[bin][m];
              }
              TIDY(ratPoint[numb].ratRes[Lplace].voxMap[bin]);
              ratPoint[numb].ratRes[Lplace].voxN[bin]++;
              ratPoint[numb].ratRes[Lplace].voxMap[bin]=ialloc(ratPoint[numb].ratRes[Lplace].voxN[bin],"map transfer",bin);
              for(m=0;m<ratPoint[numb].ratRes[Lplace].voxN[bin]-1;m++){
                ratPoint[numb].ratRes[Lplace].voxMap[bin][m]=mapTransf[m];
              }
              ratPoint[numb].ratRes[Lplace].voxMap[bin][ratPoint[numb].ratRes[Lplace].voxN[bin]-1]=place;
              TIDY(mapTransf);
  
              if(dimage->voxel[place].contN){
                mapTransf=ialloc(3*dimage->voxel[place].contN,"map transfer",3);
                  for(m=0;m<3*dimage->voxel[place].contN;m++)mapTransf[m]=dimage->voxel[place].rayMap[m];
              }
              TIDY(dimage->voxel[place].rayMap);
                dimage->voxel[place].rayMap=ialloc(3*(dimage->voxel[place].contN+1),"voxel's map",place);
              if(dimage->voxel[place].contN>0){
                for(m=0;m<3*dimage->voxel[place].contN;m++)dimage->voxel[place].rayMap[m]=mapTransf[m];
              }
              dimage->voxel[place].rayMap[3*dimage->voxel[place].contN]=numb;
              dimage->voxel[place].rayMap[3*dimage->voxel[place].contN+1]=Lplace;
              dimage->voxel[place].rayMap[3*dimage->voxel[place].contN+2]=bin;
              dimage->voxel[place].contN++;
              TIDY(mapTransf);
            }/*make sure we're within the scene*/
	    

            /*loop around to adjacent bins to check for overlap*/
            for(i=placeX-binSep[numb][bin];i<placeX+binSep[numb][bin];i++){
              if((i<0)||(i==placeX))continue;  /*as we've already marked down placeX,placeY,placeZ*/
              else if(i>=dimage->voxNumb[0])break;
              for(j=placeY-binSep[numb][bin];j<placeY+binSep[numb][bin];j++){
                if((j<0)||(j==placeY))continue;
                else if(j>=dimage->voxNumb[1])break;
                for(k=placeZ-binSep[numb][bin];k<placeZ+binSep[numb][bin];k++){
                  if((k<0)||(k==placeZ))continue;
                  else if(k>=dimage->voxNumb[2])break;
                  intersect=-1;
                  place=i*dimage->voxNumb[1]*dimage->voxNumb[2]+j*dimage->voxNumb[2]+k;
                  /*test the centre*/
                  if((x<=dimage->voxel[place].pos[0]+a)&&(x>=dimage->voxel[place].pos[0]-a)&&\
                     (y<=dimage->voxel[place].pos[1]+a)&&(y>=dimage->voxel[place].pos[1]-a)&&\
                     (z<=dimage->voxel[place].pos[2]+a)&&(z>=dimage->voxel[place].pos[2]-a)){
                    intersect=1;
                  }else{
                    /*test for closeness of the two shapes*/
                    /*vector from bin centre to voxel centre*/
                    dx=dimage->voxel[place].pos[0]-x;
                    dy=dimage->voxel[place].pos[1]-y;
                    dz=dimage->voxel[place].pos[2]-z;
                    sepSq=dx*dx+dy*dy+dz*dz;
                    maxSep=sqrt(maxSepSq[numb*maxbins+bin]);
                    if(sepSq<=maxSepSq[numb*maxbins+bin]){  /*they may well be close enough*/
                      /*determine the four corners of the lidar bin*/
                      cornBin[0][0]=ratPoint[numb].from[0]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*sin(theta-ratPoint[numb].div/2.0)*cos(thata);
                      cornBin[0][1]=ratPoint[numb].from[1]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*sin(theta-ratPoint[numb].div/2.0)*sin(thata);
                      cornBin[0][2]=ratPoint[numb].from[2]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*cos(theta-ratPoint[numb].div/2.0);

                      cornBin[1][0]=ratPoint[numb].from[0]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*sin(theta)*cos(thata-ratPoint[numb].div/2.0);
                      cornBin[1][1]=ratPoint[numb].from[1]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*sin(theta)*sin(thata-ratPoint[numb].div/2.0);
                      cornBin[1][2]=ratPoint[numb].from[2]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*cos(theta);

                      cornBin[2][0]=ratPoint[numb].from[0]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*sin(theta+ratPoint[numb].div/2.0)*cos(thata);
                      cornBin[2][1]=ratPoint[numb].from[1]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*sin(theta+ratPoint[numb].div/2.0)*sin(thata);
                      cornBin[2][2]=ratPoint[numb].from[2]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*cos(theta+ratPoint[numb].div/2.0);

                      cornBin[3][0]=ratPoint[numb].from[0]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*sin(theta)*cos(thata+ratPoint[numb].div/2.0);
                      cornBin[3][1]=ratPoint[numb].from[1]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*sin(theta)*sin(thata+ratPoint[numb].div/2.0);
                      cornBin[3][2]=ratPoint[numb].from[2]+(ratPoint[numb].bin_L/2.0*((double)bin+1.0)+ratPoint[numb].min_R)*cos(theta);

                      cornBin[4][0]=ratPoint[numb].from[0]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*sin(theta-ratPoint[numb].div/2.0)*cos(thata);
                      cornBin[4][1]=ratPoint[numb].from[1]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*sin(theta-ratPoint[numb].div/2.0)*sin(thata);
                      cornBin[4][2]=ratPoint[numb].from[2]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*cos(theta-ratPoint[numb].div/2.0);

                      cornBin[5][0]=ratPoint[numb].from[0]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*sin(theta)*cos(thata-ratPoint[numb].div/2.0);
                      cornBin[5][1]=ratPoint[numb].from[1]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*sin(theta)*sin(thata-ratPoint[numb].div/2.0);
                      cornBin[5][2]=ratPoint[numb].from[2]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*cos(theta);

                      cornBin[6][0]=ratPoint[numb].from[0]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*sin(theta+ratPoint[numb].div/2.0)*cos(thata);
                      cornBin[6][1]=ratPoint[numb].from[1]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*sin(theta+ratPoint[numb].div/2.0)*sin(thata);
                      cornBin[6][2]=ratPoint[numb].from[2]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*cos(theta+ratPoint[numb].div/2.0);

                      cornBin[7][0]=ratPoint[numb].from[0]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*sin(theta)*cos(thata+ratPoint[numb].div/2.0);
                      cornBin[7][1]=ratPoint[numb].from[1]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*sin(theta)*sin(thata+ratPoint[numb].div/2.0);
                      cornBin[7][2]=ratPoint[numb].from[2]+(ratPoint[numb].bin_L/2.0*((double)bin)+ratPoint[numb].min_R)*cos(theta);


                      /*rotate to point the axes on the end towards the voxel centre*/
                      angle=acos((dx*cos(ratPoint[numb].thata[Lplace])+dy*sin(ratPoint[numb].thata[Lplace]))/sqrt(dx*dx+dy*dy));  /*temporary angle to get d' from d*/
                      ddashX=dx*cos(angle);
                      ddashY=dy*sin(angle);
                      ddashZ=dz;

                      angle=acos(ddashZ/sqrt(ddashX*ddashX+ddashY*ddashY+ddashZ*ddashZ));   /*the angle to rotate cornBin by*/
                      sinTheta=sin(ratPoint[numb].theta[Lplace]);
                      cosTheta=cos(ratPoint[numb].theta[Lplace]);
                      sinThata=sin(ratPoint[numb].thata[Lplace]);
                      cosThata=cos(ratPoint[numb].thata[Lplace]);
                      sinBeta=sin(angle);
                      cosBeta=cos(angle);

                      Rx=cosThata*(cosTheta*cosTheta+sinTheta*(sinBeta*cosThata-cosBeta*cosTheta))-sinThata*(cosBeta*sinThata+sinBeta*cosTheta);
                      Ry=sinThata*(cosTheta*cosTheta+sinTheta*(sinBeta*cosThata-cosBeta*cosTheta))+cosThata*(cosBeta*cosThata+sinBeta*cosTheta);
                      Rz=sinTheta*cosTheta+cosTheta*(cosBeta*cosTheta-sinBeta*cosThata);

//printf("%f %f %f\n",Rx,Ry,Rz);

                      for(m=0;m<8;m++){
                        corn[n][0]*=Rx;
                        corn[n][1]*=Ry;
                        corn[n][2]*=Rz;
                      }

                      /*voxel corner coordinates*/
                      cornVox[0][0]=dimage->voxel[place].pos[0]-a;
                      cornVox[0][1]=dimage->voxel[place].pos[1]-a;
                      cornVox[0][2]=dimage->voxel[place].pos[2]-a;

                      cornVox[1][0]=dimage->voxel[place].pos[0]-a;
                      cornVox[1][1]=dimage->voxel[place].pos[1]+a;
                      cornVox[1][2]=dimage->voxel[place].pos[2]-a;

                      cornVox[2][0]=dimage->voxel[place].pos[0]+a;
                      cornVox[2][1]=dimage->voxel[place].pos[1]+a;
                      cornVox[2][2]=dimage->voxel[place].pos[2]-a;

                      cornVox[3][0]=dimage->voxel[place].pos[0]+a;
                      cornVox[3][1]=dimage->voxel[place].pos[1]-a;
                      cornVox[3][2]=dimage->voxel[place].pos[2]-a;

                      cornVox[4][0]=dimage->voxel[place].pos[0]-a;
                      cornVox[4][1]=dimage->voxel[place].pos[1]-a;
                      cornVox[4][2]=dimage->voxel[place].pos[2]+a;

                      cornVox[5][0]=dimage->voxel[place].pos[0]-a;
                      cornVox[5][1]=dimage->voxel[place].pos[1]+a;
                      cornVox[5][2]=dimage->voxel[place].pos[2]+a;

                      cornVox[6][0]=dimage->voxel[place].pos[0]+a;
                      cornVox[6][1]=dimage->voxel[place].pos[1]+a;
                      cornVox[6][2]=dimage->voxel[place].pos[2]+a;

                      cornVox[7][0]=dimage->voxel[place].pos[0]+a;
                      cornVox[7][1]=dimage->voxel[place].pos[1]-a;
                      cornVox[7][2]=dimage->voxel[place].pos[2]+a;

                      intersect=0;
                      for(m=0;m<8;m++){  /*does the edge coordinate lie within the voxel*/
                        if((cornBin[n][0]<=cornVox[3][0])&&(cornBin[n][0]<=cornVox[0][0])&&\
                           (cornBin[n][1]<=cornVox[0][1])&&(cornBin[n][1]<=cornVox[1][1])&&\
                           (cornBin[n][2]<=cornVox[0][2])&&(cornBin[n][2]<=cornVox[4][4])){
                          intersect=1;
                          break;
                        }
                      }
                    }else{
                      intersect=0;
                      //printf("not close enough to bother testing\n");
                    }
                  }


                  if(intersect==1){ /*mark on the relevant maps*/
                    mapTransf=NULL;
                    mapTransf=ialloc(ratPoint[numb].ratRes[Lplace].voxN[bin],"map transfer",1);
                    for(m=0;m<ratPoint[numb].ratRes[Lplace].voxN[bin];m++){
                      mapTransf[m]=ratPoint[numb].ratRes[Lplace].voxMap[bin][m];
                    }
                    TIDY(ratPoint[numb].ratRes[Lplace].voxMap[bin]);
                    ratPoint[numb].ratRes[Lplace].voxN[bin]++;
                    ratPoint[numb].ratRes[Lplace].voxMap[bin]=ialloc(ratPoint[numb].ratRes[Lplace].voxN[bin],"map transfer",bin);
                    for(m=0;m<ratPoint[numb].ratRes[Lplace].voxN[bin]-1;m++){
                      ratPoint[numb].ratRes[Lplace].voxMap[bin][m]=mapTransf[m];
                    }
                    ratPoint[numb].ratRes[Lplace].voxMap[bin][ratPoint[numb].ratRes[Lplace].voxN[bin]-1]=place;
                    TIDY(mapTransf);

                    if(dimage->voxel[place].contN){
                      mapTransf=ialloc(3*dimage->voxel[place].contN,"map transfer",3);
                      for(m=0;m<3*dimage->voxel[place].contN;m++)mapTransf[m]=dimage->voxel[place].rayMap[m];
                    }
                    TIDY(dimage->voxel[place].rayMap);
                    dimage->voxel[place].rayMap=ialloc(3*(dimage->voxel[place].contN+1),"voxel's map",place);
                    if(dimage->voxel[place].contN>0){
                      for(m=0;m<3*dimage->voxel[place].contN;m++)dimage->voxel[place].rayMap[m]=mapTransf[m];
                    }
                    dimage->voxel[place].rayMap[3*dimage->voxel[place].contN]=numb;
                    dimage->voxel[place].rayMap[3*dimage->voxel[place].contN+1]=Lplace;
                    dimage->voxel[place].rayMap[3*dimage->voxel[place].contN+2]=bin;
                    dimage->voxel[place].contN++;
                    TIDY(mapTransf);
                  } /*intersection marking*/                
                }/*coxel z loop*/
              }  /*voxel y loop*/
            }    /* voxel x loop*/
          } /*bin loop*/
        } /* zenith loop*/
      }   /*azimuth loop*/
    }    /*azimuth section loop*/
  }     /*scan loop*/


  fprintf(stdout,"\nThe map has been created.\n");

  /*calculate the proportion of overlap for energy conservation*/
  proportionOverlap(ratPoint,dimage);

  /*and print it out to a binary file that could be used later*/
  if(dimage->createMap) dimage=writeBinaryMaps(dimage,ratPoint);

  if(dimage->checkMap)asciiMaps(dimage,ratPoint);  /*check voxMap in an ascii file*/

  if(dimage->ends){free(dimage->ends);dimage->ends=NULL;}
  if(mapTransf){free(mapTransf);mapTransf=NULL;}
  if(maxSepSq){free(maxSepSq);maxSepSq=NULL;}
  TIDY(mapTransf);
  if(binSep){
    for(numb=0;numb<dimage->scanN;numb++)TIDY(binSep[numb]);
    free(binSep);
    binSep=NULL;
  }
  TIDY(azStart);
  TIDY(azEnd);
  TIDY(zStart);
  TIDY(zEnd);
 
  return(dimage);
}  /*prepareVoxels ##voxel mapping##*/


/*#####################################################################################################*/
/*mark up te maps once an intersection has been found in prepareVoxels() */

/*void markMaps()
{
  mapTransf=NULL;
  mapTransf=ialloc(ratPoint[numb].ratRes[Lplace].voxN[bin],"map transfer",1);
  for(m=0;m<ratPoint[numb].ratRes[Lplace].voxN[bin];m++){
    mapTransf[m]=ratPoint[numb].ratRes[Lplace].voxMap[bin][m];
  }
  TIDY(ratPoint[numb].ratRes[Lplace].voxMap[bin]);
  ratPoint[numb].ratRes[Lplace].voxN[bin]++;
  ratPoint[numb].ratRes[Lplace].voxMap[bin]=ialloc(ratPoint[numb].ratRes[Lplace].voxN[bin],"map transfer",bin);
  for(m=0;m<ratPoint[numb].ratRes[Lplace].voxN[bin]-1;m++){
    ratPoint[numb].ratRes[Lplace].voxMap[bin][m]=mapTransf[m];
  }
  ratPoint[numb].ratRes[Lplace].voxMap[bin][ratPoint[numb].ratRes[Lplace].voxN[bin]-1]=place;
  TIDY(mapTransf);

  if(dimage->voxel[place].contN){
    mapTransf=ialloc(3*dimage->voxel[place].contN,"map transfer",3);
    for(m=0;m<3*dimage->voxel[place].contN;m++)mapTransf[m]=dimage->voxel[place].rayMap[m];
  }
                        TIDY(dimage->voxel[place].rayMap);
  dimage->voxel[place].rayMap=ialloc(3*(dimage->voxel[place].contN+1),"voxel's map",place);
  if(dimage->voxel[place].contN>0){
    for(m=0;m<3*dimage->voxel[place].contN;m++)dimage->voxel[place].rayMap[m]=mapTransf[m];
  }
  dimage->voxel[place].rayMap[3*dimage->voxel[place].contN]=numb;
  dimage->voxel[place].rayMap[3*dimage->voxel[place].contN+1]=Lplace;
  dimage->voxel[place].rayMap[3*dimage->voxel[place].contN+2]=bin;
  dimage->voxel[place].contN++;
  TIDY(mapTransf);

}*//*markMaps*/


/*#####################################################################################################*/
/*calculate the overlap between range bins nd voxels*/

void proportionOverlap(RatControl *ratPoint,RatImage *dimage)
{
  int place=0,i=0;
  int numb=0,Lplace=0,bin=0;

  /*fractions in the voxel array*/
  for(place=0;place<dimage->totalVoxels;place++){
    dimage->voxel[place].fraction=falloc(dimage->voxel[place].contN,"overlap fraction",place);
    for(i=0;i<dimage->voxel[place].contN;i++){
      numb=dimage->voxel[place].rayMap[3*i];
      Lplace=dimage->voxel[place].rayMap[3*i+1];
      bin=dimage->voxel[place].rayMap[3*i+2];
      dimage->voxel[place].fraction[i]=1.0/(float)ratPoint[numb].ratRes[Lplace].voxN[bin];
    }
  }

  /*Storing fractions in the results array*/
  for(numb=0;numb<dimage->scanN;numb++){
    for(Lplace=0;Lplace<ratPoint[numb].NZscans*ratPoint[numb].NAscans;Lplace++){
      if((ratPoint[numb].ratRes[Lplace].fraction=(float **)calloc(ratPoint[numb].bins,sizeof(float *)))==NULL){
        fprintf(stderr,"Error in overlap fraction allocation %d %d\n",numb,Lplace);
        exit(1);
      }
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        if(ratPoint[numb].ratRes[Lplace].voxN[bin]>0){
          ratPoint[numb].ratRes[Lplace].fraction[bin]=falloc(ratPoint[numb].ratRes[Lplace].voxN[bin],"fractional overlap",bin);
          for(i=0;i<ratPoint[numb].ratRes[Lplace].voxN[bin];i++)\
             ratPoint[numb].ratRes[Lplace].fraction[bin][i]=1.0/(float)ratPoint[numb].ratRes[Lplace].voxN[bin];
        }
      }
    }
  }

  return;
}/*proportionOverlap*/


/*#####################################################################################################*/
/*analyse existing voxel representations called from "main" in echiMake.c*/

RatImage *analyseVoxels(RatControl *ratPoint,RatImage *dimage)
{
  RatImage *paintVoxels(RatImage *,RatControl *);
  RatImage *compareMaterials(RatImage *,RatControl *);
  RatImage *vrmlUp(RatImage *,RatControl *);
  RatImage *writeVoxels(RatImage *,RatControl *,int);
  RatControl *readVoxels(RatImage *,RatControl *);
  RatImage *asciiVoxels(RatImage *,RatControl *,char[100]);
  RatImage *clearVoxels(RatImage *);
  void flattenVoxels(RatImage *,RatControl *);
  void printHeader(RatImage *,RatControl *);
  void printFullCoords(RatImage *dimage);
  char namen[100];

  ratPoint=readVoxels(dimage,ratPoint);

  printHeader(dimage,ratPoint);

  sprintf(namen,"%s.txt",dimage->output);

  if(dimage->Vascii)dimage=asciiVoxels(dimage,ratPoint,namen);

  if(dimage->vzoom){
    if(!dimage->Vflat)dimage=paintVoxels(dimage,ratPoint);
    else                     flattenVoxels(dimage,ratPoint);
  }

  if(dimage->gnucoords)printFullCoords(dimage);

  dimage=clearVoxels(dimage);

  return(dimage);
}/*analyseVoxels*/

/*#####################################################################################################*/
/*solve for the LAD within the iterations*/

RatImage *solveLAD(RatImage *dimage,RatControl *ratPoint,int Vplace)
{
  int i=0,j=0,k=0,n=0,iter=0;
  int numb=0,Lplace=0,bin=0,band=0,mat=0;
  float highCont=0,lowCont=0;
  float *LAD=NULL,*thetaHat=NULL,psi=0;    /*use an array of gaps to save time*/
  float *gap=NULL,Tgap=0;                  /*gap fractions for weighting*/
  float AeMean[MATN],meanZen=0;              /*variables for  */
  float AeVar[MATN],oldVar[MATN],varThresh=0;/*deciding which */
  float LADstep=0;                           /*way to change  */
  char abovebelow=0;                         /*the LAD        */
  float *AePredicted=NULL,lowAe[MATN],highAe[MATN]; /*we save all Aepredicted for variance*/
  float guessGap(RatControl *,RatImage *,float,int,int,int);
  char brEak=0,blocked=0,anyBlocked=0,light=0;

  varThresh=0.005;
  LAD=falloc(2,"LAD whilst solving",Vplace);
  thetaHat=falloc(2,"theta hat whilst solving LAD",Vplace);

  if(dimage->voxel[Vplace].adjust){         /*then we can have a go at solving for LAD*/
    gap=falloc(dimage->voxel[Vplace].contN,"gap contribution",Vplace);
    TIDY(AePredicted);
    AePredicted=falloc(dimage->matN*dimage->voxel[Vplace].contN,"predicted area",0);
    /*read all relevant beams. TAKE CARE NOT TO RUN OUT OF RAM*/
    /*find average zenith angle for deciding which way to shift Omega*/
    /*this is weighted by the gap, since a very attenuated beam is not representative*/
    meanZen=0;
    Tgap=0.0;
    highCont=0.0; /*used here as contN rather thananything special*/
    for(i=0;i<dimage->voxel[Vplace].contN;i++){
      numb=dimage->voxel[Vplace].rayMap[3*i];
      Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
      bin=dimage->voxel[Vplace].rayMap[3*i+2];
      if(ratPoint[numb].ratRes[Lplace].refl==NULL){     /*this should already be here from psi*/
        ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
      }
      light=0;
      for(n=0;n<dimage->nBands;n++){
        band=dimage->band[numb][n];
        if(ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>0){
          light=1;
          break;
        }
      }
      if(light){
        gap[i]=1;
        for(j=0;j<bin;j++){
          gap[i]=guessGap(ratPoint,dimage,gap[i],numb,Lplace,j);
        }
        meanZen+=gap[i]*dimage->voxel[Vplace].fraction[i]*ratPoint[numb].Etheta[(int)(Lplace/ratPoint[numb].NAscans)];
        Tgap+=gap[i];
        highCont+=dimage->voxel[Vplace].fraction[i];
      }
    }
    if(highCont>0.0&&Tgap>0.0)meanZen/=highCont*Tgap;

    iter=0;
    brEak=0;
    LADstep=M_PI/8;  /*reset interval*/
    do{   /*##iterate to get LAD##*/
      highCont=0.0;   /*zero all running counters*/
      lowCont=0.0;
      /*Langle=0;*/
      for(i=0;i<dimage->matN;i++){
        AeMean[i]=0.0;
        lowAe[i]=0.0;
        highAe[i]=0.0;
        AeVar[i]=0.0;
      }
      for(i=0;i<dimage->voxel[Vplace].contN;i++){
        numb=dimage->voxel[Vplace].rayMap[3*i];
        Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
        bin=dimage->voxel[Vplace].rayMap[3*i+2];
        if(ratPoint[numb].ratRes[Lplace].refl==NULL){
          ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
          /*if(ratPoint[numb].theta[Langle]>M_PI/2){
          //  printf("oh NO really really heavy! Corrupt angles %d %f\n",Lplace,ratPoint[numb].theta[Langle]);
          //  if(Langle){
          //    ratPoint[numb].theta[Langle]=ratPoint[numb].theta[Langle-1];
          //  }else ratPoint[numb].theta[Langle]=M_PI/4;
          //}
          //Langle++;*/
        }
        light=0;
        for(n=0;n<dimage->nBands;n++){
          band=dimage->band[numb][n];
          if(ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>0){
            light=1;
            break;
          }
        }
        if(light){
          /*calculate the variance of Ae and minimise this with thetaHat*/
          psi=meanPsi(ratPoint,dimage,numb,Lplace,bin);
          LAD=meanLAD(ratPoint,dimage,numb,Lplace,bin,LAD);
          thetaHat=meanThetaHat(ratPoint,dimage,LAD,thetaHat,numb,Lplace);
          for(n=0;n<dimage->nBands;n++){
            band=dimage->band[numb][n];
            if(psi>THRESHOLD&&psi<1-THRESHOLD){
              AePredicted[2*i]=gap[i]*ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]/\
                               (cos(thetaHat[0])*cos(thetaHat[0])*cos(thetaHat[0])*(ratPoint[numb].albedo[2*band]+\
                                ratPoint[numb].albedo[2*band+1]*(1-psi)/psi))*dimage->voxel[Vplace].fraction[i];
              AePredicted[2*i+1]=gap[i]*ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]/\
                               (cos(thetaHat[1])*cos(thetaHat[1])*cos(thetaHat[1])*(ratPoint[numb].albedo[2*band+1]+\
                               ratPoint[numb].albedo[2*band]*psi/(1-psi)))*dimage->voxel[Vplace].fraction[i];
            }else if(psi<=THRESHOLD){  /*there is only wood*/
              AePredicted[2*i]=0;
              AePredicted[2*i+1]=gap[i]*ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]/\
                              (ratPoint[numb].albedo[2*band+1]*cos(thetaHat[1])*cos(thetaHat[1])*cos(thetaHat[1]))*dimage->voxel[Vplace].fraction[i];
            }else if(psi>=1-THRESHOLD){ /*there is only leaf*/
              AePredicted[2*i]=gap[i]*ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]/\
                              (ratPoint[numb].albedo[2*band]*cos(thetaHat[0])*cos(thetaHat[0])*cos(thetaHat[0]))*dimage->voxel[Vplace].fraction[i];
              AePredicted[2*i+1]=0;
            }
            AeMean[0]+=AePredicted[2*i];
            AeMean[1]+=AePredicted[2*i+1];
            if(ratPoint[numb].Etheta[(int)(Lplace/ratPoint[numb].NAscans)]>=meanZen){
              highAe[0]+=AePredicted[2*i];
              highAe[1]+=AePredicted[2*i+1];
              highCont+=dimage->voxel[Vplace].fraction[i];
            }else{
              lowAe[0]+=AePredicted[2*i];
              lowAe[1]+=AePredicted[2*i+1];
              lowCont+=dimage->voxel[Vplace].fraction[i];
            }
          }  /*end of light test*/
        }

      }  /*end of beam reading and sorting loop*/
      for(i=0;i<dimage->matN;i++){               /*normalisation of mean*/
        AeMean[i]/=(float)dimage->nBands*(highCont+lowCont)*Tgap;
        highAe[i]/=(float)dimage->nBands*highCont*Tgap;
        lowAe[i]/=(float)dimage->nBands*lowCont*Tgap;
      }
      for(i=0;i<dimage->voxel[Vplace].contN;i++){ /*variance calculation*/
        AeVar[0]+=(AePredicted[2*i]-AeMean[0])*(AePredicted[2*i]-AeMean[0]);
        AeVar[1]+=(AePredicted[2*i+1]-AeMean[1])*(AePredicted[2*i+1]-AeMean[1]);
      }
      if((highCont+lowCont)>0.0){
        AeVar[0]/=(highCont+lowCont);
        AeVar[1]/=(highCont+lowCont);
      }

      for(i=0;i<dimage->matN;i++){
        if(lowAe[i]>highAe[i]){        /*we need to increase omega*/
          if(iter&&!abovebelow){  /*then we have stepped over the line*/
            LADstep/=2;
          }else if(iter){   /*test to see if we are getting closer to the truth*/
            for(mat=0;mat<dimage->matN;mat++){
              if(AeVar[i]>oldVar[i]){   /*we are not going to get any better*/
                brEak=1;
                break;
              }
            }
          }
          abovebelow=1;
          dimage->voxel[Vplace].LAD[i]+=LADstep;
          if(dimage->voxel[Vplace].LAD[i]<0)dimage->voxel[Vplace].LAD[i]=0.0001;
          else if(dimage->voxel[Vplace].LAD[i]>M_PI/2)dimage->voxel[Vplace].LAD[i]=M_PI/2-0.0001;
        }else if(lowAe[i]<highAe[i]){  /*we need to decrease omega*/
          if(iter&&abovebelow){   /*we have stepped over the line*/
            LADstep/=2.0;
          }else if(iter){ /*test to see if we are converging*/
            for(mat=0;mat<dimage->matN;mat++){
              if(AeVar[i]>oldVar[i]){   /*we are not going to get any better*/
                brEak=1;
                break;
              }
            }
          }
          abovebelow=0;
          dimage->voxel[Vplace].LAD[i]-=LADstep;
          if(dimage->voxel[Vplace].LAD[i]<0)dimage->voxel[Vplace].LAD[i]=0;
          else if(dimage->voxel[Vplace].LAD[i]>M_PI/2)dimage->voxel[Vplace].LAD[i]=M_PI/2;
        }else if(lowAe[i]==highAe[i]){ /*omega is bang on for this material*/
         /* brEak=1; */
         /* break;   */
        }
      }
      if(brEak)break;
 
      if(AeVar[0]<=varThresh&&AeVar[1]<=varThresh){
/*        printf("%d tis finished early %f < %f\n",Vplace,AeVar[0],varThresh); */
        break;
      }
      for(mat=0;mat<dimage->matN;mat++)oldVar[i]=AeVar[i];
      iter++;
    }while(AeVar[0]>varThresh&&AeVar[1]>varThresh&&iter<1000);  /*#########end of LAD iteration####################*/

  }else{             /*we can't really tell much about LAD, have a go anyway*/

    /*########This is all done in one big gap checking step after the voxel steps###*/
    TIDY(LAD);
    TIDY(thetaHat);

    return(dimage);  /*this is hard, skip for now*/

    /*check for premature blocking, if none then there's nothing we can do*/
    gap=falloc(1,"trouble down't mill\n",0);
    do{
      anyBlocked=0;
      for(i=0;i<dimage->voxel[Vplace].contN;i++){
        numb=dimage->voxel[Vplace].rayMap[3*i];
        Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
        bin=dimage->voxel[Vplace].rayMap[3*i+2];
        if(ratPoint[numb].ratRes[Lplace].refl==NULL){
          ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
        }
        light=0;
        for(n=0;n<dimage->nBands;n++){
          band=dimage->band[numb][n];
          if(ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>0){
            light=1;
            break;
          }
        }
        if(light){
          gap[0]=1;
          blocked=0;
          for(j=0;j<bin;j++){
            gap[0]=guessGap(ratPoint,dimage,gap[0],numb,Lplace,j);
            if(gap[0]<=0){    /*check for premature blocking*/
              for(k=j;k<ratPoint[numb].bins;k++){
                for(n=0;n<dimage->nBands;n++){
                  band=dimage->band[numb][n];
                  if(ratPoint[numb].ratRes[Lplace].refl[(k*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>0){
                    blocked=1;
                    brEak=1;
                    break;
                  }
                }
                if(brEak)break;
              }
              break;
            }
          }
          if(blocked){  /*then we need to adjust the LAD of this bin*/
            anyBlocked=1;
          }
        }
      }
      iter++;
    }while(anyBlocked&&iter<100);

    /*the arrays are cleared afterwards BEWARE OF RAM*/
  /*  printf("Only one zenith in this bin. Not a lot we can do for you\n");  */
  }
  TIDY(LAD);
  TIDY(thetaHat);
  TIDY(gap);
  TIDY(AePredicted);
  return(dimage);
}/*solveLAD*/

/*#####################################################################################################*/
/*determine LAD and surface area using a really simple linear LAD model*/

/*the following variables are gloabl for Powell's method*/
float *xPowell=NULL,*yPowell=NULL,rhoPowell=0;
int nPowell=0;
float powellLAD(float *);

RatImage *linearVoxel(RatImage *dimage,RatControl *ratPoint,int Vplace)
{
  int i=0,nMeas=0,n=0,nZen=0;                  /*number of measurements*/
  int place=0,lastnumb=0;
  int numb=0,Lplace=0,bin=0,band=0;
  float *x=NULL,*y=NULL,*sig=NULL;  /*three arrays to regress against*/
  float minError=0,scale=0,gap=0;
  float linearGap(RatControl *,RatImage *,int,int,int);
  float m=0,c=0;
  float zen=0;
  float totArea=0,effectiveRho=0;
  float Gamma=0;
  float ytemp=0,yaddition=0;
  char empty=0;

  /*float q=0,chi=0,error[2];*/
  void fit(float *,float *,int,float *,int,float *,float *,float *,float *,float *,float *);

  /*Powell's method*/
  void powell(float *,float **,int,float,int *,float *,float (*)(float *));
  float *p=NULL,**xi=NULL;
  float ftol=0,fret=0;
  int iter=0;


  Gamma=0.85;

  band=1;  /*just use 1064nm for now*/
  scale=100.0;
  minError=0.001;


  effectiveRho=dimage->voxel[Vplace].psi*ratPoint[numb].albedo[2*band]+(1.0-dimage->voxel[Vplace].psi)*ratPoint[numb].albedo[2*band+1];

  x=falloc(dimage->voxel[Vplace].contN+1,"x linear regression",Vplace);
  y=falloc(dimage->voxel[Vplace].contN+1,"y linear regression",Vplace);
  sig=falloc(dimage->voxel[Vplace].contN+1,"linear regression error",Vplace);
  nMeas=0;
  ytemp=0.0;
  empty=1;
  n=nZen=1;
  for(i=0;i<dimage->voxel[Vplace].contN;i++){
    numb=dimage->voxel[Vplace].rayMap[3*i];
    Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
    bin=dimage->voxel[Vplace].rayMap[3*i+2];
    if(ratPoint[numb].ratRes[Lplace].refl==NULL)ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);

    if((i==0)||(gap<=THRESHOLD))gap=linearGap(ratPoint,dimage,numb,Lplace,bin);
    if(gap>THRESHOLD){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      if(i>0){
        lastnumb=dimage->voxel[Vplace].rayMap[3*(i-1)];
        if(numb==lastnumb){  /*for the current scan*/
          yaddition=ratPoint[numb].ratRes[Lplace].refl[place]/gap;
          if(yaddition>effectiveRho)yaddition=effectiveRho;
          else if(yaddition<0.0){fprintf(stderr,"Oh no, it's happening\n");exit(1);}
          ytemp+=yaddition;
          zen+=ratPoint[numb].theta[Lplace];
          if(Lplace!=dimage->voxel[Vplace].rayMap[3*(i-1)+1])n++;
          nZen++;
        }else{         /*a new set*/
          y[nMeas+1]=ytemp/(float)n;
          sig[nMeas+1]=(1.0-gap)*scale+minError;
          if(nZen>0)x[nMeas+1]=cos(2.0*zen/(float)nZen);
          else{
            fprintf(stderr,"I want to drink, to wash all of this filth from my veins\n");
            exit(1);
          }
          nMeas++;
          n=nZen=1;
          gap=linearGap(ratPoint,dimage,numb,Lplace,bin);
          if(gap>THRESHOLD){
            ytemp=ratPoint[numb].ratRes[Lplace].refl[place]/gap;
            if(ytemp>effectiveRho)ytemp=effectiveRho;
            zen=ratPoint[numb].theta[Lplace];
          }else ytemp=0.0;
        }
      }else{  /*it's the first bin*/
        ytemp=ratPoint[numb].ratRes[Lplace].refl[place]/gap;
        if(ytemp>effectiveRho)ytemp=effectiveRho;
        zen=ratPoint[numb].theta[Lplace];
        n=nZen=1;
      }
    }/*uter gap test*/
    ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
  }/*arrange all range bins into the righ tvoxels*/


  if(ytemp>0.0){
    y[nMeas+1]=ytemp/(float)n;
    sig[nMeas+1]=(1.0-gap)*scale+minError;
    if(nZen>0) x[nMeas+1]=cos(2.0*zen/(float)nZen);
    else{
      fprintf(stderr,"Oh god!\n");
      exit(1);
    }
    nMeas++;
  }

  empty=1;
  for(i=1;i<=nMeas;i++){
    if(y[i]>0.0){
      empty=0;
      break;
    }
  }


  if(!empty){
    /*fit using Powell's method with a penalty function*/
    p=falloc(3,"powell parameters",Vplace);
    p[1]=0.0; // (dimage->voxel[Vplace].Ae[0]+dimage->voxel[Vplace].Ae[1])*effectiveRho*dimage->voxel[Vplace].LAD[0];
    p[2]=effectiveRho/3.0;//   (dimage->voxel[Vplace].Ae[0]+dimage->voxel[Vplace].Ae[1])*effectiveRho*(1.0/3.0-dimage->voxel[Vplace].LAD[0]*dimage->cos2mu);
    if(!(xi=(float **)calloc(3,sizeof(float *)))){
      fprintf(stderr,"Out of memory here\n");
      exit(1);
    } 
    for(i=0;i<=2;i++)xi[i]=falloc(3,"powell chi",Vplace);
    xi[1][1]=1.0;
    xi[1][2]=0.0;
    xi[2][1]=0.0;
    xi[2][2]=1.0;
    xPowell=x;
    yPowell=y;
    nPowell=nMeas;
    rhoPowell=effectiveRho;
    ftol=0.00001;
    powell(p,xi,2,ftol,&iter,&fret,powellLAD);

    m=p[1];
    c=p[2];
    //for(i=1;i<=nMeas;i++)fprintf(stdout,"%g %g %d of %d\n",x[i],y[i],i,nMeas);
  }else{    /* the voxel's empty*/
    m=c=0.0;
  }

//exit(0);

  /*below is using the simple lonear fit, can't include a penatly function. Use powell above*/
  //fit(x,y,nMeas,sig,1,&(c),&(m),&(error[0]),&(error[1]),&(chi),&(q));


fprintf(stdout,"rho %g Gamma G %g\n",effectiveRho,c+m*dimage->cos2mu);
fflush(stdout);
  if(dimage->species==1)totArea=3.0*(c+m*dimage->cos2mu)/effectiveRho;
  else{
    fprintf(stderr,"Not set up for conifers yet\n");
    exit(1);
  }
  dimage->voxel[Vplace].Ae[0]=dimage->voxel[Vplace].psi*totArea;
  dimage->voxel[Vplace].Ae[1]=(1.0-dimage->voxel[Vplace].psi)*totArea;
  if(dimage->voxel[Vplace].Ae[0]<0.0)dimage->voxel[Vplace].Ae[0]=0.0;
  if(dimage->voxel[Vplace].Ae[1]<0.0)dimage->voxel[Vplace].Ae[1]=0.0;
  if(totArea>0.0){
  if(!dimage->species){ /*coniferous*/
      dimage->voxel[Vplace].LAD[0]=m/(totArea*Gamma*effectiveRho);
      dimage->voxel[Vplace].LAD[1]=m/(totArea*Gamma*effectiveRho);
    }else{                /*boradleaf*/
      dimage->voxel[Vplace].LAD[0]=m/(totArea*effectiveRho);
      dimage->voxel[Vplace].LAD[1]=m/(totArea*effectiveRho);
    }
  }else dimage->voxel[Vplace].LAD[0]=dimage->voxel[Vplace].LAD[1]=0.0;


//fprintf(stdout,"Fitted parameters %g %g to give an area %g\n",m,c,totArea);
  
  TIDY(x);
  TIDY(y);
  TIDY(sig);


  return(dimage);
}/*linearVoxel*/


/*#####################################################################################################*/
/* Error function for Powell's method*/

float powellLAD(float *paras)
{
  int i=0;
  float error=0.0,err=0,y=0;
  float penalty=0,steepness=0;

  steepness=100.0;

  for(i=1;i<=nPowell;i++){
    y=paras[1]*xPowell[i]+paras[2];
    err=y-yPowell[i];
    if(err<0.0)err*=-1.0;
    error+=err;
    if(y<0.0)error+=y*y*steepness;
    else if(y>rhoPowell){
      penalty=y-rhoPowell;
      error+=penalty*penalty*steepness;
    }
  }
  error/=(float)nPowell;
//fprintf(stderr,"Error %g paras %g %g\n",error,paras[1],paras[2]);
  return(error);
}/*voxelError*/


/*#####################################################################################################*/
/*Powell's method from the numerical recipes*/

#define ITMAX 200


void powell(float *p,float **xi,int n,float ftol,int *iter,float *fret,float (*func)(float *))
//float (*func)(),**xi,*fret,ftol,*p;
//int *iter,n;
{
  void linmin();
  int i,ibig,j;
  float del,fp,fptt,t,*pt,*ptt,*xit;

  pt=vector(1,n);
  ptt=vector(1,n);
  xit=vector(1,n);
  *fret=powellLAD(p);
  for (j=1;j<=n;j++) pt[j]=p[j];
  for (*iter=1;;++(*iter)) {
    fp=(*fret);
    ibig=0;
    del=0.0;
    for (i=1;i<=n;i++) {
      for (j=1;j<=n;j++) xit[j]=xi[j][i];
      fptt=(*fret);
      linmin(p,xit,n,fret,func);
      if (fabs(fptt-(*fret)) > del) {
        del=fabs(fptt-(*fret));
        ibig=i;
      }
    }
    if (2.0*fabs(fp-(*fret)) <= ftol*(fabs(fp)+fabs(*fret))) {
      free_vector(xit,1,n);
      free_vector(ptt,1,n);
      free_vector(pt,1,n);
      return;
    }
    if (*iter == ITMAX){
      fprintf(stderr,"GOt as far as %g %g\nFor data\n",p[1],p[2]);
      for(j=1;j<=nPowell;j++)fprintf(stderr,"%g %g\n",xPowell[j],yPowell[j]);
      nrerror("powell exceeding maximum iterations.");
    }
    for (j=1;j<=n;j++) {
      ptt[j]=2.0*p[j]-pt[j];
      xit[j]=p[j]-pt[j];
      pt[j]=p[j];
    }
    fptt=powellLAD(ptt);
    if (fptt < fp) {
      t=2.0*(fp-2.0*(*fret)+fptt)*SQR(fp-(*fret)-del)-del*SQR(fp-fptt);
      if (t < 0.0) {
        linmin(p,xit,n,fret,func);
        for (j=1;j<=n;j++) {
          xi[j][ibig]=xi[j][n];
          xi[j][n]=xit[j];
        }
      }
    }
  }
}
#undef ITMAX

/*#####################################################################################################*/
/*stuff for Powell's method*/

#define TOL 2.0e-4

int ncom;
float *pcom,*xicom,(*nrfunc)();

void linmin(p,xi,n,fret,func)
float (*func)(),*fret,p[],xi[];
int n;
{
        float brent(),f1dim();
        void mnbrak();
        int j;
        float xx,xmin,fx,fb,fa,bx,ax;

        ncom=n;
        pcom=vector(1,n);
        xicom=vector(1,n);
        nrfunc=func;
        for (j=1;j<=n;j++) {
                pcom[j]=p[j];
                xicom[j]=xi[j];
        }
        ax=0.0;
        xx=1.0;
        mnbrak(&ax,&xx,&bx,&fa,&fx,&fb,f1dim);
        *fret=brent(ax,xx,bx,f1dim,TOL,&xmin);
        for (j=1;j<=n;j++) {
                xi[j] *= xmin;
                p[j] += xi[j];
        }
        free_vector(xicom,1,n);
        free_vector(pcom,1,n);
}
#undef TOL


/*#####################################################################################################*/


#define ITMAX 100
#define CGOLD 0.3819660
#define ZEPS 1.0e-10
#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

float brent(ax,bx,cx,f,tol,xmin)
float (*f)(),*xmin,ax,bx,cx,tol;
{
        int iter;
        float a,b,d,etemp,fu,fv,fw,fx,p,q,r,tol1,tol2,u,v,w,x,xm;
        float e=0.0;

        a=(ax < cx ? ax : cx);
        b=(ax > cx ? ax : cx);
        x=w=v=bx;
        fw=fv=fx=(*f)(x);
        for (iter=1;iter<=ITMAX;iter++) {
                xm=0.5*(a+b);
                tol2=2.0*(tol1=tol*fabs(x)+ZEPS);
                if (fabs(x-xm) <= (tol2-0.5*(b-a))) {
                        *xmin=x;
                        return fx;
                }
                if (fabs(e) > tol1) {
                        r=(x-w)*(fx-fv);
                        q=(x-v)*(fx-fw);
                        p=(x-v)*q-(x-w)*r;
                        q=2.0*(q-r);
                        if (q > 0.0) p = -p;
                        q=fabs(q);
                        etemp=e;
                        e=d;
                        if (fabs(p) >= fabs(0.5*q*etemp) || p <= q*(a-x) || p >= q*(b-x))
                                d=CGOLD*(e=(x >= xm ? a-x : b-x));
                        else {
                                d=p/q;
                                u=x+d;
                                if (u-a < tol2 || b-u < tol2)
                                        d=SIGN(tol1,xm-x);
                        }
                } else {
                        d=CGOLD*(e=(x >= xm ? a-x : b-x));
                }
                u=(fabs(d) >= tol1 ? x+d : x+SIGN(tol1,d));
                fu=(*f)(u);
                if (fu <= fx) {
                        if (u >= x) a=x; else b=x;
                        SHFT(v,w,x,u)
                        SHFT(fv,fw,fx,fu)
                } else {
                        if (u < x) a=u; else b=u;
                        if (fu <= fw || w == x) {
                                v=w;
                                w=u;
                                fv=fw;
                                fw=fu;
                        } else if (fu <= fv || v == x || v == w) {
                                v=u;
                                fv=fu;
                        }
                }
        }
        nrerror("Too many iterations in brent");
}/*brent*/
#undef ITMAX
#undef CGOLD
#undef ZEPS
#undef SHFT


/*#####################################################################################################*/

extern int ncom;
extern float *pcom,*xicom,(*nrfunc)();

float f1dim(x)
float x;
{
        int j;
        float f,*xt;

        xt=vector(1,ncom);
        for (j=1;j<=ncom;j++) xt[j]=pcom[j]+x*xicom[j];
        f=(*nrfunc)(xt);
        free_vector(xt,1,ncom);
        return f;
}/*f1dim*/


/*#####################################################################################################*/


#define GOLD 1.618034
#define GLIMIT 100.0
#define TINY 1.0e-20
#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

void mnbrak(ax,bx,cx,fa,fb,fc,func)
float (*func)(),*ax,*bx,*cx,*fa,*fb,*fc;
{
        float ulim,u,r,q,fu,dum;

        *fa=(*func)(*ax);
        *fb=(*func)(*bx);
        if (*fb > *fa) {
                SHFT(dum,*ax,*bx,dum)
                SHFT(dum,*fb,*fa,dum)
        }
        *cx=(*bx)+GOLD*(*bx-*ax);
        *fc=(*func)(*cx);
        while (*fb > *fc) {
                r=(*bx-*ax)*(*fb-*fc);
                q=(*bx-*cx)*(*fb-*fa);
                u=(*bx)-((*bx-*cx)*q-(*bx-*ax)*r)/
                        (2.0*SIGN(FMAX(fabs(q-r),TINY),q-r));
                ulim=(*bx)+GLIMIT*(*cx-*bx);
                if ((*bx-u)*(u-*cx) > 0.0) {
                        fu=(*func)(u);
                        if (fu < *fc) {
                                *ax=(*bx);
                                *bx=u;
                                *fa=(*fb);
                                *fb=fu;
                                return;
                        } else if (fu > *fb) {
                                *cx=u;
                                *fc=fu;
                                return;
                        }
                        u=(*cx)+GOLD*(*cx-*bx);
                        fu=(*func)(u);
                } else if ((*cx-u)*(u-ulim) > 0.0) {
                        fu=(*func)(u);
                        if (fu < *fc) {
                                SHFT(*bx,*cx,u,*cx+GOLD*(*cx-*bx))
                                SHFT(*fb,*fc,fu,(*func)(u))
                        }
                } else if ((u-ulim)*(ulim-*cx) >= 0.0) {
                        u=ulim;
                        fu=(*func)(u);
                } else {
                        u=(*cx)+GOLD*(*cx-*bx);
                        fu=(*func)(u);
                }
                SHFT(*ax,*bx,*cx,u)
                SHFT(*fa,*fb,*fc,fu)
        }
}
#undef GOLD
#undef GLIMIT
#undef TINY
#undef SHFT



/*#####################################################################################################*/
/* linear fitting function from numerical recipes*/
static float sqrarg;
#define SQR(a) ((sqrarg=(a)) == 0.0 ? 0.0 : sqrarg*sqrarg)


void fit(x,y,ndata,sig,mwt,a,b,siga,sigb,chi2,q)
float *a,*b,*chi2,*q,*siga,*sigb,sig[],x[],y[];
int mwt,ndata;
{
        float gammq();
        int i;
        float wt,t,sxoss,sx=0.0,sy=0.0,st2=0.0,ss,sigdat;

        *b=0.0;
        if (mwt) {
                ss=0.0;
                for (i=1;i<=ndata;i++) {
                        wt=1.0/SQR(sig[i]);
                        ss += wt;
                        sx += x[i]*wt;
                        sy += y[i]*wt;
                }
        } else {
                for (i=1;i<=ndata;i++) {
                        sx += x[i];
                        sy += y[i];
                }
                ss=ndata;
        }
        sxoss=sx/ss;
        if (mwt) {
                for (i=1;i<=ndata;i++) {
                        t=(x[i]-sxoss)/sig[i];
                        st2 += t*t;
                        *b += t*y[i]/sig[i];
                }
        } else {
                for (i=1;i<=ndata;i++) {
                        t=x[i]-sxoss;
                        st2 += t*t;
                        *b += t*y[i];
                }
        }
        *b /= st2;
        *a=(sy-sx*(*b))/ss;
        *siga=sqrt((1.0+sx*sx/(ss*st2))/ss);
        *sigb=sqrt(1.0/st2);
        *chi2=0.0;
        if (mwt == 0) {
                for (i=1;i<=ndata;i++)
                        *chi2 += SQR(y[i]-(*a)-(*b)*x[i]);
                *q=1.0;
                sigdat=sqrt((*chi2)/(ndata-2));
                *siga *= sigdat;
                *sigb *= sigdat;
        } else {
                for (i=1;i<=ndata;i++)
                        *chi2 += SQR((y[i]-(*a)-(*b)*x[i])/sig[i]);
                *q=gammq(0.5*(ndata-2),0.5*(*chi2));
        }
}/*fit*/
#undef SQR

/*#########################################################################*/
/*function needed by fit(), again from numerical recipes*/

float gammq(a,x)
float a,x;
{
        void gcf(),gser();
        float gamser,gammcf,gln;

        if (x < 0.0 || a <= 0.0){
           fprintf(stderr,"Invalid arguments in routine gammq");
           exit(1);
        }
        if (x < (a+1.0)) {
                gser(&gamser,a,x,&gln);
                return 1.0-gamser;
        } else {
                gcf(&gammcf,a,x,&gln);
                return gammcf;
        }
}/*gammq*/

/*########################################################*/

#define ITMAX 100
#define EPS 3.0e-7
#define FPMIN 1.0e-30

void gcf(gammcf,a,x,gln)
float *gammcf,*gln,a,x;
{
        float gammln();
        int i;
        float an,b,c,d,del,h;

        *gln=gammln(a);
        b=x+1.0-a;
        c=1.0/FPMIN;
        d=1.0/b;
        h=d;
        for (i=1;i<=ITMAX;i++) {
                an = -i*(i-a);
                b += 2.0;
                d=an*d+b;
                if (fabs(d) < FPMIN) d=FPMIN;
                c=b+an/c;
                if (fabs(c) < FPMIN) c=FPMIN;
                d=1.0/d;
                del=d*c;
                h *= del;
                if (fabs(del-1.0) < EPS) break;
        }
        if (i > ITMAX){
          fprintf(stderr,"a too large, ITMAX too small in gcf");
          exit(1);
        }
        *gammcf=exp(-x+a*log(x)-(*gln))*h;
}
#undef ITMAX
#undef EPS
#undef FPMIN

/*########################################################*/

#define ITMAX 100
#define EPS 3.0e-7

void gser(gamser,a,x,gln)
float *gamser,*gln,a,x;
{
        float gammln();
        int n;
        float sum,del,ap;

        *gln=gammln(a);
        if (x <= 0.0) {
                if (x < 0.0){
                   fprintf(stderr,"x less than 0 in routine gser");
                   exit(1);
                } 
                *gamser=0.0;
                return;
        } else {
                ap=a;
                del=sum=1.0/a;
                for (n=1;n<=ITMAX;n++) {
                        ++ap;
                        del *= x/ap;
                        sum += del;
                        if (fabs(del) < fabs(sum)*EPS) {
                                *gamser=sum*exp(-x+a*log(x)-(*gln));
                                return;
                        }
                }
                fprintf(stderr,"a too large, ITMAX too small in routine gser");
                return;
        }
}
#undef ITMAX
#undef EPS

/*########################################################*/

float gammln(xx)
float xx;
{
        double x,y,tmp,ser;
        static double cof[6]={76.18009172947146,-86.50532032941677,
                24.01409824083091,-1.231739572450155,
                0.1208650973866179e-2,-0.5395239384953e-5};
        int j;

        y=x=xx;
        tmp=x+5.5;
        tmp -= (x+0.5)*log(tmp);
        ser=1.000000000190015;
        for (j=0;j<=5;j++) ser += cof[j]/++y;
        return -tmp+log(2.5066282746310005*ser/x);
}




/*#####################################################################################################*/
/*calculates gap fraction based upon the linear LAD model*/

float linearGap(RatControl *ratPoint,RatImage *dimage,int numb,int Lplace,int bin)
{
  int i=0,n=0,mat=0,Vplace=0;
  float gap=0,theta=0;
  float Gamma=0,cos2theta=0;
  float *area=NULL,m=0,tempGap=0;
  float lookUpPhase(RatImage *,float,float);

  if(dimage->species==0)Gamma=0.85; /*for conifers*/
  theta=(float)ratPoint[numb].theta[Lplace];
  cos2theta=cos(2.0*theta);

  area=falloc(dimage->matN,"gap material area",Vplace);

  gap=1.0;
  for(i=1;i<bin;i++){  /*step along the beam's path*/
    if((ratPoint[numb].ratRes[Lplace].voxN[i]!=ratPoint[numb].ratRes[Lplace].voxN[i-1])&&(ratPoint[numb].ratRes[Lplace].voxN[i]>0)){
      for(mat=0;mat<dimage->matN;mat++)area[mat]=0.0;
      m=0.0;
      for(n=0;n<ratPoint[numb].ratRes[Lplace].voxN[i];n++){
        Vplace=ratPoint[numb].ratRes[Lplace].voxMap[i][n];
        for(mat=0;mat<dimage->matN;mat++)area[mat]+=dimage->voxel[Vplace].Ae[mat];
        m+=dimage->voxel[Vplace].LAD[0];
      }
      for(mat=0;mat<dimage->matN;mat++)area[mat]/=(float)ratPoint[numb].ratRes[Lplace].voxN[i];
      m/=(float)ratPoint[numb].ratRes[Lplace].voxN[i];
      if(dimage->species==1){ /*we also need phase factor*/
        Gamma=lookUpPhase(dimage,m,theta);
      }
      if((Gamma<0.0)||(Gamma>1.0)){fprintf(stderr,"No\n");exit(1);}
      tempGap=1.0-(area[0]+area[1])*(m*(cos2theta-dimage->cos2mu)+1.0/3.0)/Gamma;
      if(tempGap>1.0)tempGap=1.0;
      else if(tempGap<0.0)tempGap=0.0;
      gap*=tempGap;
    }
    if(gap<=0.0)break;
  }/*bin loop*/

  if(gap<0.0)gap=0.0;
  else if(gap>1.0){
    fprintf(stderr,"It's broken\n");
    exit(1);
  }
  TIDY(area);
  return(gap);
}/*linearGap*/


/*#####################################################################################################*/
/*looks up Phase from the table*/

float lookUpPhase(RatImage *dimage,float m,float theta)
{
  int i=0;
  int closeI=0,closeJ=0;
  float minSep=0,sep=0;

  theta*=180.0/M_PI;
  minSep=200000.0;

  for(i=0;i<dimage->NthetaLUT;i++){
    sep=dimage->thetaLUT[i]-theta;
    sep*=sep;
    if(sep<minSep){
      minSep=sep;
      closeI=i;
    }
  }
  minSep=200000.0;
  for(i=0;i<dimage->NmLUT;i++){
    sep=dimage->mLUT[i]-m;
    sep*=sep;
    if(sep<minSep){
      minSep=sep;
      closeJ=i;
    }
  }
  if(dimage->phaseLUT[closeI][closeJ]>0.0)return(dimage->phaseLUT[closeI][closeJ]);
  else                                    return(1.0); 
}/*lookUpPhase*/


/*######################################################################################################*/
/*calculate the difference between the radiant flux error and actual radiant flux using the linear model*/

void linearRadianceError(RatControl *ratPoint,RatImage *dimage,float **refErr)
{
  int i=0,place=0;
  int Vplace=0,**contN=NULL;
  int numb=0,Lplace=0,bin=0,band=0;
  float gap=0,rho=0,error=0;
  float predRefl=0;

  if(!(contN=(int **)calloc(dimage->scanN,sizeof(int *)))){
    fprintf(stderr,"Out of memory here\n");
    exit(1);
  }
  for(numb=0;numb<dimage->scanN;numb++){
    contN[numb]=ialloc(dimage->nBands,"error contribution counter",numb);
  }

  for(numb=0;numb<dimage->scanN;numb++){
    for(band=0;band<dimage->nBands;band++){
      refErr[numb][band]=0.0;
      contN[numb][band]=0;
    }
  }

  band=1;
  for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
    for(i=0;i<dimage->voxel[Vplace].contN;i++){
      numb=dimage->voxel[Vplace].rayMap[3*i];
      Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
      bin=dimage->voxel[Vplace].rayMap[3*i+2];
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);

      gap=linearGap(ratPoint,dimage,numb,Lplace,bin);

      rho=dimage->voxel[Vplace].psi*ratPoint[numb].albedo[2*band]+(1.0-dimage->voxel[Vplace].psi)*ratPoint[numb].albedo[2*band+1];
      if(gap>0.0)predRefl=rho*(dimage->voxel[Vplace].LAD[0]*(cos(2.0*ratPoint[numb].theta[Lplace])-dimage->cos2mu)+2.0/3.0)*(dimage->voxel[Vplace].Ae[0]+dimage->voxel[Vplace].Ae[1])*gap;
      else       predRefl=0.0;

      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      error=predRefl-ratPoint[numb].ratRes[Lplace].refl[place];
      refErr[numb][band]+=error*error;
      contN[numb][band]++;

    }/*bin in voxel loop*/
  }/*voxel loop*/

  for(numb=0;numb<dimage->scanN;numb++){
    for(band=0;band<dimage->nBands;band++){
      if(contN[numb][band]>0)refErr[numb][band]=sqrt(refErr[numb][band])/(float)contN[numb][band];
    }
  }


  if(contN){
    for(numb=0;numb<dimage->scanN;numb++)TIDY(contN[numb]);
    free(contN);
    contN=NULL;
  }

  /*and then display the results*/
  for(numb=0;numb<dimage->scanN;numb++){
    for(band=0;band<dimage->nBands;band++){
      band=dimage->band[numb][band];
      fprintf(stdout,"%f ",refErr[numb][band]);
    }
    fprintf(stdout,"\n");
  }

  return;
}/*linearRadianceError*/


/*#####################################################################################################*/
/*calculate the difference between the radiant flux error and actual radiant flux*/

float **radianceError(RatControl *ratPoint,RatImage *dimage,float **refErr)
{
  int Vplace=0,i=0,j=0,n=0;
  float difference=0,predicted=0;
  int bin=0,Lplace=0,numb=0,band=0;
  float gap=0,psi=0,*LAD=NULL,*Aetot=NULL;
  float **contN=NULL;
  int Langle=0;
  float *thetaHat=NULL;
  float *meanAe(RatControl *,RatImage *,int,int,int,float *);
  float guessGap(RatControl *,RatImage *,float,int,int,int);

  if(!(thetaHat=(float *)calloc(dimage->matN,sizeof(float)))){
    ratPoint=clearAllArrays(ratPoint,dimage);
    if(!(thetaHat=(float *)calloc(dimage->matN,sizeof(float)))){
      ratPoint=clearAllArrays(ratPoint,dimage);
      thetaHat=falloc(dimage->matN,"element area",802);
    }
  }
  if(!(Aetot=(float *)calloc(dimage->matN,sizeof(float)))){
    ratPoint=clearAllArrays(ratPoint,dimage);
    Aetot=falloc(dimage->matN,"element area",801);
  }
  if(!(LAD=(float *)calloc(dimage->matN,sizeof(float)))){
    ratPoint=clearAllArrays(ratPoint,dimage);
    LAD=falloc(dimage->matN,"element area",900);
  }
  if(!(contN=(float **)calloc(dimage->scanN,sizeof(float)))){
    fprintf(stderr,"Error in error contribution allocation\n");
    exit(1);
  }
  for(numb=0;numb<dimage->scanN;numb++){
    contN[numb]=falloc(dimage->nBands,"error contribution",numb);
    for(band=0;band<dimage->nBands;band++){
      band=dimage->band[numb][band];
      refErr[numb][band]=0;
      contN[numb][band]=0.0;
    }
  }

  for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
    Langle=0;
    for(i=0;i<dimage->voxel[Vplace].contN;i++){
      /*calculate predicted intensity and compare to actual*/
      numb=dimage->voxel[Vplace].rayMap[3*i];
      Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
      bin=dimage->voxel[Vplace].rayMap[3*i+2];
      if(ratPoint[numb].ratRes[Lplace].refl==NULL){
        ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
        Langle++;
      }
      psi=meanPsi(ratPoint,dimage,numb,Lplace,bin);
      LAD=meanLAD(ratPoint,dimage,numb,Lplace,bin,LAD);
      Aetot=meanAe(ratPoint,dimage,numb,Lplace,bin,Aetot);
      gap=1;
      for(j=0;j<bin;j++){
        gap=guessGap(ratPoint,dimage,gap,numb,Lplace,j);
        if(gap<=0)break;
      }
      thetaHat=meanThetaHat(ratPoint,dimage,LAD,thetaHat,numb,Lplace);
      if(ratPoint[numb].ratRes[Lplace].refl==NULL){
        ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
        if(ratPoint[numb].ratRes[Lplace].refl==NULL){
          fprintf(stderr,"There's just not enough RAM.\n");
          exit(1);
        }
      }
      for(n=0;n<dimage->nBands;n++){
        band=dimage->band[numb][n];
        if(gap>THRESHOLD)predicted=gap*ratPoint[numb].albedo[2*band]*Aetot[0]*cos(thetaHat[0])*cos(thetaHat[0])*cos(thetaHat[0])+\
                  ratPoint[numb].albedo[2*band+1]*Aetot[1]*cos(thetaHat[1])*cos(thetaHat[1])*cos(thetaHat[1])*dimage->voxel[Vplace].fraction[i];
        else             predicted=0;
        if(predicted+ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>THRESHOLD){
          difference=(ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]-predicted)/\
           ((predicted+ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)])/2);
          refErr[numb][band]+=difference*difference;
        }
        contN[numb][band]+=dimage->voxel[Vplace].fraction[i];
      }
      if(i!=dimage->voxel[Vplace].contN-1){
        if(numb!=dimage->voxel[Vplace].rayMap[3*(i+1)]||Lplace!=dimage->voxel[Vplace].rayMap[3*(i+1)+1]){
          ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
          /*dimage=clearAngles(dimage); */
        }
      }else if(i==dimage->voxel[Vplace].contN-1){
        ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
      }
    }
  }
  ratPoint=clearAllArrays(ratPoint,dimage);
  for(numb=0;numb<dimage->scanN;numb++){
    for(n=0;n<dimage->nBands;n++){
      band=dimage->band[numb][n];
      if(contN[numb][band]>0.0){
        refErr[numb][band]=sqrt(refErr[numb][band]/contN[numb][band]);
      }else{
        fprintf(stderr,"Oh dear, no error %f %d\n",contN[numb][band],dimage->nBands);
      }
    }
  }
  TIDY(LAD);
  TIDY(Aetot);
  TIDY(thetaHat);
  if(contN){
    for(numb=0;numb<dimage->scanN;numb++){
      TIDY(contN[numb]);
    }
    free(contN);
    contN=NULL;
  }

  /*and then display the results*/
  for(numb=0;numb<dimage->scanN;numb++){
    for(n=0;n<dimage->nBands;n++){
      band=dimage->band[numb][n];
      fprintf(stdout,"%f ",refErr[numb][band]);
    }
    fprintf(stdout,"\n");
  }

  return(refErr);
}  /*##radianceError##*/

/*#####################################################################################################*/
/*calculate thetaHat for this view direction*/

float *meanThetaHat(RatControl *ratPoint,RatImage *dimage,float *LAD,float *thetaHat,int numb,int Lplace)
{
  int mat=0;
  double cos2mu=dimage->cos2mu;
  double theta45=0;

  for(mat=0;mat<dimage->matN;mat++){
    theta45=(M_PI/4-LAD[mat]*cos2mu)/(1-cos2mu);
    thetaHat[mat]=(LAD[mat]-theta45)*cos(2*ratPoint[numb].Etheta[(int)(Lplace/ratPoint[numb].NAscans)])+theta45;
    if(thetaHat[mat]<0||thetaHat[mat]>M_PI/2){
      fprintf(stderr,"Holy sump pumps! hat %f LAD %f theta %f cos2mu %f theta45 %f\n",\
             thetaHat[mat],LAD[mat],ratPoint[numb].Etheta[(int)(Lplace/ratPoint[numb].NAscans)],cos2mu,theta45);
      exit(1);
    }
  }
  return(thetaHat);
}/*meanThetaHat*/

/*#####################################################################################################*/
/*caluclate the gap through this bin*/

float guessGap(RatControl *ratPoint,RatImage *dimage,float gap,int numb,int Lplace,int bin)
{
  int band=0,i=0,mat=0,n=0;
  float thresh=M_PI/50;
  float *LAD=NULL,*Ae=NULL,psi=0;
  float *thetaHat=NULL;
  float kappa=0,ecc=0;
  float a=0,b=0,r=0,beamA=0;
  char notempty=0;

  ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);

  for(n=0;n<dimage->nBands;n++){
    band=dimage->band[numb][n];
    if(ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>0)notempty=1;
  }
  if(!notempty)return(gap);

  r=(ratPoint[numb].bin_L/2.0*((double)bin+0.5)+ratPoint[numb].min_R)*tan(ratPoint[numb].div/2);
  beamA=M_PI*r*r;

  if(!(LAD=(float *)calloc(dimage->matN,sizeof(float)))){
    ratPoint=clearAllArrays(ratPoint,dimage);
    if(!(LAD=(float *)calloc(dimage->matN,sizeof(float)))){
      fprintf(stderr,"error in LAD array allocation 900.\n");
      exit(1);
    }
  }
  if(!(Ae=(float *)calloc(dimage->matN,sizeof(float)))){
    ratPoint=clearAllArrays(ratPoint,dimage);
    Ae=falloc(dimage->matN,"guessGap element area",1);
  }
  for(mat=0;mat<dimage->matN;mat++)Ae[mat]=0;
  if(!(thetaHat=(float *)calloc(dimage->matN,sizeof(float)))){
    ratPoint=clearAllArrays(ratPoint,dimage);
    thetaHat=falloc(dimage->matN,"theta hat",1);
  }
  /*make sure the data we need is available*/
  if(ratPoint[numb].ratRes[Lplace].refl==NULL){
    ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
    if(ratPoint[numb].ratRes[Lplace].refl==NULL){
      fprintf(stderr,"There's just not enough RAM.\n");
      exit(1);
    }
  }

  LAD=meanLAD(ratPoint,dimage,numb,Lplace,bin,LAD);
  psi=meanPsi(ratPoint,dimage,numb,Lplace,bin);
  if(LAD[0]>M_PI/2){
    fprintf(stderr,"The error is agrieving may\n");
    exit(1);
  }
  thetaHat=meanThetaHat(ratPoint,dimage,LAD,thetaHat,numb,Lplace);
  for(n=0;n<dimage->nBands;n++){
    band=dimage->band[numb][n];
    Ae[0]+=ratPoint[numb].ratRes[Lplace].refl[bin*ratPoint[numb].nBands*(ratPoint[numb].n+1)+band*(ratPoint[numb].n+1)]\
           /(cos(thetaHat[0])*cos(thetaHat[0])*cos(thetaHat[0])*(ratPoint[numb].albedo[dimage->matN*band]\
           +ratPoint[numb].albedo[dimage->matN*band+1]*(1-psi)/psi));
    Ae[1]+=ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]\
           /(cos(thetaHat[1])*cos(thetaHat[1])*cos(thetaHat[1])*(ratPoint[numb].albedo[dimage->matN*band+1]\
           +ratPoint[numb].albedo[dimage->matN*band]*psi/(1-psi)));
  }
  for(mat=0;mat<dimage->matN;mat++)Ae[mat]/=(float)dimage->nBands;

  for(i=0;i<ANGLERES;i++){
    if(thetaHat[0]-dimage->omegaRatio[i]<thresh&&thetaHat[0]-dimage->omegaRatio[i]>-1*thresh){
      kappa=(float)dimage->omegaRatio[ANGLERES+i];
      break;
    }
  }
  for(mat=0;mat<dimage->matN;mat++){
    if(kappa<1){  /*then use an oblate spheroid*/
      ecc=sqrt(1-kappa*kappa);
      b=sqrt(Ae[mat]*beamA/(M_PI*(1+kappa*kappa/(2*ecc)*log((1+ecc)/(1-ecc)))));
    }else if(kappa>1){                /*treat as a prolate spheroid*/
      ecc=sqrt(1-1/(kappa*kappa));
      b=Ae[mat]*beamA/(M_PI*(1+kappa/ecc*asin(ecc)));
    }else if(kappa==1){
      b=sqrt(Ae[mat]/(2*M_PI));
    }
    a=b*kappa;
    if(ratPoint[numb].theta[Lplace]>M_PI-0.0001&&ratPoint[numb].theta[Lplace]<M_PI+0.0001){
      gap-=M_PI*b*b*sqrt(1+a*a/(b*b*tan(ratPoint[numb].theta[Lplace])*tan(ratPoint[numb].theta[Lplace])));
    }else{
      gap-=M_PI*b*a/beamA;
    }
  }
  if(LAD){free(LAD);LAD=NULL;}
  if(Ae){free(Ae);Ae=NULL;}
  if(thetaHat){free(thetaHat);thetaHat=NULL;}
  return(gap);
}  /*guessGap()*/

/*#####################################################################################################*/
/*calculate actual surface area from observed surface area*/

RatImage *clumpingCorrection(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0;
  int Vplace=0,mat=0;
  int numb=0,Lplace=0,bin=0;
  float gap=0;   /*the gap fraction of this voxel alone*/
  float Tgap;    /*total gap*/

  fprintf(stdout,"Correcting for clumping\n");

  if(!(dimage->acAe=(float *)calloc(dimage->totalVoxels*dimage->matN,sizeof(float)))){
    ratPoint=clearAllArrays(ratPoint,dimage);
    if(!(dimage->acAe=(float *)calloc(dimage->totalVoxels*dimage->matN,sizeof(float)))){
      fprintf(stderr,"error in element area array allocation in meanAe.\n");
      exit(1);
    }
  }
  
  for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
    for(i=0;i<dimage->voxel[Vplace].contN;i++){
      numb=dimage->voxel[Vplace].rayMap[3*i];
      Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
      bin=dimage->voxel[Vplace].rayMap[3*i+2];
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
      Tgap=1;
      for(j=0;j<bin-1;j++){
        Tgap=guessGap(ratPoint,dimage,Tgap,numb,Lplace,j);
      }
      gap=1;
      gap=guessGap(ratPoint,dimage,gap,numb,Lplace,bin);
      Tgap-=gap;

      if(Tgap>0){
        for(mat=0;mat<dimage->matN;mat++){
          if(gap<1){
            dimage->acAe[Vplace*dimage->matN+mat]=dimage->voxel[Vplace].Ae[mat]*(1-1/gap)/log(gap);
          }else if(gap==1){
            dimage->acAe[Vplace*dimage->matN+mat]=dimage->voxel[Vplace].Ae[mat];
          }
          if(dimage->acAe[Vplace*dimage->matN+mat]!=dimage->voxel[Vplace].Ae[mat])\
            fprintf(stdout,"Voxel %d material %d %f %f\n",Vplace,mat,dimage->acAe[Vplace*dimage->matN+mat],dimage->voxel[Vplace].Ae[mat]);
        }
      }else{   /*it is blocked within this bin*/
        /*God knows*/
        /*Apparantly He does*/
        /*If not one sparrow is forgotten (can't remember 1:1:1)*/
        /*Then I'm sure in these days of climate change*/
        /*He will be oaying special attention to the number of leaves*/

        /*Pray for the answer*/
        /*That is my plan B*/
      }

      if(i!=dimage->voxel[Vplace].contN-1){
        if(numb!=dimage->voxel[Vplace].rayMap[3*(i+1)]||Lplace!=dimage->voxel[Vplace].rayMap[3*(i+1)+1]){
          ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
        }
      }else if(i==dimage->voxel[Vplace].contN-1){
        ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
      }
    }
  }

  TIDY(dimage->acAe);
  return(dimage);
}  /*clumping correction*/

/*#####################################################################################################*/
/*calculate mean LAD for a bin*/

float *meanLAD(RatControl *ratPoint,RatImage *dimage,int numb,int Lplace,int bin,float *LAD)
{
  int mat=0,n=0,Vplace=0;
  int findBinOrder(RatImage *,RatControl *,int,int,int,int);
  float contN=0;

  for(mat=0;mat<dimage->matN;mat++)LAD[mat]=0.0;

  /*calculate or guess LAD within this bin*/
  if(ratPoint[numb].ratRes[Lplace].voxN[bin]>0){
    /*average LADs from contributing voxels*/
    for(n=0;n<ratPoint[numb].ratRes[Lplace].voxN[bin];n++){
      Vplace=ratPoint[numb].ratRes[Lplace].voxMap[bin][n];
      /*i=findBinOrder(dimage,ratPoint,Vplace,numb,Lplace,bin);*/
      for(mat=0;mat<dimage->matN;mat++){
        LAD[mat]+=dimage->voxel[Vplace].LAD[mat]*ratPoint[numb].ratRes[Lplace].fraction[bin][n];   /*dimage->voxel[Vplace].fraction[i];*/
      }
      contN+=ratPoint[numb].ratRes[Lplace].fraction[bin][n];
    }
    if(contN>0.0){
      for(mat=0;mat<dimage->matN;mat++)LAD[mat]/=contN;
    }else{
      for(mat=0;mat<dimage->matN;mat++)LAD[mat]=M_PI/4.0;
    }
  }else{             /*assume a spherical distribution*/
    for(mat=0;mat<dimage->matN;mat++)LAD[mat]=M_PI/4;
  }  /*end of LAD calculating bit*/
  if(LAD[0]>M_PI/2){
    fprintf(stderr,"hey s %f rads at scan %d %d %d\n",LAD[0],numb,Lplace,bin);
    exit(1);
  }

  return(LAD);
}/*meanLAD*/

/*#####################################################################################################*/
/*calculate mean psi for a bin*/

float meanPsi(RatControl *ratPoint,RatImage *dimage,int numb,int Lplace,int bin)
{
  int n=0,Vplace=0;
  float contN=0;
  int findBinOrder(RatImage *,RatControl *,int,int,int,int);
  float psi=0;

  if(ratPoint[numb].ratRes[Lplace].voxN[bin]>0){
    for(n=0;n<ratPoint[numb].ratRes[Lplace].voxN[bin];n++){
      Vplace=ratPoint[numb].ratRes[Lplace].voxMap[bin][n];
      /*i=findBinOrder(dimage,ratPoint,Vplace,numb,Lplace,bin);*/
      if(dimage->voxel[Vplace].psi>=0&&dimage->voxel[Vplace].psi<=1){
        psi+=dimage->voxel[Vplace].psi*ratPoint[numb].ratRes[Lplace].fraction[bin][n];   /*dimage->voxel[Vplace].fraction[i];*/
        contN+=ratPoint[numb].ratRes[Lplace].fraction[bin][n];
      }else{
        fprintf(stderr,"Uwaga! psi is %g in %d %d\n",dimage->voxel[Vplace].psi,Vplace,ratPoint[numb].ratRes[Lplace].voxN[bin]);
      }
    }
    if(contN>0.0){
      psi/=contN;
    }else{
      psi=0.5;
    }
  }else{             /*assume your favourite decimal*/
    psi=0.5;
  } 
  return(psi);
}/*meanPsi*/

/*#####################################################################################################*/
/*calculates the mean Ae for a bin from all contributing voxels, used to calculate error*/

float *meanAe(RatControl *ratPoint,RatImage *dimage,int numb,int Lplace,int bin,float *Ae)
{
  int mat=0,n=0,Vplace=0;
  int findBinOrder(RatImage *,RatControl *,int,int,int,int);
  float contN=0;

  for(mat=0;mat<dimage->matN;mat++)Ae[mat]=0;  /*COULD BE WRONG*/

  if(ratPoint[numb].ratRes[Lplace].voxN[bin]>0){
    for(n=0;n<ratPoint[numb].ratRes[Lplace].voxN[bin];n++){
      Vplace=ratPoint[numb].ratRes[Lplace].voxMap[bin][n];
      /*i=findBinOrder(dimage,ratPoint,Vplace,numb,Lplace,bin);*/
      for(mat=0;mat<dimage->matN;mat++){
        if(dimage->voxel[Vplace].Ae[mat]>=0){
          Ae[mat]+=dimage->voxel[Vplace].Ae[mat]*ratPoint[numb].ratRes[Lplace].fraction[bin][n];
          contN+=ratPoint[numb].ratRes[Lplace].fraction[bin][n];
        }
      }
    }
  }
  if(contN>0.0){
    for(mat=0;mat<dimage->matN;mat++)Ae[mat]/=contN;
  }else{
    for(mat=0;mat<dimage->matN;mat++)Ae[mat]=0;
  }
  return(Ae);
}/*meanAe*/

/*#####################################################################################################*/
/*calculates the average angle of incidence looking down on a spheroid, also called Omega hat*/

float meanThetaZ(RatImage *dimage,float a,float b)
{
  float dx=0,x=0;
  int iter=0,i=0;
  float integral=0,beta=0;

  dx=0.0001;

  iter=(int)(a/dx);

  for(i=0;i<iter;i++){
    x=i*dx;
    beta=acos(1/sqrt(b*b*x*x/(a*a*(a*a-x*x))+1));
    integral+=beta*2*M_PI*x*dx;
  }

  integral/=M_PI*a*a;
  return(integral);
}/*meanThetaZ*/

/*###############################################################################################*/
/*set up the albedo arrays*/

RatControl *labelAlbedo(RatImage *dimage,RatControl *ratPoint)
{
  int band=0,numb=0,mat=0;
  int *contN=NULL;
  void readAlbedo(RatImage *,RatControl *);
  void readPlants(RatImage *,RatControl *);

  /*to prevent reallocation*/
  if(ratPoint[numb].matCode)return(ratPoint);

  /*for now fudge the albedo array. We are solving for a two element system with known wavebands*/
  dimage->matN=2;
  contN=ialloc(dimage->matN*dimage->nBands,"albedo contribution",0);
  readPlants(dimage,ratPoint);
  readAlbedo(dimage,ratPoint);
  for(numb=0;numb<dimage->scanN;numb++){
    for(mat=0;mat<dimage->matN;mat++){       /*reset contribution counters*/
      for(band=0;band<dimage->nBands;band++){
        contN[2*band]=0;
        contN[2*band+1]=0;
      }
    }
    ratPoint[numb].albedo=falloc(dimage->matN*dimage->nBands,"albedo",numb);

    /*sort the file data into leaf and bark, this needs a list if material names, eg from matfinder.awk*/

    /*create a map for later*/
    ratPoint[numb].matCode=ialloc(ratPoint[numb].nMat,"material code",numb);
    for(mat=0;mat<ratPoint[numb].nMat;mat++)ratPoint[numb].matCode[mat]=0;

    for(mat=0;mat>2;mat++)ratPoint[numb].matCode[mat]=-1; /*not wood or leaf*/
    for(mat=2;mat<ratPoint[numb].nMat;mat++){
      for(band=0;band<dimage->nBands;band++){
        if(!strncasecmp(dimage->refFiles[mat][0],"Leaf1",5)||\
           !strncasecmp(dimage->refFiles[mat][0],"leaf",4)){
          ratPoint[numb].albedo[2*band]+=ratPoint[numb].ro[mat][band];
          ratPoint[numb].matCode[mat]=0;
          contN[2*band]++;
        }else if(!strncasecmp(dimage->refFiles[mat][0],"Branch3",7)||\
                 !strncasecmp(dimage->refFiles[mat][0],"Twig",4)||\
                 !strncasecmp(dimage->refFiles[mat][0],"Stem1",5)||\
                 !strncasecmp(dimage->refFiles[mat][0],"Trunk",5)||\
                 !strncasecmp(dimage->refFiles[mat][0],"Bough",5)||\
                 !strncasecmp(dimage->refFiles[mat][0],"Branch1",7)||\
                 !strncasecmp(dimage->refFiles[mat][0],"Branch2",7)||\
                 !strncasecmp(dimage->refFiles[mat][0],"Cuts_BG",7)||\
                 !strncasecmp(dimage->refFiles[mat][0],"branch",6)||\
                 !strncasecmp(dimage->refFiles[mat][0],"material1",9)||\
                 !strncasecmp(dimage->refFiles[mat][0],"material2",9)){
          ratPoint[numb].albedo[2*band+1]+=ratPoint[numb].ro[mat][band];
          ratPoint[numb].matCode[mat]=1;
          contN[2*band+1]++;
        }else if(!strncasecmp(dimage->refFiles[mat][0],"soil",4)){
	  ratPoint[numb].matCode[mat]=-2;
        }else{
	  ratPoint[numb].matCode[mat]=-1; /*not wood or leaf*/
	}/*material sorter*/

      }/*band loop*/
    }/*material loop*/


    /*printf("The albedo array\n");*/
    for(band=0;band<dimage->nBands;band++){
      ratPoint[numb].albedo[2*band]/=(float)contN[2*band];
      ratPoint[numb].albedo[2*band+1]/=(float)contN[2*band+1];
      /*printf("%f %f %f\n",ratPoint[numb].wavelength[dimage->band[numb][band]],ratPoint[numb].albedo[2*band],ratPoint[numb].albedo[2*band+1]);*/
    }

  }/*scan loop*/


  TIDY(contN);
  return(ratPoint);
}   /*labelAlbedo*/

/*#####################################################################################################*/
/*create a look up table to convert between Omega and a/b*/

RatImage *omegaEccentricity(RatImage *dimage)
{
  int a=0,b=0;
  int resolution=ANGLERES;

  if(!(dimage->omegaRatio=(float *)calloc(2*resolution,sizeof(float)))){
    fprintf(stderr,"error in omega to e array allocation.\n");
    exit(1);
  }
 
  for(a=0;a<resolution;a++){
    b=resolution-a;
    dimage->omegaRatio[a]=(float)meanThetaZ(dimage,(float)a,(float)b);
    dimage->omegaRatio[resolution+a]=(float)a/(float)b;
  }
  return(dimage);
}/*omegaEccentricity*/

/*##################################################################################################################*/
/*A brief program to compare psi against material usage*/

RatImage *compareMaterials(RatImage *dimage,RatControl *ratPoint)
{
  int Vplace=0,n=0,j=0;
  int numb=0,Lplace=0,bin=0;
  float *materials=NULL,*error=NULL,totalErr=0;
  float dx=0,dy=0,dz=0;
  FILE *output=NULL;
  char namen[200],notNeeded=0,*empty=NULL;

  printf("Comparing derived leaf fraction to truth from ray tracer...\n");

  sprintf(namen,"%s.materialError",dimage->output);
  if((output=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening results file %s\n",namen);
    exit(1);
  }
  if(!(materials=(float *)calloc(dimage->totalVoxels*dimage->matN,sizeof(float)))){
    fprintf(stderr,"error in voxel material array allocation.\n");
    exit(1);
  }
  if(!(empty=(char *)calloc(dimage->totalVoxels,sizeof(char)))){
    fprintf(stderr,"error in voxel material error allocation.\n");
    exit(1);
  }
  if(!(error=(float *)calloc(dimage->totalVoxels,sizeof(float)))){
    fprintf(stderr,"error in voxel material error allocation.\n");
    exit(1);
  }

  for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
    if((dimage->voxel[Vplace].Ae[0]==0)&&(dimage->voxel[Vplace].Ae[1]==0)){
      empty[Vplace]=1;
    }else empty[Vplace]=0;
    for(n=0;n<dimage->voxel[Vplace].contN;n++){
      numb=dimage->voxel[Vplace].rayMap[3*n];
      Lplace=dimage->voxel[Vplace].rayMap[3*n+1];
      bin=dimage->voxel[Vplace].rayMap[3*n+2];
      if(ratPoint[numb].ratRes[Lplace].material==NULL){
        ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,3);
        notNeeded=0;
      }
      materials[dimage->matN*Vplace]+=ratPoint[numb].ratRes[Lplace].material[bin*2*dimage->matN+2*6];
      materials[dimage->matN*Vplace+1]+=ratPoint[numb].ratRes[Lplace].material[bin*2*dimage->matN+2*2]+\
                                       ratPoint[numb].ratRes[Lplace].material[bin*2*dimage->matN+2*3]+\
                                       ratPoint[numb].ratRes[Lplace].material[bin*2*dimage->matN+2*4]+\
                                       ratPoint[numb].ratRes[Lplace].material[bin*2*dimage->matN+2*5]+\
                                       ratPoint[numb].ratRes[Lplace].material[bin*2*dimage->matN+2*7];
      /*see if we need this beam in, if not, dump it*/
      for(j=n;j<dimage->voxel[Vplace].contN;j++){
        if((dimage->voxel[Vplace].rayMap[3*j+1]==Lplace)&&(numb==dimage->voxel[Vplace].rayMap[3*j]))notNeeded=1;
        if(notNeeded){
          ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
        }
      }
    }  /*end of contribution step*/
       /*then normalise*/
    if(materials[dimage->matN*Vplace]+materials[dimage->matN*Vplace+1]>0){
      materials[dimage->matN*Vplace]/=(materials[dimage->matN*Vplace]+materials[dimage->matN*Vplace+1]);
      materials[dimage->matN*Vplace+1]/=(materials[dimage->matN*Vplace]+materials[dimage->matN*Vplace+1]);
      error[Vplace]=materials[dimage->matN*Vplace]-dimage->voxel[Vplace].psi;
    }else error[Vplace]=0;
    for(n=0;n<dimage->voxel[Vplace].contN;n++){
      numb=dimage->voxel[Vplace].rayMap[3*n];
      Lplace=dimage->voxel[Vplace].rayMap[3*n+1];
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    }
    totalErr+=error[Vplace]*error[Vplace];
  }/*end of material fraction loop*/
  ratPoint=clearAllArrays(ratPoint,dimage);

  /*now go through again and print everything out*/
  fprintf(output,"This are the errors for the voxel recontrucion\ncompared against the material fractions\n");
  fprintf(output,"The total error was %f\nVoxel, x, y, z, psi, error, dist to scan 1, 2, n...\n\n",totalErr/dimage->totalVoxels);
  for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
    if(empty[Vplace]){
      fprintf(output,"%d %d %d  %d empty",Vplace,(int)dimage->voxel[Vplace].pos[0],\
               (int)dimage->voxel[Vplace].pos[1],(int)dimage->voxel[Vplace].pos[2]);
    }else{
      fprintf(output,"%d %d %d %d %f %f %f ",Vplace,(int)dimage->voxel[Vplace].pos[0],(int)dimage->voxel[Vplace].pos[1],\
                                   (int)dimage->voxel[Vplace].pos[2],dimage->voxel[Vplace].psi,materials[Vplace*2],error[Vplace]);
      for(n=0;n<dimage->scanN;n++){
        dx=dimage->voxel[Vplace].pos[0]-ratPoint[numb].from[0];
        dy=dimage->voxel[Vplace].pos[1]-ratPoint[numb].from[1];
        dz=dimage->voxel[Vplace].pos[2]-ratPoint[numb].from[2];
        fprintf(output,"%f ",sqrt(dx*dx+dy*dy+dz*dz));
      }
    }
    fprintf(output,"\n");
  }
  fprintf(output,"##########################################################\n");
  if(output)fclose(output);
  return(dimage);
}  /*##material error function##*/

/*##################################################################################################################*/
/*Now we need some way to visualise all of this. Create images of slices*/

RatImage *paintVoxels(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0,k=0,mat=0,zen=0,numb=0;
  int pixels[2],Tpixels=0,Vplace=0;
  int *Eplace=NULL;       /*array of voxel indices for scan centres*/
  float *image=NULL,maxBright=0,viewzen=0;
  double cos2mu=dimage->cos2mu;
  double theta45=0,thetaHat=0;
  unsigned char *output=NULL;
  void update_header(),init_header(),fp_fwrite_header();
  char namen[200];
  FILE *voxPic=NULL;
  struct header hd;

  printf("Painting the voxels...\n");

  for(i=0;i<2;i++)pixels[i]=dimage->voxNumb[i]*dimage->vzoom;
  Tpixels=pixels[0]*pixels[1];
  if(!(image=(float *)calloc(Tpixels*dimage->matN,sizeof(float)))){
    fprintf(stderr,"error in voxel picture float array allocation.\n");
    exit(1);
  }
  if(!(output=(unsigned char *)calloc(Tpixels*(dimage->matN+4),sizeof(unsigned char)))){
    fprintf(stderr,"error in voxel picture byte array allocation.\n");
    exit(1);
  }

  if(!(Eplace=(int *)calloc(dimage->scanN,sizeof(int)))){
    fprintf(stderr,"error in echidna indicator array allocation.\n");
    exit(1);
  }
  for(numb=0;numb<dimage->scanN;numb++){
    /*make sure the scan is within the scene's bounds*/
    if(ratPoint[numb].from[0]<=dimage->voxCentre[0]+dimage->voxNumb[0]*dimage->voxRes/2&&\
       ratPoint[numb].from[0]>=dimage->voxCentre[0]-dimage->voxNumb[0]*dimage->voxRes/2&&\
       ratPoint[numb].from[1]<=dimage->voxCentre[1]+dimage->voxNumb[1]*dimage->voxRes/2&&\
       ratPoint[numb].from[1]>=dimage->voxCentre[1]-dimage->voxNumb[1]*dimage->voxRes/2&&\
       ratPoint[numb].from[2]<=dimage->voxCentre[2]+dimage->voxNumb[2]*dimage->voxRes/2&&\
       ratPoint[numb].from[2]>=dimage->voxCentre[2]-dimage->voxNumb[2]*dimage->voxRes/2){
      i=(int)((ratPoint[numb].from[0]-dimage->voxCentre[0])/dimage->voxRes+(float)dimage->voxNumb[0]/2.0+0.5);
      j=(int)((ratPoint[numb].from[1]-dimage->voxCentre[1])/dimage->voxRes+(float)dimage->voxNumb[1]/2.0+0.5);
      k=(int)((ratPoint[numb].from[2]-dimage->voxCentre[2])/dimage->voxRes+(float)dimage->voxNumb[2]/2.0+0.5);
      Eplace[numb]=i*dimage->voxNumb[1]*dimage->voxNumb[2]+j*dimage->voxNumb[2]+k;
    }else{ fprintf(stderr,"The scan %d was beyond the scene x %f y %f z %f\n",numb,\
             ratPoint[numb].from[0],ratPoint[numb].from[1],ratPoint[numb].from[2]);}
  }

  /*loop over view zeniths*/
  for(zen=0;zen<dimage->zenN;zen++){
    viewzen=zen*M_PI/(2*dimage->zenN);
    /*loop through voxel layers*/
    for(i=0;i<dimage->voxNumb[2];i++){
      maxBright=-1000;
      if(dimage->zenN==1){
        if(dimage->voxNumb[2]>100&&i<10){
          sprintf(namen,"%s.layer.00%d.hip",dimage->output,i);
        }else if(dimage->voxNumb[2]>100&&i<100){
          sprintf(namen,"%s.layer.0%d.hip",dimage->output,i);
        }else if(dimage->voxNumb[2]>10&&i<10){
          sprintf(namen,"%s.layer.0%d.hip",dimage->output,i);
        }else{
          sprintf(namen,"%s.layer.%d.hip",dimage->output,i);
        }
      }else{
        if(dimage->voxNumb[2]>100&&i<10){
          sprintf(namen,"%s.%d.layer.00%d.hip",dimage->output,(int)(viewzen*180/M_PI),i);
        }else if(dimage->voxNumb[2]>100&&i<100){
          sprintf(namen,"%s.%d.layer.0%d.hip",dimage->output,(int)(viewzen*180/M_PI),i);
        }else if(dimage->voxNumb[2]>10&&i<10){   /*for ease of ordering later*/
          sprintf(namen,"%s.%d.layer.0%d.hip",dimage->output,(int)(viewzen*180/M_PI),i);
        }else{
          sprintf(namen,"%s.%d.layer.%d.hip",dimage->output,(int)(viewzen*180/M_PI),i);
        }
      }

      if((voxPic=fopen(namen,"wb"))==NULL){
        printf("Error opening results file %s\n",namen);
        exit(1);
      }
      init_header(&hd,voxPic," ",dimage->matN+4,"today",pixels[0],pixels[1],8*sizeof(char),0,PFBYTE," ");
  /*  update_header(&hd,argc,argv);*/   /*do we need this line, with it's argc and argv?*/
      fp_fwrite_header(voxPic,&hd);
      for(j=0;j<pixels[0];j++){
        for(k=0;k<pixels[1];k++){
          /*set the indicator frames white*/
          output[Tpixels*dimage->matN+j*pixels[1]+k]=(unsigned char)255;     /*echidna*/
          output[Tpixels*(dimage->matN+1)+j*pixels[1]+k]=(unsigned char)255; /*empty*/
          output[Tpixels*(dimage->matN+2)+j*pixels[1]+k]=(unsigned char)255; /*blocked*/
          output[Tpixels*(dimage->matN+3)+j*pixels[1]+k]=(unsigned char)255; /*prematrley*/
          Vplace=(int)(j/dimage->vzoom)*dimage->voxNumb[1]*dimage->voxNumb[2]+\
                 (int)(k/dimage->vzoom)*dimage->voxNumb[2]+i;
          for(mat=0;mat<dimage->matN;mat++){
            if(dimage->zenN>1){
              theta45=(M_PI/4+dimage->voxel[Vplace].LAD[mat]*cos2mu)/(1-cos2mu);
              thetaHat=(theta45-dimage->voxel[Vplace].LAD[mat])*cos(2*viewzen)+theta45;
            }
            if(dimage->voxel[Vplace].Ae[mat]>=0){
              if(dimage->zenN==1)image[j*pixels[1]+k+mat*Tpixels]=dimage->voxel[Vplace].Ae[mat];
              else image[j*pixels[1]+k+mat*Tpixels]=dimage->voxel[Vplace].Ae[mat]*cos(thetaHat);
              if(image[j*pixels[1]+k+mat*Tpixels]>maxBright)maxBright=image[j*pixels[1]+k+mat*Tpixels];
            }else if(dimage->voxel[Vplace].Ae[mat]<0){
              image[j*pixels[1]+k+mat*Tpixels]=0;
              if(dimage->voxel[Vplace].Ae[mat]==-1){                     /*no beams frame 3*/
                output[Tpixels*(dimage->matN+1)+j*pixels[1]+k]=(unsigned char)0;
              }else if(dimage->voxel[Vplace].Ae[mat]==-2.0){         /*really blocked frame 4*/
                output[Tpixels*(dimage->matN+2)+j*pixels[1]+k]=(unsigned char)0;
              }else if(dimage->voxel[Vplace].Ae[mat]==-3.0){    /*prematurley blocked frame 5*/
                output[Tpixels*(dimage->matN+3)+j*pixels[1]+k]=(unsigned char)0;
              }else{
                printf("Something is wrong %d %f.\n",Vplace,dimage->voxel[Vplace].Ae[mat]);
                exit(1);
              }
            }
          }
          for(numb=0;numb<dimage->scanN;numb++){
            if(Vplace==Eplace[numb]){
              output[Tpixels*dimage->matN+j*pixels[1]+k]=(unsigned char)0;
            }
          }
        }
      }
      for(j=0;j<Tpixels*dimage->matN;j++){  /*make the image negative as well*/
        output[j]=255.0-(unsigned char)(image[j]*255.0/maxBright);
      }
      if(fwrite(&(output[0]),sizeof(unsigned char),(Tpixels*(dimage->matN+4)),voxPic)!=(Tpixels*(dimage->matN+4))){
        fprintf(stderr,"voxel image layer %d image not written\n",i);
        exit(1);
      }
      if(voxPic)fclose(voxPic);
    }/*layer step*/
  }  /*end of zenith step*/
  printf("Voxels painted\n");
  return(dimage);
}/*paintVoxels*/


/*##########################################################################################################*/
/* paint an image of the materials, but flat*/

void flattenVoxels(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0,k=0,l=0,m=0,mat=0;
  int pixels[2],Tpixels=0,place=0;
  float *output=NULL;
  FILE *opoo=NULL;
  char namen[200];
  void update_header(),init_header(),fp_fwrite_header();
  struct header hd;


  pixels[0]=dimage->voxNumb[0]*dimage->vzoom;
  pixels[1]=dimage->voxNumb[1]*dimage->vzoom;

  Tpixels=pixels[0]*pixels[1];
  output=falloc(Tpixels*dimage->matN,"flat voxel image",0);
  sprintf(namen,"%s.flatVoxels.hip",dimage->output);

  if((opoo=fopen(namen,"w"))==NULL){
    printf("Error opening image file %s\n",namen);
    exit(1);
  }

  init_header(&hd,opoo," ",dimage->matN,"today",pixels[0],pixels[1],8*sizeof(float),0,PFFLOAT," ");
  fp_fwrite_header(opoo,&hd);

  for(i=0;i<dimage->voxNumb[0];i++){
    for(j=0;j<dimage->voxNumb[1];j++){
      for(mat=0;mat<dimage->matN;mat++){
        for(l=0;l<dimage->vzoom;l++){
          for(m=0;m<dimage->vzoom;m++){
            output[(j*dimage->vzoom+l)*dimage->voxNumb[1]+(i*dimage->vzoom+m)+Tpixels*mat]=0.0;
          }
        }
        for(k=0;k<dimage->voxNumb[2];k++){
          place=i*dimage->voxNumb[1]*dimage->voxNumb[2]+j*dimage->voxNumb[2]+k;
          for(l=0;l<dimage->vzoom;l++){
            for(m=0;m<dimage->vzoom;m++){
              if(dimage->voxel[place].Ae[0]>0.0) output[(j*dimage->vzoom+l)*pixels[1]+(i*dimage->vzoom+m)+Tpixels*mat]=255.0;
            }
          }
        }/*voxel z loop*/
      }  /*material loop*/
    }    /*voxel y loop*/
  }      /*voxel x loop*/ 

/*  for(i=0;i<pixels[0];i++){
    for(j=0;j<pixels[1];j++){
      fprintf(stdout,"%f ",output[j*]);
    }
  }*/



  if(fwrite(&(output[0]),sizeof(float),(Tpixels*dimage->matN),opoo)!=Tpixels*dimage->matN){
    fprintf(stderr,"Flat voxel image not written\n");
    exit(1);
  }
  

  fprintf(stdout,"Voxels drawn to %s\n",namen);

  if(opoo){
    fclose(opoo);
    opoo=NULL;
  }
  TIDY(output);
  return;
}/*flattenVoxels*/


/*##########################################################################################################*/
/*To turn the data into a vrml file*/

RatImage *vrmlUp(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0,k=0;
  int Vplace=0,mat=0;
  float maxAe[MATN],a=0;
  FILE *vrml=NULL;
  char namen[200];

  sprintf(namen,"%s.wrl",dimage->output);
  if((vrml=fopen(namen,"w"))==NULL){
    printf("Error opening vrml results file %s\n",namen);
    exit(1);
  }
  for(i=0;i<dimage->totalVoxels;i++){
    for(mat=0;mat<dimage->matN;mat++){
      if(dimage->voxel[Vplace].Ae[mat]>maxAe[mat])maxAe[mat]=dimage->voxel[Vplace].Ae[mat];
    }
  }
  a=dimage->voxRes/2;

  fprintf(vrml,"#VRML V2.0 utf8\n#a voxel representation of a forest\n");
  for(i=0;i<dimage->voxNumb[0];i++){
    for(j=0;j<dimage->voxNumb[1];j++){
      for(k=0;k<dimage->voxNumb[2];k++){
        Vplace=i*dimage->voxNumb[1]*dimage->voxNumb[2]+j*dimage->voxNumb[2]+k;
        fprintf(vrml,"  Shape {\n    appearance Appearance {\n      material Material {\n");
        fprintf(vrml,"      diffuseColor 1.0 1.0 1.0\n      transparency %f\n",1-dimage->voxel[Vplace].Ae[0]/maxAe[0]);
        fprintf(vrml,"      }\n    }\n");
        fprintf(vrml,"      geometry IndexedFaceSet {\n        coord Coordinate {\n");
        fprintf(vrml,"        point [\n");
        fprintf(vrml,"%f %f %f,\n",dimage->voxel[Vplace].pos[0]+a,dimage->voxel[Vplace].pos[1]+a,dimage->voxel[Vplace].pos[2]+a);
        fprintf(vrml,"%f %f %f,\n",dimage->voxel[Vplace].pos[0]+a,dimage->voxel[Vplace].pos[1]+a,dimage->voxel[Vplace].pos[2]-a);
        fprintf(vrml,"%f %f %f,\n",dimage->voxel[Vplace].pos[0]-a,dimage->voxel[Vplace].pos[1]-a,dimage->voxel[Vplace].pos[2]-a);
        fprintf(vrml,"%f %f %f,\n",dimage->voxel[Vplace].pos[0]-a,dimage->voxel[Vplace].pos[1]+a,dimage->voxel[Vplace].pos[2]-a);
        fprintf(vrml,"%f %f %f,\n",dimage->voxel[Vplace].pos[0]-a,dimage->voxel[Vplace].pos[1]+a,dimage->voxel[Vplace].pos[2]+a);
        fprintf(vrml,"%f %f %f,\n",dimage->voxel[Vplace].pos[0]-a,dimage->voxel[Vplace].pos[1]-a,dimage->voxel[Vplace].pos[2]+a);
        fprintf(vrml,"%f %f %f,\n",dimage->voxel[Vplace].pos[0]+a,dimage->voxel[Vplace].pos[1]-a,dimage->voxel[Vplace].pos[2]+a);
        fprintf(vrml,"%f %f %f,\n",dimage->voxel[Vplace].pos[0]-a,dimage->voxel[Vplace].pos[1]+a,dimage->voxel[Vplace].pos[2]+a);
        fprintf(vrml,"coordIndex[\n0,1,2,3,-1,4,5,6,7,-1,\n1,6,5,2,-1,0,7,6,1,-1,\n3,4,7,0,-1,2,5,4,3,-1\n]\n}\n}\n");
      }
    }
  }
  if(vrml)fclose(vrml);
  return(dimage);
}/*vrmlUp*/  /*##turn the voxels into a vrml file##*/

/*#########################################################################################################*/
/*To write out the voxel data to a binary file*/

RatImage *writeVoxels(RatImage *dimage,RatControl *ratPoint,int iter)
{
  int i=0,j=0,mat=0;
  int *header=NULL;
  float *header1=NULL,*data=NULL;
  float *headerflo=NULL;
  FILE *output=NULL;
  char namen[200];

  if(iter>=0) sprintf(namen,"%s.%d.bivoxels",dimage->output,iter);
  else if(iter<0) sprintf(namen,"%s.final.bivoxels",dimage->output); 
  if((output=fopen(namen,"wb"))==NULL){
    printf("Error opening binary results file to write %s\n",namen);
    exit(1);
  }
  if(!(header=(int *)calloc(voxHEAD,sizeof(int)))){
    printf("error in binary header allocation.\n");
    exit(1);
  }
  header[0]=2;
  header[1]=dimage->totalVoxels;
  header[2]=dimage->matN;
  for(i=0;i<3;i++){
    header[i+3]=dimage->voxNumb[i];
  }
  header[6]=dimage->scanN;

  if(dimage->byteord)header=intSwap(header,(uint64_t)voxHEAD);
  if(fseek(output,(long)0,SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(header[0]),sizeof(int),voxHEAD,output)!=voxHEAD){
    printf("voxel binary header not written\n");
    exit(1);
  }
  TIDY(header);
  if(!(headerflo=(float *)calloc(voxFLOA,sizeof(float)))){
    printf("error in binary float header allocation.\n");
    exit(1);
  }
  headerflo[0]=dimage->voxRes;
  for(i=1;i<4;i++)headerflo[i]=dimage->voxCentre[i-1];
  if(fseek(output,(long)(voxHEAD*sizeof(int)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(headerflo[0]),sizeof(float),voxFLOA,output)!=voxFLOA){
    printf("voxel binary header not written\n");
    exit(1);
  }
  TIDY(headerflo);


  if(!(header1=(float *)calloc(dimage->scanN*3,sizeof(float)))){
    printf("error in binary float header allocation.\n");
    exit(1);
  }
  for(i=0;i<dimage->scanN;i++){
    for(j=0;j<3;j++){
      header1[3*i+j]=(float)ratPoint[i].from[j];
    }
  }
  if(dimage->byteord)header1=floSwap(header1,3*(uint64_t)dimage->scanN);
  if(fseek(output,(long)(voxHEAD*sizeof(int)+voxFLOA*sizeof(float)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(header1[0]),sizeof(float),(3*dimage->scanN),output)!=(3*dimage->scanN)){
    printf("voxel float header not written\n");
    exit(1);
  }
  TIDY(header1);

  if(!(data=(float *)calloc(dimage->totalVoxels*(2*dimage->matN+1+1),sizeof(float)))){
    printf("error in data for writting allocation.\n");
    exit(1);
  } 
  for(i=0;i<dimage->totalVoxels;i++){
    for(mat=0;mat<dimage->matN;mat++){
      data[i*(2*dimage->matN+1+1)+mat]=dimage->voxel[i].Ae[mat];
      data[i*(2*dimage->matN+1+1)+dimage->matN+mat]=dimage->voxel[i].LAD[mat];
    }
    data[i*(2*dimage->matN+1+1)+2*dimage->matN]=dimage->voxel[i].psi;
    data[i*(2*dimage->matN+1+1)+2*dimage->matN+1]=(float)dimage->voxel[i].contN;
  }
  if(dimage->byteord)data=floSwap(data,(uint64_t)dimage->totalVoxels*(2*(uint64_t)dimage->matN+1+1));
  if(fseek(output,(long)(voxHEAD*sizeof(int)+voxFLOA*sizeof(float)+3*dimage->scanN*sizeof(float)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(data[0]),sizeof(float),(dimage->totalVoxels*(2*dimage->matN+1+1)),output)!=\
                                     (dimage->totalVoxels*(2*dimage->matN+1+1))){
    printf("binary voxel data not written\n");
    exit(1);
  }
  TIDY(data);
  if(output)fclose(output);
  return(dimage);
}/*writeVoxels*/  /*##binary voxel writting##*/

/*###########################################################################*/
/*A funtion to read binary voxel data back*/

RatControl *readVoxels(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0,mat=0;
  int x=0,y=0,z=0;
  int *header=NULL;
  float *header1=NULL,*data=NULL;
  float *headerflo=NULL;
  FILE *input=NULL;

  if((input=fopen(dimage->Vinput,"rb"))==NULL){
    printf("Error opening binary results file %s\n",dimage->Vinput);
    exit(1);
  }
  if(!(header=(int *)calloc(voxHEAD,sizeof(int)))){
    printf("error in binary header allocation.\n");
    exit(1);
  }
  if(fseek(input,(long)0,SEEK_SET)){
    printf("Error seeking through the voxel results file\n");
    exit(1);
  }
  if((fread(&(header[0]),sizeof(int),voxHEAD,input))!=voxHEAD){
    printf("fread error 000\n");
    exit(1);
  }
  if(dimage->byteord)header=intSwap(header,(uint64_t)voxHEAD);
  if(header[0]!=2){
    printf("This data file is of the wrong format %d\n",header[0]);
    exit(1);
  }
  dimage->totalVoxels=header[1];
  dimage->matN=header[2];
  for(i=0;i<3;i++){
    dimage->voxNumb[i]=header[i+3];
  }
  dimage->scanN=header[6];
  TIDY(header);
  if(!(headerflo=(float *)calloc(voxFLOA,sizeof(float)))){
    printf("error in binary header allocation.\n");
    exit(1);
  }
  if(fseek(input,(long)(voxHEAD*sizeof(int)),SEEK_SET)){
    printf("Error seeking through the voxel results file\n");
    exit(1);
  }
  if((fread(&(headerflo[0]),sizeof(float),voxFLOA,input))!=voxFLOA){
    printf("fread error 010\n");
    exit(1);
  }
  dimage->voxRes=headerflo[0];
  for(i=1;i<4;i++)dimage->voxCentre[i-1]=headerflo[i];
  TIDY(headerflo);

  /*now we have all the important scan variables setup the voxel arrays*/
  if((dimage->voxel=(Voxel *)calloc(dimage->totalVoxels,sizeof(Voxel)))==NULL){
    printf("error in voxel array allocation.\n");
    exit(1);
  }
  dimage->matN=2;
  dimage->cos2mu=cos(2.0*0.955655);
  for(i=0;i<dimage->totalVoxels;i++){
    if((dimage->voxel[i].Ae=(float *)calloc(dimage->matN,sizeof(float)))==NULL){
      printf("error in voxel area allocation.\n");
      exit(1);
    }
    if((dimage->voxel[i].LAD=(float *)calloc(dimage->matN,sizeof(float)))==NULL){
      printf("error in voxel LAD allocation.\n");
      exit(1);
    }
  }/*end of voxel allocation*/

  TIDY(ratPoint);
  if((ratPoint=(RatControl *)calloc(dimage->scanN,sizeof(RatControl)))==NULL){
    printf("error in control structure allocation.\n");
    exit(1);
  }

  if(!(header1=(float *)calloc(3*dimage->scanN,sizeof(float)))){
    printf("error in scan header allocation.\n");
    exit(1);
  }
  if(fseek(input,(long)(voxHEAD*sizeof(int)+voxFLOA*sizeof(float)),SEEK_SET)){
    printf("Error seeking through the voxel results file\n");
    exit(1);
  }
  if((fread(&(header1[0]),sizeof(float),(3*dimage->scanN),input))!=(3*dimage->scanN)){
    printf("fread error in the floating header\n");
    exit(1);
  }
  if(dimage->byteord)header1=floSwap(header1,3*(uint64_t)dimage->scanN);
  for(i=0;i<dimage->scanN;i++){
    for(j=0;j<3;j++){
      ratPoint[i].from[j]=header1[3*i+j];
    }
  }
  TIDY(header1);

  if(!(data=(float *)calloc(dimage->totalVoxels*(2*dimage->matN+1+1),sizeof(float)))){
    printf("error in data array allocation 2.\n");
    exit(1);
  }
  if(fseek(input,(long)(voxHEAD*sizeof(int)+voxFLOA*sizeof(float)+3*dimage->scanN*sizeof(float)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if((fread(&(data[0]),sizeof(float),(dimage->totalVoxels*(2*dimage->matN+1+1)),input))!=
                                     (dimage->totalVoxels*(2*dimage->matN+1+1))){
    printf("fread error in the binary data reading\n");
    exit(1);
  }
  if(dimage->byteord)data=floSwap(data,(uint64_t)dimage->totalVoxels*(2*(uint64_t)dimage->matN+1+1));

  for(x=0;x<dimage->voxNumb[0];x++){
    for(y=0;y<dimage->voxNumb[1];y++){
      for(z=0;z<dimage->voxNumb[2];z++){
        i=x*dimage->voxNumb[1]*dimage->voxNumb[2]+y*dimage->voxNumb[2]+z;
        for(mat=0;mat<dimage->matN;mat++){
          dimage->voxel[i].Ae[mat]=data[i*(2*dimage->matN+1+1)+mat];
          dimage->voxel[i].LAD[0]=data[i*(2*dimage->matN+1+1)+dimage->matN+mat];
        }
        dimage->voxel[i].psi=data[i*(2*dimage->matN+1+1)+2*dimage->matN];
        dimage->voxel[i].contN=(int)data[i*(2*dimage->matN+1+1)+2*dimage->matN+1];

        dimage->voxel[i].pos[0]=dimage->voxCentre[0]+((float)x-(float)dimage->voxNumb[0]/2.0)*dimage->voxRes;
        dimage->voxel[i].pos[1]=dimage->voxCentre[1]+((float)y-(float)dimage->voxNumb[1]/2.0)*dimage->voxRes;
        dimage->voxel[i].pos[2]=dimage->voxCentre[2]+((float)z-(float)dimage->voxNumb[2]/2.0)*dimage->voxRes;
      }/*voxel z loop*/
    }  /*voxel y loop*/
  }    /*voxel x loop*/
  TIDY(data);
  if(input)fclose(input);
  return(ratPoint);
}/*readVoxels*/  /*##read the binary voxel data##*/

/*#########################################################################################*/
/*write voxels to an ascii file*/

RatImage *asciiVoxels(RatImage *dimage,RatControl *ratPoint,char namen[100])
{
  int place=0,i=0;
  FILE *opoo=NULL;

  if((opoo=fopen(namen,"w"))==NULL){
    printf("Error opening results file %s\n",namen);
    exit(1);
  }
  printf("Writting voxels to %s\n",namen);

  fprintf(opoo,"# %d\n",dimage->totalVoxels);
  fprintf(opoo,"# 1 voxel, 2 leaf area, 3 bark area, 4 leaf LAD, 5 bark LAD, 6 psi, 7 x, 8 y, 9 z\n");
  for(place=0;place<dimage->totalVoxels;place++){
    fprintf(opoo,"%d %g %g %g %g %g",place,dimage->voxel[place].Ae[0],\
     dimage->voxel[place].Ae[1],dimage->voxel[place].LAD[0]*180/M_PI,\
      dimage->voxel[place].LAD[1]*180/M_PI,dimage->voxel[place].psi);
    for(i=0;i<3;i++)fprintf(opoo," %g",dimage->voxel[place].pos[i]);
    fprintf(opoo,"\n");
  }
  if(opoo)fclose(opoo);
  return(dimage);
}/*asciiVoxels*/

/*#########################################################################################*/
/* Write out the voxel maps to a file for later useage*/

RatImage *writeBinaryMaps(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0,k=0,m=0;
  int *header1=NULL,*contN=NULL,**contNr=NULL,*map=NULL;
  int mapLength=0,beamStep=0;
  float *header2=NULL;
  FILE *rayMap=NULL,*voxMap=NULL;
  char namen[200];

  /*and print the voxMap out to a binary file that could be used later*/
  sprintf(namen,"%s.%d.%d.%d.%d.%d.%d.voxMap",dimage->output,(int)dimage->voxCentre[0],\
       (int)dimage->voxCentre[1],(int)dimage->voxCentre[2],(int)dimage->voxScene[0],(int)dimage->voxScene[1],(int)dimage->voxScene[2]);
  if((voxMap=fopen(namen,"wb"))==NULL){
    printf("Error opening the file %s to write\n",namen);
    exit(1);
  }
  printf("Writting the maps to %s ",namen);
  sprintf(namen,"%s.%d.%d.%d.%d.%d.%d.rayMap",dimage->output,(int)dimage->voxCentre[0],\
       (int)dimage->voxCentre[1],(int)dimage->voxCentre[2],(int)dimage->voxScene[0],(int)dimage->voxScene[1],(int)dimage->voxScene[2]);
  if((rayMap=fopen(namen,"wb"))==NULL){
    printf("Error opening the file %s to write\n",namen);
    exit(1);
  }
  printf("\nand %s ...\n",namen);

  /*the bit from the voxels*/
  /*the contribution numbers, the map of the map file*/
  if(!(contN=(int *)calloc(dimage->totalVoxels,sizeof(int)))){
    printf("error in element area array allocation 1.1\n");
    exit(1);
  }
  mapLength=0;
  for(i=0;i<dimage->totalVoxels;i++){
    contN[i]=dimage->voxel[i].contN;
    if(dimage->voxel[i].contN>0)mapLength+=(long int)(3*dimage->voxel[i].contN);
  }
  if(dimage->byteord)contN=intSwap(contN,(uint64_t)dimage->totalVoxels);
  if(fseek(rayMap,(long)((2*dimage->scanN+mapHEAD)*sizeof(int)+(3*dimage->scanN+mapFLOA)*sizeof(float)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(contN[0]),sizeof(int),(dimage->totalVoxels),rayMap)!=(dimage->totalVoxels)){
    printf("voxel binary header not written to %s\n",namen);
    exit(1);
  }
  TIDY(contN);

  /*the actual ray map*/
  if(!(map=(int *)calloc(mapLength,sizeof(int)))){
    printf("Trevor's error in element area array allocation 1.\n");
    exit(1);
  }
  mapLength=0;
  for(i=0;i<dimage->totalVoxels;i++){
    for(j=0;j<dimage->voxel[i].contN;j++){
      for(k=0;k<3;k++){
        map[mapLength]=dimage->voxel[i].rayMap[3*j+k];
        mapLength++;
      }
    }
  }
  if(dimage->byteord)map=intSwap(map,(uint64_t)mapLength);
  if(fseek(rayMap,(long)((mapHEAD+2*dimage->scanN)*sizeof(int)+(mapFLOA+3*dimage->scanN)*sizeof(float)+\
     dimage->totalVoxels*sizeof(int)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(map[0]),sizeof(int),(mapLength),rayMap)!=(mapLength)){
    printf("voxel binary rayMap not written to %s\n",namen);
    exit(1);
  }
  TIDY(map);

  /* voxMap   the map from the rays themselves THIS IS THE AWKWARD PART*/
  mapLength=0;
  if(!(contNr=(int **)calloc(dimage->scanN,sizeof(int *)))){
    printf("Hugh's error in element area array allocation 1.\n");
    exit(1);
  }
  for(i=0;i<dimage->scanN;i++){
    if(!(contNr[i]=(int *)calloc(ratPoint[i].NAscans*ratPoint[i].NZscans*ratPoint[i].bins,sizeof(int)))){
      printf("Derek's error in element area array allocation 1.\n");
      exit(1);
    }
    for(j=0;j<ratPoint[i].NAscans*ratPoint[i].NZscans;j++){
      for(k=0;k<ratPoint[i].bins;k++){
        contNr[i][j*ratPoint[i].bins+k]=ratPoint[i].ratRes[j].voxN[k];
        if(ratPoint[i].ratRes[j].voxN[k]>0)mapLength+=ratPoint[i].ratRes[j].voxN[k];
      }
    }
  }
  beamStep=0;
  for(i=0;i<dimage->scanN;i++){
    if(fseek(voxMap,(long)((mapHEAD+2*dimage->scanN)*sizeof(int)+(mapFLOA+3*dimage->scanN)*sizeof(float)+\
             (int)beamStep*sizeof(int)),SEEK_SET)){
      printf("Error seeking through the binary voxel file\n");
      exit(1);
    }
    beamStep+=(long int)(ratPoint[i].NAscans*ratPoint[i].NZscans*ratPoint[i].bins);
    if(dimage->byteord)contNr[i]=intSwap(contNr[i],(uint64_t)ratPoint[i].NAscans*(uint64_t)ratPoint[i].NZscans*(uint64_t)ratPoint[i].bins);
    if(fwrite(&(contNr[i][0]),sizeof(int),(ratPoint[i].NAscans*ratPoint[i].NZscans*ratPoint[i].bins),voxMap)!=\
                                          (ratPoint[i].NAscans*ratPoint[i].NZscans*ratPoint[i].bins)){
      printf("voxel binary rayMap not written to %s\n",namen);
      exit(1);
    }
    TIDY(contNr[i]);
  }
  TIDY(contNr);

  /*and then the actual map itself*/
  if(!(map=(int *)calloc(mapLength,sizeof(int)))){
    printf("Bandana's error in element area array allocation 1.\n");
    exit(1);
  }
  mapLength=0;
  for(i=0;i<dimage->scanN;i++){
    for(j=0;j<ratPoint[i].NZscans*ratPoint[i].NAscans;j++){
      for(k=0;k<ratPoint[i].bins;k++){
        for(m=0;m<ratPoint[i].ratRes[j].voxN[k];m++){
          map[mapLength]=ratPoint[i].ratRes[j].voxMap[k][m];
          mapLength++;
        }
      }
    }
  }
  if(dimage->byteord)map=intSwap(map,(uint64_t)mapLength);
  if(fseek(voxMap,(long)((mapHEAD+2*dimage->scanN)*sizeof(int)+(mapFLOA+3*dimage->scanN)*sizeof(float)+\
               (int)beamStep*sizeof(int)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(map[0]),sizeof(int),(mapLength),voxMap)!=(mapLength)){
    printf("voxel binary rayMap not written, size %d\n",mapLength);
    exit(1);
  }

  TIDY(map)else{printf("What is wrong?\n");exit(9);}



  /*write out all of the header information in one go*/

  /*float voxel information*/
  header2=falloc(mapFLOA,"map float header",0);
  header2[0]=dimage->voxRes;
  for(i=0;i<3;i++)header2[i+1]=dimage->voxCentre[i];
  if(dimage->byteord)header2=floSwap(header2,(uint64_t)mapFLOA);

  if(fseek(rayMap,(long)(mapHEAD*sizeof(int)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fseek(voxMap,(long)(mapHEAD*sizeof(int)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(header2[0]),sizeof(float),mapFLOA,rayMap)!=mapFLOA){
    printf("voxel float header not written to rayMap\n");
    exit(1);
  }
  if(fwrite(&(header2[0]),sizeof(float),mapFLOA,voxMap)!=mapFLOA){
    printf("voxel float header not written to rayMap\n");
    exit(1);
  }
  TIDY(header2);

  /*scan integer header*/
  header1=ialloc(2*dimage->scanN,"scan integer header",0);
  for(i=0;i<dimage->scanN;i++){
    header1[2*i]=ratPoint[i].NZscans;
    header1[2*i+1]=ratPoint[i].NAscans;
  }
  if(dimage->byteord)header1=intSwap(header1,2*(uint64_t)dimage->scanN);
  if(fseek(rayMap,(long)(mapHEAD*sizeof(int)+mapFLOA*sizeof(float)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fseek(voxMap,(long)(mapHEAD*sizeof(int)+mapFLOA*sizeof(float)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(header1[0]),sizeof(int),2*dimage->scanN,rayMap)!=2*dimage->scanN){
    printf("voxel binary header not written to rayMap\n");
    exit(1);
  }
  if(fwrite(&(header1[0]),sizeof(int),2*dimage->scanN,voxMap)!=2*dimage->scanN){
    printf("voxel binary header not written to voxMap\n");
    exit(1);
  }
  TIDY(header1);

  /*scan float data*/
  header2=falloc(3*dimage->scanN,"scan float map header",0);
  for(i=0;i<dimage->scanN;i++){
    for(j=0;j<3;j++)header2[3*i+j]=(float)ratPoint[i].from[j];
  }
  if(dimage->byteord)header2=floSwap(header2,3*(uint64_t)dimage->scanN);
  if(fseek(rayMap,(long)((mapHEAD+2*dimage->scanN)*sizeof(int)+mapFLOA*sizeof(float)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fseek(voxMap,(long)((mapHEAD+2*dimage->scanN)*sizeof(int)+mapFLOA*sizeof(float)),SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(header2[0]),sizeof(float),3*dimage->scanN,rayMap)!=3*dimage->scanN){
    printf("voxel binary header not written to rayMap\n");
    exit(1);
  }
  if(fwrite(&(header2[0]),sizeof(float),3*dimage->scanN,voxMap)!=3*dimage->scanN){
    printf("voxel binary header not written to voxMap\n");
    exit(1);
  }
  TIDY(header2);

  /*finally the format label*/
  if(!(header1=(int *)calloc(mapHEAD,sizeof(int)))){
    printf("AWUGGA error in element area array allocation 1.\n");
    exit(1);
  }
  header1[0]=2;
  header1[1]=dimage->totalVoxels;
  for(i=0;i<3;i++){
    header1[2+i]=dimage->voxNumb[i];
  }
  header1[6]=dimage->scanN;
  if(dimage->byteord)header1=intSwap(header1,(uint64_t)mapHEAD);
  if(fseek(rayMap,(long)0,SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fseek(voxMap,(long)0,SEEK_SET)){
    printf("Error seeking through the binary voxel file\n");
    exit(1);
  }
  if(fwrite(&(header1[0]),sizeof(int),mapHEAD,rayMap)!=mapHEAD){
    printf("voxel binary header not written to %s\n",namen);
    exit(1);
  }
  if(fwrite(&(header1[0]),sizeof(int),mapHEAD,voxMap)!=mapHEAD){
    printf("voxel binary header not written to %s\n",namen);
    exit(1);
  }
  TIDY(header1);


  TIDY(rayMap);
  TIDY(voxMap);

  return(dimage);
}/*writeBinaryMaps*/ /*write binary maps*/

/*#################################################################################*/
/* Read the binary map files*/

RatImage *readBinaryMaps(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0,k=0,file=0;
  int *header1=NULL,*contN=NULL,*map=NULL;
  long int mapLength=0,beamStep=0;
  float *header2=NULL;
  FILE *input=NULL;
  char namen[200];

  /*read the rayMap and voxMap files to make sure the headers are correct*/

  /*check both the headers*/
  for(file=0;file<2;file++){

    if(file){
      sprintf(namen,"%s.%d.%d.%d.%d.%d.%d.voxMap",dimage->output,(int)dimage->voxCentre[0],\
       (int)dimage->voxCentre[1],(int)dimage->voxCentre[2],(int)dimage->voxScene[0],(int)dimage->voxScene[1],(int)dimage->voxScene[2]);    
    }else{
      sprintf(namen,"%s.%d.%d.%d.%d.%d.%d.rayMap",dimage->output,(int)dimage->voxCentre[0],\
       (int)dimage->voxCentre[1],(int)dimage->voxCentre[2],(int)dimage->voxScene[0],(int)dimage->voxScene[1],(int)dimage->voxScene[2]);
    }

    if((input=fopen(namen,"rb"))==NULL){
      printf("There is no map file %s. Have to make one.\n",namen);
      dimage->createMap=1;
      return(dimage);
    }
    printf("Reading the data from %s\n",namen); 

    header1=ialloc(mapHEAD,"map file header",1);
    if(fseek(input,(long)0,SEEK_SET)){
      printf("Error seeking through the binary voxel file\n");
      exit(1);
    }
    if((fread(&(header1[0]),sizeof(int),mapHEAD,input))!=mapHEAD){
      printf("fread error 0\n");
      exit(1);
    }
    if(dimage->byteord)header1=intSwap(header1,(uint64_t)mapHEAD);

    if(header1[0]!=2){
      printf("This has the wrong header %d instead of %d\n",header1[0],1);
    }
    if(dimage->totalVoxels!=header1[1]){
      fprintf(stderr,"Total voxel number mismatch %d %d\n",dimage->totalVoxels,header1[1]);
      exit(1);
    }
    for(i=0;i<3;i++){
      if(dimage->voxNumb[i]!=header1[2+i]){
        fprintf(stderr,"The voxel number %d disagrees %d %d\n",i,dimage->voxNumb[i],header1[3+i]);
        exit(1);
      }
    }
    if(dimage->scanN!=header1[6]){
      fprintf(stderr,"Scan number mismatch %d %d\n",dimage->scanN,header1[9]);
      exit(1);
    }
    TIDY(header1);

    header2=falloc(mapFLOA,"map floating header",1);
    if(fseek(input,(long)(mapHEAD*sizeof(int)),SEEK_SET)){
      fprintf(stderr,"Error seeking through the binary voxel file\n");
      exit(1);
    }
    if((fread(&(header2[0]),sizeof(float),(mapFLOA),input))!=(mapFLOA)){
      fprintf(stderr,"fread error 1\n");
      exit(1);
    }
    if(dimage->byteord)header2=floSwap(header2,(uint64_t)mapFLOA);
    if(header2[0]!=dimage->voxRes){printf("voxel resolution mismatch %f %f\n",header2[0],dimage->voxRes);exit(1);}
    for(i=0;i<3;i++)if(header2[i+1]!=dimage->voxCentre[i]){printf("Voxel centre mismatch, %d %f %f\n",i,header2[i+1],dimage->voxCentre[i]);exit(1);}
    TIDY(header2);

    header1=ialloc(2*dimage->scanN,"scan map integer headrer",0);
    if(fseek(input,(long)(mapHEAD*sizeof(int)+mapFLOA*sizeof(float)),SEEK_SET)){
      fprintf(stderr,"Error seeking through the binary voxel file\n");
      exit(1);
    }
    if((fread(&(header1[0]),sizeof(int),2*dimage->scanN,input))!=2*dimage->scanN){
      fprintf(stderr,"fread error 0\n");
      exit(1);
    }
    if(dimage->byteord)header1=intSwap(header1,2*(uint64_t)dimage->scanN);
    for(i=0;i<dimage->scanN;i++){
      if(header1[2*i]!=ratPoint[i].NZscans){printf("Zenith scan mismatch %d %d %d\n",i,header1[2*i],ratPoint[i].NZscans);exit(1);}
      if(header1[2*i+1]!=ratPoint[i].NAscans){printf("Azimuth scan mismatch %d %d %d\n",i,header1[2*i+1],ratPoint[i].NAscans);exit(1);}
    }
    TIDY(header1);

    header2=falloc(3*dimage->scanN,"scan position map header",0);
    if(fseek(input,(long)((mapHEAD+2*dimage->scanN)*sizeof(int)+mapFLOA*sizeof(float)),SEEK_SET)){
      fprintf(stderr,"Error seeking through the binary voxel file\n");
      exit(1);
    }
    if((fread(&(header2[0]),sizeof(float),3*dimage->scanN,input))!=3*dimage->scanN){
      fprintf(stderr,"fread error 0\n");
      exit(1);
    }
    if(dimage->byteord)header2=floSwap(header2,3*(uint64_t)dimage->scanN);
    for(i=0;i<dimage->scanN;i++){
      for(j=0;j<3;j++){
        if(ratPoint[i].from[j]!=header2[3*i+j]){
          fprintf(stderr,"Scan coordinate mismatch for scan %d %d %f %f\n",i,j,ratPoint[i].from[j],header2[3*i+j]);
          exit(1);
        }
      }
    }
    TIDY(header2);

    if(input){
      fclose(input);
      input=NULL;
    }
  }/*end of header checking for the two files*/


  /*now read the map of which rays to put into the voxels*/

  sprintf(namen,"%s.%d.%d.%d.%d.%d.%d.rayMap",dimage->output,(int)dimage->voxCentre[0],\
    (int)dimage->voxCentre[1],(int)dimage->voxCentre[2],(int)dimage->voxScene[0],(int)dimage->voxScene[1],(int)dimage->voxScene[2]);
  if((input=fopen(namen,"rb"))==NULL){
    fprintf(stderr,"Having trouble opening the map file %s, which is odd because it worked last time\n",namen);
    exit(1);
  }

  if(!(contN=(int *)calloc(3*dimage->totalVoxels,sizeof(int)))){
    fprintf(stderr,"error in contribution array allocation 1.\n");
    exit(1);
  }
  if(fseek(input,(long)((mapHEAD+2*dimage->scanN)*sizeof(int)+(mapFLOA+3*dimage->scanN)*sizeof(float)),SEEK_SET)){
    fprintf(stderr,"Error seeking through the binary voxel file\n");
    exit(1);
  }
  if((fread(&(contN[0]),sizeof(int),dimage->totalVoxels,input))!=dimage->totalVoxels){
    fprintf(stderr,"fread error 3\n");
    exit(1);
  }
  if(dimage->byteord)contN=intSwap(contN,(uint64_t)dimage->totalVoxels);
  for(i=0;i<dimage->totalVoxels;i++){
    dimage->voxel[i].contN=contN[i];
    if(contN[i]>0)mapLength+=contN[i];
  }
  TIDY(contN);

  map=ialloc(3*mapLength,"map transfer",0);
  if(fseek(input,(long)((mapHEAD+2*dimage->scanN)*sizeof(int)+(mapFLOA+3*dimage->scanN)*sizeof(float)+\
                  dimage->totalVoxels*sizeof(int)),SEEK_SET)){
    fprintf(stderr,"Error seeking through the binary voxel file\n");
    exit(1);
  }
  if((fread(&(map[0]),sizeof(int),(3*mapLength),input))!=(3*mapLength)){
    fprintf(stderr,"fread error 4\n");
    exit(1);
  }
  if(dimage->byteord)map=intSwap(map,3*(uint64_t)mapLength);

  mapLength=0;
  for(i=0;i<dimage->totalVoxels;i++){
    if(dimage->voxel[i].contN>0){
      if(dimage->voxel[i].rayMap!=NULL){printf("Oh dear\n");exit(2);}
      if(!(dimage->voxel[i].rayMap=(int *)calloc(3*dimage->voxel[i].contN,sizeof(int)))){
        fprintf(stderr,"error in rayMap array allocation 1.\n");
        exit(1);
      }
      for(j=0;j<dimage->voxel[i].contN;j++){
        for(k=0;k<3;k++){
          dimage->voxel[i].rayMap[j*3+k]=map[mapLength];
          mapLength++;
        }   
      }
    }
  }

  TIDY(map);

  if(input){
    fclose(input);
    input=NULL;
  }
  /*the ray to voxel map has been read*/

  sprintf(namen,"%s.%d.%d.%d.%d.%d.%d.voxMap",dimage->output,(int)dimage->voxCentre[0],\
    (int)dimage->voxCentre[1],(int)dimage->voxCentre[2],(int)dimage->voxScene[0],(int)dimage->voxScene[1],(int)dimage->voxScene[2]);  
  if((input=fopen(namen,"rb"))==NULL){
    fprintf(stderr,"Having trouble opening the map file %s, which is odd because it worked last time\n",namen);
    exit(1);
  }

  /*read the contribution numbers*/
  mapLength=0;
  for(i=0;i<dimage->scanN;i++){
    if(!(contN=(int *)calloc(ratPoint[i].NAscans*ratPoint[i].NZscans*ratPoint[i].bins,sizeof(int)))){
      fprintf(stderr,"error in contribution array allocation 1.\n");
      exit(1);
    }
    if(fseek(input,(long)((mapHEAD+2*dimage->scanN)*sizeof(int)+(mapFLOA+3*dimage->scanN)*sizeof(float)+\
                    mapLength*sizeof(int)),SEEK_SET)){
      fprintf(stderr,"Error seeking through the binary voxel file %s number 25.\n",namen);
      exit(1);
    }
    mapLength+=ratPoint[i].NAscans*ratPoint[i].NZscans*ratPoint[i].bins;
    if((fread(&(contN[0]),sizeof(int),(ratPoint[i].NAscans*ratPoint[i].NZscans*ratPoint[i].bins),input))!=\
                                      (ratPoint[i].NAscans*ratPoint[i].NZscans*ratPoint[i].bins)){
      fprintf(stderr,"fread error 'eneath the mulberry shop\n");
      exit(1);
    } 
    if(dimage->byteord)contN=intSwap(contN,(uint64_t)ratPoint[i].NAscans*(uint64_t)ratPoint[i].NZscans*(uint64_t)ratPoint[i].bins);
    for(j=0;j<ratPoint[i].NZscans*ratPoint[i].NAscans;j++){
      if(!(ratPoint[i].ratRes[j].voxN=(int *)calloc(ratPoint[i].bins,sizeof(int)))){
        fprintf(stderr,"error in voxel contribution array allocation 1232.\n");
        exit(1);
      }
      for(k=0;k<ratPoint[i].bins;k++){
        ratPoint[i].ratRes[j].voxN[k]=contN[j*ratPoint[i].bins+k];
      }
    }
    TIDY(contN);
  }/*end of contribution numbers*/

  beamStep=0;
  for(i=0;i<dimage->scanN;i++){
    beamStep+=(long int)(ratPoint[i].NAscans*ratPoint[i].NZscans*ratPoint[i].bins);
  }

  mapLength=0;
  /*and onto the map itself*/
  for(i=0;i<dimage->scanN;i++){
    for(j=0;j<ratPoint[i].NAscans*ratPoint[i].NZscans;j++){
      if(!(ratPoint[i].ratRes[j].voxMap=(int **)calloc(ratPoint[i].bins,sizeof(int *)))){
        fprintf(stderr,"error in voxel map array allocation 2.\n");
        exit(1);
      }
      for(k=0;k<ratPoint[i].bins;k++){
        if(ratPoint[i].ratRes[j].voxN[k]>0){      /*don't do if voxN is negative*/
          if(!(ratPoint[i].ratRes[j].voxMap[k]=(int *)calloc(ratPoint[i].ratRes[j].voxN[k],sizeof(int)))){
            fprintf(stderr,"error in voxel map allocation for voxel %d attempting to allocate %d.\n",k,ratPoint[i].ratRes[j].voxN[k]);
            exit(1);
          }
          if(fseek(input,(long)((mapHEAD+2*dimage->scanN)*sizeof(int)+(mapFLOA+3*dimage->scanN)*sizeof(float)+\
                                          (int)beamStep*sizeof(int)+(int)mapLength*sizeof(int)),SEEK_SET)){
            fprintf(stderr,"Error seeking through the binary voxel file %s number 25.\n",namen);
            exit(1);
          }
          if((fread(&(ratPoint[i].ratRes[j].voxMap[k][0]),sizeof(int),(ratPoint[i].ratRes[j].voxN[k]),input))!=\
                                                                      (ratPoint[i].ratRes[j].voxN[k])){
            fprintf(stderr,"fread error near the bank of Scotland trying to read %d ints\n",ratPoint[i].ratRes[j].voxN[k]);
            exit(1);
          }
          if(dimage->byteord)ratPoint[i].ratRes[j].voxMap[k]=intSwap(ratPoint[i].ratRes[j].voxMap[k],(uint64_t)ratPoint[i].ratRes[j].voxN[k]);
          mapLength+=(long int)ratPoint[i].ratRes[j].voxN[k];
        } /* "contribution counter is positive" test*/
      }   /*bin step*/
    }     /*beam step*/
  }       /*scan step*/

  if(input){
    fclose(input);
    input=NULL;
  }
  /*calculate voxel locations*/
  for(i=0;i<dimage->voxNumb[0];i++){     /*step over x*/
    for(j=0;j<dimage->voxNumb[1];j++){   /*step over y*/
      for(k=0;k<dimage->voxNumb[2];k++){ /*step over z*/
        file=i*dimage->voxNumb[1]*dimage->voxNumb[2]+j*dimage->voxNumb[2]+k;
        dimage->voxel[file].pos[0]=dimage->voxCentre[0]+((float)i-(float)dimage->voxNumb[0]/2.0)*dimage->voxRes;
        dimage->voxel[file].pos[1]=dimage->voxCentre[1]+((float)j-(float)dimage->voxNumb[1]/2.0)*dimage->voxRes;
        dimage->voxel[file].pos[2]=dimage->voxCentre[2]+((float)k-(float)dimage->voxNumb[2]/2.0)*dimage->voxRes;
      }
    }
  }

  fprintf(stdout,"The voxel maps have been read\n");
  dimage->createMap=0;
  return(dimage);
} /*readBinaryMaps*/

/*#####################################################################*/
/*calculate psi for a single voxel*/

RatImage *calculatePsi(RatImage *dimage,RatControl *ratPoint,int Vplace)
{
  int i=0,j=0;
  int numb=0,Lplace=0,bin=0;
  float psi=0,Tpsi=0,Tgap=0,gap=0;
  int contN=0;
  float linearGap(RatControl *,RatImage *,int,int,int);

  /*##calculate psi##*/
  for(i=0;i<dimage->voxel[Vplace].contN;i++){
    numb=dimage->voxel[Vplace].rayMap[3*i];
    if(dimage->nBands==2){  /*we need two bands to solve this*/
      Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
      bin=dimage->voxel[Vplace].rayMap[3*i+2];
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
      if(ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+dimage->band[numb][1])*(ratPoint[numb].n+1)]>0&&\
          ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+dimage->band[numb][0])*(ratPoint[numb].n+1)]>0){
        if(!dimage->linearVoxels){
          gap=1.0;
          for(j=0;j<bin;j++){
            gap=guessGap(ratPoint,dimage,gap,numb,Lplace,j);
            if(gap<=0)break;
          }
        }else gap=linearGap(ratPoint,dimage,numb,Lplace,bin);
        if(gap>THRESHOLD){
          psi=(ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+dimage->band[numb][0])*(ratPoint[numb].n+1)]*\
               ratPoint[numb].albedo[3]-\
               ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+dimage->band[numb][1])*(ratPoint[numb].n+1)]*\
               ratPoint[numb].albedo[1])/\
              (ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+dimage->band[numb][1])*(ratPoint[numb].n+1)]*\
              (ratPoint[numb].albedo[0]-ratPoint[numb].albedo[1])-\
               ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+dimage->band[numb][0])*(ratPoint[numb].n+1)]*\
              (ratPoint[numb].albedo[2]-ratPoint[numb].albedo[3]));
          if(psi<0)psi*=-1;
          if(psi>1)psi=1;
          Tpsi+=psi*gap;
          Tgap+=gap;
          contN++;
        }
      }
      /*if we don't need this beam again clear the array*/
      if(i!=dimage->voxel[Vplace].contN-1){
        if(numb!=dimage->voxel[Vplace].rayMap[3*(i+1)]||Lplace!=dimage->voxel[Vplace].rayMap[3*(i+1)+1]){
          ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
        }
      }else if(i==dimage->voxel[Vplace].contN-1){
        ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
      }
    }
  }/*end of psi loop*/
  if(Tgap>THRESHOLD&&contN>0){
    dimage->voxel[Vplace].psi=Tpsi/Tgap;
    if(dimage->voxel[Vplace].psi<0)dimage->voxel[Vplace].psi*=-1;
    if(dimage->voxel[Vplace].psi>1)dimage->voxel[Vplace].psi=1;
    /*else if(dimage->voxel[Vplace].psi<0.0001)dimage->voxel[Vplace].psi=0.0001;*/
  }else{        /*then we can't tell anything about psi*/
    dimage->voxel[Vplace].psi=0.5;
  } /*###end of psi calculating loop####*/

  return(dimage);
} /*calculatePsi*/

/*#####################################################################*/
/*calculate area within the iterations*/

RatImage *calculateArea(RatImage *dimage,RatControl *ratPoint,int Vplace)
{
  int numb=0,Lplace=0,bin=0;
  int i=0,j=0,band=0,mat=0,n=0;
  double rflux=0;                /*radiant flux for efficiency*/
  float *Aetot=NULL,*LAD=NULL,*thetaHat=NULL;
  float psi=0,gap=0,Tgap=0;
  float contN=0;
  char brEak=0,blocked=0;

  Aetot=falloc(dimage->matN,"total element area",0);
  LAD=falloc(dimage->matN,"leaf angle distribution",4);
  thetaHat=falloc(dimage->matN,"theta hat",23);

  /*once we have LAD, calculate Ae for all beams*/
  Aetot[0]=0;
  Aetot[1]=0;

  for(i=0;i<dimage->voxel[Vplace].contN;i++){
    numb=dimage->voxel[Vplace].rayMap[3*i];
    Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
    bin=dimage->voxel[Vplace].rayMap[3*i+2];
    ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
    psi=meanPsi(ratPoint,dimage,numb,Lplace,bin);
    LAD=meanLAD(ratPoint,dimage,numb,Lplace,bin,LAD);
    thetaHat=meanThetaHat(ratPoint,dimage,LAD,thetaHat,numb,Lplace);
    gap=1;
    for(j=0;j<bin;j++){
      gap=guessGap(ratPoint,dimage,gap,numb,Lplace,j);
      if(gap<=0)break;
    }
    if(gap>THRESHOLD){
      for(n=0;n<dimage->nBands;n++){
        band=dimage->band[numb][n];
        rflux=ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]*(double)dimage->voxel[Vplace].fraction[i];
        if(psi>THRESHOLD&&psi<1-THRESHOLD){          /*a mix of materials*/
          Aetot[0]+=(float)(rflux/(cos(thetaHat[0])*cos(thetaHat[0])*cos(thetaHat[0])*\
                    (ratPoint[numb].albedo[2*band]+ratPoint[numb].albedo[2*band+1]*(1-psi)/psi)));
          Aetot[1]+=(float)(rflux/(cos(thetaHat[1])*cos(thetaHat[1])*cos(thetaHat[1])*\
                    (ratPoint[numb].albedo[2*band+1]+ratPoint[numb].albedo[2*band]*psi/(1-psi))));
        }else if(psi<=THRESHOLD){   /*pure wood*/
          /*Aetot[0] stays the same*/
          Aetot[1]+=(float)(rflux/(ratPoint[numb].albedo[2*band+1]*cos(thetaHat[1])*cos(thetaHat[1])*cos(thetaHat[1])));
        }else if(psi>=1-THRESHOLD){
          Aetot[0]+=(float)(rflux/(ratPoint[numb].albedo[2*band]*cos(thetaHat[0])*cos(thetaHat[0])*cos(thetaHat[0])));
          /*Aetot[1]+=0 stays the same*/
        }
      }
      contN+=dimage->voxel[Vplace].fraction[i];
      Tgap+=gap;
    }
  }/*end of contribution loop for areas*/

  if(Tgap>THRESHOLD&&contN>0){
    for(mat=0;mat<dimage->matN;mat++){
      dimage->voxel[Vplace].Ae[mat]=Aetot[mat]/(Tgap*contN);
    }
  }else{ /*see if the ray is prematurley blocked*/
    blocked=0;
    brEak=0;                 /*I don't think that this indicator is being used elsewhere, but beware*/
    for(i=0;i<dimage->voxel[Vplace].contN;i++){
      numb=dimage->voxel[Vplace].rayMap[3*i];
      Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
      bin=dimage->voxel[Vplace].rayMap[3*i+2];
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
      for(j=bin;j<ratPoint[numb].bins;j++){
        for(n=0;n<dimage->nBands;n++){
          band=dimage->band[numb][n];
          if(ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>0){
            blocked=1;
            brEak=1;
            break;
          }
        }
        if(brEak)break;
      }
      if(brEak)break;
    }
    if(blocked==0){       /*then it really is blocked*/
      for(mat=0;mat<dimage->matN;mat++){
        dimage->voxel[Vplace].Ae[mat]=-2;
      }
    }else if(blocked==1){ /*then it is prematurley blocked*/
      for(mat=0;mat<dimage->matN;mat++){
        dimage->voxel[Vplace].Ae[mat]=-3.0;
      }
    }
  }

  TIDY(Aetot);
  TIDY(LAD);
  TIDY(thetaHat);

  return(dimage);
}/*#calculateArea#*/

/*#####################################################################*/
/*see if we have more than one zenith in each voxel*/

RatImage *countZeniths(RatImage *dimage,RatControl *ratPoint)
{
  int Vplace=0,numb=0,Lplace=0,bin=0,band=0;
  int i=0,j=0,n=0;
  char brEak=0,contine=0;
  double angThresh=0;

  angThresh=3*M_PI/180;    /*to say two beams are far enough apart to allow LAD calculation*/
  angThresh*=angThresh;    /*the equatuion needs this squared, do now to save time*/

  for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
    dimage->voxel[Vplace].adjust=0;
    brEak=0;                           /*here "adjust" means we have multiple scans contributing*/
    for(i=0;i<dimage->voxel[Vplace].contN;i++){
      numb=dimage->voxel[Vplace].rayMap[3*i];
      Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
      bin=dimage->voxel[Vplace].rayMap[3*i+2];
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
      contine=0;
      for(n=0;n<dimage->nBands;n++){
        band=dimage->band[numb][n];
        if(ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>0)contine=1;
      }
      if(contine){
        for(j=i;j<dimage->voxel[Vplace].contN;j++){
          if(dimage->voxel[Vplace].rayMap[3*j+1]!=Lplace){ /*if this is a different beam*/
            numb=dimage->voxel[Vplace].rayMap[3*j];        /*here we change numb to j, not i, BEWARE*/
            Lplace=dimage->voxel[Vplace].rayMap[3*j+1];
            bin=dimage->voxel[Vplace].rayMap[3*j+2];
            ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
            if(ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>0){
              /*see if the zeniths are sufficiently different*/
              if((ratPoint[numb].Etheta[(int)(Lplace/ratPoint[numb].NAscans)]-\
                 ratPoint[numb].Etheta[(int)(dimage->voxel[Vplace].rayMap[3*i+1]/\
                 ratPoint[dimage->voxel[Vplace].rayMap[3*i]].NAscans)])*\
                 (ratPoint[numb].Etheta[(int)(Lplace/ratPoint[numb].NAscans)]-\
                 ratPoint[numb].Etheta[(int)(dimage->voxel[Vplace].rayMap[3*i+1]/\
                 ratPoint[dimage->voxel[Vplace].rayMap[3*i]].NAscans)])>\
                 angThresh){
                dimage->voxel[Vplace].adjust=1;
                brEak=1;
                break;
              }
            }
            ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
            /*dimage=clearAngles(dimage);*/
          }
        }
      }
      numb=dimage->voxel[Vplace].rayMap[3*i];   /*now we have finished with j go back to i*/
      Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
      if(i!=dimage->voxel[Vplace].contN-1){
        if(numb!=dimage->voxel[Vplace].rayMap[3*(i+1)]||Lplace!=dimage->voxel[Vplace].rayMap[3*(i+1)+1]){
          ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
        }
      }else if(i==dimage->voxel[Vplace].contN-1){
        ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
      }
      if(brEak)break;
    }
  }  /*end of multiple zenith calculation loop*/
  return(dimage);
}

/*################################################################################*/
/*Calculate the initial estimates of psi*/

RatImage *initialPsi(RatImage *dimage,RatControl *ratPoint)
{
  int i=0;
  int Vplace=0,numb=0,Lplace=0,bin=0;
  float contN=0;
  float psi=0;

  /*calculate psi, assuming leaves and bark have the same distribution*/
  fprintf(stdout,"Calculating leaf fractions...\n");
  for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
    dimage->voxel[Vplace].psi=0.0;
    contN=0.0;
    for(i=0;i<dimage->voxel[Vplace].contN;i++){
      numb=dimage->voxel[Vplace].rayMap[3*i];
      if(dimage->nBands==2){
        Lplace=dimage->voxel[Vplace].rayMap[3*i+1];
        bin=dimage->voxel[Vplace].rayMap[3*i+2]*ratPoint[numb].nBands*(ratPoint[numb].n+1);
        ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
        if(ratPoint[numb].ratRes[Lplace].refl[bin]>0||ratPoint[numb].ratRes[Lplace].refl[bin+ratPoint[numb].n+1]>0){
          psi=(ratPoint[numb].ratRes[Lplace].refl[bin+(ratPoint[numb].n+1)*dimage->band[numb][0]]*ratPoint[numb].albedo[3]-\
               ratPoint[numb].ratRes[Lplace].refl[bin+(ratPoint[numb].n+1)*dimage->band[numb][1]]*ratPoint[numb].albedo[1])/\
               (ratPoint[numb].ratRes[Lplace].refl[bin+(ratPoint[numb].n+1)*dimage->band[numb][1]]*\
               (ratPoint[numb].albedo[0]-ratPoint[numb].albedo[1])-\
               ratPoint[numb].ratRes[Lplace].refl[bin+(ratPoint[numb].n+1)*dimage->band[numb][0]]*\
               (ratPoint[numb].albedo[2]-ratPoint[numb].albedo[3]))*dimage->voxel[Vplace].fraction[i];
          if(psi<0)psi*=-1;
          if(psi>0.999)psi=0.999;
          dimage->voxel[Vplace].psi+=psi;
          contN+=dimage->voxel[Vplace].fraction[i];
        }
        ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
        /*dimage=clearAngles(dimage);  */
      }
    }
    if(contN>0.0){
      dimage->voxel[Vplace].psi/=contN;
      if(dimage->voxel[Vplace].psi<0)dimage->voxel[Vplace].psi*=-1;
      if(dimage->voxel[Vplace].psi>0.999)dimage->voxel[Vplace].psi=0.999;
      else if(dimage->voxel[Vplace].psi<0.0001)dimage->voxel[Vplace].psi=0.0001;  /*to prevent division by zero later*/
    }else{
      dimage->voxel[Vplace].psi=0.5;
    }
  }
  return(dimage);
}/*initialPsi*/


/*#################################################################################*/
/*resize gaps to prevent premature blocking using the linear model*/

void resizeLinearGaps(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0,n=0;
  int numb=0,Lplace=0,bin=0,band=0,place=0;
  int Vplace=0,nVox=0;
  int *done=NULL;
  int iter=0;
  float gap=0,factor=0;
  char anyBlocked=0,light=0,new=0;
  int *markInt(int,int *,int);


  factor=2.0;  /*factor to reduce surface areas by*/
  iter=0;
  band=1;
  do{   /*iterate unitl none are prematurely blocked*/
    for(numb=0;numb<dimage->scanN;numb++){
fprintf(stdout,"Resizing %d for the %d time\n",numb,iter);
      anyBlocked=0;
      for(Lplace=0;Lplace<ratPoint[numb].NZscans*ratPoint[numb].NAscans;Lplace++){
        ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
        gap=1.0;
        for(bin=0;bin<ratPoint[numb].bins;bin++){
          if(ratPoint[numb].ratRes[Lplace].voxN[bin]<0)break;   /*if we leave the scene, break*/
          gap=linearGap(ratPoint,dimage,numb,Lplace,bin);  /*it would be more efficient to do a summnastive version of this*/
          if(gap<=THRESHOLD){
            light=0;
            for(bin++;bin<ratPoint[numb].bins;bin++){ /* don't stop this one as it can still indicate premature blocking*/
              place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
              if(ratPoint[numb].ratRes[Lplace].refl[place]>0.0){
                light=1;
                anyBlocked=1;
                break;
              }
            }/*light detection loop*/
            if(light){  /*then it has been prematurley blocked, reduce all surface areas*/
              for(j=0;j<bin;j++){ /*all voxels within bins up to that point*/
                place=findReflIndex(dimage,ratPoint,numb,j,band,0);
                /*step through all intersecting voxels and reduce area*/
                for(n=0;n<ratPoint[numb].ratRes[Lplace].voxN[j];n++){
                  Vplace=ratPoint[numb].ratRes[Lplace].voxMap[j][n];
                  new=1;
                  for(i=0;i<nVox;i++){
                    if(Vplace==done[i]){
                      new=0;
                      break;
                    }
                  }
                  if(new){
                    dimage->voxel[Vplace].Ae[0]/=factor;
                    dimage->voxel[Vplace].Ae[1]/=factor;
                    done=markInt(nVox,done,Vplace);
                    nVox++;
                  }
                }/*voxel within loop*/
              }
            }/*premature blocking test*/
            break;
          }/*gao closed test*/
        }/*bin loop*/
        ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
      }/*beam loop*/
    }/*scan loop*/
    iter++;  /*keep count to stop it gpoing on for too long*/
  }while(anyBlocked&&(iter<20));


  TIDY(done);
  return;
}/*resizeLinearGaps*/


/*########*/

void resizeLinearGapsWithin(RatImage *dimage,RatControl *ratPoint)
{

  return;
}/*resizeLinearGapsWithin*/


/*#################################################################################*/
/*resize gaps to prevent premature blocking*/

RatImage *resizeGaps(RatImage *dimage,RatControl *ratPoint)
{
  int j=0,n=0,m=0;
  int Vplace=0,numb=0,Lplace=0,bin=0;
  int Bplace=0,iter=0,band=0,mat=0;
  float gap=0;
  double mu=dimage->mu;
  double LADstep=0;
  char light=0,blocked=0,anyBlocked=0;

  /*The gap predictions will be way off. This will have lead to some very wrong attenuation corrections*/
  /*now check all the data for zero gaps, altering the LAD all long the root to compensate*/

  iter=0;
  do{   /*###########gap resizeing iterations#*/
    for(numb=0;numb<dimage->scanN;numb++){
      anyBlocked=0;
      for(Lplace=0;Lplace<ratPoint[numb].NZscans*ratPoint[numb].NAscans;Lplace++){
        /*calculate the gap fraction and check that it is not blocked when it should not be*/
        ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
        if(ratPoint[numb].Etheta[(int)(Lplace/ratPoint[numb].NAscans)]>=mu)LADstep=M_PI/-200;
        else if(ratPoint[numb].Etheta[(int)(Lplace/ratPoint[numb].NAscans)]<mu)LADstep=M_PI/200;
        gap=1;
        for(bin=0;bin<ratPoint[numb].bins;bin++){
          if(ratPoint[numb].ratRes[Lplace].voxN[bin]==-1)break;
          Bplace=bin*ratPoint[numb].nBands*(ratPoint[numb].n+1);
          light=0;
          for(m=0;m<dimage->nBands;m++){
            band=dimage->band[numb][m];
            if(ratPoint[numb].ratRes[Lplace].refl[Bplace+band*(ratPoint[numb].n+1)]>0){
              light=1;
              break;
            }
          }
          if(light){  /*no point doing this unless there is a return from here*/
            gap=guessGap(ratPoint,dimage,gap,numb,Lplace,bin);
            if(gap<=0){
              blocked=0;
              for(j=bin+1;j<ratPoint[numb].bins;j++){
                if(ratPoint[numb].ratRes[Lplace].voxN[j]==-1)break;
                for(m=0;m<dimage->nBands;m++){
                  band=dimage->band[numb][m];
                  if(ratPoint[numb].ratRes[Lplace].refl[j*ratPoint[numb].nBands*(ratPoint[numb].n+1)+band*(ratPoint[numb].n+1)]>0){
                    blocked=1;  /*then we have blocked a ray before it should have been*/
                    anyBlocked=1;
                    break;
                  }
                }
              }
              if(blocked){       /*then we need to adjust the contributing voxels*/
                for(j=0;j<ratPoint[numb].bins;j++){
                  for(n=0;n<ratPoint[numb].ratRes[Lplace].voxN[bin];n++){
                    Vplace=ratPoint[numb].ratRes[Lplace].voxMap[bin][n];
                    if(n&&(Vplace!=ratPoint[numb].ratRes[Lplace].voxMap[bin][n-1])){
                      for(mat=0;mat<dimage->matN;mat++){
                        dimage->voxel[Vplace].LAD[mat]+=LADstep;
                        if(dimage->voxel[Vplace].LAD[mat]<0.013)dimage->voxel[Vplace].LAD[mat]=0.013;
                        else if(dimage->voxel[Vplace].LAD[mat]>M_PI/2-0.013)dimage->voxel[Vplace].LAD[mat]=M_PI/2-0.013;
                      }
                    }
                  }
                }/*end of LAD adjustment loop*/
              }/*end of premature block test*/
              break;  /*once a beam has been blocked finish with it*/
            }/*end of gap test*/
          }/*end of light test*/
        }/*end of bin  stepping*/
        ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
        /*dimage=clearAngles(dimage); */
      }  /*end of beam stepping*/
    }    /*end of gap resizing loop*/
/*    printf("Gap resize without %d\n",iter); */
    iter++;
  }while(anyBlocked&&(iter<100));    /*premature blocking loop*/

  return(dimage);
}/*resizeGaps*/

/*#####################################################################################*/
/*gap resize within iterations*/

RatImage *resizeGapsWithin(RatImage *dimage,RatControl *ratPoint)
{
  int j=0,n=0,m=0,Vplace=0,mat=0;
  int numb=0,Lplace=0,bin=0,band=0;
  int iter=0;
  float gap=0,mu=dimage->mu,LADstep=0;
  char brEak=0,anyAdjustable=0;
  char blocked=0,anyBlocked=0;

  do{
    anyBlocked=0;
    for(numb=0;numb<dimage->scanN;numb++){
      for(Lplace=0;Lplace<ratPoint[numb].NZscans*ratPoint[numb].NAscans;Lplace++){
        if(ratPoint[numb].Etheta[(int)(Lplace/ratPoint[numb].NAscans)]>=mu)LADstep=M_PI/-200;
        else if(ratPoint[numb].Etheta[(int)(Lplace/ratPoint[numb].NAscans)]<mu)LADstep=M_PI/200;
        ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
        gap=1;
        for(bin=0;bin<ratPoint[numb].bins;bin++){
          if(ratPoint[numb].ratRes[Lplace].voxN[bin]==-1)break;
          gap=guessGap(ratPoint,dimage,gap,numb,Lplace,bin);
          ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
          if(gap<=0){   /*make sure it is a correct gap*/
            blocked=0;
            brEak=0;
            for(j=bin;j<ratPoint[numb].bins;j++){
              if(ratPoint[numb].ratRes[Lplace].voxN[j]==-1)break;
              for(m=0;m<dimage->nBands;m++){
                band=dimage->band[numb][m];
                if(ratPoint[numb].ratRes[Lplace].refl[(j*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>0){
                  blocked=1;
                  anyBlocked=1;
                  brEak=1;
                  break;
                }
              }
              if(brEak)break;
            }
            if(blocked){   /*we must adjust LADs of beams with only one zenith*/
              for(j=0;j<ratPoint[numb].bins;j++){  /*create a list of adjustable voxels*/
                for(n=0;n<ratPoint[numb].ratRes[Lplace].voxN[j];n++){
                  Vplace=ratPoint[numb].ratRes[Lplace].voxMap[j][n];
                  if(dimage->voxel[Vplace].adjust==0){
                    for(mat=0;mat<dimage->matN;mat++){
                      dimage->voxel[Vplace].LAD[mat]+=LADstep;
                      if(dimage->voxel[Vplace].LAD[mat]<0.013)dimage->voxel[Vplace].LAD[mat]=0.013;
                      else if(dimage->voxel[Vplace].LAD[mat]>M_PI/2-0.013)dimage->voxel[Vplace].LAD[mat]=M_PI/2-0.013;
                    }
                    anyAdjustable=1;
                  }
                }
              }
            }
            break;
          }
        }
        ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
        /*dimage=clearAngle(dimage);*/
      }
    }/*end of gap checking loop*/
    /*printf("Gap resize within %d %d\n",inter,iter);*/
    iter++;
  }while(anyAdjustable&&anyBlocked&&iter<100);    /*end of premature blocking step*/

  if(!anyAdjustable){
    fprintf(stdout,"None are not adjustable\n");
    dimage->anyAdjustable=0;        /*an indicator to save time later*/
  }else dimage->anyAdjustable=1;

  return(dimage);
}/*resizeGapsWithin*/

/*#####################################################################################*/
/*set up initial areas*/

RatImage *initialAreas(RatImage *dimage,RatControl *ratPoint)
{
  int n=0,j=0,m=0,mat=0,band=0;
  int Vplace=0,numb=0,Lplace=0,bin=0;
  double rflux=0;
  float *Aetot=NULL,*LAD=NULL,*thetaHat=NULL;
  float gap=0,Tgap=0,psi=0,contN=0;
  char brEak=0;

  fprintf(stdout,"Setting up initial areas...\n");

  Aetot=falloc(dimage->matN,"total element area",1);
  LAD=falloc(dimage->matN,"leaf angle distribution",1);
  thetaHat=falloc(dimage->matN,"theta hat",0);

  for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
    Aetot[0]=0;
    Aetot[1]=0;
    Tgap=0;    /*reset gap weighting normalisation factor*/
    contN=0.0;
    for(n=0;n<dimage->voxel[Vplace].contN;n++){
      numb=dimage->voxel[Vplace].rayMap[3*n];
      Lplace=dimage->voxel[Vplace].rayMap[3*n+1];
      bin=dimage->voxel[Vplace].rayMap[3*n+2];
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
      brEak=0;
      gap=1.0;
      for(j=0;j<bin;j++){
        gap=guessGap(ratPoint,dimage,gap,numb,Lplace,j);
        if(gap<=0.000001){
          break;
        }
      }
      if(gap>THRESHOLD){
        LAD=meanLAD(ratPoint,dimage,numb,Lplace,bin,LAD);
        psi=meanPsi(ratPoint,dimage,numb,Lplace,bin);
        thetaHat=meanThetaHat(ratPoint,dimage,LAD,thetaHat,numb,Lplace);
        if(thetaHat[0]>M_PI/2||thetaHat[0]<0.000001){printf("HEAVY ERROR IN AVERAGING %f\n",thetaHat[0]*180/M_PI);exit(1);}
        if(thetaHat[0]>M_PI/2||thetaHat[1]>M_PI/2){printf("Error in hat angles %f\n",thetaHat[0]*180/M_PI);exit(1);}
        for(m=0;m<dimage->nBands;m++){
          band=dimage->band[numb][m];
          rflux=ratPoint[numb].ratRes[Lplace].refl[(bin*ratPoint[numb].nBands+1)*(ratPoint[numb].n+1)]*(double)dimage->voxel[Vplace].fraction[n];
          Aetot[0]+=(float)(rflux/(cos(thetaHat[0])*cos(thetaHat[0])*cos(thetaHat[0])*\
                  (ratPoint[numb].albedo[dimage->matN*band]+ratPoint[numb].albedo[dimage->matN*band+1]*(1-psi)/psi)));
          Aetot[1]+=(float)(rflux/(cos(thetaHat[1])*cos(thetaHat[1])*cos(thetaHat[1])*\
                  (ratPoint[numb].albedo[band*dimage->matN+1]+ratPoint[numb].albedo[band*dimage->matN]*psi/(1-psi))));
        }
        Tgap+=gap;
        contN+=dimage->voxel[Vplace].fraction[n];
        ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
      }/*end of gap size test*/
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
      /*dimage=clearAngles(dimage);*/
    }
    if(contN>0.0&&Tgap>0.00000001){
      for(mat=0;mat<dimage->matN;mat++){
        dimage->voxel[Vplace].Ae[mat]=Aetot[mat]/(contN*(float)dimage->nBands*Tgap);
      }
    }else if(dimage->voxel[Vplace].contN<1){
      for(mat=0;mat<dimage->matN;mat++){
        dimage->voxel[Vplace].Ae[mat]=-1.0;
      }
    }else if(Tgap<=0.00000001){
      for(mat=0;mat<dimage->matN;mat++){
        dimage->voxel[Vplace].Ae[mat]=-2.0;
      }
    }else if(contN==0.0){
      fprintf(stderr,"This should not be\n");
      exit(1);
    }
  } /*end of initial area estimate*/

  TIDY(Aetot);
  TIDY(LAD);
  TIDY(thetaHat);
  return(dimage);
}/*initialAreas*/

/*##########################################################################*/
/*estimate initial surface areas using linear LAD model*/

void initialLinearAreas(RatImage *dimage,RatControl *ratPoint)
{
  int n=0;
  int contN=0,band=0;
  int Vplace=0,place=0,numb=0,Lplace=0,bin=0;
  float psi=0,rho=0,Aetot=0;

  fprintf(stdout,"estimating initial areas\n");
  band=dimage->band[numb][1]; /*use a single band for now*/

  for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
    Aetot=0.0;
    contN=0;
    for(n=0;n<dimage->voxel[Vplace].contN;n++){
      numb=dimage->voxel[Vplace].rayMap[3*n];
      Lplace=dimage->voxel[Vplace].rayMap[3*n+1];
      bin=dimage->voxel[Vplace].rayMap[3*n+2];

      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);

      psi=meanPsi(ratPoint,dimage,numb,Lplace,bin);
      rho=psi*ratPoint[numb].albedo[band*dimage->matN]+(1.0-psi)*ratPoint[numb].albedo[band*dimage->matN+1];

      Aetot+=ratPoint[numb].ratRes[Lplace].refl[place]*3.0/(2.0*rho);  /*we've ignored gaps for now*/
      contN++;

      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);

    }/*contribution loop*/
    if(contN>0){                                    /*the voxel isn;t bloecked*/
      Aetot/=(float)contN;
      dimage->voxel[Vplace].Ae[0]=Aetot*psi;
      dimage->voxel[Vplace].Ae[1]=Aetot*(1.0-psi);
    }else if(!dimage->voxel[Vplace].contN){         /*no beams intersect this voxel*/
      dimage->voxel[Vplace].Ae[0]=dimage->voxel[Vplace].Ae[1]=-1.0;
    }else{                                          /*beams intersect but they appear blocked*/
      dimage->voxel[Vplace].Ae[0]=dimage->voxel[Vplace].Ae[1]=-2.0;
      fprintf(stderr,"This shouldn't, you've made a mistake\n");
      exit(1);
    }
  }/*voxel loop*/

  return;
}/*initialLinearAreas*/

/*##########################################################################*/
/*print out the voxel file header informatrion*/

void printHeader(RatImage *dimage,RatControl *ratPoint)
{
  int i=0;

  /*read back the voxel parameters*/
  /*#############message of intent################*/
  printf("To make sure everything is working I will read out the header information\n");
  if(dimage->scanN>0){
    printf("This was created from %d scans\nCentred at\n",dimage->scanN);
    for(i=0;i<dimage->scanN;i++)printf("x %f y %f z %f\n",ratPoint[i].from[0],ratPoint[i].from[1],ratPoint[i].from[2]);
  }else if(dimage->scanN==0)printf("This was created from the raw object file\n");
  else{ printf("the scan numbers seem to be negative, which is wrong\n");exit(1);}
  printf("The scene held %d voxels in lines of %d %d %d containing %d materials\n",\
    dimage->totalVoxels,dimage->voxNumb[0],dimage->voxNumb[1],dimage->voxNumb[2],dimage->matN);
  printf("The cubic voxels had sides of %f mm  centred on %f %f %f\n",dimage->voxRes,\
    dimage->voxCentre[0],dimage->voxCentre[1],dimage->voxCentre[2]);

  return;
}/*printHeader*/

/*##########################################################################*/
/*print out coordinates of full voxels for use in gnuplot*/

void printFullCoords(RatImage *dimage)
{
  int i=0,j=0,k=0;
  int Vplace=0;
  float x=0,y=0,z=0;
  FILE *leaf=NULL,*bark=NULL;
  char namen[200];

  sprintf(namen,"%s.leaf.gnucoords",dimage->output);
  if((leaf=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening results file %s\n",namen);
    exit(1);
  }
  fprintf(stdout,"Writting coordinates to %s ",namen);
  sprintf(namen,"%s.bark.gnucoords",dimage->output);
  fprintf(stdout,"and %s\n",namen);
  if((bark=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening results file %s\n",namen);
    exit(1);
  }

  for(i=0;i<dimage->voxNumb[0];i++){
    x=(i-dimage->voxNumb[0]/2)*dimage->voxRes+dimage->voxCentre[0];
    for(j=0;j<dimage->voxNumb[1];j++){
      y=(j-dimage->voxNumb[1]/2)*dimage->voxRes+dimage->voxCentre[1];
      for(k=0;k<dimage->voxNumb[2];k++){
        z=(k-dimage->voxNumb[2]/2)*dimage->voxRes+dimage->voxCentre[2];
        Vplace=i*dimage->voxNumb[1]*dimage->voxNumb[2]+j*dimage->voxNumb[2]+k;
        if(dimage->voxel[Vplace].Ae[0]>0)fprintf(leaf,"%f %f %f\n",x,y,z);
        if(dimage->voxel[Vplace].Ae[1]>0)fprintf(bark,"%f %f %f\n",x,y,z);
      }
    }
  }

  if(leaf){
    fclose(leaf);
    leaf=NULL;
  }
  if(bark){
    fclose(bark);
    bark=NULL;
  }
  return;
}/*printFullCoords*/

/*######################################################################*/
/*clear out voxel arrays */

RatImage *clearVoxels(RatImage *dimage)
{
  int Vplace=0;

  if(dimage->voxel){
    for(Vplace=0;Vplace<dimage->totalVoxels;Vplace++){
      TIDY(dimage->voxel[Vplace].rayMap);
      TIDY(dimage->voxel[Vplace].Ae);
      TIDY(dimage->voxel[Vplace].LAD);
      TIDY(dimage->voxel[Vplace].fraction);
    }
    free(dimage->voxel);
    dimage->voxel=NULL;
  }
  return(dimage);
}/*clearVoxels*/

/*#####################################################################*/
/*clear out the beam to voxel maps*/

void clearBeamVoxel(RatControl *ratPoint,RatImage *dimage)
{
  int numb=0,Lplace=0,bin=0;

  for(numb=0;numb<dimage->scanN;numb++){
    for(Lplace=0;Lplace<ratPoint[numb].NZscans*ratPoint[numb].NAscans;Lplace++){
      if(ratPoint[numb].ratRes[Lplace].fraction){
        for(bin=0;bin<ratPoint[numb].bins;bin++){
          TIDY(ratPoint[numb].ratRes[Lplace].fraction[bin]);
        }
        TIDY(ratPoint[numb].ratRes[Lplace].voxMap);
      }
      TIDY(ratPoint[numb].ratRes[Lplace].voxN);
      if(ratPoint[numb].ratRes[Lplace].voxMap){
        for(bin=0;bin<ratPoint[numb].bins;bin++){
          TIDY(ratPoint[numb].ratRes[Lplace].voxMap[bin]);
        }
        free(ratPoint[numb].ratRes[Lplace].voxMap);
        ratPoint[numb].ratRes[Lplace].voxMap=NULL;
      }
    }
  }

  return;
}/*clearBeamVoxel*/

/*#####################################################################*/
/*clear out arrays used in the inversion*/

void clearTheRest(RatImage *dimage,RatControl *ratPoint,float **refErr,float **oldErr)
{
  int i=0,j=0,n=0;

  for(i=0;i<dimage->scanN;i++){
    for(j=0;j<ratPoint[i].NAscans*ratPoint[i].NZscans;j++){
      if(ratPoint[i].ratRes[j].voxMap){
        for(n=0;n<ratPoint[i].bins;n++){
          TIDY(ratPoint[i].ratRes[j].voxMap[n]);
        }
        free(ratPoint[i].ratRes[j].voxMap);
        ratPoint[i].ratRes[j].voxMap=NULL;
      }
      TIDY(ratPoint[i].ratRes[j].voxN);
    }/*beam step*/
    TIDY(ratPoint[i].albedo);
    TIDY(ratPoint[i].Etheta);
    if(ratPoint[i].ro){
      for(j=0;j<ratPoint[i].nMat;j++)TIDY(ratPoint[i].ro[j]);
      free(ratPoint[i].ro);
      ratPoint[i].ro=NULL;
    }
  }/*scan loop*/
  if(refErr){
    for(i=0;i<dimage->scanN;i++){
      TIDY(refErr[i]);
    }
    free(refErr);
    refErr=NULL;
  }
  if(oldErr){
    for(i=0;i<dimage->scanN;i++){
      TIDY(oldErr[i]);
    }
    free(oldErr);
    oldErr=NULL;
  }
  TIDY(dimage->omegaRatio);
  TIDY(dimage->plants);
  if(dimage->refFiles){
    for(i=0;i<ratPoint[0].nMat;i++){
      if(dimage->refFiles[i]){
        for(j=0;j<2;j++)TIDY(dimage->refFiles[j]);
        TIDY(dimage->refFiles[i]);
        dimage->refFiles[i]=NULL;
      }
    }
    free(dimage->refFiles);
    dimage->refFiles=NULL;
  }

  return;
}/*clearTheRest*/

/*#####################################################################*/
/*print oput the voxel maps as ascii files*/

void asciiMaps(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0,numb=0,bin=0;
  char namen[200];
  FILE *opoo=NULL;

  sprintf(namen,"%s.voxMap.txt",dimage->output);
  if((opoo=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening results file %s\n","voxMap");
    exit(1);
  }
  /*check the map*/
  for(i=0;i<dimage->totalVoxels;i++){
    fprintf(opoo,"%d %d %ld %d %d %d",i,dimage->voxel[i].contN,dimage->voxel[i].coord,(int)dimage->voxel[i].pos[0],\
      (int)dimage->voxel[i].pos[1],(int)dimage->voxel[i].pos[2]);
    if(dimage->voxel[i].contN>0){
      for(j=0;j<dimage->voxel[i].contN;j++)fprintf(opoo," %d %d %d",dimage->voxel[i].rayMap[3*j],dimage->voxel[i].rayMap[3*j+1],dimage->voxel[i].rayMap[3*j+2]);
      fprintf(opoo," fraction");
      for(j=0;j<dimage->voxel[i].contN;j++)fprintf(opoo," %f",dimage->voxel[i].fraction[j]);
    }
    fprintf(opoo,"\n");
  }
  if(opoo){
    fclose(opoo);
    opoo=NULL;
  }
  sprintf(namen,"%s.rayMap.txt",dimage->output);
  if((opoo=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening results file %s\n","voxMap");
    exit(1);
  }
  for(numb=0;numb<dimage->scanN;numb++){
    for(i=0;i<ratPoint[numb].NZscans*ratPoint[numb].NAscans;i++){
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        fprintf(opoo,"%d %d %d %d",numb,i,bin,ratPoint[numb].ratRes[i].voxN[bin]);
        if(ratPoint[numb].ratRes[i].voxN[bin]>0){
          for(j=0;j<ratPoint[numb].ratRes[i].voxN[bin];j++)fprintf(opoo," %d\n",ratPoint[numb].ratRes[i].voxMap[bin][j]);
          fprintf(opoo," fractions");
          for(j=0;j<ratPoint[numb].ratRes[i].voxN[bin];j++)fprintf(opoo," %f",ratPoint[numb].ratRes[i].fraction[bin][j]);
        }
        fprintf(opoo,"\n");
      }
    }
  }
  if(opoo){
    fclose(opoo);
    opoo=NULL;
  }

  return;
}/*asciiMaps*/

/*#######################################################################################*/
/*read the reflectance files from the plants.matlib file*/

void readPlants(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,matN=0;
  char line[200];
  char guff1[100],**guff2=NULL,**guff3=NULL;
  FILE *plants=NULL;


  if(dimage->plants==NULL){   /*if NULL set to "plants.matlib"*/
    dimage->plants=challoc((uint64_t)strlen("plants.matlib")+1,"plant file name",0);
    strcpy(dimage->plants,"plants.matlib");
  }

  if((plants=fopen(dimage->plants,"r"))==NULL){
    fprintf(stderr,"Error opening material file %s\n",dimage->plants);
    exit(1);
  }

  if(!(guff2=(char **)calloc(100,sizeof(char *)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }
  if(!(guff3=(char **)calloc(100,sizeof(char *)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }
  for(i=0;i<100;i++){
    guff2[i]=challoc(100,"temporary plant reader",0);
    guff3[i]=challoc(100,"temporary plant reader",1);
  }

  while(fgets(line,200,plants)!=NULL){
    if(sscanf(line,"%s %s %s",guff1,guff2[matN],guff3[matN])!=3) {
      fprintf(stderr, "Badly formatted material file\n");
      exit(1);
    }
    if(strncasecmp(guff1,"#",1))matN++;
  }
  if(!(dimage->refFiles=(char ***)calloc(matN+2,sizeof(char **)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }

  for(i=0;i<matN;i++){
    if(!(dimage->refFiles[i+2]=(char **)calloc(2,sizeof(char *)))){
      fprintf(stderr,"error in material LUT allocation.\n");
      exit(1);
    }
    dimage->refFiles[i+2][0]=challoc((uint64_t)strlen(guff2[i])+1,"material type",i+2);
    dimage->refFiles[i+2][1]=challoc((uint64_t)strlen(guff3[i])+1,"material reflectance name",i+2);
    strcpy(dimage->refFiles[i+2][0],guff2[i]);
    strcpy(dimage->refFiles[i+2][1],guff3[i]);
    TIDY(guff2[i]);
    TIDY(guff3[i]);
  }
  TIDY(guff2);
  TIDY(guff3);

  if(plants){
    fclose(plants);
    plants=NULL;
  }

  for(i=0;i<dimage->scanN;i++){
    if(matN+2!=ratPoint[i].nMat){
      fprintf(stderr,"material number mismatch %d %d\n",matN+2,ratPoint[i].nMat);
    }
  }
  return;
}/*readPlants*/

/*#####################################################################################*/
/*read spectral files for albedos*/

void readAlbedo(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,mat=0,band=0,nMat=0,numb=0;
  char line[200],hash[100];
  float *wave=NULL,*refl=NULL;
  FILE *spectra=NULL;

  wave=falloc(1000,"wavelength temporary",0);
  refl=falloc(1000,"reflectance temporary",0);

  for(numb=0;numb<dimage->scanN;numb++){
    if(!(ratPoint[numb].ro=(float **)calloc(ratPoint[numb].nMat,sizeof(float *)))){
      fprintf(stderr,"error in material LUT allocation.\n");
      exit(1);
    }
    for(mat=2;mat<ratPoint[numb].nMat;mat++){
      ratPoint[numb].ro[mat]=falloc(dimage->nBands,"reflectance from file",mat);
      nMat=0;
      if((spectra=fopen(dimage->refFiles[mat][1],"r"))==NULL){
        fprintf(stderr,"Error opening material data output %s\n",dimage->refFiles[mat][1]);
        exit(1);
      }
      /*get all spectral information*/
      while(fgets(line,200,spectra)!=NULL){
        if(sscanf(line,"%s",hash)!=1){
          fprintf(stderr,"Badly formatted waveband file\n");
          exit(1);
        }
        if(strncasecmp(hash,"#",1)){
          if(sscanf(line,"%f %f",&wave[nMat],&refl[nMat])!=2) {
            fprintf(stderr, "Badly formatted waveband file\n");
            exit(1);
          }
          nMat++;
        }
      }
      /*find the closest wavelength*/
      for(band=0;band<dimage->nBands;band++){
        i=0;
        while(wave[i]<ratPoint[numb].wavelength[dimage->band[numb][band]]&&i<nMat)i++;
        if(i>0){
          if((wave[i]-ratPoint[numb].wavelength[dimage->band[numb][band]])*(wave[i]-ratPoint[numb].wavelength[dimage->band[numb][band]])<\
            (wave[i-1]-ratPoint[numb].wavelength[dimage->band[numb][band]])*(wave[i-1]-ratPoint[numb].wavelength[dimage->band[numb][band]]))\
               ratPoint[numb].ro[mat][band]=refl[i];
          else ratPoint[numb].ro[mat][band]=refl[i-1];
        }else  ratPoint[numb].ro[mat][band]=refl[i];
      }
      if(spectra){
        fclose(spectra);
        spectra=NULL;
      }
    }
  }/*scan step*/
  TIDY(refl);
  TIDY(wave);

  return;
}/*readAlbedo*/

/*#########################################################################*/
/*find voxel contribution number*/

int findBinOrder(RatImage *dimage,RatControl *ratPoint,int Vplace,int numb,int Lplace,int bin)
{
  int order=0;
  char found=0;

  for(order=0;order<dimage->voxel[Vplace].contN;order++){
    if(dimage->voxel[Vplace].rayMap[3*order]!=numb)continue;
    else if(dimage->voxel[Vplace].rayMap[3*order+1]!=Lplace)continue;
    else if(dimage->voxel[Vplace].rayMap[3*order+2]!=bin)continue;
    else {
      found=1;
      break;
    }
  }
  if(!found&&dimage->voxel[Vplace].contN){
    fprintf(stderr,"errrrrr %d %d %d %d\n",Vplace,numb,Lplace,bin);
    exit(1);
  }

  return(order);
}/*findBinOrder*/

/*######################################################*/
/*check hetereognity of phase function*/

void phaseHeterogneity(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int i=0,j=0,k=0,bin=0,band=0,mat=0;
  int Lplace=0,place=0,mPlace=0;
  int contN=0;
  double refl=0,diRefl=0;
  float *cover=NULL,*phase=NULL;
  float meanPhase=0,stdev=0;
  RatControl *labelAlbedo(RatImage *,RatControl *);
  FILE *opoo=NULL;
  char namen[200];
  
  /*set up names nd tings for later*/
  ratPoint=labelAlbedo(dimage,ratPoint);

  /*read reflectances and material indices from files*/
  dimage->matN=2;

  cover=falloc(dimage->matN+1,"cover heterogneity",numb);
  phase=falloc(ratPoint[numb].NZscans*ratPoint[numb].NAscans,"phase function",numb);
  
  sprintf(namen,"%s.phaseHet",dimage->output);
  if((opoo=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening results file %s\n",namen);
    exit(1);
  }
  fprintf(stderr,"Printing phase function to %s\n",namen);
  fprintf(opoo,"# 1 leaf cover, 2 branch cover, 3 soil cover, 4 gap fraction, 5 phase, 6 refl, 7 direct refl, 8 leaf albedo, 9 bark albedo\n");
  fflush(opoo);
  
  for(i=0;i<ratPoint[numb].NZscans;i++){
    fprintf(stdout,"zenith ring %d of %d\n",i+1,ratPoint[numb].NZscans);
    for(j=0;j<ratPoint[numb].NAscans;j++){
      Lplace=i*ratPoint[numb].NAscans+j;
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
      refl=0.0;
      diRefl=0.0;
      for(mat=0;mat<dimage->matN;mat++)cover[mat]=0.0;
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
        refl+=ratPoint[numb].ratRes[Lplace].refl[place];
	place=findReflIndex(dimage,ratPoint,numb,bin,band,1);
        diRefl+=ratPoint[numb].ratRes[Lplace].refl[place];
        for(k=0;k<ratPoint[numb].nMat;k++){
          mat=ratPoint[numb].matCode[k];
	  if(mat>-1){
            mPlace=findMaterialIndex(dimage,ratPoint,numb,bin,k,0,0);
            cover[mat]+=ratPoint[numb].ratRes[Lplace].material[mPlace];
	  }else if(mat==-2){
            mPlace=findMaterialIndex(dimage,ratPoint,numb,bin,k,0,0);
	    cover[2]+=ratPoint[numb].ratRes[Lplace].material[mPlace];
	  }
	}
      }/*bin loop*/
      if((refl>0.0)&&((cover[0]>0.0)||(cover[1]>0.0))){
        phase[Lplace]=(float)refl/(ratPoint[numb].albedo[0]*cover[0]/100.0+ratPoint[numb].albedo[1]*cover[1]/100.0);
        meanPhase+=phase[Lplace];
        fprintf(opoo,"%f %f %f %f %f %f %f %f %f\n",cover[0],cover[1],cover[2],\
			ratPoint[numb].ratRes[Lplace].sky,phase[Lplace],\
			refl,diRefl,ratPoint[numb].albedo[0],ratPoint[numb].albedo[1]);
	fflush(opoo);
	contN++;
      }else{
        phase[Lplace]=-1.0;  /*mark with a flag*/
      }	       
      
      
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    }/*azimuth loop*/
  }/*zenith loop*/
  if(contN)meanPhase/=(float)contN;
  else{
    fprintf(stderr,"There seems to be no light in this scan\n");
    exit(1);
  }
  
  /*Calculate standard deviation*/
  contN=0;
  for(i=0;i<ratPoint[numb].NZscans;i++){
    for(j=0;j<ratPoint[numb].NAscans;j++){
      Lplace=i*ratPoint[numb].NAscans+j;
      if(phase[Lplace]>-1.0){
        stdev+=(phase[Lplace]-meanPhase)*(phase[Lplace]-meanPhase);
        contN++;
      }
    }
  }
  if(contN>0){
    stdev=sqrt(stdev/(float)contN);
  }

  fprintf(opoo,"# Mean phase %f, stdev %f\n",meanPhase,stdev);
 
  if(opoo){
    fclose(opoo);
    opoo=NULL;
  }
  TIDY(phase);
  TIDY(cover);
  
  return;
}/*phaseHeterogneity*/


/*##################################################*/
/* Measure the true gap fraction*/

void trueGap(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int i=0,j=0,Lplace=0,bin=0,mat=0;
  int place=0;
  int nBins=0,zenBin=0;
  int *ringMap=NULL,*ninRing=NULL;
  float **fraction=NULL;
  double maxZen=0;
  char namen[200];
  FILE *opoo=NULL;

  sprintf(namen,"%s.gapFraction",dimage->output);
  if(dimage->intent)fprintf(stdout,"Measuring true gap fraction\n");

  /*determine which bins are in which zenith range*/
  if(fabs(ratPoint[numb].theta[ratPoint[numb].NAscans])<fabs(ratPoint[numb].theta[0]))maxZen=fabs(ratPoint[numb].theta[0]);
  else                                                           maxZen=fabs(ratPoint[numb].theta[ratPoint[numb].NAscans]);
  nBins=(int)(maxZen/dimage->tGapres);

  /*map it*/
  ringMap=ialloc(ratPoint[numb].NZscans,"zenith gap map",numb);
  ninRing=ialloc(nBins,"zenith ring contents",numb);
  if(!(fraction=(float **)calloc(nBins,sizeof(float *)))){
    fprintf(stderr,"error in gap fraction array allocation.\n");
    exit(1);
  }
  for(i=0;i<nBins;i++){
    fraction[i]=falloc(ratPoint[numb].nMat+1,"gap fraction",i);
    for(mat=0;mat<ratPoint[numb].nMat+1;mat++)fraction[i][mat]=0.0;
    ninRing[i]=0;
  }

  for(i=0;i<ratPoint[numb].NZscans;i++){
    Lplace=i*ratPoint[numb].NAscans;
    zenBin=(int)(nBins*fabs(ratPoint[numb].theta[Lplace])/maxZen);
    if(zenBin<0)zenBin=0;
    else if(zenBin>=nBins)zenBin=nBins-1;
    ringMap[i]=zenBin;
    ninRing[zenBin]+=ratPoint[numb].NAscans;
  }

  /*now go through and measure the gap from action from the sky*/
  for(i=0;i<ratPoint[numb].NZscans;i++){
    Lplace=i*ratPoint[numb].NAscans;
    if(dimage->intent){
      fprintf(stdout,"Zenith %f\n",ratPoint[numb].theta[Lplace]*180.0/M_PI);
      fflush(stdout);
    }
    for(j=0;j<ratPoint[numb].NAscans;j++){
      Lplace=i*ratPoint[numb].NAscans+j;
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        for(mat=0;mat<ratPoint[numb].nMat;mat++){
          place=findMaterialIndex(dimage,ratPoint,numb,bin,mat,0,0);
          fraction[ringMap[i]][mat]+=ratPoint[numb].ratRes[Lplace].material[place];
        }
      }
      fraction[ringMap[i]][mat]+=ratPoint[numb].ratRes[Lplace].sky;
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    }
  }


  if((opoo=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening results file %s\n",namen);
    exit(1);
  } 

  for(zenBin=0;zenBin<nBins;zenBin++){
    fprintf(opoo,"%g",(float)zenBin*dimage->tGapres*180.0/M_PI);
    for(mat=0;mat<ratPoint[numb].nMat+1;mat++)fprintf(opoo," %f",fraction[zenBin][mat]/((float)ninRing[zenBin]*100.0));
    fprintf(opoo," %d\n",ninRing[zenBin]);
  }

  if(opoo){
    fclose(opoo);
    opoo=NULL;
  }
  if(dimage->intent)fprintf(stdout,"Written to %s\n",namen);

  if(fraction){
    for(i=0;i<nBins;i++)TIDY(fraction[i]);
    free(fraction);
    fraction=NULL;
  }
  TIDY(ninRing);
  TIDY(ringMap);
  return;
}/*trueGap*/


/*#################################################################*/
/* Measure the gap fraction using the method of Jupp et al. 2009*/

void juppGap(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int i=0,j=0,Lplace=0,bin=0,band=0,mat=0;
  int place=0;
  int nBins=0;
  int *ringMap=NULL,*ninRing=NULL;
  float **fraction=NULL,refl=0,dirRefl=0;
  float phase=0,*albedo=NULL;   /*phase function and albedo*/
  double standRadSq=0,radiusSq=0;   /*radii for cylindrical plots*/

  char namen[200];
  FILE *opoo=NULL;
  //RatControl *labelAlbedo(RatImage *,RatControl *);


  /*load up albedo*/
  //labelAlbedo(dimage,ratPoint);
  phase=0.25;  /*estimate from Jupp et al 2009*/
 
  sprintf(namen,"%s.juppGap",dimage->output);
  if(dimage->intent)fprintf(stdout,"Measuring Jupp's gap fraction\n");
  
  /*determine which bins are in which zenith range*/
/*  if(fabs(ratPoint[numb].theta[ratPoint[numb].NAscans])<fabs(ratPoint[numb].theta[0]))maxZen=fabs(ratPoint[numb].theta[0]);
  else                                                           maxZen=fabs(ratPoint[numb].theta[ratPoint[numb].NAscans]);
  nBins=(int)(maxZen/dimage->jGapres);                           
*/
  /*map it*/

/*  ringMap=ialloc(ratPoint[numb].NZscans,"zenith gap map",numb);
  ninRing=ialloc(nBins,"zenith ring contents",numb);
  if(!(fraction=(float **)calloc(nBins,sizeof(float *)))){
    fprintf(stderr,"error in gap fraction array allocation.\n");
    exit(1);
  } 
  for(i=0;i<nBins;i++){
    fraction[i]=falloc(dimage->nBands,"gap fraction",i);
    for(band=0;band<dimage->nBands;band++)fraction[i][band]=0.0;
    ninRing[i]=0;
  } 
  
  for(i=0;i<ratPoint[numb].NZscans;i++){
    Lplace=i*ratPoint[numb].NAscans;
    zenBin=(int)(nBins*fabs(ratPoint[numb].theta[Lplace])/maxZen);
    if(zenBin<0)zenBin=0;
    else if(zenBin>=nBins)zenBin=nBins-1;
    ringMap[i]=zenBin;
    ninRing[zenBin]+=ratPoint[numb].NAscans;
  } 
  
*/
  if((opoo=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening results file %s\n",namen);
    exit(1);
  }

  if(dimage->standRadius>0.0)standRadSq=(double)(dimage->standRadius*dimage->standRadius*4.0);

  /*now go through and measure the gap from action from the sky*/
  fprintf(opoo,"# 1 zenith,");
  for(band=0;band<dimage->nBands;band++)fprintf(opoo," %d %g total , %d %g direct ,",2*band+2,ratPoint[numb].wavelength[band],2*band+3,ratPoint[numb].wavelength[band]);
  fprintf(opoo," %d sky ,",2*dimage->nBands+2);
  for(mat=0;mat<ratPoint[numb].nMat;mat++)fprintf(opoo," %d mat %d ,",2*dimage->nBands+3+mat,mat);
  fprintf(opoo,"\n");
  for(i=0;i<ratPoint[numb].NZscans;i++){
    Lplace=i*ratPoint[numb].NAscans;
    if(dimage->intent){
      fprintf(stdout,"Zenith %f\n",ratPoint[numb].theta[Lplace]*180.0/M_PI);
      fflush(stdout);
    }
    for(j=0;j<ratPoint[numb].NAscans;j++){
      fprintf(opoo,"%f",ratPoint[numb].theta[Lplace]*180.0/M_PI);
      Lplace=i*ratPoint[numb].NAscans+j;
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
      for(band=0;band<dimage->nBands;band++){
        refl=dirRefl=0.0;
        for(bin=0;bin<ratPoint[numb].bins;bin++){
          if(dimage->standRadius>0.0){
            radiusSq=((double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R)*sin(ratPoint[numb].theta[Lplace]);
            radiusSq*=radiusSq;
            if(radiusSq>standRadSq)break;
          }
          place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
          refl+=(float)ratPoint[numb].ratRes[Lplace].refl[place];
          place=findReflIndex(dimage,ratPoint,numb,bin,band,1);
          dirRefl+=(float)ratPoint[numb].ratRes[Lplace].refl[place];
        }
        fprintf(opoo," %f %f",refl,dirRefl);
      }
      fprintf(opoo," %f",ratPoint[numb].ratRes[Lplace].sky/100.0);
      /*now the fracti0n of each material*/ 
      for(mat=0;mat<ratPoint[numb].nMat;mat++){
        refl=0.0;
        for(bin=0;bin<ratPoint[numb].bins;bin++){
          if(dimage->standRadius>0.0){
            radiusSq=((double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R)*sin(ratPoint[numb].theta[Lplace]);
            radiusSq*=radiusSq;
            if(radiusSq>standRadSq)break;
          }
          place=findMaterialIndex(dimage,ratPoint,numb,bin,mat,0,0);
          refl+=ratPoint[numb].ratRes[Lplace].material[place];
        }
        fprintf(opoo," %f",refl/100.0);
      }
      fprintf(opoo,"\n");
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    }
  }


/*
  for(zenBin=0;zenBin<nBins;zenBin++){
    fprintf(opoo,"%g",(float)zenBin*dimage->jGapres*180.0/M_PI);
    for(band=0;band<dimage->nBands;band++)fprintf(opoo," %f",fraction[zenBin][band]/((float)ninRing[zenBin]*100.0));
    fprintf(opoo," %d\n",ninRing[zenBin]);
  }
*/

  if(opoo){
    fclose(opoo);
    opoo=NULL;
  }
  if(dimage->intent)fprintf(stdout,"Written to %s\n",namen);

  if(fraction){
    for(i=0;i<nBins;i++)TIDY(fraction[i]);
    free(fraction);
    fraction=NULL;
  }
  TIDY(ninRing);
  TIDY(ringMap);
  TIDY(albedo);
  TIDY(ratPoint[numb].albedo);
  return;
}/*juppGap*/


/*################################################################################################*/
/* read in a look up table of spheroid shpase functions*/

void readPhaseLUT(RatImage *dimage)
{
  int i=0,j=0;
  FILE *file=NULL;
  char guff[50],temp1[50],temp2[50];
  char *tempP1=NULL;
  char namen[200],line[2000];


  if(dimage->intent)fprintf(stdout,"Reading look up table\n");

  strcpy(namen,"rossPhase.LUT.dat");
  if((file=fopen(namen,"r"))==NULL){
    fprintf(stderr,"Error opening results file %s\n",namen);
    exit(1);
  }


  /*read the first line to get the file dimensions*/
  i=0;
  if(fgets(line,200,file)==NULL){
    fprintf(stderr,"Error reading the first line of %s\n",namen);
    exit(1);
  }
  if(sscanf(line,"%s %s %s",guff,temp1,temp2)!=3) {
    fprintf(stderr, "Badly formatted phase LUT file\n");
    exit(1);
  } 
  if(!strncasecmp(guff,"##",2)){
    dimage->NthetaLUT=atoi(temp1);
    dimage->NmLUT=atoi(temp2);
  }else{
   fprintf(stderr,"Somthing ain't right\n\"%s\"\n",guff);
   exit(1);
  }

  dimage->thetaLUT=falloc(dimage->NthetaLUT,"phase look up table angles",0);
  dimage->mLUT=falloc(dimage->NmLUT,"phase look up table eccentricities",0);
  if(!(dimage->phaseLUT=(float **)calloc(dimage->NthetaLUT,sizeof(float *)))){
    fprintf(stderr,"error in initial phase LUT allocation. Of the first dimension\n");
    exit(1);
  }
  for(i=0;i<dimage->NthetaLUT;i++)dimage->phaseLUT[i]=falloc(dimage->NmLUT,"phase LUT",i);


  i=-1;
  while(fgets(line,2000,file)!=NULL){
    if(!strncasecmp(line,"#",1))i=-1;
    if(i>=dimage->NmLUT){
      fprintf(stderr,"Errm stepped to line %d of %d\n",i,dimage->NmLUT);
      break;
    }
    if(i>=0){  /*read in the LUT*/
      j=-1;
      tempP1=strtok(line," ");  /*start of srtok*/
      do{
        if(j>=0){
          dimage->phaseLUT[j][i]=atof(tempP1);
        }else{
          dimage->mLUT[i]=atof(tempP1);
        }
        tempP1=strtok(NULL," ");
        if(j>=dimage->NthetaLUT){
          fprintf(stderr,"Errm stepped to column %d of %d\n",j,dimage->NthetaLUT);
          break;
        }
        j++;
      }while(tempP1);
      if(j<dimage->NthetaLUT){
        fprintf(stderr,"too short\n");
      }
    }else{     /*read in the angles*/
      j=-1;
      tempP1=strtok(line," ");  /*start of srtok*/
      do{
        if(j>=0){
          if(j>=dimage->NthetaLUT){
            fprintf(stderr,"Errm stepped to column %d of %d in the angles\n",j,dimage->NthetaLUT);
            break;
          }
          dimage->thetaLUT[j]=atof(tempP1);
        }
        tempP1=strtok(NULL," ");
        j++;
      }while(tempP1);
      if(j<dimage->NthetaLUT){
        fprintf(stderr,"too short\n");
      }
    }
    i++;
  }


  if(file){
    fclose(file);
    file=NULL;
  }
  TIDY(tempP1);

  if(dimage->intent)fprintf(stdout,"LUT read\n");

  return;
}/*readPhaseLUT*/


/*that's the end*/
/*######################################################*/
