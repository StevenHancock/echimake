BEGIN{
  energy=0.001;
  telArea=1.0;
  atmos=0.6;
  effIns=0.1;
  i=0;
  nbands=0
}

{
  if($0&&NF&&$1!="#"){
    range[i]=$1;
    for(j=2;j<=NF;j++)signal[j-2,i]=$j;
    if(!i)nbands=NF-1;
    else if(NF-1!=nbands){
      printf("oh dear, band number mismatch %d %d %s\n",NF-1,nbands,$0);
      exit(1);
    }
    i++;
  }
}

END{
  for(band=0;band<nbands;band++){
    photonOut=energy*2050*10^16/1.986445213*effIns*telArea*atmos;
    printf("# 1 band\n# 2 range\n# 3 photons\n");
    for(j=1;j<=i;j++){
      if(range[j]>0.0){
        print band,range[j],signal[band,j]*photonOut/(range[j]*range[j]/1000000);
      }
    }
  }

}
