#include "echiMake.h"
#include <nrutil.h>

#define ITMAX 200

float *signal;
int signalLength;
int nGauss;

void decomposeGauss(RatImage *dimage,RatControl *ratPoint,int numb)
{
  float **parameters=NULL;
  float **fitGaussians(RatImage *,RatControl *,int);
  int beam=0,i=0,j=0;

  nGauss=dimage->nGauss;

  printf("Decomposing signal into %d Gaussians\n",nGauss);

  /*allocate space for decomposed*/
  dimage->GaussParams=fitGaussians(dimage,ratPoint,numb);

  printf("Decomposed\n");

  for(beam=0;beam<ratPoint[numb].NAscans*ratPoint[numb].NZscans*dimage->nBands;beam++){
    for(i=0;i<nGauss;i++){
      printf("Beam %d Gaussian %d",beam,i);
      for(j=0;j<3;j++)printf(" %f",dimage->GaussParams[beam][3*i+j]);
      printf("\n");
    }
  }

  if(dimage->GaussParams){
    for(beam=0;beam<ratPoint[numb].NAscans*ratPoint[numb].NZscans*dimage->nBands;beam++){
      TIDY(dimage->GaussParams[beam]);
    }
    free(dimage->GaussParams);
    dimage->GaussParams=NULL;
  }


  return;
}


float **fitGaussians(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int i=0,Lplace=0,band=0,pPlace=0;
  float **p=NULL;
  float **xi=NULL;
  float fittingFunction(float *);
  float (*funk)(float *);
  float *fitted=NULL;
  void arrangePowell(RatControl *,RatImage *,int,int,int);
  void powell(float *,float **,int,float,int *,float *,float (*funk)());


  funk=fittingFunction;

  /*double *signal=NULL*/

  if(!(p=(float **)calloc(ratPoint[numb].NAscans*ratPoint[numb].NZscans*dimage->nBands,sizeof(float *)))){
    fprintf(stderr,"error in float buffer allocation.\n");
    exit(1);
  }
  for(i=0;i<ratPoint[numb].NAscans*ratPoint[numb].NZscans*dimage->nBands;i++){
    p[i]=falloc(3*nGauss,"fitted Gaussian",i);
  }
  if(!(xi=(float **)calloc(3*nGauss,sizeof(float *)))){
    fprintf(stderr,"error in float buffer allocation.\n");
    exit(1);
  }
  for(i=0;i<3*nGauss;i++){
    xi[i]=falloc(3*nGauss,"xi squared",i);
    xi[i][i]=1.0;                           /*set to unit vector*/
  }


  signal=falloc(ratPoint[numb].bins,"signal to fit",numb);
  for(Lplace=0;Lplace<ratPoint[numb].NAscans*ratPoint[numb].NZscans;Lplace++){
    ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
    signalLength=ratPoint[numb].bins;
    for(band=0;band<dimage->nBands;band++){
      pPlace=numb*ratPoint[numb].NAscans*ratPoint[numb].NZscans*dimage->nBands+Lplace*dimage->nBands+band;
      p[pPlace]=falloc(3*nGauss,"fitted Gauss parameter",numb);
      arrangePowell(ratPoint,dimage,numb,Lplace,band);


      /*######## REMOVE BEFORE USE ####*/
      powell(p[pPlace],xi,3*nGauss,(float)dimage->Athresh,&(dimage->diters),fitted,funk);

      /*powell(p,xi,n,ftol,iter,fret,func)*/


    }
  }

  TIDY(signal);
  return(p);
}/*fitGaussians*/


/*#################################################################*/
/*function to be fitted*/

float fittingFunction(float *p)
{
  int i=0,bin=0;
  float y=0.0;

  for(bin=0;bin<signalLength;bin++){
    for(i=0;i<nGauss;i++){
      y+=p[3*i]*(float)gaussian((double)bin,(double)p[3*i+1],(double)p[3*i+2]);
    }
    y-=signal[bin];
  }
 
  return(y);
}/*fittingFunction*/


/*#################################################################*/
/*pack reflectance array into single array for fitting*/

void arrangePowell(RatControl *ratPoint,RatImage *dimage,int numb,int Lplace,int band)
{
  int bin=0,place=0;

  for(bin=0;bin<ratPoint[numb].bins;bin++){
    place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
    signal[bin]=(float)ratPoint[numb].ratRes[Lplace].refl[place];
  }

  return;
}/*arrangePowell*/


