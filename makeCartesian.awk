BEGIN{
  pi=4.0*atan2(1.0,1.0);
}

($0){
  if($1!="#"){
    r=$1;
    x=r*sinZen*cosAz+x0;
    y=r*sinZen*sinAz+y0;
    z=r*cosZen+z0;
    print x,y,z,$4,zenDeg,azDeg;
  }else if(($1=="#")&&(NF==3)){
    zenDeg=$2;
    azDeg=$3;
    zen=zenDeg*pi/180.0;
    az=azDeg*pi/180.0;
    sinZen=sin(zen);
    cosZen=cos(zen);
    sinAz=sin(az);
    cosAz=cos(az);
  }else if(($1=="#")&&($2=="scan")){
    x0=$4;
    y0=$5;
    z0=$6;
  }
}

