#include "stdio.h"
#include "stdlib.h"
#include "math.h"

/*#############################*/
/*# Converts from reflectance #*/
/*# to photos count           #*/
/*#############################*/

int main(int argc,char **argv)
{
  int bin=0,band=0,place=0;
  int nbands=0,nbins=0;
  double energy=0,effIns=0,atmos=0;
  double telArea=0,range=0;
  double photonOut=0;
  double *signal=NULL,*photons=NULL;       /*photons are doubles as ints aren't big enough*/
  void usage(char **);
  void checkArguments(int,int,int,char *);
  double *dalloc(int,char *);
  FILE *input=NULL,*output=NULL;

  energy=0.001;
  telArea=1.0;
  atmos=0.6;
  effIns=0.1;

  for (i=1;i<argc;i++){
    if (*argv[i]=='-'){
      if(!strncasecmp(argv[i],"-input",6)){
        checkArguments(1,i,argc,"-input");
        if((input=fopen(argv[++i],"r"))==NULL){
          printf("Error opening input file %s\n",argv[i-1]);
          exit(1);
        }
      }else if(!strncasecmp(argv[i],"-nbands",7)){
        checkArguments(1,i,argc,"-nbands");
        nbands=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-nbins",6)){
        checkArguments(1,i,argc,"-nbins");
        nbins=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-power",6)){
        checkArguments(1,i,argc,"-power");
        energy=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-telescope",10)){
        checkArguments(1,i,argc,"-telescope");
        telArea=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-atmosphere",10)){
        checkArguments(1,i,argc,"-telescope");
        atmos=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-transmission",13)){
        checkArguments(1,i,argc,"-transmission");
        effIns=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-output",7)){
        checkArguments(1,i,argc,"-output");
        if((output=fopen(argv[++i],"w"))==NULL){
          printf("Error opening results file %s\n",argv[i-1]);
          exit(1);
        }
      }else if(!strncasecmp(argv[i],"-u",2)||!strncasecmp(argv[i],"-help",5)){
        usage(argv);
        exit(0);
      }else{
        fprintf(stderr,"%s: unknown argument on command line: %s\n",argv[0],argv[i]);   /* Program_name = argv[0] */
        usage(argv);
        exit(1);
      }
    }
  }

  if(input==NULL){
    fprintf(stderr,"No input specified, -input\n");
    exit(1);
  }else if(nbins<1){
    fprintf(stderr,"A positive number of bins are needed, not %d. Use -nbins\n",nbins);
    exit(1);
  }else if(nbands<1){
    fprintf(stderr,"A positive number of bands are needed, not %d. Use -nbands\n",nbands);
    exit(1);
  }
  if(output==NULL){  /*output with a default name*/
    if((output=fopen("photonCount","w"))==NULL){
      printf("Error opening results file %s\n","photonCount");
      exit(1);
    }
  }

  /*read the input*/
  signal=dalloc(nbins*nbands,"signal");
  while(fgets(line,200,input)!=NULL){
    strcpy(guff1,"#");
    if(sscanf(line,"%s %s",guff1,guff2)!=2) {
      fprintf(stderr, "Badly formatted data file\n");
      exit(1);
    }
    if(strncasecmp(guff1,"#",1)){
      range[i]=atof(guff1);
      signal[i]=atof(guff2);
      i++;
    }
  }
  if(input){
    fclose(input);
    input=NULL;
  }

  photons=dalloc(nbins*nbands,"photon count");

  /*rescale signal to photon count*/
  for(band=0;band<nbands;band++){
    photonOut=energy*wavelength[band]*pow(10,16)/1.986445213*effIns*telArea/(range*range)*atmos;
    for(bin=0;bin<nbins;bin++){
      
    }
  }

  return(0);
}/*main*/

/*#################################################################################*/

void checkArguments(int numberOfArguments,int thisarg,int argc,char *option)
{
  int i=0;

  for(i=0;i<numberOfArguments;i++){
    if(thisarg+1+i>=argc){
      printf("error in number of arguments for %s option: %d required\n",option,numberOfArguments);
      exit(1);
    }
  }
  return;
}/*checkArguments*/
 
/*###################################################################################*/

void usage(char **argv){
  fprintf(stderr, "usage: %s [options]\n", argv[0]);
  fprintf(stderr, "where the options are:\n");



  return;
}

/*###################################################################################*/

double *dalloc(int length,char *namen)
{
  double *jimlad=NULL;
  if(!(jimlad=(double *)calloc(length,sizeof(double)))){
    fprintf(stderr,"error in %s array allocation\n",namen);
    fprintf(stderr,"allocating %d\n",length);
    exit(1);
  }
  return(jimlad);
}/*dalloc*/
