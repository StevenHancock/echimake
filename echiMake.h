#ifndef ECHIMAKE_H
#define ECHIMAKE_H

/*######################################################################################*/
/*#This is a program for reading the output of starat, an echidna simulating ray tracer#*/
/*#It is split into three. This, analMake.c and voxMake.c. This part comtains the      #*/
/*#controlling functions and bits.                                                     #*/
/*# 25th October 2006              Steven Hancock, Geography, UCL                      #*/
/*######################################################################################*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#define RATMAKER
#define VOXELS
#include "starat.h"
#include "tools.h"

#include "hipl_format.h"
#define THRESHOLD 0.0001 /*threshold for doubles*/

/*functions used by all functions*/
RatControl *resultsArrange(RatImage *,RatControl *,int,int,char);
RatControl *clearArrays(RatControl *,int,int,RatImage *);
RatControl *clearAllArrays(RatControl *,RatImage *);
int swappedBytes(int,int);
int findReflIndex(RatImage *,RatControl *,int,int,int,int);
int findMaterialIndex(RatImage *,RatControl *,int,int,int,int,int);
float meanThetaZ(RatImage *,float,float);
double decimalMod(double,double);

/*######################################################*/
#endif
