#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define TIDY(arr) if((arr)){free((arr));(arr)=NULL;}  /*free an array*/

typedef struct{
  char ***refFiles;
  int matN;
  float *wavelength;
  int nBands;
  float **albedo;
}listing;

int main()
{
  int i=0,j=0,k=0;
  char *plantFile=NULL,*bandFile=NULL;
  listing *readPlants(char *);
  listing *readWavebands(char *,listing *);
  listing *readAlbedo(listing *);
  listing *list=NULL;

  if(!(plantFile=(char *)calloc(strlen("plants.matlib")+1,sizeof(char)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }
  strcpy(plantFile,"plants.matlib");

  list=readPlants(plantFile);


  if(!(bandFile=(char *)calloc(strlen("wavebands.dat")+1,sizeof(char)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }
  strcpy(bandFile,"wavebands.dat");

  list=readWavebands(bandFile,list);


  printf("matN %d, nbands %d\n",list->matN,list->nBands);
  for(i=2;i<list->matN+2;i++){
    printf("%d %s %s",i,list->refFiles[i][0],list->refFiles[i][1]);
    for(j=0;j<list->nBands;j++){
      printf(" %f",list->wavelength[j]);
    }
    printf("\n");
  }

  list=readAlbedo(list);


  for(i=2;i<list->matN+2;i++){
    printf("%d %s %s\n",i,list->refFiles[i][0],list->refFiles[i][1]);
  }
  for(i=2;i<list->matN+2;i++)printf("%s ",list->refFiles[i][0]);
  printf("\n");
  for(j=0;j<list->nBands;j++){
    printf(" %f",list->wavelength[j]);
    for(k=2;k<list->matN+2;k++)printf(" %f",list->albedo[k][j]);
    printf("\n");
  }

  if(list){
    if(list->refFiles){
      for(i=0;i<list->matN;i++){
        if(list->refFiles[i]){
          TIDY(list->refFiles[i][0]);
          TIDY(list->refFiles[i][1]);
          free(list->refFiles[i]);
          list->refFiles[i]=NULL;
        }
      }
      free(list->refFiles);
      list->refFiles=NULL;
    }
    free(list);
    list=NULL;
  }

  return(0);
}

listing *readAlbedo(listing *list)
{
  int i=0,mat=0,band=0,nMat=0;
  char line[200],hash[100];
  float *wave=NULL,*refl=NULL;
  FILE *spectra=NULL;

  if(!(wave=(float *)calloc(1000,sizeof(float)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }
  if(!(refl=(float *)calloc(1000,sizeof(float)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }

  if(!(list->albedo=(float **)calloc(list->matN,sizeof(float *)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }
  for(mat=2;mat<list->matN+2;mat++){
    if(!(list->albedo[mat]=(float *)calloc(list->nBands,sizeof(float)))){
      fprintf(stderr,"error in material LUT allocation.\n");
      exit(1);
    }
    nMat=0;
    if((spectra=fopen(list->refFiles[mat][1],"r"))==NULL){
      fprintf(stderr,"Error opening material data output %s\n",list->refFiles[mat][1]);
      exit(1);
    }
    /*get all spectral information*/
    while(fgets(line,200,spectra)!=NULL){
      if(sscanf(line,"%s",hash)!=1){
        fprintf(stderr,"Badly formatted waveband file\n");
        exit(1);
      }
      if(strncasecmp(hash,"#",1)){
        if(sscanf(line,"%f %f",&wave[nMat],&refl[nMat])!=2) {
          fprintf(stderr, "Badly formatted waveband file\n");
          exit(1);
        }
        nMat++;
      }
    }
printf("%d\n",nMat);
    /*find the closest wavelength*/
    for(band=0;band<list->nBands;band++){
      i=0;
      while(wave[i]<list->wavelength[band]&&i<nMat)i++;
      if(i>0){
        if((wave[i]-list->wavelength[band])*(wave[i]-list->wavelength[band])<\
          (wave[i-1]-list->wavelength[band])*(wave[i-1]-list->wavelength[band]))list->albedo[mat][band]=refl[i];
        else                                                                    list->albedo[mat][band]=refl[i-1];
      }else                                                                     list->albedo[mat][band]=refl[i];
printf("%f\n",list->albedo[mat][band]);
    }
    if(spectra){
      fclose(spectra);
      spectra=NULL;
    }
  }
  TIDY(refl);
  TIDY(wave);

for(mat=2;mat<list->matN+2;mat++){
  for(band=0;band<list->nBands;band++){
    printf("%d %d",mat,band);
    fflush(stdout);
    printf(" %f\n",list->albedo[mat][band]);
  }
}

  return(list);
}

listing *readWavebands(char *bandFile,listing *list)
{
  int i=0;
  char line[200],guff[100];
  float *temp=NULL;
  FILE *wave=NULL;

  if((wave=fopen(bandFile,"r"))==NULL){
    fprintf(stderr,"Error opening material data output %s\n",bandFile);
    exit(1);
  }
  if(!(temp=(float *)calloc(1000,sizeof(float)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }

  while(fgets(line,200,wave)!=NULL){
    if(sscanf(line,"%s %f",guff,&temp[list->nBands])!=2) {
      fprintf(stderr, "Badly formatted waveband file\n");
      exit(1);
    }
    if(strncasecmp(guff,"#",1))list->nBands++;
  }
  if(!(list->wavelength=(float *)calloc(list->nBands,sizeof(float)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }
  for(i=0;i<list->nBands;i++){
    list->wavelength[i]=temp[i];
  }
  TIDY(temp);
  if(wave){
    fclose(wave);
    wave=NULL;
  }

  return(list);
}


listing *readPlants(char *plantFile)
{
  int i=0,matN=0;
  char line[200];
  char guff1[100],**guff2=NULL,**guff3=NULL;
  FILE *plants=NULL;
  listing *list=NULL;

  if(!(list=(listing *)calloc(1,sizeof(listing)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }


  if((plants=fopen(plantFile,"r"))==NULL){
    fprintf(stderr,"Error opening material data output %s\n",plantFile);
    exit(1);
  }

  if(!(guff2=(char **)calloc(100,sizeof(char *)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }
  if(!(guff3=(char **)calloc(100,sizeof(char *)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }
  for(i=0;i<100;i++){
    if(!(guff2[i]=(char *)calloc(100,sizeof(char)))){
      fprintf(stderr,"error in material LUT allocation.\n");
      exit(1);
    }
    if(!(guff3[i]=(char *)calloc(100,sizeof(char)))){
      fprintf(stderr,"error in material LUT allocation.\n");
      exit(1);
    }
  }

  while(fgets(line,200,plants)!=NULL){
    if(sscanf(line,"%s %s %s",guff1,guff2[matN],guff3[matN])!=3) {
      fprintf(stderr, "Badly formatted material file\n");
      exit(1);
    }
    if(strncasecmp(guff1,"#",1))matN++;
  }
  if(!(list->refFiles=(char ***)calloc(matN+2,sizeof(char **)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }

  for(i=0;i<matN;i++){
    if(!(list->refFiles[i+2]=(char **)calloc(2,sizeof(char *)))){
      fprintf(stderr,"error in material LUT allocation.\n");
      exit(1);
    }
    if(!(list->refFiles[i+2][0]=(char *)calloc(strlen(guff2[i])+1,sizeof(char)))){
      fprintf(stderr,"error in material LUT allocation.\n");
      exit(1);
    }
    if(!(list->refFiles[i+2][1]=(char *)calloc(strlen(guff3[i])+1,sizeof(char)))){
      fprintf(stderr,"error in material LUT allocation.\n");
      exit(1);
    }
    strcpy(list->refFiles[i+2][0],guff2[i]);
    strcpy(list->refFiles[i+2][1],guff3[i]);
    TIDY(guff2[i]);
    TIDY(guff3[i]);
  }  
  TIDY(guff2);
  TIDY(guff3);

  if(plants){
    fclose(plants);
    plants=NULL;
  }

  list->matN=matN;

  return(list);
}
