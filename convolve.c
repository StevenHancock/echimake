/*##################################################################*/
/*#  Contains all deconvolution related funtions needed by         #*/
/*#  echiMake.c.  It could even become a library one day           #*/
/*#  7th November 2007, Steven Hancock, UCL Geography              #*/
/*##################################################################*/
#include "echiMake.h"



/*##################################################################*/
/*deconvolve by FFT*/

void deconvolveFFT(RatControl *ratPoint,RatImage *dimage,int numb,int Lplace)
{
  /*unsigned long int i=0;*/
  int length=0,band=0,n=0;
  double test=0;
  double *image=NULL,*pulse=NULL;
  double *deconvolved=NULL;
  void unarrange(RatControl *,RatImage *,int,int,int,double *,int,int);
  void combineMultiScat(RatImage *,RatControl *,int,int);
  double *goldsMethod(double *,double *,int,int,double,RatImage *,RatControl *,double);
  double *janssonsMethod(double *,double *,int,int,double,RatImage *,RatControl *,double);
  double *reArrange(RatControl *,RatImage *,int,int,int,int,int);
  double *arrangePulse(RatControl *,RatImage *,int,int,double);
  double *blurPulse(double *,double,double,int);
  char checkLight(RatImage *,RatControl *,int,int,int,int);
  char light=0;
  int tempn=0;

/*
 # FILE *opoo=NULL;
 # int i=0;
 # opoo=fopen("decont","w");
*/

  if(dimage->idealFile&&(numb>0))return;  /*if this is the ideal file don't deconvolve it*/


  if(dimage->deconOrders){
    combineMultiScat(dimage,ratPoint,numb,Lplace);
    tempn=ratPoint[numb].n;
    ratPoint[numb].n=2;
  }
  if(dimage->sdecon){
    dimage->Plength=ratPoint[numb].Plength[0];
    dimage->Pres=ratPoint[numb].Pres;
    dimage->EpLength=ratPoint[numb].EpLength[0];
  }

  if(dimage->Plength<0){printf("Bad pulse length %f\n",dimage->Plength);exit(1);}
  /*quick pulse length and resolution check*/
  if(dimage->Plength>0&&(ratPoint[numb].bin_L<dimage->Pres)){
    printf("This doesn't work with a pulse resolution less than range resolution bin %f pulse %f\n",ratPoint[numb].bin_L,dimage->Pres);
    printf("This is beasuase of the resampling\n");
    exit(1);
  }


  /*select a suitable length, sampled at pulse resolution*/
  length=(int)((double)ratPoint[numb].bins*ratPoint[numb].bin_L/dimage->Pres+0.5);
  test=log((double)length)/log(2.0)-(int)(log((double)length)/log(2.0)+0.5);
  if(test>THRESHOLD||test<-THRESHOLD){
    printf("Padding from %d to ",length);
    length=pow(2.0,(float)((int)(log((double)ratPoint[numb].bins*ratPoint[numb].bin_L/dimage->Pres)/log(2.0)+0.5)+1));
  }
  printf("Length %d for deconvolution...\n",length);

  pulse=arrangePulse(ratPoint,dimage,length,numb,0.0);
  /*blur pulse is waveform is smoothed*/
  if(dimage->conlength>0.0)pulse=blurPulse(pulse,(double)dimage->conlength,(double)dimage->Pres,length);
  if(dimage->noilength>0.0)pulse=blurPulse(pulse,(double)dimage->noilength,(double)dimage->Pres,length);

  for(band=0;band<dimage->nBands;band++){

    light=checkLight(dimage,ratPoint,numb,Lplace,band,0);

    if(light){

      image=reArrange(ratPoint,dimage,numb,Lplace,band,length,0);

      if(!dimage->jansson)deconvolved=goldsMethod(image,pulse,length,dimage->diters,ratPoint[numb].bin_L/(double)dimage->Pres,dimage,ratPoint,ratPoint[numb].maxAmp[Lplace][band]);
      else                deconvolved=janssonsMethod(image,pulse,length,dimage->diters,ratPoint[numb].bin_L/(double)dimage->Pres,dimage,ratPoint,ratPoint[numb].maxAmp[Lplace][band]);

      printf(" Band %d of %d done\n",band+1,dimage->nBands);

/*
#      printf("bin %f res %f\n",ratPoint[numb].bin_L,dimage->Pres);
#      for(i=1;i<=length;i++){
#        fprintf(opoo,"%f %f %f %f\n",(double)i*dimage->Pres-ratPoint[numb].min_R,\
#          image[2*i-1],deconvolved[2*i-1],pulse[2*i-1]);
#      }
*/

      unarrange(ratPoint,dimage,numb,Lplace,band,deconvolved,length,0);

      TIDY(deconvolved);
      TIDY(image);
    }/*light check*/
  }
  /*FILE *ooo=NULL;*/
  /*repeat for multplte scattering if required*/
  if(dimage->deconOrders){
    for(band=0;band<dimage->nBands;band++){
      for(n=1;n<3;n++){
        light=checkLight(dimage,ratPoint,numb,Lplace,band,n);

        if(light){
          if(image){printf("What?\n");exit(1);}
          image=reArrange(ratPoint,dimage,numb,Lplace,band,length,n);

          if(!dimage->jansson)deconvolved=goldsMethod(image,pulse,length,dimage->diters,ratPoint[numb].bin_L/(double)dimage->Pres,dimage,ratPoint,-1);
          else                deconvolved=janssonsMethod(image,pulse,length,dimage->diters,ratPoint[numb].bin_L/(double)dimage->Pres,dimage,ratPoint,ratPoint[numb].maxAmp[Lplace][band]);
          /*
          #if(n==1){
          #ooo=fopen("stewie","w");
          #for(i=0;i<length;i++)fprintf(ooo,"%d %f %f\n",i,image[2*i+1],deconvolved[2*i+1]);
          #if(ooo)fclose(ooo);ooo=NULL;
          #}
          */
          printf(" Band %d of %d done\n",band+1,dimage->nBands);

          unarrange(ratPoint,dimage,numb,Lplace,band,deconvolved,length,0);

          TIDY(deconvolved);
          TIDY(image);
        }/*light check*/
      }
    }
  }/*multiple scattering*/

  /*if(opoo)fclose(opoo);*/

  TIDY(pulse);
  TIDY(deconvolved);

  if(dimage->deconOrders){
    ratPoint[numb].n=tempn;
  }

  return;
}/*deconvolveFFT*/

/*#######################################################################*/
/*Fast Fourier transform, from numerical recipes*/
#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr

void fourier(double *data,unsigned long nn,int isign)
{
  unsigned long n,mmax,m,j,istep,i;
  double wtemp,wr,wpr,wpi,wi,theta;
  double tempr,tempi;
  double test=0;

  /*check nn is suitable*/
  test=log((double)nn)/log(2.0)-(int)(log((double)nn)/log(2.0)+0.5);
  if(test>THRESHOLD&&test<-THRESHOLD){
    printf("The array is not an even power of 2 long, %f\n",log((double)nn)/log(2.0)-(int)(log((double)nn)/log(2.0)+0.5));
    exit(1);
  }

  n=nn << 1;
  j=1;
  for (i=1;i<n;i+=2) {
    if (j > i) {
      SWAP(data[j],data[i]);
      SWAP(data[j+1],data[i+1]);
    }
    m=n >> 1;
    while (m >= 2 && j > m) {
      j -= m;
      m >>= 1;
    }
    j += m;
  }
  mmax=2;
  while (n > mmax) {
    istep=mmax << 1;
    theta=isign*(6.28318530717959/mmax);
    wtemp=sin(0.5*theta);
    wpr = -2.0*wtemp*wtemp;
    wpi=sin(theta);
    wr=1.0;
    wi=0.0;
    for (m=1;m<mmax;m+=2) {
      for (i=m;i<=n;i+=istep) {
        j=i+mmax;
        tempr=wr*data[j]-wi*data[j+1];
        tempi=wr*data[j+1]+wi*data[j];
        data[j]=data[i]-tempr;
        data[j+1]=data[i+1]-tempi;
        data[i] += tempr;
        data[i+1] += tempi;
      }
      wr=(wtemp=wr)*wpr-wi*wpi+wr;
      wi=wi*wpr+wtemp*wpi+wi;
    }
    mmax=istep;
  }
} /*fourier*/
#undef SWAP

/*##########################################################*/
/*##################################################################*/
/*deconvolve using Gold's method*/

double *goldsMethod(double *i,double *pulse,int end,int iters,double bin_L,RatImage *dimage,RatControl *ratPoint,double maxAmp)
{
  int j=0,real=0,imag=0;
  double *deconvolved=NULL,*o=NULL;
  double *workspace1=NULL,*w=NULL;
  double norm=0,start=0; /*,last=0;*/
  double error=0,refl=0;                 /*for convergence check*/
  double totalCon=0,totalIdeal=0;        /*for convergence check*/
  int place=0,bin=0,binScaler=0,k=0;     /*for convergence check*/
  float statusscale=0;
  /*char convergence=0;*/
  char namen[200];
  void fourier(double *,unsigned long,int);
  int count=0;

  double thresh=0,max=0;/*############## REMOVE BEFORE USE ###########*/
  FILE *tracker=NULL;
   if(dimage->convergeCheck){
     sprintf(namen,"%s.tracker",dimage->output);
     if((tracker=fopen(namen,"w"))==NULL){
      printf("Error opening deconvolved data file\n");
      exit(1);
    }
  }
 

  if(bin_L==0.0)bin_L=1.0;  /*needed for normalisation, though I can't think why*/

  /*now see if we can deconvolve*/
  if(!(o=(double *)calloc(2*end+1,sizeof(double)))){
    printf("error in results allocation 2.\n");
    exit(1);
  }
  if(!(workspace1=(double *)calloc(2*end+1,sizeof(double)))){
    printf("error in results allocation 2.\n");
    exit(1);
  }
  if(!(w=(double *)calloc(2*end+1,sizeof(double)))){
    printf("error in results allocation 2.\n");
    exit(1);
  }
  if(!(deconvolved=(double *)calloc(2*end+1,sizeof(double)))){
    printf("error in results allocation 2.\n");
    exit(1);
  }
  for(j=1;j<=2*end;j++)o[j]=i[j];

  fourier(pulse,(unsigned long)end,1);

  for(j=1;j<=2*end;j++) workspace1[j]=o[j];

  /*the status indicator*/
  for(j=0;j<100;j++)printf("|");
  printf(" deconvolution status\n");
  statusscale=(float)iters/100.0;


  if(dimage->convergeCheck){
    if(dimage->idealFile){   /*read the ideal file*/
      ratPoint=resultsArrange(dimage,ratPoint,1,0,0);
      binScaler=(int)(ratPoint[1].bin_L/dimage->Pres);
      /*Find the true signal start from material information*/
    }else{                   /*find blurred signal start*/
      /*# Old method using no apriori information
      #for(j=1;j<=end;j++){
      #  if(i[2*j-1]>0.0){
      #   start=(double)j/2.0*bin_L;
      #    break;
      #  } 
      #}*/
      /*get signal stary from material information*/
      start=-1.0;
      for(bin=0;bin<ratPoint[0].bins;bin++){
        for(j=0;j<ratPoint[0].nMat;j++){
          place=findMaterialIndex(dimage,ratPoint,0,bin,j,0,0);
          if(ratPoint[0].ratRes[0].material[place]>0.0){
            start=(double)bin*ratPoint[0].bin_L;
            break;
          }
        }
        if(start>0.0)break;
      }/*bin loop*/
    }
  }/*convergence check startup*/

  do{  /*the reblurring*/
    fourier(workspace1,(unsigned long)end,1);
    for(j=1;j<=end;j++){   /*calculate workspace2*/
      real=2*j-1;
      imag=2*j;
      w[real]=workspace1[real]*pulse[real]-workspace1[imag]*pulse[imag];
      w[imag]=workspace1[imag]*pulse[real]+workspace1[real]*pulse[imag];
    }
    fourier(w,(unsigned long)end,-1);
    for(j=1;j<=end;j++){
      real=2*j-1;
      imag=2*j;
      w[real]/=end;
      w[imag]/=end;
      norm=(w[real]*w[real]+w[imag]*w[imag]);
      deconvolved[real]=(w[real]*(o[real]*i[real]-o[imag]*i[imag])+w[imag]*(o[imag]*i[real]+o[real]*i[imag]))/norm;
      deconvolved[imag]=(w[real]*(o[imag]*i[real]+o[real]*i[imag])+w[imag]*(o[imag]*i[imag]-o[real]*i[real]))/norm;
      o[real]=deconvolved[real];
      o[imag]=deconvolved[imag];
      workspace1[real]=o[real];
      workspace1[imag]=o[imag];
    }
    if(count>0){  /*a progress report*/
      if((float)count/statusscale-(int)((float)count/statusscale)==0.0)printf("|");
      fflush(stdout);
    }else{
      printf("|");
      fflush(stdout);
    }

    if(maxAmp>-1.0){   /*then check maximum amplitude for convergence*/
      for(j=1;j<=end;j++){
        real=2*j-1;
        imag=2*j;
        if(deconvolved[real]/bin_L>=maxAmp){
          count=iters;  /*we've reached the end, force exit*/
        }
      }
    }/*amplitude convergence checker*/

    if(dimage->convergeCheck){  /*check the convergence of the file*/
      /*#convergence=0;
      #for(j=1;j<=end;j+=2){
      #  if(deconvolved[j]>0.0)break;
      #}
      #fprintf(stdout,"converge %f %f %f %e\n",start,(double)j*bin_L,last,(double)j*bin_L-last);
      #//if(start-(double)j*bin_L>=15000.0)convergence=0;
      #//if(sqrt(((double)j*bin_L-last)*((double)j*bin_L-last))<0.0000000000003)convergence=1;
      #//else 
      #last=(double)j*bin_L;
      #if(convergence){
      #  count=iters+1;
      #}*/
      if(dimage->idealFile){  /*calculate RMSE from ideal file*/
        /*#### REMOVE BEFORE USE ####*/
	/*bit of a hack, ensure total energy is preserved*/
        totalCon=totalIdeal=0.0;
	for(j=1;j<=end;j++)totalCon+=sqrt(deconvolved[2*j-1]*deconvolved[2*j-1]+deconvolved[2*j]*deconvolved[2*j]);
        for(bin=0;bin<ratPoint[1].bins;bin++){
          place=findReflIndex(dimage,ratPoint,1,bin,0,0);
          totalIdeal+=ratPoint[1].ratRes[0].refl[place];
        }
        /*Hack ends*/

        error=refl=0.0;
        bin=k=0;
        for(j=1;j<=end;j++){
          k=(int)((double)(j-1)*dimage->Pres/ratPoint[1].bin_L+0.5);
          if(k>=ratPoint[1].bins)break;
          if(bin!=k){
            refl*=totalIdeal/totalCon;
            place=findReflIndex(dimage,ratPoint,1,bin,0,0);
            //fprintf(tracker,"%d %f %f\n",bin,refl,ratPoint[1].ratRes[0].refl[place]);
            error+=sqrt((refl-ratPoint[1].ratRes[0].refl[place])*(refl-ratPoint[1].ratRes[0].refl[place]));
            refl=0.0;
            bin=k;
          }
          refl+=sqrt(deconvolved[2*j-1]*deconvolved[2*j-1]+deconvolved[2*j]*deconvolved[2*j]);
        }
        fprintf(tracker,"%d %f\n",count,error);
      }else{      /*see how far back the leading edge has shifted*/
        max=0.0;
        for(j=1;j<=2*end;j++)if(deconvolved[2*j-1]>max)max=deconvolved[2*j-1];
        thresh=max*0.001;
        for(j=1;j<=2*end;j++)if(deconvolved[2*j-1]>thresh){
          fprintf(tracker,"%d %f\n",count,(double)(j-1)*dimage->Pres-start);
          break;
        }
      }
    }/*convergence check*/

    count++;
  }while(count<iters);
  TIDY(workspace1);
  TIDY(w);
  TIDY(o);

  if(dimage->idealFile){  /*clear the ideal waveform*/
    ratPoint=clearArrays(ratPoint,1,0,dimage);
  }
  if(tracker){
    fclose(tracker);
    tracker=NULL;
  }

  fourier(pulse,(unsigned long)end,-1);
  for(j=1;j<=2*end;j++){   /*rescale the energy*/
    deconvolved[j]/=bin_L;
    pulse[j]/=end;
  }

  return(deconvolved);
}/*goldsMethod*/

/*#######################################################################*/
/*deconvolve by Jansson's method*/

double *janssonsMethod(double *i,double *pulse,int end,int iters,double bin_L,RatImage *dimage,RatControl *ratPoint,double maxAmp)
{
  int j=0,real=0,imag=0;
  double *deconvolved=NULL,*o=NULL;
  double *workspace1=NULL,*w=NULL;
  double r=0,energy=0;
  float statusscale=0;
  int count=0;
  void fourier(double *,unsigned long,int);

  r=10.0;

  for(j=1;j<=end;j++)energy+=i[j];

  if(bin_L==0.0)bin_L=1.0;  /*needed for normalisation, though I can't think why*/

  /*now see if we can deconvolve*/
  if(!(o=(double *)calloc(2*end+1,sizeof(double)))){
    printf("error in results allocation 2.\n");
    exit(1);
  }
  if(!(workspace1=(double *)calloc(2*end+1,sizeof(double)))){
    printf("error in results allocation 2.\n");
    exit(1);
  }
  if(!(w=(double *)calloc(2*end+1,sizeof(double)))){
    printf("error in results allocation 2.\n");
    exit(1);
  }
  if(!(deconvolved=(double *)calloc(2*end+1,sizeof(double)))){
    printf("error in results allocation 2.\n");
    exit(1);
  }
  for(j=1;j<=2*end;j++)o[j]=i[j];

  /*the status indicator*/
  for(j=0;j<100;j++)printf("|");
  printf(" deconvolution status\n");
  statusscale=(float)iters/100.0;

  do{  /*the reblurring*/
    for(j=1;j<=2*end;j++) workspace1[j]=o[j];
    fourier(workspace1,(unsigned long)end,1);
    for(j=1;j<=end;j++){   /*calculate workspace2*/
      real=2*j-1;
      imag=2*j;
      w[real]=workspace1[real]*pulse[real]-workspace1[imag]*pulse[imag];
      w[imag]=workspace1[imag]*pulse[real]+workspace1[real]*pulse[imag];
    }
    fourier(w,(unsigned long)end,-1);
    for(j=1;j<=end;j++){
      real=2*j-1;
      imag=2*j;
      w[real]/=end;
      w[imag]/=end;
      if(o[real]<=(0.5*maxAmp))r=2.0*o[real]/energy;
      else                     r=2.0*(1.0-1.0*o[real]/energy);
      deconvolved[real]=o[real]*r*(i[real]-w[real]);
      deconvolved[imag]=o[imag]*r*(i[imag]-w[imag]);
      o[real]=deconvolved[real];
      o[imag]=deconvolved[imag];
    }
    if(count>0){  /*a progress report*/
      if((float)count/statusscale-(int)((float)count/statusscale)==0.0)printf("|");
      fflush(stdout);
    }else{
      printf("|");
      fflush(stdout);
    }
    count++;
  }while(count<iters);

  for(j=1;j<=2*end;j++)deconvolved[j]/=bin_L;

  TIDY(workspace1);
  TIDY(w);
  TIDY(o);

  return(deconvolved);
}/*janssonsMethod*/

/*######################################################################*/
/*extend pulse function by any smoothing function, returns Fourier space*/

double *blurPulse(double *pulse,double conlength,double Pres,int length)
{
  int i=0;
  double *blurredPulse=NULL,*smoother=NULL;
  double sigma=0,total=0;
  void fourier(double *,unsigned long,int);

  sigma=conlength/4.0;

  /*all arrays are complex*/
  smoother=dalloc(2*length+1,"pulse smoother",0);
  blurredPulse=dalloc(2*length+1,"blurred pulse",0);

  for(i=1;i<=length;i++){
    smoother[2*i-1]=gaussian((double)i*Pres,sigma,0.0)+gaussian((double)i*Pres,sigma,(double)length*Pres);
    smoother[2*i]=0.0;
    total+=smoother[2*i-1];
  }
  /*rescale to taste*/
  if(total!=1.0)for(i=1;i<=length;i++)smoother[2*i-1]/=total;

  fourier(pulse,(unsigned long)length,1);
  fourier(smoother,(unsigned long)length,1);

  for(i=1;i<=length;i++){
    blurredPulse[2*i-1]=pulse[2*i-1]*smoother[2*i-1]-pulse[2*i]*smoother[2*i];
    blurredPulse[2*i]=pulse[2*i-1]*smoother[2*i]+pulse[2*i]*smoother[2*i-1];
  }

  TIDY(smoother);
  TIDY(pulse);

  /*should check energy once more, but done at end anyway. Leave out for efficiency*/

  fourier(blurredPulse,(unsigned long)length,-1);
  for(i=1;i<=2*length;i++)blurredPulse[i]/=(double)length;

  return(blurredPulse);
}/*blurPulse*/


/*#############################################################*/
/*prepare a waveform for deconvolution*/
/*end is packed with zeros if it is the wrong length, should not affect results*/

double *reArrange(RatControl *ratPoint,RatImage *dimage,int numb,int Lplace,int band,int length,int order)
{
  int i=0,bin=0,place=0;
  double *image=NULL;  /*the complex waveform*/

  if(length<(int)((double)ratPoint[numb].bins*ratPoint[numb].bin_L/dimage->Pres+0.5)){
    printf("What's going on here... length %d, bins %d?\n",length,\
      (int)((double)ratPoint[numb].bins*ratPoint[numb].bin_L/dimage->Pres+0.5));
    exit(1);
  }

  if(!(image=(double *)calloc(2*length+10,sizeof(double)))){
    fprintf(stderr,"error in visible image buffer allocation number 1.\n");
    exit(1);
  }

  for(i=1;i<=length;i++){
    if(2*i>2*length){printf("balls 2\n");exit(1);}
    image[2*i-1]=0.0;
    image[2*i]=0.0;
  }

  for(bin=0;bin<ratPoint[numb].bins;bin++){
    i=(int)((double)bin*ratPoint[numb].bin_L/dimage->Pres+0.5)+1;
    if(i>0&&i<=length){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,order);
      image[2*i-1]=ratPoint[numb].ratRes[Lplace].refl[place];
    }
  }

  return(image);
}/*reArrange, for FFT*/

/*#############################################################*/
/*unpack complex deconvolved array into refl*/

void unarrange(RatControl *ratPoint,RatImage *dimage,int numb,int Lplace,int band,double *deconvolved,int length,int order)
{
  int bin=0,i=0,place=0;
  int resRatio=0;
  double oriEnergy=0,deconEnergy=0;
  double energyScale=0;

  resRatio=ratPoint[numb].bin_L/dimage->Pres;

  /*first scale the two to conserve energy*/
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    place=findReflIndex(dimage,ratPoint,numb,bin,band,order);
    oriEnergy+=ratPoint[numb].ratRes[Lplace].refl[place];
  }
  if(oriEnergy<=0.0){printf("What? %f\n",oriEnergy);exit(1);}
  for(i=1;i<=length;i++)deconEnergy+=deconvolved[2*i-1];

  for(bin=0;bin<ratPoint[numb].bins;bin++){
    place=findReflIndex(dimage,ratPoint,numb,bin,band,order);
    ratPoint[numb].ratRes[Lplace].refl[place]=0.0;
  }

  for(i=1;i<=length;i++){
    bin=(int)((double)(i-1)*dimage->Pres/ratPoint[numb].bin_L+0.5);
    if(bin>=ratPoint[numb].bins)break; /*to cope with the padding*/
    place=findReflIndex(dimage,ratPoint,numb,bin,band,order);
    ratPoint[numb].ratRes[Lplace].refl[place]+=deconvolved[2*i-1];
  }
  if(deconEnergy!=oriEnergy&&oriEnergy>0.0){           /*rescale energy if needed*/
    energyScale=deconEnergy/oriEnergy;  /*later for efficiency*/
    /*printf("Deconvolved or convolved waveform must be rescaled by %f\n",energyScale);*/
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,order);
      ratPoint[numb].ratRes[Lplace].refl[place]/=energyScale;
    }
  }else if(oriEnergy<0.0){
    fprintf(stderr,"original energy mistake %f\n",oriEnergy);
    exit(1);
  }

  return;
}/*unarrange*/

/*#############################################################*/
/*prepare the pulse for deconvolution*/

double *arrangePulse(RatControl *ratPoint,RatImage *dimage,int length,int numb,double sigma)
{
  unsigned long int i=0;
  double *pulse=NULL;
  double gaussian(double,double,double);
  double x=0,energy=0,max=0;

/*FILE *opoo=NULL;
opoo=fopen("jtull","w");*/

  if(!(pulse=(double *)calloc(2*length+1,sizeof(double)))){
    fprintf(stderr,"error in visible image buffer allocation number 1.\n");
    exit(1);
  }

  if(sigma==0.0){       /*so that different widths can be used to convolve and deconvolve*/
    if(dimage->Plength>0&&!dimage->sdecon){
      sigma=(double)dimage->Plength/4.0;

      for(i=1;i<=length;i++){
        x=(double)((double)(i-1)*(double)dimage->Pres);
        pulse[2*i-1]=(dimage->funk(x,sigma,0.0)+dimage->funk(x,sigma,(double)length*(double)dimage->Pres))*(double)dimage->Pres;
        pulse[2*i]=0.0;                                                            /*imaginary*/
      }
    }else if(ratPoint[numb].Plength[0]>0.0){  /*use the one from the data file*/
      for(i=1;i<2*length;i++)pulse[i]=0.0;
      /*# for(i=0;i<ratPoint[numb].EpLength[0];i++){
        #pulse[2*i-1]=ratPoint[numb].pulse[i-1];
      #}*/

      /*## really this should depend upon whether the pulse is an even or odd number of elements long ##*/

      for(i=0;i<ratPoint[numb].EpLength[0];i++){
        if(i>=ratPoint[numb].EpLength[0]/2){   /*put to front of pulse array*/
          if(2*(i-ratPoint[numb].EpLength[0]/2+1)-1>0&&2*(i-ratPoint[numb].EpLength[0]/2+1)-1<=2*length){
            pulse[2*(i-ratPoint[numb].EpLength[0]/2+1)-1]=(double)ratPoint[numb].pulse[0][i];
          }else{
            fprintf(stderr,"The pulse is leaking\n"); /* %d of %d lead %d EpLength %d\n",(int)(2*(i-(int)ratPoint[numb].EpLength[0]/2+1)-1),(int)(2*length),i,(int)(ratPoint[numb].EpLength[0]));*/
            exit(1);
          }
        }else{                              /*put to back of pulse array*/
          if(2*(length-(int)ratPoint[numb].EpLength[0]/2+i)-1>0&&2*(length-(int)ratPoint[numb].EpLength[0]/2+i)-1<=2*length){
            pulse[2*(length-ratPoint[numb].EpLength[0]/2+i)-1]=(double)ratPoint[numb].pulse[0][i];
          }else{
            fprintf(stderr,"The pulse is leaking\n"); /* %d of %d trail\n",2*(length-(int)ratPoint[numb].EpLength[0]/2+i)-1,i);*/
            exit(1);
          }
        }
      }
      /*for(i=1;i<length;i++)fprintf(opoo,"%ld %f\n",i,pulse[2*i-1]);*/
    }else{
      printf("You're not being clear about the pulse in file %d\n",numb);
      exit(1);
    }
  }else{    /*create our own pulse*/
    for(i=1;i<=length;i++){
      x=(double)((double)(i-1)*(double)dimage->Pres);
      pulse[2*i-1]=dimage->funk(x,sigma,0.0)+dimage->funk(x,sigma,(double)length*(double)dimage->Pres);
      if(pulse[2*i-1]>max)max=pulse[2*i-1];
      /*pulse[2*i-1]=(dimage->funk(x,sigma,0.0)+dimage->funk(x,sigma,(double)length*(double)dimage->Pres))*(double)dimage->Pres;*/
      pulse[2*i]=0.0;                                                            /*imaginary*/
      /*then truncate the pulse*/
      if(pulse[2*i-1]<max*0.00001)pulse[2*i-1]=0.0;
    }
  }

  /*ensure the energy is 1*/
  for(i=1;i<=length;i++)energy+=pulse[2*i-1];
  if(energy!=1.0&&energy>0.0){
    /*printf("rescaling pulse by %f\n",1.0/energy);*/
    for(i=1;i<=length;i++)pulse[2*i-1]/=energy;
  }else if(energy<=0.0){
    fprintf(stderr,"Error in pulse energy %f\n",energy);
    exit(1);
  }
  /*if(opoo)fclose(opoo);*/
  return(pulse);
}/*arrangePulse, for FFT*/

/*##################################################################################*/
/*see if there is a return in this band for FFT*/

char checkLight(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace,int band,int order)
{
  int bin=0,place=0;
  char light=0;

  for(bin=0;bin<ratPoint[numb].bins;bin++){
    place=findReflIndex(dimage,ratPoint,numb,bin,band,order);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>0.0){
      light=1;
      break;
    }
  }
  return(light);
}/*checkLight*/

/*###################################################################################*/
/*resave deconvolved data to a new file*/

void saveDeconvolved(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int Lplace=0,tScans=0;
  int arrLength=0,marrLength=0;
  int *compLeng=NULL,*header1=NULL;
  double *header2=NULL;
  long int *coords=NULL;
  double *jimlad=NULL;
  float *matlad=NULL;
  char intLidar=0,namen[100];
  int *intHeader(RatImage *,RatControl *,char,int);
  double *doHeader(RatControl *,RatImage *,char,int);
  FILE *dopoo=NULL;
  int encodeRefl(RatControl *,int,int,double *,int);
  int encodeMaterial(RatControl *,int,int,float *,int);

  if((dimage->Plength==0&&!dimage->sdecon) && (dimage->coarsen==1.0)){
    printf("The option \"-save\" is redundant if you're not deconvolving or coarsening\n");
    return;
  }
  sprintf(namen,"%s.decon.data",dimage->output);
  if((dopoo=fopen(namen,"wb"))==NULL){
    printf("Error opening deconvolved data file\n");
    exit(1);
  }
  printf("Writting out deconvolved data to %s\n",namen);
  if(namen)free(namen);

  tScans=ratPoint[numb].NAscans*ratPoint[numb].NZscans;

  if(!(coords=(long int *)calloc(3*tScans,sizeof(long int)))){
    fprintf(stderr,"error in deconvolved data coordinate array allocation.\n");
    exit(1);
  }
  if(!(compLeng=(int *)calloc(2*tScans,sizeof(int)))){
    fprintf(stderr,"error in deconvolved compression length array allocation.\n");
    exit(1);
  }
  if(!dimage->deconOrders){
    arrLength=ratPoint[numb].bins*ratPoint[numb].nBands*(ratPoint[numb].n+1)*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs);
    marrLength=(ratPoint->bins*ratPoint->nMat*2+1)*(ratPoint->withinSegs+ratPoint->withoutSegs);
  }else{
    arrLength=ratPoint[numb].bins*ratPoint[numb].nBands*3*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs);
    marrLength=(ratPoint->bins*ratPoint->nMat*2+1)*(ratPoint->withinSegs+ratPoint->withoutSegs);
  }

  /*arrange the int header*/
  if(!ratPoint[numb].encoded){printf("nooooo\n");exit(1);}       /*this format will not work yet*/
  else if(ratPoint[numb].encoded&&ratPoint[numb].headerLength==12){
    intLidar=1;
  }else if(ratPoint[numb].encoded&&ratPoint[numb].headerLength==9){
    intLidar=0;
  }

  /*set up the headers*/
  header1=intHeader(dimage,ratPoint,intLidar,numb);
  header2=doHeader(ratPoint,dimage,intLidar,numb);

  if(dimage->byteord){
    header1=intSwap(header1,ratPoint[numb].headerLength);
    header2=doSwap(header2,ratPoint[numb].header2Length);
  }

  for(Lplace=0;Lplace<tScans;Lplace++){

    printf("Deconvolving beam %d of %d\n",Lplace+1,tScans);

    ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);

    if(!(jimlad=(double *)calloc(arrLength+2,sizeof(double)))){
      printf("error in binary results allocation.\n");
      exit(1);
    }

    compLeng[2*Lplace]=encodeRefl(ratPoint,numb,Lplace,jimlad,arrLength);

    if(!(matlad=(float *)calloc(marrLength+1,sizeof(float)))){
      printf("error in binary material results allocation.\n");
      exit(1);
    }
    compLeng[2*Lplace+1]=encodeMaterial(ratPoint,numb,Lplace,matlad,marrLength);

    /*now work out the compression map*/
    if(Lplace>0){
      coords[Lplace*3]=(int)(coords[3*(Lplace-1)+2]+(int)sizeof(float));
    }else if(Lplace==0){
      coords[Lplace*3]=(int)(ratPoint[numb].headerLength*sizeof(int)+ratPoint[numb].header2Length*sizeof(double)+\
        ratPoint->NZscans*ratPoint->NAscans*3*sizeof(int));
    }
    coords[3*Lplace+1]=(int)(coords[3*Lplace]+compLeng[2*Lplace]*(int)sizeof(double));
    coords[3*Lplace+2]=(int)(coords[3*Lplace+1]+compLeng[2*Lplace+1]*(int)sizeof(float));

    /*write the data to the new file*/
    if(dimage->byteord){   /*if the computer is little endian swap bytes before writting*/
      jimlad=doSwap(jimlad,compLeng[2*Lplace]);
      matlad=floSwap(matlad,compLeng[2*Lplace+1]);
    }
    if(fseek(dopoo,coords[3*Lplace],SEEK_SET)){printf("fseek error for deconvolved refl %d to %d\n",Lplace,(int)coords[3*Lplace]);exit(1);}
    if(fwrite(&(jimlad[0]),sizeof(double),compLeng[2*Lplace],dopoo)!=compLeng[2*Lplace]){
      printf("Lidar data not written %d\n",Lplace);
      exit(1);
    }
    if(fseek(dopoo,coords[3*Lplace+1],SEEK_SET)){printf("fseek error for matlad %d to %d\n",Lplace,(int)coords[3*Lplace+1]);exit(1);}
    if(fwrite(&(matlad[0]),sizeof(float),compLeng[2*Lplace+1],dopoo)!=compLeng[2*Lplace+1]){
      printf("Material data not written %d\n",Lplace);
      exit(1);
    }

    ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    TIDY(jimlad);
    TIDY(matlad);
  }/*beam step*/

  /*then write out the headers and maps to end*/
  if(dimage->byteord)coords=lintSwap(coords,3*tScans);
  if(fseek(dopoo,(long int)(ratPoint[numb].headerLength*sizeof(int)+ratPoint[numb].header2Length*sizeof(double)),SEEK_SET)){
    printf("fseek error for compression header\n");exit(1);}
  if(fwrite(&(coords[0]),sizeof(int),3*tScans,dopoo)!=3*tScans){
    printf("error writting compression map\n");
    exit(1);
  }
  if(fseek(dopoo,(long int)(ratPoint[numb].headerLength*sizeof(int)),SEEK_SET)){printf("fseek error for double header\n");exit(1);}
  if(fwrite(&(header2[0]),sizeof(double),ratPoint[numb].header2Length,dopoo)!=ratPoint[numb].header2Length){
    printf("Data double header not written\n");
    exit(1);
  }
  if(fseek(dopoo,0,SEEK_SET)){printf("fseek error for int header\n");exit(1);}
  if(fwrite(&(header1[0]),sizeof(int),ratPoint[numb].headerLength,dopoo)!=ratPoint[numb].headerLength){
    printf("Data int header not written\n");
    exit(1);
  }

  TIDY(header1);
  TIDY(header2);
  TIDY(coords);

  if(dopoo){
    fclose(dopoo);
    dopoo=NULL;
  }
  return;
}/*saveDeconvolved*/

/*###########################################################*/
/*convolve a waveform, for the testing of deconvolution*/

void convolveFFT(RatControl *ratPoint,RatImage *dimage,int numb,int Lplace,double pLength)
{
  int band=0,i=0;
  long int length=0;
  void unarrange(RatControl *,RatImage *,int,int,int,double *,int,int);
  void fourier(double *,unsigned long,int);
  double *reArrange(RatControl *,RatImage *,int,int,int,int,int);
  double *arrangePulse(RatControl *,RatImage *,int,int,double);
  double *pulse=NULL,test=0;
  double *image=NULL,*convolved=NULL;
  char checkLight(RatImage *,RatControl *,int,int,int,int);
  char light=0;

  if(dimage->idealFile)return;

  if(dimage->Pres==0.0)dimage->Pres=ratPoint[numb].bin_L/6.0;
  /*select a suitable length, sampled at pulse resolution*/
  length=(int)((double)ratPoint[numb].bins*ratPoint[numb].bin_L/dimage->Pres+0.5);
  test=log((double)length)/log(2.0)-(int)(log((double)length)/log(2.0)+0.5);
  if(test>THRESHOLD||test<-THRESHOLD){
    printf("Padding from %ld to ",length);
    length=pow(2.0,(float)((int)(log((double)ratPoint[numb].bins*ratPoint[numb].bin_L/dimage->Pres)/log(2.0)+0.5)+1));
  }
  printf("%ld for convolution...\n",length);

  pulse=arrangePulse(ratPoint,dimage,length,numb,pLength/4.0);
  fourier(pulse,length,1);

  convolved=dalloc(2*length+1,"convolved waveform",numb);
  for(band=0;band<dimage->nBands;band++){

    light=checkLight(dimage,ratPoint,numb,Lplace,band,0);

    if(light){

      image=reArrange(ratPoint,dimage,numb,Lplace,band,length,0);
      fourier(image,length,1);

      for(i=1;i<=length;i++){
        convolved[2*i-1]=image[2*i-1]*pulse[2*i-1]-image[2*i]*pulse[2*i];
        convolved[2*i]=image[2*i-1]*pulse[2*i]+image[2*i]*pulse[2*i-1];
      }
      fourier(convolved,length,-1);
      for(i=1;i<=2*length;i++){
        convolved[i]/=(double)length;
      }

      printf(" Band %d of %d done\n",band+1,dimage->nBands);

      unarrange(ratPoint,dimage,numb,Lplace,band,convolved,length,0);

      TIDY(image);
    }/*light check*/
  }/*band loop*/

  TIDY(convolved);
  TIDY(pulse);

  return;
}/*convolveFFT*/

/*##########################################################*/
/*define the pulse shape*/

void setPulseLength(RatImage *dimage)
{
  int i=0,place=0,halfPoint=0;
  float E=0;
  double sigma=0;
  double gaussian(double,double,double);

  sigma=(double)dimage->Plength/4.0;

  for(i=0;i<100000;i++){
    E=(float)dimage->funk((double)i*(double)dimage->Pres,sigma,0);
    if(E<=0.0000001){
      dimage->EpLength=2*i;
      halfPoint=i;
      break;
    }
  }

  if(!(dimage->pulse=(float *)calloc(dimage->EpLength,sizeof(float)))){
    printf("error in pulse shape allocation.\n");
    exit(1);
  }
  for(i=0;i<dimage->EpLength;i++){
    place=i-halfPoint;
    dimage->pulse[i]=(float)dimage->funk((double)place*(double)dimage->Pres,sigma,0);
  }
  return;
}/*setPulseLength*/

/*###########################################################*/
/*set up the int header and return an indicator*/

int *intHeader(RatImage *dimage,RatControl *ratPoint,char intLidar,int numb)
{
  int *header1=NULL;

  /*for deconvolved pulses there is no pulse length at all*/
  if(!(header1=(int *)calloc(ratPoint[numb].headerLength,sizeof(int)))){
    printf("error in binary header allocation.\n");
    exit(1);
  }
  /*arrange the int header*/
  if(!ratPoint[numb].encoded){printf("nooooo\n");exit(1);}       /*this format will not work yet*/
  else if(ratPoint[numb].encoded&&(ratPoint[numb].headerLength==12||ratPoint[numb].headerLength==13)){
    intLidar=1;
    header1[0]=16;  /*there is now no format 12*/
  }else if(ratPoint[numb].encoded&&(ratPoint[numb].headerLength==9||ratPoint[numb].headerLength==10)){
    intLidar=0;
    header1[0]=17;
  }

  if(intLidar){     /*integer lidar*/
    header1[1]=(int)ratPoint[numb].min_R;
    header1[2]=(int)ratPoint[numb].max_R;
    header1[3]=(int)ratPoint[numb].bin_L;
    header1[4]=ratPoint[numb].bins;
    header1[5]=ratPoint[numb].NZscans;
    header1[6]=ratPoint[numb].NAscans;
    if(!dimage->deconOrders){
      header1[7]=ratPoint[numb].n;
    }else header1[7]=2;
    header1[8]=dimage->nBands;
    header1[9]=ratPoint[numb].nMat;
    header1[10]=ratPoint[numb].withinSegs;
    header1[11]=ratPoint[numb].withoutSegs;
    header1[12]=0;
  }else if(!intLidar){ /*double lidar*/
    header1[1]=ratPoint[numb].bins;
    header1[2]=ratPoint[numb].NZscans;
    header1[3]=ratPoint[numb].NAscans;
    if(!dimage->deconOrders){
      header1[4]=ratPoint[numb].n;
    }else header1[4]=2;
    header1[5]=dimage->nBands;
    header1[6]=ratPoint[numb].nMat;
    header1[7]=ratPoint[numb].withinSegs;
    header1[8]=ratPoint[numb].withoutSegs;
    if(dimage->deconOrders)header1[9]=0;
    else                   header1[9]=ratPoint[numb].EpLength[0];
  }

  return(header1);
}/*intHeader*/

/*########################################################################*/
/*set up the double header*/

double *doHeader(RatControl *ratPoint,RatImage *dimage,char intLidar,int numb)
{
  int j=0;
  double *header2=NULL;

  if(!(header2=(double *)calloc(ratPoint[numb].header2Length,sizeof(double)))){
    printf("error in binary results allocation.\n");
    exit(1);
  }
  /*binary header*/
  if(intLidar){
    for(j=0;j<3;j++){
      header2[j]=ratPoint->from[j];
    }
    header2[3]=ratPoint[numb].Ldiv;
    header2[4]=ratPoint[numb].div;
    header2[5]=ratPoint[numb].divSt;
    header2[6]=ratPoint[numb].iZen;
    header2[7]=ratPoint[numb].fZen;
    header2[8]=ratPoint[numb].iAz;
    header2[9]=ratPoint[numb].fAz;
    for(j=10;j<10+dimage->nBands;j++){
      header2[j]=ratPoint[numb].wavelength[dimage->band[numb][j-10]];
    }
    header2[10+ratPoint[numb].nBands]=0.0;
    header2[11+ratPoint[numb].nBands]=0.0;
  }else if(!intLidar){
    header2[0]=ratPoint[numb].min_R;
    header2[1]=ratPoint[numb].max_R;
    header2[2]=ratPoint[numb].bin_L;
    for(j=0;j<3;j++){
      header2[j+3]=ratPoint[numb].from[j];
    }
    header2[6]=ratPoint[numb].Ldiv;
    header2[7]=ratPoint[numb].div;
    header2[8]=ratPoint[numb].divSt;
    header2[9]=ratPoint[numb].iZen;
    header2[10]=ratPoint[numb].fZen;
    header2[11]=ratPoint[numb].iAz;
    header2[12]=ratPoint[numb].fAz;
    for(j=13;j<31+dimage->nBands;j++){
      header2[j]=ratPoint[numb].wavelength[dimage->band[numb][j-13]];
    }
    if(dimage->deconOrders){  /*if it is deconvolved*/
      header2[13+ratPoint[numb].nBands]=0.0;
      header2[14+ratPoint[numb].nBands]=0.0;
    }else{
      header2[13+ratPoint[numb].nBands]=ratPoint[numb].Plength[0];
      header2[14+ratPoint[numb].nBands]=ratPoint[numb].Pres;
    }
  }
  return(header2);
}/*doHeader*/

/*###########################################################################*/
/*combine multiple scattering into a single array*/

void combineMultiScat(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace)
{
  int bin=0,n=0,band=0;
  double *refl=NULL;

  if(!(refl=(double *)calloc(ratPoint[numb].bins*ratPoint[numb].nBands*3,sizeof(double)))){
    fprintf(stderr,"error in scatter combination array allocation.\n");
    exit(1);
  }
  for(n=0;n<ratPoint[numb].bins*ratPoint[numb].nBands*3;n++)refl[n]=0.0;

  for(bin=0;bin<ratPoint[numb].bins;bin++){
    for(band=0;band<dimage->nBands;band++){
      refl[(bin*ratPoint[numb].nBands+dimage->band[numb][band])*3]=ratPoint[numb].ratRes[Lplace].\
        refl[(bin*ratPoint[numb].nBands+dimage->band[numb][band])*(ratPoint[numb].n+1)];
      for(n=1;n<=ratPoint[numb].n;n++){
        refl[(bin*ratPoint[numb].nBands+dimage->band[numb][band])*3+1]+=ratPoint[numb].ratRes[Lplace].\
        refl[(bin*ratPoint[numb].nBands+dimage->band[numb][band])*(ratPoint[numb].n+1)+n];
      }
    }
  }
  TIDY(ratPoint[numb].ratRes[Lplace].refl);
  if(!(ratPoint[numb].ratRes[Lplace].refl=(double *)calloc(ratPoint[numb].bins*ratPoint[numb].nBands*3,sizeof(double)))){
    fprintf(stderr,"error in scatter combination array allocation.\n");
    exit(1);
  }
  for(n=0;n<ratPoint[numb].bins*ratPoint[numb].nBands*3;n++)ratPoint[numb].ratRes[Lplace].refl[n]=refl[n];
  TIDY(refl);
  return;
}/*combineMultiScat*/

/*############################################################################*/
/*A low pass filter for noise reduction in signals with known pulses*/

void lowPassFilter(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace)
{
  int i=0,band=0;
  long int length=0;
  int tgate=0,lgate=0;
  double test=0;
  double *clipped=NULL,*pulse=NULL;
  double *reArrange(RatControl *,RatImage *,int,int,int,int,int);
  double *arrangePulse(RatControl *,RatImage *,int,int,double);
  void unarrange(RatControl *,RatImage *,int,int,int,double *,int,int);


  /*select a suitable length, sampled at pulse resolution*/
  length=(int)((double)ratPoint[numb].bins*ratPoint[numb].bin_L/dimage->Pres+0.5);
  test=log((double)length)/log(2.0)-(int)(log((double)length)/log(2.0)+0.5);
  if(test>THRESHOLD||test<-THRESHOLD){
    length=pow(2.0,(float)((int)(log((double)ratPoint[numb].bins*ratPoint[numb].bin_L/dimage->Pres)/log(2.0)+0.5)+1));
  }

  /*find the clip frequency*/
  pulse=arrangePulse(ratPoint,dimage,length,numb,0.0);
  fourier(pulse,(unsigned long)length,1);
  for(i=length/2;i>0;i--){  /*find max positive frequency*/
    if(pulse[2*i-1]>0.0){
      tgate=(int)((float)i*dimage->clip)+1;
      if(tgate>length/2)tgate=length/2;
      else if(tgate<=0)tgate=0;
      break;
    }
  }
  for(i=length/2+1;i<=length;i++){ /*and max negative frequency*/
    if(pulse[2*i-1]>0.0){
      lgate=length-(int)((float)(length-i)*dimage->clip);
      if(lgate>length)lgate=length;
      else if(lgate<=length/2)lgate=length/2+1;
      break;
    }
  }
  printf("%ld %d %d\n",length,tgate,lgate);

  if(!(clipped=(double *)calloc(2*length+1,sizeof(double)))){
    fprintf(stderr,"error in scatter combination array allocation.\n");
    exit(1);
  }

  for(band=0;band<dimage->nBands;band++){
    clipped=reArrange(ratPoint,dimage,numb,Lplace,band,length,0);
    fourier(clipped,(unsigned long)length,1);
    for(i=tgate;i<=length/2;i++){  /*clip high frequencies*/
      clipped[2*i-1]=0.0;
      clipped[2*i]=0.0;
    }
    for(i=lgate;i>length/2;i--){
      clipped[2*i-1]=0.0;
      clipped[2*i]=0.0;
    }
    fourier(clipped,(unsigned long)length,-1);
    for(i=1;i<=length;i++){
      clipped[2*i-1]/=(double)length;
      if(clipped[2*i-1]<0.0)clipped[2*i-1]=0.0;
    }
    unarrange(ratPoint,dimage,numb,Lplace,band,clipped,length,0);
  }/*band loop*/

  TIDY(pulse);
  TIDY(clipped);

  return;
}/*lowPassFilter*/

/*########################################################################*/
/*Deconvolve by convolution with a Laplacian*/

void convolveLaplace(RatControl *ratPoint,RatImage *dimage,int numb,int Lplace)
{
  int i=0,band=0;
  unsigned long int length=0;
  double test=0,max=0;
  double *pulse=NULL;
  double *image=NULL,*convolved=NULL;
  double *Laplacian=NULL;   /*the Laplacian operator*/
  double gaussecond(double,double,double);
  void unarrange(RatControl *,RatImage *,int,int,int,double *,int,int);
  void fourier(double *,unsigned long,int);
  double *reArrange(RatControl *,RatImage *,int,int,int,int,int);
  char checkLight(RatImage *,RatControl *,int,int,int,int);
  char light=0;

  printf("#\n#\nDeconvolving by convolution with the Laplacian\n#\n#\n");
	  
  if(dimage->Pres==0.0)dimage->Pres=ratPoint[numb].bin_L/6.0;
  /*select a suitable length, sampled at pulse resolution*/
  length=(int)((double)ratPoint[numb].bins*ratPoint[numb].bin_L/dimage->Pres+0.5);
  test=log((double)length)/log(2.0)-(int)(log((double)length)/log(2.0)+0.5);
  if(test>THRESHOLD||test<-THRESHOLD){
    printf("Padding from %d to ",(int)length);
    length=pow(2.0,(float)((int)(log((double)ratPoint[numb].bins*\
      ratPoint[numb].bin_L/dimage->Pres)/log(2.0)+0.5)+1));
  }
  printf("%ld for convolution...\n",length);
	  
  /*calculate Laplacian of pulse*/
  Laplacian=dalloc(ratPoint[numb].EpLength[0],"Laplacian",numb);
  Laplacian[0]=0.0;
  Laplacian[ratPoint[numb].EpLength[0]-1]=0.0;
  printf("## %d\n",ratPoint[numb].EpLength[0]);
  max=0.0;
  for(i=1;i<ratPoint[numb].EpLength[0]-1;i++){
    Laplacian[i]=-1.0*gaussecond((double)i*ratPoint[numb].Pres,(double)ratPoint[numb].Plength[0]/4.0,(double)ratPoint[numb].EpLength[0]*ratPoint[numb].Pres/2.0);
    if(Laplacian[i]>max)max=Laplacian[i];
                   /*(double)(ratPoint[numb].pulse[i-1]+ratPoint[numb].pulse[i+1]-\
                     4.0*ratPoint[numb].pulse[i])/ratPoint[numb].Pres;*/
  }   /*then rescale to have a maximum of 1*/
/*#  for(i=1;i<ratPoint[numb].EpLength[0]-1;i++){
  #  Laplacian[i]/=max;
  #}*/

  /*arrange Laplacian into a complex array*/
  pulse=dalloc(2*length+1,"Laplacian of pulse",numb);
  for(i=0;i<2*length+1;i++)pulse[i]=0.0;
  
  for(i=0;i<ratPoint[numb].EpLength[0];i++){
    if(i>=ratPoint[numb].EpLength[0]/2){   /*put to front of pulse array*/
      if(2*(i-ratPoint[numb].EpLength[0]/2+1)-1>0&&2*(i-ratPoint[numb].EpLength[0]/2+1)-1<=2*length){
        pulse[2*(i-ratPoint[numb].EpLength[0]/2+1)-1]=Laplacian[i];
      }else{
        fprintf(stderr,"The pulse is leaking %d of %d lead %d EpLength %d\n",\
	  (int)(2*(i-(int)ratPoint[numb].EpLength[0]/2+1)-1),(int)(2*length),i,(int)ratPoint[numb].EpLength[0]);
        exit(1);
      }
    }else{                              /*put to back of pulse array*/
      if(2*(length-(int)ratPoint[numb].EpLength[0]/2+i)-1>0&&\
	 2*(length-(int)ratPoint[numb].EpLength[0]/2+i)-1<=2*length){
        pulse[2*(length-ratPoint[numb].EpLength[0]/2+i)-1]=(double)Laplacian[i];
      }else{
        fprintf(stderr,"The pulse is leaking %d of %d trail\n",\
	  (int)(2*(length-(int)ratPoint[numb].EpLength[0]/2+i)-1),i);
        exit(1);
      }
    }
  }
  TIDY(Laplacian);
  fourier(pulse,length,1);



  
  convolved=dalloc(2*length+1,"convolved waveform",numb);
  for(band=0;band<dimage->nBands;band++){

    light=checkLight(dimage,ratPoint,numb,Lplace,band,0);

    if(light){

      image=reArrange(ratPoint,dimage,numb,Lplace,band,length,0);
      fourier(image,length,1);

      for(i=1;i<=length;i++){
        convolved[2*i-1]=image[2*i-1]*pulse[2*i-1]-image[2*i]*pulse[2*i];
        convolved[2*i]=image[2*i-1]*pulse[2*i]+image[2*i]*pulse[2*i-1];
      }
      fourier(convolved,length,-1);
      for(i=1;i<=2*length;i++){
        convolved[i]/=(double)length;
      }

      printf(" Band %d of %d done\n",band+1,dimage->nBands);

      unarrange(ratPoint,dimage,numb,Lplace,band,convolved,length,0);

      TIDY(image);
    }/*light check*/
  }/*band loop*/


  TIDY(pulse);
  TIDY(convolved);
  
  return;
}/*convolveLaplace*/


/*#################################################################################*/
/*find maximum ampltiude by fitting largest possible pulse within data*/

void maxDeconvolvedAmp(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace)
{
  int bin=0,k=0,place=0,band=0;
  double sigma=0,amp=0,offset=0;
  double x=0,y=0;
  double maxAmp=0,energy=0;
  double sTep=0,thresh=0;
  char tooBig=0,above=0;


  thresh=0.00000001;

  if(dimage->sdecon){
    dimage->Plength=ratPoint[numb].Plength[0];
    dimage->Pres=ratPoint[numb].Pres;
    dimage->EpLength=ratPoint[numb].EpLength[0];
  }
  sigma=(double)dimage->Plength/4.0;

  if(!ratPoint[numb].maxAmp[Lplace]){
    ratPoint[numb].maxAmp[Lplace]=dalloc(dimage->nBands,"deconvolution criterion",Lplace);
  }

  if(dimage->conlength>0.0){  /*need to broaden the pulse*/
    sigma+=(double)dimage->conlength/4.0;
  }


  for(band=0;band<dimage->nBands;band++){


    /*centre a Gaussian here, move centre along*/
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      //printf("bin %d ",bin);
      offset=(double)bin*ratPoint[numb].bin_L;
      /*first guess*/
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      amp=ratPoint[numb].ratRes[Lplace].refl[place];
      sTep=amp/2.0;
      above=1;

      /*iterate to get the right size*/
      do{
        tooBig=0;
        for(k=0;k<ratPoint[numb].bins;k++){
          x=(double)k*ratPoint[numb].bin_L;
          y=amp*gaussian(x,sigma,offset)*(sigma*sqrt(2.0*M_PI));
          place=findReflIndex(dimage,ratPoint,numb,k,band,0);
          if((y-ratPoint[numb].ratRes[Lplace].refl[place])>thresh){
            tooBig=1;
            break;
          }
        }/*check for protrusions*/


        if(tooBig){
          if(amp==0.0)break;
          if(!above)sTep/=2.0;
          amp-=sTep;
          above=1;
          if(amp<0.0)amp=0.0;
        }else{
          if(above)sTep/=2.0;
          amp+=sTep;
          above=0;
        }
      }while((sTep>0.000001)&&(amp>=0.0));/*amplitude adjusting loop*/
       if(amp>maxAmp)maxAmp=amp;
       //printf("%f %f %f\n",maxAmp,amp,(double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R);

    }/*Gaussian centre loop*/

    /*calculate energy of largest Gaussian*/
    energy=0.0;
    offset=(double)(ratPoint[numb].bins/2)*ratPoint[numb].bin_L;
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      x=(double)bin*ratPoint[numb].bin_L;
      energy+=maxAmp*gaussian(x,sigma,offset)*(sigma*sqrt(2.0*M_PI));
    }


    printf("Maximum amplitude probably %f\n",energy);
    ratPoint[numb].maxAmp[Lplace][band]=energy; //0.014; //energy;
  }/*band loop*/

  return;
}/*maxDeconvolvedAmp*/


/*###########################################################################################*/
