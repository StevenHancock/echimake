/*###############################################*/
/*# Tools for dealing with imaging lidar files  #*/
/*# Supplementary to echiMake                   #*/
/*# Steven Hancock UCL  17th January 2008       #*/
/*###############################################*/
#include "echiMake.h"

void IlyRead(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int i=0,j=0;
  int headerLength=0,decheadLen=0;
  int *header=NULL;
  double *decHeader=NULL;
  int *codeMap=NULL;
  int mapLength=0;
  int *headRead(RatImage *,int,int);
  int nx=0,ny=0;
  int InPsf[2],CnPsf[2];     /*number of elements in illumination and camera psfs*/
  double imagingPlaneDim[2],focalLength=0;
  double focalAxis[3],PoyntingZen=0,PoyntingAz=0;
  double x=0,y=0,alpha=0,beta=0,twist=0;
  void stateIntent(RatImage *,RatControl *,int,int);

  headerLength=1;

  header=headRead(dimage,numb,headerLength);
  if(header[0]!=50){
    printf("not an imaging lidar file\n");
    exit(1);
  }

  TIDY(header);
  headerLength=5;
  header=headRead(dimage,numb,headerLength);
  if(header[1]!=sizeof(int)){

  }
  if(header[2]!=sizeof(float)){
  
  }
  if(header[3]!=sizeof(double)){

  }
  headerLength=header[4];
  header=headRead(dimage,numb,headerLength);

  ratPoint[numb].bins=header[5];
  ratPoint[numb].NZscans=header[6];
  ratPoint[numb].NAscans=header[7];
  InPsf[0]=header[8];
  InPsf[1]=header[9];


  ratPoint[numb].n=header[10];
  ratPoint[numb].nBands=header[11];
  ratPoint[numb].nMat=header[12];
  decheadLen=header[13];

  /*
  #header[15]=light[camN].Nx;
  #header[16]=light[camN].Ny;
  */

  ratPoint[numb].EpLength=header[17];
printf("Pulse length %d\n",ratPoint[numb].EpLength);

  /*
  #for(i=0;i<2;i++)header[18+i]=camera[camN].nPsf[i];
  #header[20]=camera[camN].cameraType;
  */


  /*the double header*/
  decHeader=dalloc(decheadLen,"decimal header",0);
  ratPoint[numb].wavelength=dalloc(ratPoint[numb].nBands,"wavelength",numb);

  if(fseek(dimage->data[numb],headerLength*sizeof(int),SEEK_SET)){
    printf("fseek error for double header %d\n",numb);
    exit(1);
  }
  if(fread(&(decHeader[0]),sizeof(double),decheadLen,dimage->data[numb])!=decheadLen){
    printf("error reading decimal header %d\n",numb);
    exit(1);
  }
  if(dimage->byteord)decHeader=doSwap(decHeader,decheadLen);

  ratPoint[numb].from[0]=decHeader[0];
  ratPoint[numb].from[1]=decHeader[1];
  ratPoint[numb].from[2]=decHeader[2];

  /*
  #decHeader[3]=camera[camN].principalPoint.x;
  #decHeader[4]=camera[camN].principalPoint.y;
  #decHeader[5]=camera[camN].principalPoint.z;
  #decHeader[6]=camera[camN].cameraX.x;
  #decHeader[7]=camera[camN].cameraX.y;
  #decHeader[8]=camera[camN].cameraX.z;
  #decHeader[9]=camera[camN].cameraY.x;
  #decHeader[10]=camera[camN].cameraY.y;
  #decHeader[11]=camera[camN].cameraY.z;
  */

  ratPoint[numb].min_R=decHeader[12];
  ratPoint[numb].max_R=decHeader[13];
  ratPoint[numb].bin_L=decHeader[14];

  twist=decHeader[15];
  ratPoint[numb].iZen=decHeader[16];
  ratPoint[numb].iAz=decHeader[17];

  /*
  decHeader[18]=camera[camN].aspectRatio;
  for(i=0;i<2;i++)decHeader[19+i]=camera[camN].fov[i];
  decHeader[21]=camera[camN].fovMax;
  */
  focalAxis[0]=decHeader[22];
  focalAxis[1]=decHeader[23];
  focalAxis[2]=decHeader[24];
  focalLength=decHeader[25];

  imagingPlaneDim[0]=decHeader[26];
  imagingPlaneDim[1]=decHeader[27];

  ratPoint[numb].Pres=decHeader[28];
  /*
  decHeader[29]=camera[camN].pulseStart;
  */

  for(i=0;i<ratPoint[numb].nBands;i++)ratPoint[numb].wavelength[i]=decHeader[30+i];


  ratPoint[numb].headerLength=headerLength;
  ratPoint[numb].header2Length=decheadLen;

  if(!dimage->teast){
    if(dimage->intent)stateIntent(dimage,ratPoint,numb,header[0]);
  }

  /*  header printing for debugging
  for(i=0;i<headerLength;i++)printf("%d %d\n",i,header[i]);
  for(i=0;i<decheadLen;i++)printf("%d %f\n",i,decHeader[i]);
  */

  TIDY(header);
  TIDY(decHeader);

  if(!(ratPoint[numb].ratRes=(RatResults *)calloc(ratPoint[numb].NZscans*ratPoint[numb].NAscans,sizeof(RatResults)))){
    printf("error in results allocation %d.\n",numb);
    exit(1);
  }

  /*derive rest of the control information*/
  ratPoint[numb].max_R=ratPoint[numb].min_R+ratPoint[numb].bin_L*(double)ratPoint[numb].bins;
  ratPoint[numb].withinSegs=0;
  ratPoint[numb].withoutSegs=1;
  ratPoint[numb].encoded=1;
  nx=ratPoint[numb].NAscans;
  ny=ratPoint[numb].NZscans;
  ratPoint[numb].theta=dalloc(nx*ny,"zenith allocation",numb);
  ratPoint[numb].thata=dalloc(nx*ny,"azimuth allocation",numb);
  if(focalAxis[2]>0.0)PoyntingZen=atan2(sqrt(focalAxis[0]*focalAxis[0]+focalAxis[1]*focalAxis[1]),focalAxis[2])-M_PI;
  else PoyntingZen=M_PI/2,0;
  if(focalAxis[0]>0.0)PoyntingAz=atan2(focalAxis[1],focalAxis[0]);
  else if(focalAxis[1]>0.0)PoyntingAz=0.0;
  else if(focalAxis[1]<0.0)PoyntingAz=M_PI;
  else if(focalAxis[1]==0.0)PoyntingAz=0.0;
  for(i=0;i<nx;i++){
    x=((double)i/(double)nx-0.5)*imagingPlaneDim[0];
    for(j=0;j<ny;j++){
      y=((double)j/(double)ny-0.5)*imagingPlaneDim[1];
      alpha=atan2(x*x+y*y,focalLength);
      beta=atan2(y,x)+twist;
      ratPoint[numb].theta[j*nx+i]=alpha+PoyntingZen;
      ratPoint[numb].thata[j*nx+i]=beta+PoyntingAz;
    }
  }


  mapLength=3*ratPoint[numb].NZscans*ratPoint[numb].NAscans;
  codeMap=ialloc(mapLength,"code map",numb);
  if(fseek(dimage->data[numb],headerLength*sizeof(int)+decheadLen*sizeof(double),SEEK_SET)){
    printf("fseek error for code map %d\n",numb);
    exit(1);
  }
  if(fread(&(codeMap[0]),sizeof(int),mapLength,dimage->data[numb])!=mapLength){
    printf("error reading code mape %d\n",numb);
    exit(1);
  }
  if(dimage->byteord)codeMap=intSwap(codeMap,mapLength);
  mapLength=ratPoint[numb].NZscans*ratPoint[numb].NAscans;  /*renumber for speed of loops*/
  for(i=0;i<mapLength;i++){
    ratPoint[numb].ratRes[i].coords[0]=(long int)codeMap[3*i];
    ratPoint[numb].ratRes[i].compLeng[0]=(long int)((codeMap[3*i+1]-codeMap[3*i])/sizeof(double));
    if((codeMap[3*i+1]-codeMap[3*i])%sizeof(double)){printf("Warning\n");exit(1);}
    ratPoint[numb].ratRes[i].coords[1]=(long int)codeMap[3*i+1];
    ratPoint[numb].ratRes[i].coords[2]=(long int)codeMap[3*i+2];
    ratPoint[numb].ratRes[i].compLeng[1]=(long int)((codeMap[3*i+2]-codeMap[3*i+1])/sizeof(float));
    if((codeMap[3*i+2]-codeMap[3*i+1])%sizeof(float)){printf("Warning\n");exit(1);}
  }
  TIDY(codeMap);

  /*read the pulse*/
  if(ratPoint[numb].EpLength>0){ /*then read the pulse in*/
    double *pulse=NULL;
    
    if(!(pulse=(double *)calloc(ratPoint[numb].EpLength,sizeof(double)))){
      fprintf(stderr,"error in pulse %d allocation.\n",numb);
      exit(1);
    }
    if(fseek(dimage->data[numb],(long)(ratPoint[numb].ratRes[ratPoint[numb].NAscans*ratPoint[numb].NZscans-1].coords[2]+sizeof(double)),SEEK_SET)){\
      printf("fseek error to %ld\n",ratPoint[numb].ratRes[ratPoint[numb].NAscans*ratPoint[numb].NZscans-1].coords[3]);exit(1);}
    if(fread(&(pulse[0]),sizeof(double),ratPoint[numb].EpLength,dimage->data[numb])!=ratPoint[numb].EpLength){
      printf("error reading angle %d",i);
      exit(1);
    }
    if(!(ratPoint[numb].pulse=(float *)calloc(ratPoint[numb].EpLength,sizeof(float)))){
      fprintf(stderr,"error in pulse %d allocation.\n",numb);
      exit(1);
    }
    for(i=0;i<ratPoint[numb].EpLength;i++)ratPoint[numb].pulse[i]=(float)pulse[i];
    TIDY(pulse);
  }else if(dimage->Plength<0&&ratPoint[numb].EpLength==0){
    printf("The file contains no pulse shape, can't use \"-deconvolve -ve\"\n");
    exit(1);
  }/*pulse reading*/


  /*illumination psf*/
  if(InPsf[0]*InPsf[1]){
    double *iPsf=NULL;

    if(!(iPsf=(double *)calloc(InPsf[0]*InPsf[1],sizeof(double)))){
      fprintf(stderr,"error in pulse %d allocation.\n",numb);
      exit(1);
    }
    if(fseek(dimage->data[numb],(long)(ratPoint[numb].ratRes[ratPoint[numb].NAscans*ratPoint[numb].NZscans-1].coords[2]+\
     2*sizeof(double)+ratPoint[numb].EpLength*sizeof(double)),SEEK_SET)){
      printf("fseek error to %ld\n",ratPoint[numb].ratRes[ratPoint[numb].NAscans*ratPoint[numb].NZscans-1].coords[3]);
      exit(1);
    }
    if(fread(&(iPsf[0]),sizeof(double),InPsf[0]*InPsf[1],dimage->data[numb])!=InPsf[0]*InPsf[1]){
      printf("error reading illumination psf %d\n",i);
      exit(1);
    }
    TIDY(iPsf);   /*free it up for now as I have no idea how to use it*/
  }

  /*camera psf*/
  if(ratPoint[numb].NZscans*ratPoint[numb].NAscans){
    double *cPsf=NULL;

    if(!(cPsf=(double *)calloc(ratPoint[numb].NZscans*ratPoint[numb].NAscans,sizeof(double)))){
      fprintf(stderr,"error in pulse %d allocation.\n",numb);
      exit(1);
    }
    if(fseek(dimage->data[numb],(long)(ratPoint[numb].ratRes[ratPoint[numb].NAscans*ratPoint[numb].NZscans-1].coords[2]+\
     2*sizeof(double)+ratPoint[numb].EpLength*sizeof(double)+InPsf[0]*InPsf[1]*sizeof(double)),SEEK_SET)){
      printf("fseek error to %ld\n",ratPoint[numb].ratRes[ratPoint[numb].NAscans*ratPoint[numb].NZscans-1].coords[3]);
      exit(1);
    }
    if(fread(&(cPsf[0]),sizeof(double),ratPoint[numb].NZscans*ratPoint[numb].NAscans,dimage->data[numb])!=ratPoint[numb].NZscans*ratPoint[numb].NAscans){
      printf("error reading camera psf %d\n",i);
      exit(1);
    }

    TIDY(cPsf);
  }else{printf("You what?\n");}

  return;
}/*IlyRead*/

/*########################################################################################*/
/*Read the integer header of ilydar files*/

int *headRead(RatImage *dimage,int numb,int headerLength)
{
  int *header=NULL;

  header=(int *)ialloc(headerLength,"integer header",0);
  if(fseek(dimage->data[numb],0,SEEK_SET)){
    printf("fseek error for integer header %d\n",numb);
    exit(1);
  }
  if(fread(&(header[0]),sizeof(int),headerLength,dimage->data[numb])!=headerLength){
    printf("error reading angle\n");
    exit(1);
  }
  if(dimage->byteord)header=intSwap(header,headerLength);
  return(header);
}/*headread*/

/*########################################################################################*/
