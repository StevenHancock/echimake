# Makefile for iter_thresh
#
# Turn on debugging
# CFLAGS = -O
# STRING = -lmartstring.sun4u
#LEWIS_LIB = /home/plewis/bpms/src
LEWIS_LIB = ../libRAT/src

LOCAL_OTHERS = voxMake.o analMake.o convolve.o # fitting.o IlyMake.o
TOOLS = tools
TOOLDIR=../headers
NRdir=../recipes
NR= $(NRdir)/f1dim.o $(NRdir)/brent.o  $(NRdir)/mnbrak.o  $(NRdir)/powell.o $(NRdir)/linmin.o $(NRdir)/nrutil.o
NRhere=f1dim.o brent.o mnbrak.o powell.o linmin.o nrutil.o

HOME = /Users/dill
#LIBS = -lm -lmatrix_${ARCH} -lalloc_${ARCH} -lvect_${ARCH} -lerr_${ARCH} -lhipl_${ARCH} -lrand_${ARCH} -lc
LIBS = -lm -lhipl_${ARCH} -ltiff
INCLS = /usr/include -I/usr/local/include -I../headers -I${LEWIS_LIB}/lib -I${HOME}/src/starat #-I$(NRdir) 
CFLAGS +=  -L${LEWIS_LIB}/lib -DDOUBLEDEF  -DMATCHK -D${ARCH} -D_NO_NAG
LIBRARY = ${LEWIS_LIB}
CFLAGS += -Wall
CFLAGS += -g
#CFLAGS += -O3

CC = gcc
#CC= /opt/SUNWspro/bin/cc

#CFLAGS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64

THIS=echiMake

$(THIS):	$(THIS).o $(LOCAL_OTHERS)     # $(NR) $(TOOLDIR)/$(TOOLS).o
		$(CC) $(CFLAGS) $@.o $(LOCAL_OTHERS) -o $@ $(LIBS) $(CFLAGS) -I$(INCLS)   # $(NRhere)

.c.o:		$<
		$(CC) $(CFLAGS) -I. -I$(INCLS) -D$(ARCH)  -c $<

clean:
		\rm -f *% *~ *.o #$(TOOLDIR)/*.o

install:
		touch $(HOME)/bin/$(ARCH)/$(THIS)
		mv $(HOME)/bin/$(ARCH)/$(THIS) $(HOME)/bin/$(ARCH)/$(THIS).old
		cp $(THIS) $(HOME)/bin/$(ARCH)/$(THIS)


			
