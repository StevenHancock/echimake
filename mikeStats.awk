{
  band[NR]=$1;
  count=0;
  maxF[NR]=NF;
  for(i=2;i<NF;i++){
    refl[NR]+=$i;
    flux[NR,count]=$i;
    count++
  }
  if(count)refl[NR]/=count;
}
END{
  for(i=0;i<NR;i++){
    /*caluclate standard deviation*/
    sar=0;
    for(j=0;j<maxF[i];j++){
      sar+=(refl[i]-flux[i,j])*(refl[i]-flux[i,j]);
    }
    if(maxF[i]){
      stdev=sqrt(sar/maxF[i]);
      print band[i],refl[i],stdev;
    }
  }
}
