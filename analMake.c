/*######################################################################################*/
/*#This is a program for reading the output of starat, an echidna simulating ray tracer#*/
/*#It doesn't have many options as yet, it will be built on a function at a time       #*/
/*# 25th October 2006              Steven Hancock, Geography, UCL                      #*/
/*######################################################################################*/

/*#####################################################################################################*/
#include "echiMake.h"
#include "tiffio.h"

/*This function sorts the hemispherical data into a square hips image*/
RatImage *sortHemisphere(RatControl *ratPoint,int numb,RatImage *dimage)
{
  int i=0,j=0,k=0,l=0,m=0,n=0,p=0;             /*loop control integers*/
  double thetaP=0,thataP=0;                    /*pixel angular coordinates*/
  double maxThe=0,minThe=0,maxTha=0,minTha=0;  /*beam boundaries*/
  float Cscale=0;                              /*relates colour and range*/
  int x=0,y=0;                                 /*cartesian coordinates of pixels*/
  int first=0,last=-1,returns=0;               /*for scaling colour image*/
  int band=0;  /*this added on 14th August 2008, other than that 2006*/
  char mode=0;
  char foundHit=0;     /*to stop pulse length intensity getting diluted*/
  void writeTiffFloat(float *,int,int,int,char *,char *);
  void writeTiffUChar(unsigned char *,int,int,int,char *,char *);


  mode=1;   /*hard wire to tiff for now*/

  
  fprintf(stdout,"Creating the hemispherical image(s).\n");

  /*calculate colour scale*/
  Cscale=255.0/(float)ratPoint[numb].bins;

  /*step through image pixels*/
  for(i=0;i<dimage->imageSi*dimage->imageSi;i++){
    x=i%dimage->imageSi-dimage->imageSi/2;               /*cartesian pixel coordinates*/
    if(x)y=i/dimage->imageSi-dimage->imageSi/2 + 1;
    else y=i/dimage->imageSi-dimage->imageSi/2;
    if(dimage->mzoom)thetaP=sqrt(x*x+y*y)/dimage->mzoom;      /*thetaP depends on the zoom*/
    else if(dimage->czoom)thetaP=sqrt(x*x+y*y)/dimage->czoom; /*beware of that when adding*/
    else if(dimage->matzoom)thetaP=sqrt(x*x+y*y)/dimage->matzoom;        /*new image types*/
    if(y<0)thataP=atan2(-1*(double)y,(double)x);
    else thataP=2*M_PI+atan2(-1*(double)y,(double)x);
    /*step through annuli, find the relevant one*/
    for(k=0;k<ratPoint[numb].NZscans;k++){
      /*to skip over missing data independent of where it falls in the annulus*/
      m=0;  /*reset space counter*/
      while((ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]>3*M_PI)&&(m<=ratPoint[numb].NAscans)){ m++;printf("Mskip\n");}
      if(m==ratPoint[numb].NAscans){fprintf(stderr,"Breaking\n");break;}
      if(dimage->view){   /*whether the field of view or beam divergence (default) is use to decide beam size*/
        if(ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]>dimage->point){
          maxThe=ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]-dimage->point+ratPoint[numb].Ldiv/2;
          minThe=ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]-dimage->point-ratPoint[numb].Ldiv/2;
        }else{
          maxThe=dimage->point-ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]+ratPoint[numb].Ldiv/2;
          minThe=dimage->point-ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]-ratPoint[numb].Ldiv/2;
        }
      }else{ /*use fov*/
        if(ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]>dimage->point){
          maxThe=ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]-dimage->point+ratPoint[numb].div/2;
          minThe=ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]-dimage->point-ratPoint[numb].div/2;
        }else{
          maxThe=dimage->point-ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]+ratPoint[numb].div/2;
          minThe=dimage->point-ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]-ratPoint[numb].div/2;
        }
      }
      m=0; /*reset space counter again for azimuth*/
      if(maxThe>thetaP&&minThe<=thetaP){
        /*test for annulus segments*/
        for(j=0;(j+m)<ratPoint[numb].NAscans;j++){
          while((ratPoint[numb].theta[k*ratPoint[numb].NAscans+m]>3*M_PI)&&((j+m)<=ratPoint[numb].NAscans))m++;
          if((j+m)==ratPoint[numb].NAscans){fprintf(stderr,"Breaking for missing data\n");break;}
          if(dimage->view){
            if(thetaP>(ratPoint[numb].Ldiv/2.0)){
              maxTha=ratPoint[numb].thata[k*ratPoint[numb].NAscans+j+m]+atan2(tan(ratPoint[numb].Ldiv/2),sin(thetaP));
              minTha=ratPoint[numb].thata[k*ratPoint[numb].NAscans+j+m]-atan2(tan(ratPoint[numb].Ldiv/2),sin(thetaP));
              if(ratPoint[numb].theta[k*ratPoint[numb].NAscans+j+m]<dimage->point){
                /*then the beam is looking backwards and the azimuth needs correcting*/
                maxTha=maxTha+M_PI;
                minTha=minTha+M_PI;
              }
            }else if(thetaP<=ratPoint[numb].Ldiv/2){  /*If in inner ring*/
              maxTha=2.0*M_PI;
              minTha=0;
            }
          }else if(!dimage->view){
            if(thetaP>ratPoint[numb].div/2){
              maxTha=ratPoint[numb].thata[k*ratPoint[numb].NAscans+j+m]+atan2(tan(ratPoint[numb].div/2),\
                sin(ratPoint[numb].theta[k*ratPoint[numb].NAscans+j+m]));
              minTha=ratPoint[numb].thata[k*ratPoint[numb].NAscans+j+m]-atan2(tan(ratPoint[numb].div/2),\
                sin(ratPoint[numb].theta[k*ratPoint[numb].NAscans+j+m]));
              if(ratPoint[numb].theta[k*ratPoint[numb].NAscans+j+m]<dimage->point){
                /*then the beam is looking backwards and the azimuth needs correcting*/
                maxTha=maxTha+M_PI;
                minTha=minTha+M_PI;
              }
            }else if(thetaP<=ratPoint[numb].Ldiv/2){
              maxTha=2*M_PI;
              minTha=0;
            }
          }

          if((thataP<=maxTha)&&(thataP>=minTha)){
            /*Read the relevant data*/
            ratPoint=resultsArrange(dimage,ratPoint,numb,k*ratPoint[numb].NAscans+j+m,4);

            foundHit=0;   /*to stop pulse length intensity getting diluted*/
            for(l=0;l<ratPoint[numb].bins;l++){
              if(dimage->mzoom){        /*visible (previously monochrome) image*/
                for(p=0;p<dimage->nBands;p++){
	  	  band=dimage->band[numb][p];
                  if(ratPoint[numb].ratRes[k*ratPoint[numb].NAscans+j+m].refl[l*ratPoint[numb].nBands*(ratPoint[numb].n+1)+\
                    band*(ratPoint[numb].n+1)]>0.0){
                    dimage->image[i+dimage->imageSi*dimage->imageSi*p]=dimage->image[i+dimage->imageSi*dimage->imageSi*p]\
                    +ratPoint[numb].ratRes[k*ratPoint[numb].NAscans+j+m].refl[l*ratPoint[numb].nBands*(ratPoint[numb].n+1)+\
                    band*(ratPoint[numb].n+1)]*dimage->gain/(ratPoint[numb].min_R+(ratPoint[numb].bin_L*((double)l+0.5))); /**ratPoint[numb].bin_L);*/
                    foundHit=1;
                  }
                }
              }
              if(dimage->czoom){        /*pseudo colour lidar image*/
                if(ratPoint[numb].ratRes[k*ratPoint[numb].NAscans+j+m].refl[l*ratPoint[numb].nBands*(ratPoint[numb].n+1)]>0&&l!=last){
                  returns++;
                  if(returns==1)first=l;
                  if(l>last)last=l;
                }
                dimage->NcontC[i]++;
              }
              if(dimage->matzoom){      /*material image*/
                /*step through RGB*/
                for(p=0;p<3;p++){   /*band loop*/
                  for(n=0;n<dimage->nRGB[p];n++){
                    if(ratPoint[numb].ratRes[k*ratPoint[numb].NAscans+j+m].material[l*2*ratPoint[numb].nMat+2*dimage->matRGB[p][n]]>0){
                      dimage->tempMI[p*dimage->imageSi*dimage->imageSi+i]=dimage->tempMI[p*dimage->imageSi*dimage->imageSi+i]\
                        +ratPoint[numb].ratRes[k*ratPoint[numb].NAscans+j+m].material[l*2*ratPoint[numb].nMat+2*dimage->matRGB[p][n]];
                      dimage->NcontM[i]++;
                    }
                  }
                }/*band loop*/
              }
            }
            if(foundHit) dimage->Ncont[i]++;
            ratPoint=clearArrays(ratPoint,numb,k*ratPoint[numb].NAscans+j+m,dimage);
          }
        }
      }
    }
    if(dimage->czoom){        /*decide pixel values for pseudo colour lidar image*/
      if(returns==0){
        dimage->imageC[i]=0;
        dimage->imageC[i+dimage->imageSi*dimage->imageSi]=0;
        dimage->imageC[i+2*dimage->imageSi*dimage->imageSi]=0;
      }else if(returns==1){
        dimage->imageC[i]=255;
        dimage->imageC[i+dimage->imageSi*dimage->imageSi]=(unsigned char)(255-(float)first*Cscale);
        dimage->imageC[i+2*dimage->imageSi*dimage->imageSi]=0;
      }else if(returns>1){
        dimage->imageC[i]=0;
        dimage->imageC[i+dimage->imageSi*dimage->imageSi]=(unsigned char)((float)(last-first)*Cscale);
        dimage->imageC[i+2*dimage->imageSi*dimage->imageSi]=255-(unsigned char)((float)(last-first)*Cscale);
      }
      /*reset pseudo colour controllers*/
      first=0;
      last=-1;
      returns=0;
    }
  }  /*this is the end of the loop over pixels*/
  /*normalise any images before returning*/
  if(dimage->mzoom){
    for(i=0;i<dimage->imageSi*dimage->imageSi;i++){
      if(dimage->Ncont[i]>0){
        for(p=0;p<dimage->nBands;p++){
          dimage->image[i+dimage->imageSi*dimage->imageSi*p]=dimage->image[i+dimage->imageSi*dimage->imageSi*p]/(float)dimage->Ncont[i];
        }
      }else if(dimage->Ncont[i]==0){
        for(p=0;p<dimage->nBands;p++){
          dimage->image[i+dimage->imageSi*dimage->imageSi*p]=0;
        }
      }
    }
  }
  if(dimage->matzoom){
    for(i=0;i<dimage->imageSi*dimage->imageSi;i++){
      for(p=0;p<3;p++){
        if(dimage->NcontM[i]>0){
          dimage->imageM[p*dimage->imageSi*dimage->imageSi+i]=(unsigned char)(dimage->imageM[p*dimage->imageSi*dimage->imageSi+i]+\
          dimage->tempMI[p*dimage->imageSi*dimage->imageSi+i]*ratPoint[numb].bins*2.55/(float)((int)((dimage->NcontM[i]+2)/3)));
        }else if(dimage->NcontM[i]==0)dimage->imageM[p*dimage->imageSi*dimage->imageSi+i]=0;
      }
    }
    TIDY(dimage->tempMI);
  }


  /*and then write it to an image file*/
  if(mode==0){   /*hips*/
    if(dimage->mzoom){
      if(fwrite(dimage->image,sizeof(float),(size_t)(dimage->imageSi*dimage->imageSi*dimage->nBands),dimage->monochrome)!=\
                                            (size_t)(dimage->imageSi*dimage->imageSi*dimage->nBands)){
        fprintf(stderr,"monochrome image not written\n");
        exit(1);
      }
    }
    if(dimage->czoom){
      if(fwrite(dimage->imageC,sizeof(unsigned char),(size_t)(3*dimage->imageSi*dimage->imageSi*ratPoint[numb].nBands),dimage->colour)!=\
                                                     (size_t)(3*dimage->imageSi*dimage->imageSi*ratPoint[numb].nBands)){
        fprintf(stderr,"colour image not written\n");
        exit(1);
      }
    }
    if(dimage->matzoom){
      if(fwrite(dimage->imageM,sizeof(unsigned char),(size_t)(3*dimage->imageSi*dimage->imageSi),dimage->materialI)!=\
                                                     (size_t)(3*dimage->imageSi*dimage->imageSi)){
        fprintf(stderr,"material image not written\n");
        exit(1);
      }
    }
  }else if(mode==1){   /*tiff*/
    if(dimage->mzoom)       writeTiffFloat(dimage->image,dimage->imageSi,dimage->imageSi,dimage->nBands,dimage->output,"echidna.tif");
    else if(dimage->czoom)  writeTiffUChar(dimage->imageC,dimage->imageSi,dimage->imageSi,3,dimage->output,"colour.tif");
    else if(dimage->matzoom)writeTiffUChar(dimage->imageM,dimage->imageSi,dimage->imageSi,3,dimage->output,"material.tif");
  }/*output format*/


  if(dimage->monochrome)fclose(dimage->monochrome);
  if(dimage->colour)fclose(dimage->colour);
  if(dimage->materialI)fclose(dimage->materialI);
  TIDY(dimage->image);
  TIDY(dimage->imageC);
  TIDY(dimage->imageM);
  fprintf(stdout,"The image(s) has been created.\n\n");
  return(dimage);
}/*sortHemisphere*/


/*#############################################################################*/
/*write a tiff image from floats*/

void writeTiffFloat(float *data,int nX,int nY,int nBands,char *root,char *ending)
{
  int i=0,j=0,band=0;
  float min=0,max=0;
  unsigned char *buff=NULL;
  char namen[200];
  TIFF *file=NULL;

  sprintf(namen,"%s.%s",root,ending);

  file=TIFFOpen(namen,"w");
  TIFFSetField(file, TIFFTAG_IMAGEWIDTH, nX);                        /* set the width of the image*/
  TIFFSetField(file, TIFFTAG_IMAGELENGTH, nY);                       /* set the height of the image*/
  TIFFSetField(file, TIFFTAG_SAMPLESPERPIXEL, nBands);               /* set number of channels per pixel*/
  if(nBands==3)     TIFFSetField(file, TIFFTAG_BITSPERSAMPLE,8);     /* set the size of the channels*/
  else if(nBands==1)TIFFSetField(file, TIFFTAG_BITSPERSAMPLE,8,8,8); /* set the size of the channels*/
  TIFFSetField(file, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);      /* set the origin of the image.*/
  if(nBands==3)TIFFSetField(file,TIFFTAG_PHOTOMETRIC,2);
  else if(nBands==1)TIFFSetField(file, TIFFTAG_PHOTOMETRIC,PHOTOMETRIC_MINISBLACK);   /*mode 1 is greyscale with 0 as black*/
  /*Some other essential fields to set that you do not have to understand for now.*/
  TIFFSetField(file, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);

  /*determine min and max*/
  min=1000000.0;
  max=-1000000.0;
  for(i=nX*nY*nBands-1;i>=0;i--){
    if(data[i]<min)min=data[i];
    if(data[i]>max)max=data[i];
  }/*determine min and max*/

  buff=(unsigned char *)_TIFFmalloc(nX*nBands);
  TIFFSetField(file, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(file,nX));
  for(j=0;j<nY;j++){
    for(i=0;i<nX;i++){
      for(band=0;band<nBands;band++)buff[i*nBands+band]=(unsigned char)((data[band*nX*nY+j*nX+i]-min)*255.0/(max-min));
    }
    if(TIFFWriteScanline(file,buff,j,0)<0){
      fprintf(stderr,"Error writing\n");
      exit(1);
    }
  }
  if(buff){
    _TIFFfree(buff);
    buff=NULL;
  }
  TIFFClose(file);
  fprintf(stdout,"Drawn to %s\n",namen);
  return;
}/*writeTiffFloat*/


/*#############################################################################*/
/*write a tiff image from floats*/

void writeTiffUChar(unsigned char *data,int nX,int nY,int nBands,char *root,char *ending)
{
  int i=0,j=0,band=0;
  unsigned char *buff=NULL;
  char namen[200];
  TIFF *file=NULL;

  sprintf(namen,"%s.%s",root,ending);

  file=TIFFOpen(namen,"w");
  TIFFSetField(file, TIFFTAG_IMAGEWIDTH, nX);                        /* set the width of the image*/
  TIFFSetField(file, TIFFTAG_IMAGELENGTH, nY);                       /* set the height of the image*/
  TIFFSetField(file, TIFFTAG_SAMPLESPERPIXEL, nBands);               /* set number of channels per pixel*/
  if(nBands==3)     TIFFSetField(file, TIFFTAG_BITSPERSAMPLE,8);     /* set the size of the channels*/
  else if(nBands==1)TIFFSetField(file, TIFFTAG_BITSPERSAMPLE,8,8,8); /* set the size of the channels*/
  TIFFSetField(file, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);      /* set the origin of the image.*/
  if(nBands==3)TIFFSetField(file,TIFFTAG_PHOTOMETRIC,2);
  else if(nBands==1)TIFFSetField(file, TIFFTAG_PHOTOMETRIC,PHOTOMETRIC_MINISBLACK);   /*mode 1 is greyscale with 0 as black*/
  /*Some other essential fields to set that you do not have to understand for now.*/
  TIFFSetField(file, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);


  buff=(unsigned char *)_TIFFmalloc(nX*nBands);
  TIFFSetField(file, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(file,nX));
  for(j=0;j<nY;j++){
    for(i=0;i<nX;i++){
      for(band=0;band<nBands;band++)buff[i*nBands+band]=data[band*nX*nY+j*nX+i];
    }
    if(TIFFWriteScanline(file,buff,j,0)<0){
      fprintf(stderr,"Error writing\n");
      exit(1);
    }
  }
  if(buff){
    _TIFFfree(buff);
    buff=NULL;
  }
  TIFFClose(file);
  fprintf(stdout,"Drawn to %s\n",namen);
  return;
}/*writeTiffUChar*/


/*##############################################################################*/
/*Prepare structure and image headers for sortHemisphere*/

RatImage *imagePrepare(RatControl *ratPoint,int numb,RatImage *dimage,int argc,char **argv)
{
  struct header hd;  /*monochrome image header*/
  struct header col; /*colour image header*/
  struct header mat; /*material image header*/
  void update_header(),init_header(),fp_fwrite_header();
  int i=0,j=0;
  char namen[100];
  char mode=0;

  mode=1;  /*hard wired to tiff for now*/

  if(mode==0){/*hips*/
    if(dimage->mzoom){
      if(dimage->scanN==1)sprintf(namen,"%s.echidna.hip",dimage->output);
      else sprintf(namen,"%s.%d.%d.%d.echidna.hip",dimage->output,(int)\
        (ratPoint[i].from[0]),(int)(ratPoint[i].from[1]),(int)(ratPoint[i].from[2]));
      if((dimage->monochrome=fopen(namen,"wb"))==NULL){
        fprintf(stderr,"Error opening monochrome image");
        exit(1);
      }
    }
    if(dimage->czoom){
      if(dimage->scanN==1)sprintf(namen,"%s.colour.hip",dimage->output);
      else sprintf(namen,"%s.%d.%d.%d.colour.hip",dimage->output,(int)(ratPoint[i].from[0]),\
        (int)(ratPoint[i].from[1]),(int)(ratPoint[i].from[2]));
      if((dimage->colour=fopen(namen,"wb"))==NULL){
        fprintf(stderr,"Error opening colour image");
        exit(1);
      }
    }
    if(dimage->matzoom){
      if(dimage->scanN==1)sprintf(namen,"%s.material.hip",dimage->output);
      else sprintf(namen,"%s.%d.%d.%d.material.hip",dimage->output,(int)(ratPoint[i].from[0]),\
        (int)(ratPoint[i].from[1]),(int)(ratPoint[i].from[2]));
      if((dimage->materialI=fopen(namen,"wb"))==NULL){
        fprintf(stderr,"Error opening material image");
        exit(1);
      }
    }
  }/*mode test*/


  if(ratPoint[numb].iZen<-1*M_PI)dimage->point=-1*M_PI;
  else if(ratPoint[numb].fZen>M_PI)dimage->point=M_PI;
  else dimage->point=0;

  /*calculate size of the image*/
  dimage->maxZen=-10;  /**some arbitrary starting point**/
  for(i=ratPoint[numb].NZscans*ratPoint[numb].NAscans-1;i>=0;i--){
    if(sqrt((ratPoint[numb].theta[i]-dimage->point)*(ratPoint[numb].theta[i]-dimage->point))>dimage->maxZen&&
      ratPoint[numb].theta[i]<3*M_PI){
      dimage->maxZen=sqrt((ratPoint[numb].theta[i]-dimage->point)*(ratPoint[numb].theta[i]-dimage->point));
    }
  }
  printf("max Zen %f\n",dimage->maxZen*180/M_PI);

  /*prepare each image in turn*/
  if(dimage->mzoom){
    /*prepare monochrome image*/
    if(dimage->view){
      dimage->imageSi=(int)((2*dimage->maxZen+ratPoint[numb].Ldiv)*dimage->mzoom+1);
    }else{
      dimage->imageSi=(int)((2*dimage->maxZen+ratPoint[numb].div)*dimage->mzoom+1);
    }
    if(!(dimage->image=(float *)calloc(dimage->imageSi*dimage->imageSi*dimage->nBands,sizeof(float)))){
      fprintf(stderr,"error in visible image buffer allocation number 1.\n");
      exit(1);
    }
    if(!(dimage->Ncont=(int *)calloc(dimage->imageSi*dimage->imageSi,sizeof(int)))){
      fprintf(stderr,"error in contribution counter allocation.\n");
      exit(1);
    }
    if(mode==0){
      init_header(&hd,dimage->monochrome," ",dimage->nBands,"today",dimage->imageSi,dimage->imageSi,8*sizeof(float),0,PFFLOAT," ");
      update_header(&hd,argc,argv);
      fp_fwrite_header(dimage->monochrome,&hd);
    }
  }
  if(dimage->czoom){
    /*prepare colour image*/
    if(dimage->view){
      dimage->imageSi=(int)((2*dimage->maxZen+ratPoint[numb].Ldiv)*dimage->czoom+1);
    }else{
      dimage->imageSi=(int)((2*dimage->maxZen+ratPoint[numb].div)*dimage->czoom+1);
    }
    if(!(dimage->imageC=(unsigned char *)calloc(3*dimage->imageSi*dimage->imageSi,sizeof(unsigned char)))){
      fprintf(stderr,"error in image allocation number 1.\n");
      exit(1);
    }
    /*each beam contains all the colours, so NcontC need only be the size of one band*/
    if(!(dimage->NcontC=(int *)calloc(dimage->imageSi*dimage->imageSi,sizeof(int)))){
      fprintf(stderr,"error in contribution counter allocation.\n");
      exit(1);
    }
    if(mode==0){
      init_header(&col,dimage->colour," ",3*ratPoint[numb].nBands,"today",(int)dimage->imageSi,(int)dimage->imageSi,8*sizeof(char),0,PFBYTE," ");
      update_header(&col,argc,argv);
      fp_fwrite_header(dimage->colour,&col);
    }
  }
  if(dimage->matzoom){
    if(ratPoint[numb].nMat==0){
      fprintf(stderr,"This data file has no material information. You can't create a material image.\n");
      exit(1);
    }
    for(i=0;i<3;i++){ /*to make sure we aren't colouring materials that aren't there*/
      for(j=0;j<dimage->nRGB[i];j++){
        if(dimage->matRGB[i][j]>ratPoint[numb].nMat){
          fprintf(stderr,"There is no material %d\n",dimage->matRGB[i][j]);
          exit(1);
        }
      }
    }
    /*prepare material image*/
    if(dimage->view){
      dimage->imageSi=(int)((2*dimage->maxZen+ratPoint[numb].Ldiv)*dimage->matzoom+1);
    }else{
      dimage->imageSi=(int)((2*dimage->maxZen+ratPoint[numb].div)*dimage->matzoom+1);
    }
    if(!(dimage->imageM=(unsigned char *)calloc(3*dimage->imageSi*dimage->imageSi,sizeof(unsigned char)))){
      fprintf(stderr,"error in material image allocation number 1.\n");
      exit(1);
    }
    /*and the temporary float array*/ 
    if(!(dimage->tempMI=(float *)calloc(3*dimage->imageSi*dimage->imageSi,sizeof(float)))){
      fprintf(stderr,"error in temporary material array allocation.\n");
      exit(1);
    }
    /*each beam contains all the colours, so NcontC need only be the size of one band*/
    if(!(dimage->NcontM=(int *)calloc(dimage->imageSi*dimage->imageSi,sizeof(int)))){
      fprintf(stderr,"error in material contribution counter allocation.\n");
      exit(1);
    }
    if(mode==0){
      init_header(&mat,dimage->materialI," ",3,"today",(int)dimage->imageSi,(int)dimage->imageSi,8*sizeof(unsigned char),0,PFBYTE," ");
      update_header(&mat,argc,argv);
      fp_fwrite_header(dimage->materialI,&mat);
    }
  }
  return(dimage);
}/*imagePrepare*/


/*###############################################################################################*/
/*This one puts the binary file into a readable ASCII file, for your enjoyment*/
/*mode 0 is to output the lidar information, materials as ints, mode 1, same as 0*/
/*with materials in floats, mode 2, the height profile, mode ...*/
/*It ouputs over the range controlled by dimage->Arange[]*/

void asciiWrite(RatControl *ratPoint,int numb,RatImage *dimage)
{
  int n=0,i=0,j=0,k=0,m=0,p=0,q=0,band=0;
  int Zstart=0,Zstop=0,Astart=0,Astop=0;
  int reflLength=0,materialLength=0,place=0;
  void printLegend(RatImage *,RatControl *,int);
  int *angleToIndices(RatImage *,RatControl *,int),*indices=NULL;
  FILE *output=NULL;
  char namen[200];
  char writeIt=0;

  reflLength=ratPoint[numb].bins*ratPoint[numb].nBands*(ratPoint[numb].n+1);
  materialLength=ratPoint[numb].bins*ratPoint[numb].nMat*2;

  if(dimage->scanN==1)sprintf(namen,"%s.%d.%d.lidar",dimage->output,\
    (int)(dimage->Arange[0]*180/M_PI),(int)(dimage->Arange[1]*180/M_PI));
  else sprintf(namen,"%s.%d.%d.%d.%d.%d.lidar",dimage->output,(int)ratPoint[numb].from[0],\
    (int)ratPoint[numb].from[1],(int)ratPoint[numb].from[2],\
    (int)(dimage->Arange[0]*180/M_PI),(int)(dimage->Arange[1]*180/M_PI));
  if((output=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening ascii results file %s\n",namen);
    exit(1);
  }

  /*convert the angular range to the integer controls*/
  indices=angleToIndices(dimage,ratPoint,numb);
  Zstart=indices[0];
  Zstop=indices[1];
  Astart=indices[2];
  Astop=indices[3];
  TIDY(indices);

  if(dimage->intent)printf("Writting data to an ascii file %s\n",namen);
  fprintf(output,"# This is an echi.rat data file\n# The numbers on top are the zenith and azimuth angles\
 in degrees\n# The blocks of numbers are range, wavelength, refl, refl of each order of interaction.\n# scan centre %g %g %g\n",ratPoint[numb].from[0],ratPoint[numb].from[1],ratPoint[numb].from[2]);

  ratPoint=clearAllArrays(ratPoint,dimage);

  for(p=Zstart;p<Zstop;p++){
    for(m=Astart;m<Astop;m++){
      n=p*ratPoint[numb].NAscans+m;

      if(!dimage->firstReturn){   /*if we are to output waveform*/
        if(!dimage->jusAng){
          fprintf(output,"\n# %g %g\n\n",ratPoint[numb].theta[n]*180.0/M_PI,ratPoint[numb].thata[n]*180.0/M_PI);
          ratPoint=resultsArrange(dimage,ratPoint,numb,n,0);
          for(i=0;i<ratPoint[numb].bins;i++){

            if(!dimage->asciiSave)writeIt=1;
            else{    /*only print out if bin contains light*/
              writeIt=0;
              for(band=0;band<dimage->nBands;band++){
                place=findReflIndex(dimage,ratPoint,numb,i,band,0);
                if(ratPoint[numb].ratRes[n].refl[place]>0.0){
                  writeIt=1;
                  break;
                }
              }
            }
            if(writeIt){
	      fprintf(output,"%g ",(double)i*ratPoint[numb].bin_L+ratPoint[numb].min_R);
              for(q=0;q<dimage->withinSegs+dimage->withoutSegs;q++){
                /*if(dimage->withinSegs+dimage->withoutSegs>1)*/
                fprintf(output," Seg%d ",q);
                for(j=0;j<dimage->nBands;j++){
                  band=dimage->band[numb][j];
                  fprintf(output,"%g %g ",ratPoint[numb].wavelength[band],\
                    ratPoint[numb].ratRes[n].refl[q*reflLength+(i*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]);
                  for(k=0;k<ratPoint[numb].n;k++){
                    fprintf(output,"%g ",ratPoint[numb].ratRes[n].refl[q*reflLength+(i*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)+k+1]);
                  }
                }
                fprintf(output,"  mat ");
                for(j=0;j<2*ratPoint[numb].nMat;j++){
                  fprintf(output,"%g ",ratPoint[numb].ratRes[n].material[q*materialLength+i*ratPoint[numb].nMat*2+j]);
                }
              }
              fprintf(output,"%g",ratPoint[numb].ratRes[n].sky);
              fprintf(output,"\n");
            }
          }
          ratPoint=clearArrays(ratPoint,numb,n,dimage);
          fprintf(output,"\n");
          fflush(output);
        }else if(dimage->jusAng){
          fprintf(output,"\n%f %f",ratPoint[numb].theta[n]*180/M_PI,ratPoint[numb].thata[n]*180/M_PI);
        }
      }else if(dimage->firstReturn){  /*output the pointcloud*/
        ratPoint=resultsArrange(dimage,ratPoint,numb,n,0);
        /*see if it's empty*/
        writeIt=0;
        for(band=0;band<dimage->nBands;band++){
          for(i=0;i<ratPoint[numb].bins;i++){
            place=findReflIndex(dimage,ratPoint,numb,i,band,0);
            if(ratPoint[numb].ratRes[n].refl[place]>0.0){
              writeIt=1;
              break;
            }
          }
        }/*band loop*/
        if(writeIt){
          fprintf(output,"%g %g %g",ratPoint[numb].theta[n]*180.0/M_PI,ratPoint[numb].thata[n]*180.0/M_PI,(double)i*ratPoint[numb].bin_L+ratPoint[numb].min_R);
          for(band=0;band<dimage->nBands;band++){
            place=findReflIndex(dimage,ratPoint,numb,i,band,0);
            fprintf(output," %g",ratPoint[numb].ratRes[n].refl[place]);
          }
          fprintf(output,"\n");
        }else if(!dimage->asciiSave){
          fprintf(output,"%g %g %g %g\n",ratPoint[numb].theta[n]*180.0/M_PI,ratPoint[numb].thata[n]*180.0/M_PI,0.0,0.0);
        }
        ratPoint=clearArrays(ratPoint,numb,n,dimage);
      }/*waveform - first return check*/
    }/*azimuth loop*/
  }/*zenith loop*/
  if(dimage->LUTlegend){
    printLegend(dimage,ratPoint,numb);
  }/*legend printing*/

  if(output){
    fclose(output);
    output=NULL;
  }
  if(dimage->intent)printf("Ascii data written\n");
}

/*########################################################################################*/
/*add up total radiant flux for each band*/

void totalFlux(RatControl *ratPoint,int numb,RatImage *dimage)
{
  int zen=0,az=0,band=0,bin=0;
  int Zstart=0,Zstop=0,Astart=0,Astop=0;
  int place=0,Lplace=0;
  double *flux=NULL;
  int *angleToIndices(RatImage *,RatControl *,int),*indices=NULL;
  FILE *output=NULL;
  char namen[200];

  if(dimage->scanN==1)sprintf(namen,"%s.%d.%d.flux",dimage->output,\
    (int)(dimage->Arange[0]*180/M_PI),(int)(dimage->Arange[1]*180/M_PI));
  else sprintf(namen,"%s.%d.%d.%d.%d.%d.flux",dimage->output,(int)ratPoint[numb].from[0],\
    (int)ratPoint[numb].from[1],(int)ratPoint[numb].from[2],\
    (int)(dimage->Arange[0]*180/M_PI),(int)(dimage->Arange[1]*180/M_PI));
  if((output=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening ascii results file %s\n",namen);
    exit(1);
  }

  /*convert the angular range to the integer controls*/
  indices=angleToIndices(dimage,ratPoint,numb);
  Zstart=indices[0];
  Zstop=indices[1];
  Astart=indices[2];
  Astop=indices[3];
  TIDY(indices);

  if(dimage->intent)printf("Writting data to an ascii file %s\n",namen);
  fprintf(output,"# This is the total flux from an echi.rat data file\n");

  ratPoint=clearAllArrays(ratPoint,dimage);
  flux=dalloc(dimage->nBands,"total flux",Lplace);

  for(zen=Zstart;zen<Zstop;zen++){
    for(az=Astart;az<Astop;az++){
      Lplace=zen*ratPoint[numb].NAscans+az;
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
      for(band=0;band<dimage->nBands;band++){
        flux[band]=0.0;
        /*add up all radiant fluxes*/
        for(bin=0;bin<ratPoint[numb].bins;bin++){
          place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
          flux[band]+=ratPoint[numb].ratRes[Lplace].refl[place];
        }
        fprintf(output,"Beam %d wavelength %g flux %f\n",Lplace,ratPoint[numb].wavelength[dimage->band[numb][band]],flux[band]);
      }
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    }
  }

  TIDY(flux)
  return;
}/*totalFlux*/

/*########################################################################################*/
/*calculate mean radiant flux ans standard deviation of several beams*/

void beamStats(RatImage *dimage,RatControl *ratPoint)
{
  int numb=0,Lplace=0,band=0,bin=0;
  int place=0;
  double temp=0;
  double **trefl=NULL,***brefl=NULL;
  double *meanRefl=NULL,**meanBin=NULL;
  double *stan=NULL,**stanB=NULL;
  char namen[200];
  FILE *opoo=NULL;

  /*check all scans have the same number of bins*/
  if(dimage->scanN<=1){
    fprintf(stderr,"Not enough scans for a comparison %d\n",dimage->scanN);
    exit(1);
  }
  for(numb=1;numb<dimage->scanN;numb++){
    if(ratPoint[numb-1].bins!=ratPoint[numb].bins){
      fprintf(stderr,"Disasterous!\n");
      exit(1);
    }
    if(ratPoint[numb-1].nBands!=ratPoint[numb].nBands){
      fprintf(stderr,"Band number mismatch, unable to calculate stats\n");
      exit(1);
    }
  }

  sprintf(namen,"%s.stats",dimage->output);
  if((opoo=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening ascii beam statistics %s\n",namen);
    exit(1);
  }
  printf("outputing statistics to %s\n",namen);

  /*allocate the necessary space*/
  if(!(trefl=(double **)calloc(dimage->nBands,sizeof(double *)))){
    fprintf(stderr,"error in scanner radiance allocation.\n");
    exit(1);
  }
  if(!(meanBin=(double **)calloc(dimage->nBands,sizeof(double *)))){
    fprintf(stderr,"error in scanner radiance allocation.\n");
    exit(1);
  }
  if(!(brefl=(double ***)calloc(dimage->nBands,sizeof(double **)))){
    fprintf(stderr,"error in scanner radiance allocation.\n");
    exit(1);
  }
  if(!(stanB=(double **)calloc(dimage->nBands,sizeof(double *)))){
    fprintf(stderr,"error in scanner radiance allocation.\n");
    exit(1);
  }
  meanRefl=dalloc(dimage->nBands,"mean reflectance",0);
  stan=dalloc(dimage->nBands,"standard deviation",0);
  for(band=0;band<dimage->nBands;band++){
    trefl[band]=dalloc(dimage->scanN,"scan total radiance",numb);
    if(!(brefl[band]=(double **)calloc(dimage->scanN,sizeof(double *)))){
      fprintf(stderr,"error in scanner bin radiance allocation.\n");
      exit(1);
    }
    meanBin[band]=dalloc(ratPoint[0].bins,"mean bin radiance",band);
    for(numb=0;numb<dimage->scanN;numb++){
      brefl[band][numb]=dalloc(ratPoint[numb].bins,"bin scanner",band);
    }
    stanB[band]=dalloc(ratPoint[0].bins,"bin standard deviation",band);
  }

  fprintf(opoo,"# wavelength reflectance standard_deviation for %d scans\n",dimage->scanN);

  for(band=0;band<dimage->nBands;band++){
    for(numb=0;numb<dimage->scanN;numb++){
      for(Lplace=0;Lplace<ratPoint[numb].NAscans*ratPoint[numb].NZscans;numb++){
        ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,1);
        for(bin=0;bin<ratPoint[numb].bins;bin++){
          place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
          meanRefl[band]+=ratPoint[numb].ratRes[Lplace].refl[place];
          trefl[band][numb]+=ratPoint[numb].ratRes[Lplace].refl[place];
          meanBin[band][bin]+=ratPoint[numb].ratRes[Lplace].refl[place];
          brefl[band][numb][bin]+=ratPoint[numb].ratRes[Lplace].refl[place];
        }
        ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
      }
    }/*scan loop*/
    meanRefl[band]/=(double)dimage->scanN;
    temp=0.0;
    for(numb=0;numb<dimage->scanN;numb++){
      temp+=(trefl[band][numb]-meanRefl[band])*(trefl[band][numb]-meanRefl[band]);
    }
    stan[band]=sqrt(temp/(double)(dimage->scanN-1));
    for(bin=0;bin<ratPoint[0].bins;bin++){
      meanBin[band][bin]/=(double)dimage->scanN;
      temp=0.0;
      for(numb=0;numb<dimage->scanN;numb++){
        temp+=(brefl[band][numb][bin]-meanBin[band][bin])*(brefl[band][numb][bin]-meanBin[band][bin]);
      }
      stanB[band][bin]=sqrt(temp/(double)(dimage->scanN-1));
    }
    fprintf(opoo,"%g %g %g",ratPoint[0].wavelength[dimage->band[0][band]],meanRefl[band],stan[band]);
    if(dimage->stats==1){
      for(bin=0;bin<ratPoint[0].bins;bin++){
        if(meanBin[band][bin]>0.0)fprintf(opoo," bin %d %g %g",bin,meanBin[band][bin],stanB[band][bin]);
      }
    }
    if(dimage->rawStats){  /*print out the raw reflectance values*/
      for(numb=0;numb<dimage->scanN;numb++){
        fprintf(opoo,"%f ",trefl[band][numb]);
      }
    }
    fprintf(opoo,"\n");
  }

  TIDY(meanRefl);
  TIDY(stan);
  if(trefl){
    for(band=0;band<dimage->nBands;band++){
      TIDY(trefl[band]);
    }
    free(trefl);
    trefl=NULL;
  }
  if(stanB){
    for(band=0;band<dimage->nBands;band++){
      TIDY(stanB[band]);
    }
    free(stanB);
    stanB=NULL;
  }
  if(meanBin){
    for(band=0;band<dimage->nBands;band++){
      TIDY(meanBin[band]);
    }
    free(meanBin);
    meanBin=NULL;
  }
  if(brefl){
    for(band=0;band<dimage->nBands;band++){
      if(brefl[band]){
        for(numb=0;numb>dimage->scanN;numb++){
          TIDY(brefl[band][numb]);
        }
        free(brefl[band]);
        brefl[band]=NULL;
      }
      TIDY(brefl[band]);
    }
    free(brefl);
    brefl=NULL;
  }

  if(opoo){
    fclose(opoo);
    opoo=NULL;
  }

  return;
}/*beamStats*/

/*########################################################################################*/
/*prints out an ascii legend for the ascii file*/

void printLegend(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int i=0,j=0;      /*loop control variables*/
  int mat=0,band=0;
  char namen[200];
  char **materialLUT(RatImage *,RatControl *,int);
  char **matType=NULL;
  FILE *legend=NULL;

  matType=materialLUT(dimage,ratPoint,numb);

  if(dimage->scanN==1)sprintf(namen,"%s.%d.%d.LUT",dimage->output,\
    (int)(dimage->Arange[0]*180/M_PI),(int)(dimage->Arange[1]*180/M_PI));
  else sprintf(namen,"%s.%d.%d.%d.%d.%d.LUT",dimage->output,(int)ratPoint[numb].from[0],\
    (int)ratPoint[numb].from[1],(int)ratPoint[numb].from[2],\
    (int)(dimage->Arange[0]*180/M_PI),(int)(dimage->Arange[1]*180/M_PI));
  if((legend=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening ascii legend file %s\n",namen);
    exit(1);
  }

  if(dimage->intent)printf("Printing a legend to %s\n",namen);
  fprintf(legend,"1 range\n");
  for(i=0;i<ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs;i++){
    fprintf(legend,"%d seg%d\n",2+i*(dimage->nBands*(2+ratPoint[numb].n)),i);
    for(band=0;band<dimage->nBands;band++){
      fprintf(legend,"%d Lambda %d, wavelength %g\n%d total refl\n",2+i*(dimage->nBands*(2+ratPoint[numb].n))+\
        1+band*(2+ratPoint[numb].n),dimage->band[numb][band],ratPoint[numb].wavelength[dimage->band[numb][band]],\
        2+i*(dimage->nBands*(2+ratPoint[numb].n))+1+band*(2+ratPoint[numb].n)+1);
      for(j=0;j<ratPoint[numb].n;j++){
        fprintf(legend,"%d refl %d\n",2+i*(dimage->nBands*(2+ratPoint[numb].n))+\
        1+band*(2+ratPoint[numb].n)+1+j+1,j);
      }
    }
  }/*reflectance*/
  fprintf(legend,"%d mat\n",1+(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs)*(1+dimage->nBands*(2+ratPoint[numb].n))+1);
  for(mat=0;mat<ratPoint[numb].nMat;mat++){
    fprintf(legend,"%d direct %s\n",1+(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs)*\
      (1+dimage->nBands*(2+ratPoint[numb].n)+1)+1+2*mat,matType[mat]);
    fprintf(legend,"%d indirect %s\n",1+(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs)*\
      (1+dimage->nBands*(2+ratPoint[numb].n)+1)+1+2*mat+1,matType[mat]);
  }/*materials*/

  if(legend){
    fclose(legend);
    legend=NULL;
  }
  if(matType){
    for(mat=0;mat<ratPoint[numb].nMat;mat++){
      TIDY(matType[mat]);
    }
    free(matType);
    matType=NULL;
  }
  return;
} /*printLegend*/

/*#################################################################*/
/*set up the object to canopy material LUT*/

char **materialLUT(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int i=0,matN=0,*index=NULL;
  char **matType=NULL,line[200];
  char **matTypeOut=NULL;
  char guff1[100],guff2[100];
  FILE *legend=NULL;

  if((legend=fopen("materialLUT.txt","r"))==NULL){
    fprintf(stderr,"Error opening material legend %s\n","materialLUT.txt");
    fprintf(stderr,"This must be created using 'forage -justlegend object.obj'\n");
    exit(1);
  }

  /*create a look up tabel to convert between object materials and the two I want*/
  if(!(matType=(char **)calloc(ratPoint[numb].nMat,sizeof(char *)))){
    fprintf(stderr,"error in material type allocation.\n");
    exit(1);
  }
  for(i=0;i<ratPoint[numb].nMat;i++){
    if(!(matType[i]=(char *)calloc(100,sizeof(char)))){
      fprintf(stderr,"error in material type allocation %d.\n",i);
      exit(1);
    }
  }
  if(!(index=(int *)calloc(ratPoint[numb].nMat,sizeof(int)))){
    fprintf(stderr,"error in material LUT allocation.\n");
    exit(1);
  }
  for(i=0;i<ratPoint[numb].nMat;i++){
    index[i]=-1;
  }
  i=0;
  while(fgets(line,200,legend)!=NULL){
    strcpy(guff1,"#");
    if(sscanf(line,"%s %s",guff1,guff2)!=2) {
      fprintf(stderr, "Badly formatted material file\n");
      exit(1);
    }
    if(strncasecmp(guff1,"#",1)){
      index[i]=atoi(guff1);
      strcpy(matType[i],guff2); 
      i++;
    }
  }
  matN=i;
/*  I think this has fallen into disrepair
#  if(matN+2!=ratPoint[numb].nMat){
#    printf("material number mismatch %d %d\n",matN+2,ratPoint[numb].nMat);
#  }*/
  if(legend){
    fclose(legend);
    legend=NULL;
  }
  if(!(matTypeOut=(char **)calloc(ratPoint[numb].nMat,sizeof(char *)))){
    fprintf(stderr,"error in material LUT label allocation.\n");
    exit(1);
  }
  for(i=0;i<ratPoint[numb].nMat;i++){
    if(index[i]>-1){
      if(!(matTypeOut[index[i]]=(char *)calloc(strlen(matType[i])+1,sizeof(char)))){
        fprintf(stderr,"error in material LUT label allocation.\n");
        exit(1);
      }
      strcpy(matTypeOut[index[i]],matType[i]);
    }else if(i==ratPoint[numb].nMat-2){
      if(!(matTypeOut[0]=(char *)calloc(strlen("preset")+1,sizeof(char)))){
        fprintf(stderr,"error in material LUT label allocation.\n");
        exit(1);
      }
      strcpy(matTypeOut[0],"preset");
    }else if(i==ratPoint[numb].nMat-1){
      if(!(matTypeOut[1]=(char *)calloc(strlen("preset")+1,sizeof(char)))){
        fprintf(stderr,"error in material LUT label allocation.\n");
        exit(1);
      }
      strcpy(matTypeOut[1],"preset");
    }else{
      fprintf(stderr,"Oh dear oh dear %d %d\n",i,index[i]);
      exit(1);
    }
    TIDY(matType[i]);
  }
  TIDY(matType);
  TIDY(index);

  return(matTypeOut);
}  /*materialLUT*/



/*######################################################################################################*/
/*oututs an ascii file showing the maximum heights, centred on the instrument*/

void heightProfile(RatControl *ratPoint,int numb,RatImage *dimage)
{
  double *Hprof=NULL,height=0;             /*height profile and temp value*/
  double thresh=0;    /*a threshold lest we want to do some filtering*/
  int i=0,j=0,k=0,n=0,m=0,bin=0;
  int Lplace=0,Pplace=0;                   /*beam and profile indices*/
  /*int centre=0;*/                            /*central beam zenith index*/
  int Hnum[2],Tnum=0;                      /*number of x, y and total pixels*/
  float x=0,y=0;                           /*height profile coordinates*/
  /*double minTheta=0;*/                       /*for finding beam centre*/
  char namen[200];
  FILE *output=NULL;
  double xb0=0,xb1=0,xb2=0,xb3=0;          /*variables for the mapping*/
  double yb0=0,yb1=0,yb2=0,yb3=0;
  double xp0=0,xp1=0,yp0=0,yp1=0;
  double minXb=0,maxXb=0,minYb=0,maxYb=0;
  double r=0,omega=0;
  char xintercept=0,yintercept=0;
  int *maptransf=NULL;


  if(dimage->scanN==1)sprintf(namen,"%s.maxheight",dimage->output);
  else sprintf(namen,"%s.%d.%d.%d.maxheight",dimage->output,(int)ratPoint[numb].from[0],\
    (int)ratPoint[numb].from[1],(int)ratPoint[numb].from[2]);
  if((output=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening max height ascii results file %s\n",namen);
    exit(1);
  }

  printf("\nCalculating height profile...\nTo %s\n",namen);

  Hnum[0]=(int)(dimage->Hdim[0]/dimage->Hres+0.5);
  Hnum[1]=(int)(dimage->Hdim[1]/dimage->Hres+0.5);
  Tnum=Hnum[0]*Hnum[1];

  if(!(Hprof=(double *)calloc(Tnum,sizeof(double)))){
    fprintf(stderr,"error in height profile allocation.\n");
    exit(1);
  }
  if(!(dimage->HbinN=(int *)calloc(Tnum,sizeof(int)))){
    fprintf(stderr,"error in height profile contribution allocation.\n");
    exit(1);
  }
  if(!(dimage->Hmap=(int **)calloc(Tnum,sizeof(int *)))){
    fprintf(stderr,"error in height profile map allocation.\n");
    exit(1);
  }

  /*put nonsense values in. This only works if there are no objects lower than 0.*/
  for(i=0;i<Tnum;i++){
    Hprof[i]=-1;  /*# BEWARE #*/
    dimage->HbinN[i]=0;
  }

  /*now find the middle of the scan and if it bends back on itself and stuff*/
/*
*  minTheta=-1000;
*  for(m=0;m<ratPoint[numb].NZscans;m++){
*    Lplace=m*ratPoint[numb].NAscans;
*    if(ratPoint[numb].theta[Lplace]==0||\
*      (ratPoint[numb].theta[Lplace]-M_PI>-0.00001&&\
*       ratPoint[numb].theta[Lplace]-M_PI<0.00001)){
*      centre=m;
*      break;
*    }
*  }
*/

  printf("creating a bin to pixel map...\n");
  /*step through height segments and create a bin to pixel map*/
  for(i=0;i<ratPoint[numb].NZscans;i++){
    for(j=0;j<ratPoint[numb].NAscans;j++){
      Lplace=i*ratPoint[numb].NAscans+j;
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        /*if there is anything within this bin then do the mapping and things*/
        if(ratPoint[numb].ratRes[Lplace].refl[bin*ratPoint[numb].nBands*(ratPoint[numb].n+1)]>thresh){
          /*calculate the centre of this bin*/
          x=(bin+0.5)*ratPoint[numb].bin_L*sin(ratPoint[numb].theta[Lplace])*cos(ratPoint[numb].thata[Lplace]);
          y=(bin+0.5)*ratPoint[numb].bin_L*sin(ratPoint[numb].theta[Lplace])*sin(ratPoint[numb].thata[Lplace]);
          /*and then the edges*/
          r=ratPoint[numb].bin_L/2.0*(double)bin*tan(ratPoint[numb].div/2);
          omega=acos(1/sqrt(16*r*r/(ratPoint[numb].bin_L*ratPoint[numb].bin_L)+1.0));
          xb0=x+sqrt(r*r+ratPoint[numb].bin_L*ratPoint[numb].bin_L/16)*cos(ratPoint[numb].theta[Lplace]+omega-M_PI/2)\
              *cos(ratPoint[numb].thata[Lplace]);
          yb0=y-sqrt(r*r+ratPoint[numb].bin_L*ratPoint[numb].bin_L/16)*cos(ratPoint[numb].theta[Lplace]+omega-M_PI/2)\
              *sin(ratPoint[numb].thata[Lplace]);
          xb1=x+r*sin(ratPoint[numb].thata[Lplace]);
          yb1=y-r*cos(ratPoint[numb].thata[Lplace]);
          xb2=x-r*sin(ratPoint[numb].thata[Lplace]);
          yb2=y+r*cos(ratPoint[numb].thata[Lplace]);
          xb3=x-sqrt(r*r+ratPoint[numb].bin_L*ratPoint[numb].bin_L/16)*cos(ratPoint[numb].theta[Lplace]+omega-M_PI/2)\
              *cos(ratPoint[numb].thata[Lplace]);
          yb3=y+sqrt(r*r+ratPoint[numb].bin_L*ratPoint[numb].bin_L/16)*cos(ratPoint[numb].theta[Lplace]+omega-M_PI/2)\
              *sin(ratPoint[numb].thata[Lplace]);
          /*now search through pixels to see which this bin falls within, if we are within the scene*/
          if(xb3<=dimage->Hres*Hnum[0]/2&&xb3>=-1*dimage->Hres*Hnum[0]/2&&\
             yb3<=dimage->Hres*Hnum[1]/2&&yb3>=-1*dimage->Hres*Hnum[1]/2){
            for(k=0;k<Hnum[0];k++){
              /*our checks depend on the relative sizes of the pixels and bins*/
              if(x>=0){
                xp0=(k-Hnum[0]/2+0.5)*dimage->Hres;
                xp1=(k-Hnum[0]/2-0.5)*dimage->Hres;
              }else{
                xp0=(k-Hnum[0]/2-0.5)*dimage->Hres;
                xp1=(k-Hnum[0]/2+0.5)*dimage->Hres;
              }
              if(dimage->Hres*2>=r){ /*see if bin edges are within pixel*/
                if(xb0<=xp0&&xb0>=xp1)     xintercept=1;
                else if(xb1<=xp0&&xb1>=xp1)xintercept=1;
                else if(xb2<=xp0&&xb2>=xp1)xintercept=1;
                else if(xb3<=xp0&&xb3>=xp1)xintercept=1;
                else                       xintercept=0;
              }else if(dimage->Hres*2<r){ /*see if pixel edged are within bin*/
                /*find min and max xb*/
                if(xb0>=xb1&&xb0>=xb2&&xb0>=xb3)maxXb=xb0;
                else if(xb1>=xb0&&xb1>=xb2&&xb1>=xb3)maxXb=xb1;
                else if(xb2>=xb0&&xb2>=xb1&&xb2>=xb3)maxXb=xb2;
                else if(xb3>=xb0&&xb3>=xb1&&xb3>=xb2)maxXb=xb3;
                if(xb0<=xb1&&xb0<=xb2&&xb0<=xb3)minXb=xb0;
                else if(xb1<=xb0&&xb1<=xb2&&xb1<=xb3)minXb=xb1;
                else if(xb2<=xb0&&xb2<=xb1&&xb2<=xb3)minXb=xb2;
                else if(xb3<=xb0&&xb3<=xb2&&xb3<=xb2)minXb=xb3;

                if(xp1<=maxXb&&xp0>=minXb)     xintercept=1;
                else if(xp1<=maxXb&&xp0>=minXb)xintercept=1;
                else if(xp1<=maxXb&&xp0>=minXb)xintercept=1;
                else if(xp1<=maxXb&&xp0>=minXb)xintercept=1;
                else                           xintercept=0;
              }else xintercept=0;

              if(xintercept){
                for(n=0;n<Hnum[1];n++){
                  /*test for y intercept*/
                  if(y>=0){
                    yp0=(k-Hnum[1]/2+0.5)*dimage->Hres;
                    yp1=(k-Hnum[1]/2-0.5)*dimage->Hres;
                  }else{
                    yp0=(k-Hnum[1]/2-0.5)*dimage->Hres;
                    yp1=(k-Hnum[1]/2+0.5)*dimage->Hres;
                  }
                  if(dimage->Hres*2>=r){ /*see if bin edges are within pixel*/
                    if(yb0<=yp0&&yb0>=yp1)     yintercept=1;
                    else if(yb1<=yp0&&yb1>=yp1)yintercept=1;
                    else if(yb2<=yp0&&yb2>=yp1)yintercept=1;
                    else if(yb3<=yp0&&yb3>=yp1)yintercept=1;
                    else                       yintercept=0;
                  }else if(dimage->Hres*2<r){ /*see if pixel edged are within bin*/
                    /*find min and max yb*/
                    if(yb0>=yb1&&yb0>=yb2&&yb0>=yb3)maxYb=yb0;
                    else if(yb1>=yb0&&yb1>=yb2&&yb1>=yb3)maxYb=yb1;
                    else if(yb2>=yb0&&yb2>=yb1&&yb2>=yb3)maxYb=yb2;
                    else if(yb3>=yb0&&yb3>=yb1&&yb3>=yb2)maxYb=yb3;
                    if(yb0<=yb1&&yb0<=yb2&&yb0<=yb3)minYb=yb0;
                    else if(yb1<=yb0&&yb1<=yb2&&yb1<=yb3)minYb=yb1;
                    else if(yb2<=yb0&&yb2<=yb1&&yb2<=yb3)minYb=yb2;
                    else if(yb3<=yb0&&yb3<=yb2&&yb3<=yb2)minYb=yb3;

                    if(yp1<=maxYb&&yp0>=minYb)     yintercept=1;
                    else if(yp1<=maxYb&&yp0>=minYb)yintercept=1;
                    else if(yp1<=maxYb&&yp0>=minYb)yintercept=1;
                    else if(yp1<=maxYb&&yp0>=minYb)yintercept=1;
                    else                           yintercept=0;
                  }else yintercept=0;
                  if(yintercept){
                    /*allocate this bin to this pixel*/
                    Pplace=k*Hnum[1]+n;
                    if(!(maptransf=(int *)calloc(2*dimage->HbinN[Pplace],sizeof(int)))){
                      fprintf(stderr,"error in height profile map allocation.\n");
                      exit(1);
                    }
                    for(m=0;m<dimage->HbinN[Pplace]*2;m++)maptransf[m]=dimage->Hmap[Pplace][m];
                    dimage->HbinN[Pplace]++;
                    TIDY(dimage->Hmap[Pplace]);
                    dimage->Hmap[Pplace]=NULL;
                    if(!(dimage->Hmap[Pplace]=(int *)calloc(2*dimage->HbinN[Pplace],sizeof(int)))){
                      fprintf(stderr,"error in height profile map 2 allocation.\n");
                      exit(1);
                    }
                    for(m=0;m<(dimage->HbinN[Pplace]-1)*2;m++)dimage->Hmap[Pplace][m]=maptransf[m];
                    dimage->Hmap[Pplace][dimage->HbinN[Pplace]*2-2]=Lplace;
                    dimage->Hmap[Pplace][dimage->HbinN[Pplace]*2-1]=bin;
                    if(maptransf)free(maptransf);
                    maptransf=NULL;

                  }/*y intercept check*/
                }  /*y pixel step*/
              } /*x intercept check*/
            }   /*x pixel step*/
          }/*end of within bounds check*/
        }  /*end of radiance test*/
      }  /*end of bin loop*/
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    }   /*azimuth loop*/
  }     /*zenith loop*/
  printf("Mapped\n");

  /*check map*/
  for(i=0;i<Hnum[0];i++){
    for(j=0;j<Hnum[1];j++){
      Pplace=i*Hnum[1]+j;
      printf("%d ",(int)dimage->HbinN[Pplace]);
    }
    printf("\n");
  }


  /*now we have a map we can have a go at finding the maximum height for each pixel*/
  for(i=0;i<Hnum[0];i++){
    for(j=0;j<Hnum[1];j++){
      Pplace=i*Hnum[1]+j;
      for(n=0;n<dimage->HbinN[Pplace];n++){
        Lplace=dimage->Hmap[Pplace][2*n];
        bin=dimage->Hmap[Pplace][2*n+1];
        height=(bin+0.5)*ratPoint[numb].bin_L/2*cos(ratPoint[numb].theta[Lplace]);
        if(height>Hprof[Pplace])Hprof[Pplace]=height;
      } 
    }
  }

  /*output results to an ascii file*/
  for(i=0;i<Hnum[0];i++){
    for(j=0;j<Hnum[1];j++){
      Pplace=i*Hnum[1]+i;
      fprintf(output,"%d ",(int)Hprof[Pplace]);
    }
    fprintf(output,"\n");
  }
  fclose(output);
}

/*######################################################################################################*/
/*find the direct and multiple scattered fractions for each zenith ring*/

void scatterFraction(RatImage *dimage,RatControl *ratPoint,int Numb)
{
  int i=0,j=0,k=0,c=0,m=0;
  int numb=0,place=0,side=0;
  int centre=0;  /*number of annuli*/
  register int time=0,l=0;
  double *direct=NULL,*total=NULL,*multiple=NULL;  /*mean values*/
  double *dirVar=NULL,*toVar=NULL,*multVar=NULL;   /*variances*/
  FILE *output=NULL;
  char namen[200];

  if(dimage->scanN==1)sprintf(namen,"%s.contributions",dimage->output);
  else sprintf(namen,"%s.%d.%d.%d.contributions",dimage->output,(int)(ratPoint[Numb].from[0]),\
    (int)(ratPoint[Numb].from[1]),(int)(ratPoint[Numb].from[2]));
  if((output=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error Opening results file %s\n",namen);
    exit(1);
  }

  printf("\nOutputting scattering to %s...\n\n",namen);

  /*find the centre, the bit looking straight up*/
  for(i=0;i<ratPoint[Numb].NZscans;i++){
    if(sin(ratPoint[Numb].theta[i*ratPoint[Numb].NAscans])<0.00001&&\
      sin(ratPoint[Numb].theta[i*ratPoint[Numb].NAscans])>-0.00001)centre=i;
  }
  if(centre==0||centre==ratPoint[Numb].NZscans-1){
    fprintf(stderr,"You have used a lopsided zenith range. %d %d\nNothing wrong with that, but this program won't work\n",\
      centre,ratPoint[Numb].NZscans);
    exit(1);
  }
  if(!(direct=(double *)calloc(centre+1,sizeof(double)))){
    fprintf(stderr,"error in direct contributions allocation.\n");
    exit(1);
  }
  if(!(total=(double *)calloc(centre+1,sizeof(double)))){
    fprintf(stderr,"error in total reflectance allocation.\n");
    exit(1);
  }
  if(!(multiple=(double *)calloc(centre+1,sizeof(double)))){
    fprintf(stderr,"error in multiple contributions allocation.\n");
    exit(1);
  }
  if(!(dirVar=(double *)calloc(centre+1,sizeof(double)))){
    fprintf(stderr,"error in direct contributions allocation.\n");
    exit(1);
  }
  if(!(toVar=(double *)calloc(centre+1,sizeof(double)))){
    fprintf(stderr,"error in total reflectance allocation.\n");
    exit(1);
  }
  if(!(multVar=(double *)calloc(centre+1,sizeof(double)))){
    fprintf(stderr,"error in multiple contributions allocation.\n");
    exit(1);
  }

  for(time=0;time<2;time++){
    /*step around zeniths, from the centre outwards*/
    for(i=0;i<=centre;i++){
      /*for both sides*/
      for(side=0;side<2;side++){
        if(side)numb=i;
        else{ /*search through the other side to find the correct angle*/
          k=0;
          while(k<ratPoint[Numb].NZscans){
            if(ratPoint[Numb].NZscans-(i+k)<ratPoint[Numb].NZscans&&ratPoint[Numb].NZscans-(i+k)>=0){
              if(cos(ratPoint[Numb].theta[numb])-cos(ratPoint[Numb].theta[(ratPoint[Numb].NZscans-(i+k))\
                *ratPoint[Numb].NAscans])<0.00001&&cos(ratPoint[Numb].theta[numb])-\
                 cos(ratPoint[Numb].theta[(ratPoint[Numb].NZscans-(i+k))*ratPoint[Numb].NAscans])>-0.00001){
                numb=ratPoint[Numb].NZscans-(i+k);
                break;
              }
            }
            k++;
          }
        }
        if(numb>=0&&numb<ratPoint[Numb].NZscans){
          /*step around azimuth*/
          for(j=0;j<ratPoint[Numb].NAscans;j++){
            place=numb*ratPoint[Numb].NAscans+j;
            /*read refl and step through ranging bins*/
            ratPoint=resultsArrange(dimage,ratPoint,Numb,place,0);
            for(k=0;k<ratPoint[Numb].bins;k++){     /*step through bins*/
              for(m=0;m<dimage->nBands;m++){ /*through wavebands*/
                l=dimage->band[Numb][m];
                if(!time){ /*calculate the means normaliser as we go along to ease the variance calculation*/
                  total[numb]=total[numb]+\
                    ratPoint[Numb].ratRes[place].refl[k*(ratPoint[Numb].n+1)*ratPoint[Numb].nBands+(ratPoint[Numb].n+1)*l]/\
                    (2*ratPoint[Numb].NAscans*ratPoint[Numb].nBands);
                  direct[numb]=direct[numb]+\
                    ratPoint[Numb].ratRes[place].refl[k*(ratPoint[Numb].n+1)*ratPoint[Numb].nBands+(ratPoint[Numb].n+1)*l+1]/\
                    (2*ratPoint[Numb].NAscans*ratPoint[Numb].nBands);
                  if(ratPoint[Numb].n>1){
                    for(c=2;c<ratPoint[Numb].n;c++){ /*through the orders of scattering*/
                      multiple[numb]=multiple[numb]+\
                        ratPoint[Numb].ratRes[place].refl[k*(ratPoint[Numb].n+1)*ratPoint[Numb].nBands+(ratPoint[Numb].n+1)*l+c]/\
                    (2*ratPoint[Numb].NAscans*ratPoint[Numb].nBands);
                    }
                  }else multiple[numb]=0;
                }else{     /*calculate the variances*/
                  toVar[numb]=toVar[numb]+(ratPoint[Numb].ratRes[place].refl[k*(ratPoint[Numb].n+1)*\
                    ratPoint[Numb].nBands+(ratPoint[Numb].n+1)*l]-direct[numb])*\
                    (ratPoint[Numb].ratRes[place].refl[k*(ratPoint[Numb].n+1)*ratPoint[Numb].nBands+(ratPoint[Numb].n+1)*l]\
                    -direct[numb]);
                  dirVar[numb]=dirVar[numb]+(ratPoint[Numb].ratRes[place].refl[k*(ratPoint[Numb].n+1)*\
                    ratPoint[Numb].nBands+(ratPoint[Numb].n+1)*l+1]-direct[numb])*\
                   (ratPoint[Numb].ratRes[place].refl[k*(ratPoint[Numb].n+1)*ratPoint[Numb].nBands+(ratPoint[Numb].n+ 1)*l+1]\
                   -direct[numb]);
                  for(c=2;c<ratPoint[Numb].n;c++){ /*through the orders of scattering*/
                    multVar[numb]=multVar[numb]+(ratPoint[Numb].ratRes[place].refl[k*(ratPoint[Numb].n+1)*\
                      ratPoint[Numb].nBands+(ratPoint[Numb].n+1)*l+c]-multiple[numb])*\
                      (ratPoint[Numb].ratRes[place].refl[k*(ratPoint[Numb].n+1)*ratPoint[Numb].nBands+(ratPoint[Numb].n+1)*l+c]\
                      -multiple[numb]);
                  }
                }
              }
            }
            ratPoint=clearArrays(ratPoint,Numb,place,dimage); /*save RAM*/
          }
        }
      }
    }
  }/*now we know the total values we can find the variances, repeat the same loop*/

  fprintf(output,"Zenith, Radiance, Variance, Direct, Variance, Scattered, Variance.\n");
  /*normalise and write out results*/
  for(i=centre;i>=0;i--){
    if(total[i]) fprintf(output,"%f %f %f %f %f %f %f\n",sqrt(ratPoint[Numb].theta[i*ratPoint[Numb].NAscans]*\
      ratPoint[Numb].theta[i*ratPoint[Numb].NAscans])*180/M_PI,\
      total[i],toVar[i]/(2*ratPoint[Numb].NAscans*ratPoint[Numb].nBands),\
      direct[i]/total[i]*100,dirVar[i]*100/(2*ratPoint[Numb].NAscans*ratPoint[Numb].nBands),\
      multiple[i]/total[i]*100,multVar[i]*100/(2*ratPoint[Numb].NAscans*ratPoint[Numb].nBands));
    else fprintf(output,"%f %f %f %f %f %f %f\n",ratPoint[Numb].theta[i*ratPoint[Numb].NAscans]*180/M_PI,0.0,0.0,0.0,0.0,0.0,0.0);
  }
  if(direct)free(direct);if(total)free(total);if(multiple)free(multiple);
  if(dirVar)free(dirVar);if(toVar)free(toVar);if(multVar)free(multVar);
  if(output)fclose(output);
}

/*######################################################################*/
/*make a panoramic image*/

RatImage *panorama(RatControl *ratPoint,RatImage *dimage,int numb,int argc,char **argv)
{
  int x=0,y=0,i=0,j=0;
  int Lplace=0,bin=0,band=0;
  int pixPlace=0;
  int imageSi=0,xSi=0,ySi=0;
  float *shade=NULL,max=0;
  unsigned char *pixel=NULL;
  FILE *panorama=NULL;
  char namen[200];
  void update_header(),init_header(),fp_fwrite_header();
  struct header hd;

  printf("Creating a panoramic image...\n");

  if(dimage->scanN==1)sprintf(namen,"%s.panorama",dimage->output);
  else sprintf(namen,"%s.%d.panorama",dimage->output,numb);
  if((panorama=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening ascii results file %s\n",namen);
    exit(1);
  }

  xSi=(int)(ratPoint[numb].NAscans/dimage->pzoom);
  ySi=(int)(ratPoint[numb].NZscans/dimage->pzoom);
  imageSi=xSi*ySi;
  printf("%d\n",imageSi);
  if(!(shade=(float *)calloc(imageSi*ratPoint[numb].nBands,sizeof(float)))){
    fprintf(stderr,"error in panoramic image buffer allocation number 1.\n");
    exit(1);
  }

  for(y=0;y<ySi;y++){
    for(x=0;x<xSi;x++){
      pixPlace=y*xSi+x;
if(pixPlace>=imageSi){fprintf(stderr,"pixel error\n");exit(1);}
      for(i=0;i<dimage->pzoom;i++){
        for(j=0;j<dimage->pzoom;j++){
          Lplace=(dimage->pzoom*y+i)*ratPoint[numb].NAscans+(dimage->pzoom*x+j);
if(Lplace>=ratPoint[numb].NZscans*ratPoint[numb].NAscans){fprintf(stderr,"beam error\n");exit(1);}
          ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
          for(bin=0;bin<ratPoint[numb].bins;bin++){
            for(band=0;band<ratPoint[numb].nBands;band++){
              shade[pixPlace+band*imageSi]+=ratPoint[numb].ratRes[Lplace].refl\
                [bin*ratPoint[numb].nBands*(ratPoint[numb].n+1)+band*(ratPoint[numb].n+1)]/((bin+0.5)*ratPoint[numb].bin_L/2);
            }
          }
          ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
        }
      }
    }
  }
  /*find max pixel brightness*/
  for(i=0;i<imageSi*ratPoint[numb].nBands;i++)if(shade[i]>max)max=shade[i];
  if(!(pixel=(unsigned char *)calloc(imageSi*ratPoint[numb].nBands,sizeof(unsigned char)))){
    fprintf(stderr,"error in panoramic image allocation number 1.\n");
    exit(1);
  }
  for(i=0;i<imageSi*ratPoint[numb].nBands;i++){
    pixel[i]=(unsigned char)(shade[i]*255.0/max);
  }
  TIDY(shade);
  init_header(&hd,panorama," ",ratPoint[numb].nBands,"today",xSi,ySi,8*sizeof(unsigned char),0,PFBYTE," ");
  update_header(&hd,argc,argv);
  fp_fwrite_header(panorama,&hd);
  if(fwrite(&(pixel[0]),sizeof(unsigned char),(size_t)(imageSi*ratPoint[i].nBands),panorama)!=(size_t)(imageSi*ratPoint[i].nBands)){
    fprintf(stderr,"panoramic image not written\n");
    exit(1);
  }
  printf("Panoramic image created.\n");
  TIDY(pixel);
  TIDY(panorama);
  return(dimage);
}

/*############################################################################################################*/
/*check the data for any misplaced numbers*/

RatImage *checkData(RatControl *ratPoint,int numb,RatImage *dimage)
{
  int i=0,j=0,d=0,seg=0;
  int Lplace=0,bin=0,band=0,n=0,mat=0;
  int reflLength=0;
  float Tmat=0;
  FILE *opoo=NULL;
  char namen[100];

  reflLength=ratPoint[numb].bins*ratPoint[numb].nBands*(ratPoint[numb].n+1);

  sprintf(namen,"%s.errorlog",dimage->output);
  if((opoo=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening data error log%s\n",namen);
    exit(1);
  }
  printf("Checking data for errors, printing to %s\n",namen);
  for(i=0;i<ratPoint[numb].NZscans;i++){
    for(j=0;j<ratPoint[numb].NAscans;j++){
      Lplace=i*ratPoint[numb].NAscans+j;
      /*check angles*/
      if(ratPoint[numb].theta[Lplace]>2*M_PI||ratPoint[numb].theta[Lplace]<-2*M_PI)\
         fprintf(opoo,"Theta error for %d. %f not equal to %f\n",Lplace,ratPoint[numb].theta[Lplace],\
                                              ratPoint[numb].iZen+i*ratPoint[numb].div);
      if(ratPoint[numb].thata[Lplace]>2*M_PI||ratPoint[numb].thata[Lplace]<-2*M_PI)\
         fprintf(opoo,"Thata error for %d. %f not equal to %f\n",Lplace,ratPoint[numb].thata[Lplace],ratPoint[numb].iAz+j*ratPoint[numb].div);
      /*check the compression map*/
      if(Lplace) if(ratPoint[numb].ratRes[Lplace].coords[0]<ratPoint[numb].ratRes[Lplace-1].coords[2])\
        fprintf(opoo,"Compression map error %d %d out of order %f\n",Lplace,(int)ratPoint[numb].ratRes[Lplace].coords[0],0.0);
      for(d=1;d<3;d++){
        if(ratPoint[numb].ratRes[Lplace].coords[d]<ratPoint[numb].ratRes[Lplace].coords[d-1])\
          fprintf(opoo,"Compression map error %d out of order %d comes before %d coord %d\n",Lplace,\
            ratPoint[numb].ratRes[Lplace].coords[d],ratPoint[numb].ratRes[Lplace].coords[d-1],d);
      }
      /*then check through the data*/
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,4);
      Tmat=0;
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        for(band=0;band<ratPoint[numb].nBands;band++){
          for(seg=0;seg<dimage->withinSegs+dimage->withoutSegs;seg++){
            if(ratPoint[numb].ratRes[Lplace].refl[seg*reflLength+(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]>1)\
                fprintf(opoo,"refl error %d %f\n",Lplace,\
                  ratPoint[numb].ratRes[Lplace].refl[seg*reflLength+(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]);
            for(n=1;n<ratPoint[numb].n;n++){
              if((ratPoint[numb].ratRes[Lplace].refl[seg*reflLength+(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)+n]-\
                  ratPoint[numb].ratRes[Lplace].refl[seg*reflLength+(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)])>THRESHOLD)\
                fprintf(opoo,"refl error beam %d bin %d order %d band %d segment %d total %f scattered %f ratio %f\n",Lplace,bin,n,band,seg,\
                 ratPoint[numb].ratRes[Lplace].refl[seg*reflLength+(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)],\
                 ratPoint[numb].ratRes[Lplace].refl[seg*reflLength+(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)+n],\
                 ratPoint[numb].ratRes[Lplace].refl[seg*reflLength+(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]/\
                 ratPoint[numb].ratRes[Lplace].refl[seg*reflLength+(bin*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)+n]);
            }
          }
        }  /*end of radiant flux testing*/
        for(mat=0;mat<ratPoint[numb].nMat;mat++){
          Tmat+=ratPoint[numb].ratRes[Lplace].material[bin*ratPoint[numb].nMat*2+mat*2];
          if(ratPoint[numb].ratRes[Lplace].material[mat*2+d]>100.00002||\
             ratPoint[numb].ratRes[Lplace].material[bin*dimage->matN*2+mat*2]<0)\
            fprintf(opoo,"material error %d. %f\n",Lplace,ratPoint[numb].ratRes[Lplace].material[bin*dimage->matN*2+mat*2]);
        }
      }
      if(Tmat>100.00002){
        fprintf(opoo,"Total percentage accounted for %f made from %f and %f\n",\
                Tmat+ratPoint[numb].ratRes[Lplace].sky,Tmat,ratPoint[numb].ratRes[Lplace].sky);
      }
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    }
  }
  fprintf(opoo,"Those are the obvious errors in this data file. Happy?\n");
  if(opoo)fclose(opoo);
  return(dimage);
} /*checkData*/

/*#############################################################################################*/
/*extract tree height from lidar data, and find apparent truth*/

void invertAscope(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0,k=0;
  int place=0;
  int numb=0,Lplace=0;
  int topBin=0,botBin=0;
  int dtopBin=0,dbotBin=0;
  char gap=0,dgap=0;               /*indicator*/
  double max=0,min=0;
  double dist=0,thresh=0;

  if(dimage->scanN!=1){
    fprintf(stderr,"You should not trying to invert tree height from more than one file at a time\n");
    exit(1);
  }
  if(ratPoint[0].NAscans*ratPoint[0].NZscans!=1){
    fprintf(stderr,"A-scope files should only really have one beam, not %d\n",\
      ratPoint[0].NAscans*ratPoint[0].NZscans);
    exit(1);
  }
  if(dimage->nBands!=1){
    fprintf(stderr,"A-scope inversion should only really be done with one waveband, not %d\n",dimage->nBands);
    exit(1);
  }

  numb=0;
  Lplace=0;
  ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);  /*deconvolution in here*/

  /*find noise level*/
  while(dist<=dimage->Athresh&&i<ratPoint[numb].bins){
    dist=(double)i*ratPoint[numb].bin_L+ratPoint[numb].min_R;
    place=findReflIndex(dimage,ratPoint,numb,i,0,0);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>thresh)thresh=ratPoint[numb].ratRes[Lplace].refl[place];
    i++;
  }

  printf("\nAttempting to retrieve tree height\n");
  /*find tree top*/
  for(i=0;i<ratPoint[0].bins;i++){
    place=findReflIndex(dimage,ratPoint,numb,i,0,0);
    if(ratPoint[0].ratRes[0].refl[place]>thresh){
      topBin=i;
      break;
    }
  }
  /*find the bottom*/
  min=10000;
  max=-10000;
  for(i=ratPoint[numb].bins-2;i>=topBin;i--){
    place=findReflIndex(dimage,ratPoint,numb,i,0,0);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>thresh){
      printf("Found the start of the bottom at %f, intensity %f\n",(double)i*ratPoint[numb].bin_L+\
         ratPoint[numb].min_R,ratPoint[numb].ratRes[Lplace].refl[place]);
      for(j=i;j>=topBin;j--){
        place=findReflIndex(dimage,ratPoint,numb,j,0,0);
        if(ratPoint[numb].ratRes[Lplace].refl[place]>max)max=ratPoint[numb].ratRes[Lplace].refl[place];
        else if(ratPoint[numb].ratRes[Lplace].refl[place]<max){
          printf("Found the peak at %f intensity %f\n",(double)j*ratPoint[numb].bin_L+ratPoint[numb].min_R,\
            ratPoint[numb].ratRes[Lplace].refl[place]);
          botBin=j+1;
          for(k=j;k>topBin;k--){  /*see how low it falls*/
            place=findReflIndex(dimage,ratPoint,numb,k,0,0);
            if(ratPoint[numb].ratRes[Lplace].refl[place]<min){
              min=ratPoint[numb].ratRes[Lplace].refl[place];
              if(min<thresh){
                gap=1;
                break;
              }
            }else if(ratPoint[numb].ratRes[Lplace].refl[place]>min){ /*back up*/
              gap=1;
              break;
            }
          }
          break;
        }
      }
      break;
    }
  }
  /*repeat using only direct light*/
  /*find tree top*/
  for(i=0;i<ratPoint[0].bins;i++){
    place=findReflIndex(dimage,ratPoint,numb,i,0,1);
    if(ratPoint[0].ratRes[0].refl[place]>thresh){
      dtopBin=i;
      break;
    }
  }
  /*find the bottom*/
  min=10000;
  max=-10000;
  for(i=ratPoint[numb].bins-2;i>=dtopBin;i--){
    place=findReflIndex(dimage,ratPoint,numb,i,0,1);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>thresh){
      printf("Found the start of the bottom at %f, intensity %f\n",(double)i*ratPoint[numb].bin_L+\
         ratPoint[numb].min_R,ratPoint[numb].ratRes[Lplace].refl[place]);
      for(j=i;j>=dtopBin;j--){
        place=findReflIndex(dimage,ratPoint,numb,j,0,1);
        if(ratPoint[numb].ratRes[Lplace].refl[place]>max)max=ratPoint[numb].ratRes[Lplace].refl[place];
        else if(ratPoint[numb].ratRes[Lplace].refl[place]<max){
          printf("Found the peak at %f intensity %f\n",(double)j*ratPoint[numb].bin_L+ratPoint[numb].min_R,\
            ratPoint[numb].ratRes[Lplace].refl[place]);
          dbotBin=j+1;
          for(k=j;k>dtopBin;k--){  /*see how low it falls*/
            place=findReflIndex(dimage,ratPoint,numb,k,0,1);
            if(ratPoint[numb].ratRes[Lplace].refl[place]<min){
              min=ratPoint[numb].ratRes[Lplace].refl[place];
              if(min<thresh){
                dgap=1;
                break;
              }
            }else if(ratPoint[numb].ratRes[Lplace].refl[place]>min){ /*back up*/
              dgap=1;
              break;
            }
          }
          break;
        }
      }
      break;
    }
  }


  ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);

  printf("The threshold picked was %f\n",thresh);
  printf("Top %d at %f, bottom %d at %f\n",topBin,topBin*ratPoint[numb].bin_L+ratPoint[numb].min_R,\
                                       botBin,botBin*ratPoint[numb].bin_L+ratPoint[numb].min_R);

  if(!gap){
    printf("There is no gap between the ground and canopy.\nUnreliable inversion\n");
    printf("Tree height might be %f\n",(double)(botBin-topBin)*ratPoint[0].bin_L/2.0);
  }else{
    if(min<=thresh){
      printf("There is a clear gap between the ground and canopy\n");
    }else if(min>thresh){
      printf("There is a gap between the ground and canopy, though it' not great %f\n",min);
    }
    printf("Tree height is %f\n",(double)(botBin-topBin)*ratPoint[0].bin_L/2.0);
  }
  printf("\nIgnoring multiple scattering\n");
  if(!gap){
    printf("There is no gap between the ground and canopy.\nUnreliable inversion\n");
    printf("Tree height might be %f\n",(double)(dbotBin-dtopBin)*ratPoint[0].bin_L/2.0);
  }else{
    if(min<=thresh){
      printf("There is a clear gap between the ground and canopy\n");
    }else if(min>thresh){
      printf("There is a gap between the ground and canopy, though it' not great %f\n",min);
    }
    printf("Tree height is %f\n",(double)(dbotBin-dtopBin)*ratPoint[0].bin_L/2.0);
  }
  return;
}/*invertAscope*/

/*#########################################################*/
/*deconvolve the lidar beam from the pulse*/

void deconvolvePulse(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace)
{
  int bin=0,j=0,n=0;
  int band=0,rPlace=0;
  int reflLength=0;
  int rPlaceTwo=0;
  int pulseBins=0;                        /*number of bins a pulse stradles*/
  double *pulse=NULL,*tpulse=NULL;
  double *refl=NULL,temp=0;
  char light=0;
  char isolated=0,distinguishable=0;
  void fitPulseNibble(RatImage *,RatControl *,int,int,int,int,double *,int);

  printf("deconvolution\n");

  if(ratPoint->withinSegs+ratPoint->withoutSegs!=1){
    fprintf(stderr,"Currently this only works with single segment sensors, not %d\n",\
      ratPoint->withinSegs+ratPoint->withoutSegs);
    exit(1);
  }
  if(!(pulse=(double *)calloc(dimage->EpLength,sizeof(double)))){
    fprintf(stderr,"Can't allocate memory to deconvolve lidar beam, number 2\n");
    exit(1);
  }
  if(!(tpulse=(double *)calloc(dimage->EpLength,sizeof(double)))){
    fprintf(stderr,"Can't allocate memory to deconvolve lidar beam, number 2\n");
    exit(1);
  }

  /*find pulse length in terms of lidar bins*/
  pulseBins=(int)(dimage->EpLength/ratPoint[numb].bin_L+0.5);

  if(ratPoint[numb].ratRes[Lplace].refl){
    reflLength=ratPoint[numb].bins*ratPoint[numb].nBands*(ratPoint[numb].n+1);
    if(!(refl=(double *)calloc(reflLength*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs),sizeof(double)))){
      fprintf(stderr,"Can't allocate memory to deconvolve lidar beam\n");
      exit(1);
    }

    /*deconvolve each wavelength separatley*/
    for(band=0;band<dimage->nBands;band++){
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        light=0;
        rPlace=(bin*ratPoint[numb].nBands+dimage->band[numb][band])*(ratPoint[numb].n+1);
        if(ratPoint[numb].ratRes[Lplace].refl[rPlace]>0){
          light=1;
        }
        if(light){   /*then we need to deconvolve*/
          /*find out how isolated the return is*/
          isolated=0;
          for(j=bin+pulseBins-1;j<bin+pulseBins+2;j++){
            rPlace=(j*ratPoint[numb].nBands+dimage->band[numb][band])*(ratPoint[numb].n+1);
            if(ratPoint[numb].ratRes[Lplace].refl[rPlace]==0){
              isolated=1;
              break;
            }
            if(isolated)break;
          } /*isolation test*/

          if(isolated){  /*fit a gaussian and deconvolve that*/

            printf("fitting a pulse to bin %d\n",bin);
            fitPulseNibble(dimage,ratPoint,numb,Lplace,bin,band,refl,pulseBins);

          }else{         /*look harder*/
            distinguishable=0;
            for(j=bin+1;j<bin+pulseBins+1;j++){
              rPlace=(j*ratPoint[numb].nBands+dimage->band[numb][band])*(ratPoint[numb].n+1);
              rPlaceTwo=((j-1)*ratPoint[numb].nBands+dimage->band[numb][band])*(ratPoint[numb].n+1);
              if(ratPoint[numb].ratRes[Lplace].refl[rPlace]>ratPoint[numb].ratRes[Lplace].refl[rPlaceTwo]){
                distinguishable=1;
                break;
              }
              if(distinguishable)break;
            } /*peak separation test*/
            if(distinguishable){ /*fit the leading edge of a Gaussian*/

              printf("tip is distinguishable\n");

            }else{               /*gawd knows*/

              fprintf(stderr,"Really really hard\n");
              exit(1);

            }/*peak check*/
          }/*isolation check*/

        }/*light check*/
      }/*bin loop*/

      /*now transfer*/
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        for(n=0;n<ratPoint[numb].n+1;n++){
          Lplace=findReflIndex(dimage,ratPoint,numb,bin,band,n);
          temp=0;                         /*to hold anything that hasn't been deconvolved*/
          if(ratPoint[numb].ratRes[Lplace].refl[Lplace]!=0)temp=ratPoint[numb].ratRes[Lplace].refl[Lplace];
          ratPoint[numb].ratRes[Lplace].refl[Lplace]=refl[Lplace]+temp;
        }
      }

    }/*band loop*/

    /*now transfer*/
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      for(band=0;band<dimage->nBands;band++){
        for(n=0;n<ratPoint[numb].n+1;n++){
          Lplace=findReflIndex(dimage,ratPoint,numb,bin,band,n);
          temp=0;                         /*to hold anything that hasn't been deconvolved*/
          if(ratPoint[numb].ratRes[Lplace].refl[Lplace]!=0)temp=ratPoint[numb].ratRes[Lplace].refl[Lplace];
          ratPoint[numb].ratRes[Lplace].refl[Lplace]=refl[Lplace]+temp;
        }
      }
    }

    TIDY(refl);
  }else{fprintf(stderr,"you're an idiot\n");exit(1);}/*reflectance deconvolution*/

  if(ratPoint[numb].ratRes[Lplace].material){


  }/*deconvolve material information*/

  TIDY(pulse);
  TIDY(tpulse);

  return;
}

/*##########################################################*/
/*fit a pulse to an isolated return and nibble off*/

void fitPulseNibble(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace,int bin,int band,double *refl,int pulseBins)
{
  int i=0,count=0;
  int pbin=0,rPlace=0;
  int effectiveLength=0;  /*effective pulse dimensions for truncated pulses*/
  int effPulseBins=0;     /* see above*/
  double *binnedPulse=NULL,*scaledPulse=NULL;
  double *tempPulse=NULL;
  double pulseCentre=0;
  double startCentre=0;
  double error=0,terror=0;
  double totalBeamEnergy=0,energyCheck=0;
  double threshold=0.0001;
  double step=0,dist=0;
  double leftE=0,rightE=0;                     /*energies for skew*/
  char skew=0;                                 /*an indicator, -1 left, 0 none, +1 right*/
  void nibblePulse(RatImage *,RatControl *,double *,int,int,int,double,double *,int);
  double findatip(RatImage *,RatControl *,int,int,int,int,int);

  if(!(binnedPulse=(double *)calloc(pulseBins+2,sizeof(double)))){
    fprintf(stderr,"error in binned pulse shape allocation.\n");
    exit(1);
  }
  if(!(scaledPulse=(double *)calloc(dimage->EpLength,sizeof(double)))){
    fprintf(stderr,"error in binned pulse shape allocation.\n");
    exit(1);
  }

  /*find amplitude from energy conservation*/
  for(i=0;i<pulseBins;i++){
    rPlace=findReflIndex(dimage,ratPoint,numb,bin+i,band,0);
    totalBeamEnergy+=ratPoint[numb].ratRes[Lplace].refl[rPlace]*ratPoint[numb].bin_L;
  }
  effectiveLength=dimage->EpLength;
  for(i=0;i<dimage->EpLength;i++){
    scaledPulse[i]=totalBeamEnergy*dimage->pulse[i];
    energyCheck+=scaledPulse[i]*dimage->Pres;
    if(i<=(int)(dimage->EpLength/2)&&scaledPulse[i]<THRESHOLD/100){   /*the pulse length is truncated*/
      effectiveLength=(dimage->EpLength-i)*2;
    }
  }
  if(effectiveLength!=dimage->EpLength){   /*the pulse needs reshuffling*/
    if(!(tempPulse=(double *)calloc(effectiveLength,sizeof(double)))){
      fprintf(stderr,"error in temporary pulse shape allocation.\n");
      exit(1);
    }
    for(i=0;i<effectiveLength;i++){
      tempPulse[i]=scaledPulse[i+(dimage->EpLength-effectiveLength)/2];
    }
    TIDY(scaledPulse);
    if(!(scaledPulse=(double *)calloc(effectiveLength,sizeof(double)))){
      fprintf(stderr,"error in temporary pulse shape allocation.\n");
      exit(1);
    }
    for(i=0;i<effectiveLength;i++){
      scaledPulse[i]=tempPulse[i];
    }
    TIDY(tempPulse);
    effPulseBins=(double)effectiveLength*dimage->Pres/ratPoint[numb].bin_L;
  }else{
    effPulseBins=pulseBins;
  }  /*dealing with truncated pulses (truncated by rounding)*/

  if((totalBeamEnergy-energyCheck)*(totalBeamEnergy-energyCheck)>THRESHOLD*THRESHOLD){
    fprintf(stderr,"Energy error, %f %f\n",energyCheck,totalBeamEnergy);
    fprintf(stderr,"Maybe a tolerance is needed?\n");
    exit(1);
  }

  /*estimate pulse centre*/
  pulseCentre=findatip(dimage,ratPoint,numb,Lplace,bin,band,pulseBins+2);
  /*pulseCentre=((double)bin+(double)effPulseBins/2.0)*ratPoint[numb].bin_L-ratPoint[numb].min_R;*/
  startCentre=pulseCentre;

  step=ratPoint[numb].bin_L; /*the centre is going to be in the bin, somewhere*/

  do{   /*############ least squares iteration #################*/

/*
#    if(rightE>0&&leftE>0){
#      fprintf(stderr,"rounding and stuff is adding extra energy, thresholds needed?\n");
#      exit(1);
#    }else if(rightE<0&&leftE<0){
#      fprintf(stderr,"rounding and stuff is losing energy, thresholds needed?\n");
#      exit(1);
#    }
*/

    if(leftE<rightE){        /*shift centre right*/
      if(skew<0)step/=2.0;   /*stepped over answer*/
      skew=1;
      pulseCentre+=step;
      printf("shift right\n");
    }else if(leftE>rightE){  /*shift centre left*/
      if(skew>0)step/=2.0;   /*stepped over answer*/
      skew=-1;
      pulseCentre-=step;
      printf("shift left\n");
    }else if(leftE==rightE&&count!=0){
      skew=0;
      printf("There is no skew, at all\n");
      printf("total error %f\n",terror);
      break;
    }else{
      printf("Some complex fiddleing needed\n");
    }

    if(step<THRESHOLD){
      printf("the step has become negligeable\n");
      break;
    }
    if(sqrt((pulseCentre-startCentre)*(pulseCentre-startCentre))>ratPoint[numb].bin_L){
      printf("our initial estimate was way off\n");
    }

    /*bin up pulse*/
    for(i=0;i<pulseBins+2;i++)binnedPulse[i]=0;
    for(i=0;i<effectiveLength;i++){                   /*dist is from old centre*/
      dist=startCentre-pulseCentre+((double)i-(double)effectiveLength/2.0)*(double)dimage->Pres;
      pbin=(int)((pulseBins+2)/2)+(int)(dist/ratPoint[numb].bin_L+0.5);
      if(pbin<0||pbin>pulseBins){
        fprintf(stderr,"crap %d of %d at %d of %d spread %f\n",pbin,pulseBins,\
			i,effectiveLength,startCentre-pulseCentre);
        exit(1);
      }
      binnedPulse[pbin]+=scaledPulse[i];
printf("%d %f %d %f\n",i,scaledPulse[i],pbin,binnedPulse[pbin]);
    }

    leftE=0;
    rightE=0;
    /*find mean square error and direction of any skew*/
    for(i=bin;i<bin+pulseBins+2;i++){
      rPlace=findReflIndex(dimage,ratPoint,numb,i,band,0);
      error=ratPoint[numb].ratRes[Lplace].refl[rPlace]-binnedPulse[i-bin];
      terror+=error*error;
      if(i-bin<(int)((float)(pulseBins+2)/2.0))leftE+=error;
      else                                    rightE+=error; 
    }
    terror=sqrt(terror)/(double)pulseBins;
    leftE=leftE*2.0/(double)pulseBins;
    rightE=rightE*2.0/(double)pulseBins;

    count++;
    printf("iteration %d, error %f, left %f, right %f, centre %f\n",count,terror,leftE,rightE,pulseCentre);

  }while(terror>threshold&&count<1000);

  nibblePulse(dimage,ratPoint,refl,numb,Lplace,band,pulseCentre,binnedPulse,pulseBins);

  TIDY(binnedPulse);
  TIDY(scaledPulse);

  return;
}/*fitPulseNibble*/

/*##########################################################*/
/*nibble (deconvolve) a known pulse from a beam*/

void nibblePulse(RatImage *dimage,RatControl *ratPoint,double *refl,int numb,int Lplace,int band,double pulseCentre,double *pulse,int pulseBins)
{
  int i=0,bin=0,n=0;
  int rPlace=0;
  double energy=0;

  printf("nibbleing\n");
/*  for(n=0;n<ratPoint[numb].n+1;n++){*/
  n=0;  /*a simplifying fudge for now*/
    energy=0;
    for(i=0;i<pulseBins;i++){
      bin=(int)((pulseCentre-ratPoint[numb].min_R)/ratPoint[numb].bin_L+0.5)+i-(int)((double)pulseBins/2.0);
      rPlace=findReflIndex(dimage,ratPoint,numb,bin,band,n);
      ratPoint[numb].ratRes[Lplace].refl[rPlace]-=pulse[i]*dimage->Pres/ratPoint[numb].bin_L;
      energy+=pulse[i]*dimage->Pres/ratPoint[numb].bin_L;
    }
    bin=(int)((pulseCentre-ratPoint[numb].min_R)/ratPoint[numb].bin_L+0.5);
    rPlace=findReflIndex(dimage,ratPoint,numb,bin,band,n);
    refl[rPlace]+=energy;
  /*}*/

  return;
} /*nibblePulse*/

/*##########################################################*/
/*find an isolated pulse peak*/

double findatip(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace,int bin,int band,int pulseBins)
{
  int i=0,rPlace=0,nbin=0;
  int centralBin=0;
  double pulseCentre=0,max=0;
  char overtop=0;

  for(i=0;i<pulseBins;i++){
    nbin=bin+i;
    rPlace=findReflIndex(dimage,ratPoint,numb,nbin,band,0);
    if(ratPoint[numb].ratRes[Lplace].refl[rPlace]>max){
      max=ratPoint[numb].ratRes[Lplace].refl[rPlace];
      centralBin=nbin;
    }
printf("%f %f\n",ratPoint[numb].ratRes[Lplace].refl[rPlace],max);
    if(ratPoint[numb].ratRes[Lplace].refl[rPlace]<max)overtop=1;
  }

  if(!overtop){
    fprintf(stderr,"balls can't get over the top\n");
    exit(1);
  }

  pulseCentre=((double)nbin+0.5)*ratPoint[numb].bin_L-ratPoint[numb].bin_L;

  return(pulseCentre);
}/*findatip*/

/*#################################################*/
/*print the header information to screen*/

void printdataHeader(RatControl *ratPoint,RatImage *dimage,int numb)
{
  int i=0;
  int *header1=NULL;
  double *header2=NULL;

 if(!(header1=(int *)calloc(ratPoint[numb].headerLength,sizeof(int)))){
    fprintf(stderr,"error in header1 array allocation.\n");
    exit(1);
  }
 if(fseek(dimage->data[numb],(long)0,SEEK_SET)){printf("error seeking to the file start\n");exit(1);}
 if(fread(&(header1[0]),sizeof(int),ratPoint[numb].headerLength,dimage->data[numb])!=ratPoint[numb].headerLength){
    fprintf(stderr,"error reading the header.\n");
    exit(1);
  }
  printf("The integer header, of length %d\n",ratPoint[numb].headerLength);
  for(i=0;i<ratPoint[numb].headerLength;i++){
    printf("%d %d\n",i,header1[i]);
  }
  TIDY(header1);
  if(!(header2=(double *)calloc(ratPoint[numb].header2Length,sizeof(double)))){
    fprintf(stderr,"error in header2 array allocation.\n");
    exit(1);
  }
  if(fseek(dimage->data[numb],(long)(ratPoint[numb].headerLength*sizeof(int)),SEEK_SET)){fprintf(stderr,"error seeking to header2\n");exit(1);}
  if(fread(&(header2[0]),sizeof(double),ratPoint[numb].header2Length,dimage->data[numb])!=ratPoint[numb].header2Length){
    fprintf(stderr,"error reading the header.\n");
    exit(1);
  }
  printf("The double header. length %d\n",ratPoint[numb].header2Length);
  for(i=0;i<ratPoint[numb].header2Length;i++){
    printf("%d %f\n",i,header2[i]);
  }
  TIDY(header2);

  return;
}/*printHeader*/


/*###################################################################*/
/* Quick and dirty function to train Gaussians*/

void trainGauss(RatControl *ratPoint,RatImage *dimage,int numb)
{
  int i=0,j=0,Lplace=0,bin=0,band=0;
  int place=0,oldPlace=0,newPlace=0;
  char started=0,ascending=0,descending=0;
  int waveStart=0,waveEnd=0,lastMax=0;
  int *nfeat=NULL,*nMax=NULL,maxFeat=0;
  int **featureBin=NULL,**featureType=NULL;  /*max=1,min=-1,inflection=2*/
  int maxBand=0,oldband=0,separation=0;
  float **amplitude=NULL,**sigma=NULL,width=0;
  float **energy=NULL,integral=0.0,*sigmaMax=NULL;
  float **ampofMax=NULL,**sigofMax=NULL,**eofMax=NULL,**posofMax=NULL;   /*characteristics of maxima points*/
  int **maximaBin=NULL;
  double start=0,end=0;
  double thresh=0,threshSq=0.0,max=0;
  FILE *opoo=NULL;
  char namen[200],found=0,maxNdif=0;
  char universal=0,*posMatch=NULL;
  int *markInt(int,int *,int);
  int *deleteInt(int,int *,int);
  float *markFloat(int,float *,float);
  float *deleteFloat(int,float *,int);
  double *grad=NULL,*Laplace=NULL;
  double *totale=NULL,maxE=0;               /*total energy for each band*/
  RatControl *ratPoke=NULL;                 /*temporary array to allow convolution just for this function*/
  void convolveFFT(RatControl *,RatImage *,int,int,double);

  float tamp=0;  /*## REMOVE BEFORE USE ##*/
 
  printf("Training Gaussian\n");
  
  /*for now*/
  if(ratPoint[numb].NAscans*ratPoint[numb].NZscans!=1){
    fprintf(stderr,"train Gauss is still a quick and dirty function and can't deal with %d pulses\n",\
      ratPoint[numb].NAscans*ratPoint[numb].NZscans);
    exit(1);
  }

  /*read waveform and smooth if needed*/
  ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
  
  if(dimage->pestlength>0.0){            /*create a dummy ratPoint with variables needed for convolution*/
    
    if(!(ratPoke=(RatControl *)calloc(1,sizeof(RatControl)))){
      fprintf(stderr,"error in waveform feature array allocation\n");
      exit(1);
    }
    ratPoke[0].bins=ratPoint[numb].bins;
    ratPoke[0].bin_L=ratPoint[numb].bin_L;
    ratPoke[0].min_R=ratPoint[numb].min_R;
    ratPoke[0].max_R=ratPoint[numb].max_R;
    ratPoke[0].n=ratPoint[numb].n;
    ratPoke[0].nBands=ratPoint[numb].nBands;
    ratPoke[0].Plength=ratPoint[numb].Plength;
    ratPoke[0].NAscans=1;
    ratPoke[0].NZscans=1;

    if(!(ratPoke[0].ratRes=(RatResults *)calloc(1,sizeof(RatResults)))){
      fprintf(stderr,"error in waveform feature array allocation\n");
      exit(1);
    }
    ratPoke[0].ratRes[0].refl=dalloc(ratPoke[0].nBands*ratPoke[0].bins*(ratPoke[0].n+1),"temporary training reflectance",numb);
    for(band=0;band<dimage->nBands;band++){
      for(bin=0;bin<ratPoke[0].bins;bin++){
        place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
        ratPoke[0].ratRes[0].refl[place]=ratPoint[numb].ratRes[Lplace].refl[place];
      }
    }

    convolveFFT(ratPoke,dimage,numb,Lplace,(double)dimage->pestlength);
  }else{                                       /*just point to the existing structure*/
    ratPoke=&(ratPoint[numb]);
  }

  Lplace=0;
  numb=0;

  grad=dalloc(ratPoke[numb].bins,"flux gradient",numb);
  Laplace=dalloc(ratPoke[numb].bins,"flux second differential",numb);
  totale=dalloc(dimage->nBands,"band energy",numb);

  nfeat=ialloc(dimage->nBands,"feature number",numb);
  if(!(featureBin=(int **)calloc(dimage->nBands,sizeof(int *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }
  if(!(featureType=(int **)calloc(dimage->nBands,sizeof(int *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }
  if(!(amplitude=(float **)calloc(dimage->nBands,sizeof(float *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }
  if(!(sigma=(float **)calloc(dimage->nBands,sizeof(float *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }
  if(!(energy=(float **)calloc(dimage->nBands,sizeof(float *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }

  
  for(band=0;band<dimage->nBands;band++){
    integral=0.0;
    lastMax=-1;
    started=0;
    totale[band]=0.0;
    nfeat[band]=0;
    featureBin[band]=NULL;
    featureType[band]=NULL;
    amplitude[band]=NULL;
    sigma[band]=NULL;
    energy[band]=NULL;
    
    /*find max signal, gradient and second differential*/
    place=findReflIndex(dimage,ratPoke,numb,0,band,0);
    if(ratPoke[numb].ratRes[Lplace].refl[place]>max)max=ratPoke[numb].ratRes[Lplace].refl[place];
    totale[band]+=ratPoke[numb].ratRes[Lplace].refl[place];

    for(bin=1;bin<ratPoke[numb].bins;bin++){             /*loop from second return on so we can calculate gradient*/
      place=findReflIndex(dimage,ratPoke,numb,bin,band,0);
      oldPlace=findReflIndex(dimage,ratPoke,numb,bin-1,band,0);

      if(ratPoke[numb].ratRes[Lplace].refl[place]>max)max=ratPoke[numb].ratRes[Lplace].refl[place];
      grad[bin]=ratPoke[numb].ratRes[Lplace].refl[place]-ratPoke[numb].ratRes[Lplace].refl[oldPlace];
      if(bin<ratPoke[numb].bins-1){
        newPlace=findReflIndex(dimage,ratPoke,numb,bin+1,band,0);
        Laplace[bin]=ratPoke[numb].ratRes[Lplace].refl[newPlace]+ratPoke[numb].ratRes[Lplace].refl[oldPlace]-\
         2.0*ratPoke[numb].ratRes[Lplace].refl[place];
      }
      totale[band]+=ratPoke[numb].ratRes[Lplace].refl[place];
    }

    thresh=(double)dimage->Athresh*max;
    threshSq=thresh*thresh;
    place=findReflIndex(dimage,ratPoke,numb,0,band,0);
    integral+=ratPoke[numb].ratRes[Lplace].refl[place];           /*add the zeroth bin*/
    for(bin=1;bin<ratPoke[numb].bins;bin++){
      place=findReflIndex(dimage,ratPoke,numb,bin,band,0);
      oldPlace=findReflIndex(dimage,ratPoke,numb,bin-1,band,0);

      if(started&&ratPoke[numb].ratRes[Lplace].refl[place]>thresh)waveEnd=bin; /*keep track of last return*/

      if(!ascending&&!descending&&(grad[bin]>thresh)){  /*leading edge*/
        if(!nfeat[band]){  /*start of waveform*/
          started=1;
          waveStart=bin;
        }

        featureBin[band]=markInt(nfeat[band],&(featureBin[band][0]),bin);
        featureType[band]=markInt(nfeat[band],&(featureType[band][0]),2);
        amplitude[band]=markFloat(nfeat[band],&(amplitude[band][0]),(float)ratPoke[numb].ratRes[Lplace].refl[place]);
        energy[band]=markFloat(nfeat[band],&(energy[band][0]),0.0);
        sigma[band]=markFloat(nfeat[band],&(sigma[band][0]),0);
        nfeat[band]++;

        ascending=1;
        descending=0;
      }else if(ascending&&(grad[bin]<-1.0*thresh)){ /*hit a maximum*/

        /*##### HACK, REMOVE BEFORE USE ####*/
        /*this hack ensures the maximum value is taken instead of the shoulder of a Dirac delta*/
	if((bin>1)&&(bin<ratPoke[numb].bins-1)){
          j=findReflIndex(dimage,ratPoke,numb,bin-1,band,0);
          newPlace=findReflIndex(dimage,ratPoke,numb,bin+1,band,0);
          tamp=(float)ratPoke[numb].ratRes[Lplace].refl[place];
          if((float)ratPoke[numb].ratRes[Lplace].refl[j]>tamp)tamp=(float)ratPoke[numb].ratRes[Lplace].refl[j];
          if((float)ratPoke[numb].ratRes[Lplace].refl[newPlace]>tamp)tamp=(float)ratPoke[numb].ratRes[Lplace].refl[newPlace];
        }

        featureBin[band]=markInt(nfeat[band],&(featureBin[band][0]),bin);
        featureType[band]=markInt(nfeat[band],&(featureType[band][0]),1);
        /*amplitude[band]=markFloat(nfeat[band],&(amplitude[band][0]),(float)ratPoke[numb].ratRes[Lplace].refl[place]);*/
        amplitude[band]=markFloat(nfeat[band],&(amplitude[band][0]),tamp);
        energy[band]=markFloat(nfeat[band],&(energy[band][0]),0.0);

        start=end=-1.0;                /*set to nonesense values*/
        for(j=bin-1;j>=0;j--){
          newPlace=findReflIndex(dimage,ratPoke,numb,j,band,0);
          if(ratPoke[numb].ratRes[Lplace].refl[newPlace]<=ratPoke[numb].ratRes[Lplace].refl[place]*exp(-0.5)){
            start=(double)j*ratPoke[numb].bin_L+ratPoke[numb].min_R;
            break;
          }
        }
        for(j=bin+1;j<ratPoke[numb].bins;j++){
          newPlace=findReflIndex(dimage,ratPoke,numb,j,band,0);
          if(ratPoke[numb].ratRes[Lplace].refl[newPlace]<=ratPoke[numb].ratRes[Lplace].refl[place]*exp(-0.5)){
            end=(double)j*ratPoke[numb].bin_L+ratPoke[numb].min_R;
            break;
          }
        }
        if(start!=-1.0&&end!=-1.0){
          width=(end-start)/2.0;
        }else if(start!=-1.0){           /*use only start for width*/
          width=start-((double)bin*ratPoke[numb].bin_L+ratPoke[numb].min_R);
        }else if(end!=-1.0){             /*use only end*/
          width=end-((double)bin*ratPoke[numb].bin_L+ratPoke[numb].min_R);
        }else{                           /*if we can't find any use the other features*/
          if(nfeat[band]){   /*if this is not the first*/
            found=0;
            for(i=nfeat[band]-1;i>=0;i--){
              if(featureType[band][i]==-1||featureType[band][i]==2){  /*find when the amp drops below exp(-1/2) of max or else a minimum or a leading edge, whichever's first*/
                width=(bin-featureBin[band][i])*ratPoke[numb].bin_L;
                found=1;
                break;
              }
            }
            if(!found)width=100.0;
          }else width=(float)(bin-waveStart)*ratPoke[numb].bin_L;  /*it's the first, use leading edge to set width*/
        }
        sigma[band]=markFloat(nfeat[band],&(sigma[band][0]),width);

        ascending=0;
        descending=1;
        lastMax=nfeat[band];  /*record the last maximum for energy*/
        nfeat[band]++;
      }else if(descending&&(grad[bin]>thresh)){ /*hit a minimum*/
        featureBin[band]=markInt(nfeat[band],&(featureBin[band][0]),bin);
        featureType[band]=markInt(nfeat[band],&(featureType[band][0]),-1);
        amplitude[band]=markFloat(nfeat[band],&(amplitude[band][0]),(float)ratPoke[numb].ratRes[Lplace].refl[place]);
        energy[band]=markFloat(nfeat[band],&(energy[band][0]),0.0);
        if(lastMax>0)energy[band][lastMax]+=integral/totale[band];
        integral=0.0;

        if(nfeat[band]){   /*if this is not the first*/
          found=0;
          for(i=nfeat[band]-1;i>=0;i--){
            if(featureType[band][i]==1){
              width=(bin-featureBin[band][i])*ratPoke[numb].bin_L;
              found=1;
              break;
            }
          }
          if(!found)width=100.0;
        }else width=(float)(bin-waveStart)*ratPoke[numb].bin_L; /*it's the first, use leading edge to set width*/
        sigma[band]=markFloat(nfeat[band],&(sigma[band][0]),width); 

        ascending=1;
        descending=0;
        nfeat[band]++;
      }else if(descending&&ratPoke[numb].ratRes[Lplace].refl[place]<thresh){   /*trailing edge*/
        featureBin[band]=markInt(nfeat[band],&(featureBin[band][0]),bin);
        featureType[band]=markInt(nfeat[band],&(featureType[band][0]),3);
        amplitude[band]=markFloat(nfeat[band],&(amplitude[band][0]),(float)ratPoke[numb].ratRes[Lplace].refl[place]);
        energy[band]=markFloat(nfeat[band],&(energy[band][0]),0.0);
        if(lastMax>0)energy[band][lastMax]+=integral/totale[band];
        integral=0.0;

        sigma[band]=markFloat(nfeat[band],&(sigma[band][0]),0);
        nfeat[band]++;

        ascending=0;
        descending=0;
      }else if(grad[bin]*grad[bin+1]>threshSq&&Laplace[bin]*Laplace[bin+1]<-1.0*threshSq){  /*point of inflection*/
        featureBin[band]=markInt(nfeat[band],&(featureBin[band][0]),bin);
        featureType[band]=markInt(nfeat[band],&(featureType[band][0]),4);
        amplitude[band]=markFloat(nfeat[band],&(amplitude[band][0]),(float)ratPoke[numb].ratRes[Lplace].refl[place]);
        energy[band]=markFloat(nfeat[band],&(energy[band][0]),-1.0*Laplace[bin]*Laplace[bin+1]);

        start=-1.0;           /*set to nonesensical value*/
        if(grad[bin]>0.0){              /*look for width down slope*/
          for(j=bin-1;j>=0;j--){
            newPlace=findReflIndex(dimage,ratPoke,numb,j,band,0);
            if(ratPoke[numb].ratRes[Lplace].refl[newPlace]<=ratPoke[numb].ratRes[Lplace].refl[place]*exp(-0.5)){
              start=(double)j;
              break;
            }
          }
        }else if(grad[bin]<0.0){        /*look for width up slope*/
          for(j=bin+1;j<ratPoke[numb].bins;j++){
            newPlace=findReflIndex(dimage,ratPoke,numb,j,band,0);
            if(ratPoke[numb].ratRes[Lplace].refl[newPlace]<=ratPoke[numb].ratRes[Lplace].refl[place]*exp(-0.5)){
              start=(double)j;
              break;
            }
          }
        }else{
          fprintf(stderr,"NOOOOOOO\n");
          exit(1);
        }
        if(start!=-1.0)width=(fabs((double)bin-start))*ratPoke[numb].bin_L;
        else{
          if(nfeat[band]){   /*if this is not the first*/
            found=0;
            for(i=nfeat[band]-1;i>=0;i--){
              if(featureType[band][i]==-1||featureType[band][i]==2){  /*find when the amp drops below exp(-1/2) of max or else a minimum or a leading edge, whichever's first*/
                width=(bin-featureBin[band][i])*ratPoke[numb].bin_L;
                found=1;
                break;
              }
            }
            if(!found)width=100.0;
          }
        }
        sigma[band]=markFloat(nfeat[band],&(sigma[band][0]),width);
        nfeat[band]++;
      }else if(grad[bin]<thresh&&grad[bin]>-1*thresh){       /*the signal is flat*/
        ascending=0;
        descending=0;
      }else if(grad[bin]>thresh){
        ascending=1;
        descending=0;
      }else if(grad[bin]<-1.0*thresh){
        ascending=0;
        descending=1;
      }else if(!started&&grad[bin]<-1.0*thresh){  /*something fishy happening*/
        fprintf(stderr,"What's going on here! NOOOOOOOOOOOOOOOO\n");
        exit(1);
      }
      integral+=ratPoke[numb].ratRes[Lplace].refl[place];
      
    }/*bin loop and end of feature search*/

    if(lastMax>0)if(energy[band][lastMax]==0.0)energy[band][lastMax]=integral;
    integral=0.0;

    /* perhaps find widths once all feature locations are known*/

  }/*band loop*/

  /*skip the next bit if not multi-spectral*/
  if(dimage->nBands==1)goto MONOCHROME;
 
  /*now search through bands again to choose which parameters to print out*/

  nMax=ialloc(dimage->nBands,"number of maxima",numb);
  if(!(maximaBin=(int **)calloc(dimage->nBands,sizeof(int *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }
  if(!(ampofMax=(float **)calloc(dimage->nBands,sizeof(float *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }
  if(!(sigofMax=(float **)calloc(dimage->nBands,sizeof(float *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }
  if(!(posofMax=(float **)calloc(dimage->nBands,sizeof(float *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }
  if(!(eofMax=(float **)calloc(dimage->nBands,sizeof(float *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }

  maxNdif=0;
  for(band=0;band<dimage->nBands;band++){
    maximaBin[band]=NULL;
    ampofMax[band]=NULL;
    sigofMax[band]=NULL;
    posofMax[band]=NULL;
    eofMax[band]=NULL;
    nMax[band]=0;
    for(i=0;i<nfeat[band];i++){
      if(featureType[band][i]==1){
        maximaBin[band]=markInt(nMax[band],&(maximaBin[band][0]),featureBin[band][i]);
	ampofMax[band]=markFloat(nMax[band],&(ampofMax[band][0]),amplitude[band][i]);
        sigofMax[band]=markFloat(nMax[band],&(sigofMax[band][0]),sigma[band][i]);
        eofMax[band]=markFloat(nMax[band],&(eofMax[band][0]),energy[band][i]);	
        nMax[band]++;
      }
    }
    /*check we have the same number of maxima in each band. Should also check position*/
    if(band){
      if(nMax[band]!=nMax[0]){ /*then it's likely we have not denoised sufficiently*/
	maxNdif=1;
      }
    }	    
    if(nMax[band]>maxFeat)maxFeat=nMax[band];
  }

/*fprintf(stderr,"\nBefore\n\n");
  for(band=0;band<dimage->nBands;band++){
    for(i=0;i<nMax[band];i++)fprintf(stderr,"band %d feature %d of %d %d %f %f\n",band,i,nMax[band],maximaBin[band][i],ampofMax[band][i],sigofMax[band][i]);
  }*/


  if(maxNdif){
    /*if there aren't the same number of maxima in each band we need to do something about it*/
    /*first pass pick those that occur at the same point (within one bin) in all bands*/
    posMatch=challoc((uint64_t)dimage->nBands,"maxima position match",0);
    for(oldband=0;oldband<dimage->nBands;oldband++){
      for(i=0;i<nMax[oldband];i++){
        for(band=0;band<dimage->nBands;band++)posMatch[band]=0;
        posMatch[oldband]=1;
        for(band=0;band<dimage->nBands;band++){

          if(band!=oldband){
            for(j=0;j<nMax[band];j++){
              separation=maximaBin[band][j]-maximaBin[oldband][i];
              if((separation<=1)&&(separation>=-1))posMatch[band]=1;
            }
          }
        }/*inner band loop*/
        /*if all bands have this feature mark it down*/
        universal=1;
        for(band=0;band<dimage->nBands;band++)universal*=(char)posMatch[band];
        if(!universal){   /*delete it*/
          maximaBin[oldband]=deleteInt(nMax[oldband],&(maximaBin[oldband][0]),i);
          ampofMax[oldband]=deleteFloat(nMax[oldband],&(ampofMax[oldband][0]),i);
          sigofMax[oldband]=deleteFloat(nMax[oldband],&(sigofMax[oldband][0]),i);
          eofMax[oldband]=deleteFloat(nMax[oldband],&(eofMax[oldband][0]),i);
          nMax[oldband]--;
          i--;
        }
      }/*maxima loop*/
    }/*outer band loop*/
    TIDY(posMatch);
  }/*difference sorting section*/

  /*#fprintf(stderr,"\nAfterwards\n\n");
  #for(band=0;band<dimage->nBands;band++){
  #   for(i=0;i<nMax[band];i++)fprintf(stderr,"band %d feature %d of %d %d %f %f\n",band,\
        i,nMax[band],maximaBin[band][i],ampofMax[band][i],sigofMax[band][i]);
  #}*/


  sigmaMax=falloc(nMax[oldband],"maximum sigma",numb);
  /*if there are the same number of maxima in each band use greatest sigma values*/
  for(band=0;band<dimage->nBands;band++){
    for(i=0;i<nMax[band];i++){
      if(sigofMax[band][i]>sigmaMax[i])sigmaMax[i]=sigofMax[band][i];
    }
  }


  MONOCHROME:  /*a "goto" label*/


  /*use features from the most energetic band*/
  for(band=0;band<dimage->nBands;band++){
    if(totale[band]>maxE){
      maxE=totale[band];
      maxBand=band;
    }
  }	  



  printf("Found %d features\n",nfeat[maxBand]);

  /*print out initial parameter estimates*/
  sprintf(namen,"%s.pestimates",dimage->output);
  if((opoo=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening ascii results file %s\n",namen);
    exit(1);
  }



  fprintf(opoo,"# Gaussian estimates. 1 range, 2 amplitude, 3 width, 4 type, 5 energy");

  if(dimage->nBands>1){ /*print out only maxima*/
    for(band=1;band<dimage->nBands;band++)fprintf(opoo,", %d band%d",5+band,band+1);
    fprintf(opoo,"\n");
    fprintf(opoo,"%f %f range\n",(double)waveStart*ratPoke[numb].bin_L+ratPoke[numb].min_R,\
      (double)waveEnd*ratPoke[numb].bin_L+ratPoke[numb].min_R);
    for(i=0;i<nMax[0];i++){
      fprintf(opoo,"%f %f %f %d %f",(double)maximaBin[maxBand][i]*ratPoke[numb].bin_L+ratPoke[numb].min_R,\
       ampofMax[0][i],sigmaMax[i],1,eofMax[maxBand][i]);
      for(band=1;band<dimage->nBands;band++)fprintf(opoo," %f",ampofMax[band][i]);
      fprintf(opoo,"\n");
    }
  }else if(dimage->nBands==1){  /*prints out all features, not just maxima*/
    fprintf(opoo,"\n");
    maxBand=0;
    for(i=0;i<nfeat[maxBand];i++){
      fprintf(opoo,"%g %g %g %d %g\n",(double)featureBin[maxBand][i]*ratPoke[numb].bin_L+ratPoke[numb].min_R,\
        amplitude[0][i],sigma[maxBand][i],featureType[maxBand][i],energy[maxBand][i]);
    }
  }

  ratPoke=clearArrays(ratPoke,numb,Lplace,dimage);
  ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
  
  TIDY(nfeat);
  if(featureBin){
    for(band=0;band<dimage->nBands;band++)TIDY(featureBin[band]);
    free(featureBin);
    featureBin=NULL;
  }
  if(featureType){
    for(band=0;band<dimage->nBands;band++)TIDY(featureType[band]);
    free(featureType);
    featureType=NULL;
  }
  if(amplitude){
    for(band=0;band<dimage->nBands;band++)TIDY(amplitude[band]);
    free(amplitude);
    amplitude=NULL;
  }
  if(sigma){
    for(band=0;band<dimage->nBands;band++)TIDY(sigma[band]);
    free(sigma);
    sigma=NULL;
  }
  if(energy){
    for(band=0;band<dimage->nBands;band++)TIDY(energy[band]);
    free(energy);
    energy=NULL;
  }
  TIDY(sigmaMax);
  if(posofMax){
    for(band=0;band<dimage->nBands;band++)TIDY(posofMax[band]);
    free(posofMax);
    posofMax=NULL;
  }
  if(ampofMax){
    for(band=0;band<dimage->nBands;band++)TIDY(ampofMax[band]);
    free(ampofMax);
    ampofMax=NULL;
  }
  if(maximaBin){
    for(band=0;band<dimage->nBands;band++)TIDY(maximaBin);
    free(maximaBin);
    maximaBin=NULL;
  }
  if(sigofMax){
    for(band=0;band<dimage->nBands;band++)TIDY(sigofMax[band]);
    free(sigofMax);
    sigofMax=NULL;
  }
  if(eofMax){
    for(band=0;band<dimage->nBands;band++)TIDY(eofMax[band]);
    free(eofMax);
    eofMax=NULL;
  }

  TIDY(totale);
  TIDY(grad);
  TIDY(Laplace);
  TIDY(nMax);
  if(opoo){
    fclose(opoo);
    opoo=NULL;
  }
  if(ratPoke!=&(ratPoint[numb])){  /*free ratPoke*/
    if(ratPoke){
      if(ratPoke->ratRes){
        TIDY(ratPoke->ratRes->refl);
	free(ratPoke->ratRes);
	ratPoke->ratRes=NULL;
      }
      free(ratPoke);
      ratPoke=NULL;
    } 
  }else{                        /*point it elsewhere*/
    ratPoke=NULL;
  }

  
  return;
}/*trainGaussian*/

/*###################################################################*/
/*extract features from a waveform*/

void analyseBeam(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int i=0,j=0,k=0;
  int bin=0,beams=0,band=0;
  int Lplace=0,fplace=0;
  int *indices=NULL;
  int Zstart=0,Zstop=0;
  int Astart=0,Astop=0;
  int *angleToIndices(RatImage *,RatControl *,int);
  void allocateFeautreSpace(RatControl *,RatImage *,int,int);
  void transferFeatures(RatImage *,RatControl *,int,int,int);
  void allocateFeatureFlux(RatImage *,RatControl *,int,int,int);
  void classifyWaveform(RatImage *,RatControl *,int,int,int);
  void labelFeatureTypes(RatControl *,int);
  void printWaveFeatures(RatControl *,RatImage *,int,int *);
  void clearFeatures(RatImage *,RatControl *,int,int);
  double fract=0.001,noise=0.001;    /*fraction of peak intensity for step acceptance*/

  indices=angleToIndices(dimage,ratPoint,numb);
  Zstart=indices[0];
  Zstop=indices[1];
  Astart=indices[2];
  Astop=indices[3];

  beams=(Zstart-Zstop)*(Astart-Astop);
  if(!(ratPoint[numb].ratFeat=(RatFeat **)calloc(beams,sizeof(RatFeat *)))){
    fprintf(stderr,"error in waveform feature array allocation\n");
    exit(1);
  }
  labelFeatureTypes(ratPoint,numb);

  if(beams!=1){fprintf(stderr,"Are you sure you want to use %d beams?\n",beams);exit(1);}
  for(i=Zstart;i<Zstop;i++){
    for(j=Astart;j<Astop;j++){
      fplace=(i-Zstart)*(Astart-Astop)+j-Astart;
      Lplace=i*ratPoint[numb].NAscans+j;
      if(!(ratPoint[numb].ratFeat[fplace]=(RatFeat *)calloc(dimage->nBands,sizeof(RatFeat)))){
        fprintf(stderr,"error in waveform feature array allocation for beam %d\n",fplace);
        exit(1);
      }
      allocateFeautreSpace(ratPoint,dimage,numb,fplace);
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
      for(band=0;band<dimage->nBands;band++){
        ratPoint[numb].ratFeat[fplace][band].totalE=0.0;       /*set all beam variables to 0*/
        ratPoint[numb].ratFeat[fplace][band].nFeat=0;
        ratPoint[numb].ratFeat[fplace][band].peak=0.0;

        for(bin=0;bin<ratPoint[numb].bins;bin++){              /*get some waveform stats*/
          ratPoint[numb].ratFeat[fplace][band].totalE+=\
            ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,bin,band,0)];
          if(ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,bin,band,0)]>\
            ratPoint[numb].ratFeat[fplace][band].peak)ratPoint[numb].ratFeat[fplace][band].peak=\
            ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,bin,band,0)];
        }  /*waveform stat loop*/
/*printf("Maximum is %f, energy stored %f\n",ratPoint[numb].ratFeat[fplace][band].peak,ratPoint[numb].ratFeat[fplace][band].totalE);*/

        /*and now back up the beam looking for features*/
        for(bin=ratPoint[numb].bins-2;bin>=0;bin--){
          if(ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,bin,band,0)]-\
             ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,bin+1,band,0)]>\
             fract*ratPoint[numb].ratFeat[fplace][band].peak){
     printf("Found a step at %f from %f to %f\n",(double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R,\
              ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,bin+1,band,0)]*100000,\
              ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,bin,band,0)]*100000);
            ratPoint[numb].ratFeat[fplace][band].nFeat++;

            transferFeatures(dimage,ratPoint,numb,band,fplace);

            ratPoint[numb].ratFeat[fplace][band].pos[ratPoint[numb].ratFeat[fplace][band].nFeat-1][2]=bin;
            /*find the next turning point*/
            for(k=bin-1;k>=0;k--){
              if(ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,k+1,band,0)]>\
                 ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,k,band,0)]){
                ratPoint[numb].ratFeat[fplace][band].pos[ratPoint[numb].ratFeat[fplace][band].nFeat-1][1]=k;
                for(;k>=0;k--){                    /*search for end of feature*/
                  if(ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,k,band,0)]>\
                     ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,k+1,band,0)]||\
                     ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,k,band,0)]<\
                     noise*ratPoint[numb].ratFeat[fplace][band].peak){
                    ratPoint[numb].ratFeat[fplace][band].pos[ratPoint[numb].ratFeat[fplace][band].nFeat-1][0]=k;
                    break;
                  }
                }
                break;
              }
            }
            bin=k+1;
          }
        }/*bin loop of feature finding*/

        /*now features have been found set the radiant flux parameters*/
        allocateFeatureFlux(dimage,ratPoint,numb,band,fplace);
        for(k=0;k<ratPoint[numb].ratFeat[fplace][band].nFeat;k++){
          ratPoint[numb].ratFeat[fplace][band].max[k]=\
            ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,\
            ratPoint[numb].ratFeat[fplace][band].pos[k][1],band,0)];
          ratPoint[numb].ratFeat[fplace][band].lead[k]=\
            ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,\
            ratPoint[numb].ratFeat[fplace][band].pos[k][0],band,0)];
          ratPoint[numb].ratFeat[fplace][band].trail[k]=\
            ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,\
            ratPoint[numb].ratFeat[fplace][band].pos[k][2],band,0)];
          for(bin=ratPoint[numb].ratFeat[fplace][band].pos[k][0];\
              bin<ratPoint[numb].ratFeat[fplace][band].pos[k][2];bin++){
                ratPoint[numb].ratFeat[fplace][band].energy[k]+=\
                  ratPoint[numb].ratRes[Lplace].refl[findReflIndex(dimage,ratPoint,numb,bin,band,0)];
          }
        }
        if(ratPoint[numb].ratFeat[fplace][band].nFeat)classifyWaveform(dimage,ratPoint,numb,band,fplace);
        else printf("Found nothing for beam %d, band %d\n",fplace,band);
      }/*band loop*/
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    }/*azimuth step*/
  }  /*zenith step*/

  printWaveFeatures(ratPoint,dimage,numb,indices);

  clearFeatures(dimage,ratPoint,numb,beams);
  TIDY(indices);
 
  return;
}/*analyseBeam*/

/*#################################################################*/
/*decide what each feature in a waveform is*/

void classifyWaveform(RatImage *dimage,RatControl *ratPoint,int numb,int band,int fplace)
{

  fprintf(stderr,"Doesn't work yet\n");
  exit(1);

  return;
}/*classifyWaveform*/

/*####################################################################################*/
/*print waveform features*/

void printWaveFeatures(RatControl *ratPoint,RatImage *dimage,int numb,int *indices)
{
  int i=0,j=0,k=0;
  int band=0,beams=0;
  int fplace=0;
  int Zstart=0,Zstop=0;
  int Astart=0,Astop=0;

  Zstart=indices[0];
  Zstop=indices[1];
  Astart=indices[2];
  Astop=indices[3];
  beams=(Zstart-Zstop)*(Astart-Astop);

  for(i=0;i<beams;i++){
    fplace=i;
    for(band=0;band<dimage->nBands;band++){
      fprintf(stdout,"beam %d, band %d had %d features  and an energy of %f\n",i,band,\
        ratPoint[numb].ratFeat[fplace][band].nFeat,ratPoint[numb].ratFeat[fplace][band].totalE);
      for(k=0;k<ratPoint[numb].ratFeat[fplace][band].nFeat;k++){
        fprintf(stdout,"feature %d",k);
          for(j=0;j<3;j++)fprintf(stdout," %f",(double)ratPoint[numb].ratFeat[fplace][band].pos[k][j]*\
            ratPoint[numb].bin_L+ratPoint[numb].min_R);
        fprintf(stdout," energy %f\n",ratPoint[numb].ratFeat[fplace][band].energy[k]);
      }
    }
  }

  return;
}/*printWaveFeatures*/

/*#################################################################*/
/*transfer all feature arrays when a new one is found*/

void transferFeatures(RatImage *dimage,RatControl *ratPoint,int numb,int band,int fplace)
{
  int i=0,j=0;
  int **pos=NULL;

  if(!(pos=(int **)calloc(ratPoint[numb].ratFeat[fplace][band].nFeat-1,sizeof(int *)))){
    fprintf(stderr,"error in angle index array allocatio\n");
    exit(1);
  }
  for(i=0;i<ratPoint[numb].ratFeat[fplace][band].nFeat-1;i++){
    pos[i]=ialloc(3,"feature position transfer",i);
    for(j=0;j<3;j++)pos[i][j]=ratPoint[numb].ratFeat[fplace][band].pos[i][j];
    TIDY(ratPoint[numb].ratFeat[fplace][band].pos[i]);
  }
  TIDY(ratPoint[numb].ratFeat[fplace][band].pos);

  if(!(ratPoint[numb].ratFeat[fplace][band].pos=(int **)calloc(ratPoint[numb].ratFeat[fplace][band].nFeat,sizeof(int *)))){
    fprintf(stderr,"error in angle index array allocatio\n");
    exit(1);
  }
  for(i=0;i<ratPoint[numb].ratFeat[fplace][band].nFeat;i++){
    ratPoint[numb].ratFeat[fplace][band].pos[i]=ialloc(3,"feature position",i);
    for(j=0;j<3;j++)pos[i][j]=ratPoint[numb].ratFeat[fplace][band].pos[i][j]=pos[i][j];
    TIDY(pos[i]);
  }
  TIDY(pos);

  return;
}/*transferFeatures*/

/*#################################################################*/
/*allocate waveform feature flux arrays*/

void allocateFeatureFlux(RatImage *dimage,RatControl *ratPoint,int numb,int band,int fplace)
{
  int i=0;

  ratPoint[numb].ratFeat[fplace][band].max=dalloc(ratPoint[numb].ratFeat[fplace][band].nFeat,"feature maximum",fplace);
  ratPoint[numb].ratFeat[fplace][band].energy=dalloc(ratPoint[numb].ratFeat[fplace][band].nFeat,"feature energy",fplace);
  ratPoint[numb].ratFeat[fplace][band].lead=dalloc(ratPoint[numb].ratFeat[fplace][band].nFeat,"leading edge",fplace);
  ratPoint[numb].ratFeat[fplace][band].trail=dalloc(ratPoint[numb].ratFeat[fplace][band].nFeat,"trailing edge",fplace);

  /*allocate the certainties*/
  if(!(ratPoint[numb].ratFeat[fplace][band].certainty=(double **)calloc(ratPoint[numb].ratFeat[fplace][band].nFeat,sizeof(double *)))){
    fprintf(stderr,"error in angle index array allocatio\n");
    exit(1);
  }
  for(i=0;i<ratPoint[numb].ratFeat[fplace][band].nFeat;i++){
    ratPoint[numb].ratFeat[fplace][band].certainty[i]=dalloc(ratPoint[numb].nFeaTypes,"feature certainty",i);
  }

  return;
}/*allocateFeatureFlux*/

/*#################################################################*/
/*convert an angular range to an index range*/

int *angleToIndices(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int *indices=NULL;
  int i=0,n=0,j=0;
  int Zstart=0,Zstop=0;
  int Astart=0,Astop=0;

  if(!(indices=(int *)calloc(4,sizeof(int)))){
    fprintf(stderr,"error in angle index array allocatio\n");
    exit(1);
  }
  if(dimage->Arange[0]<361*M_PI/180){
    /*convert the angular range to the integer controls*/
    for(i=0;i<ratPoint[numb].NZscans;i++){
      n=i*ratPoint[numb].NAscans;
      if((ratPoint[numb].theta[n]-dimage->Arange[0])<0.00001&&\
        (ratPoint[numb].theta[n]-dimage->Arange[0])>-0.00001){
        Zstart=i;
      }
      if((ratPoint[numb].theta[n]-dimage->Arange[1])<0.00001&&\
        (ratPoint[numb].theta[n]-dimage->Arange[1])>-0.00001){
        Zstop=i+1;
        break;
      }
    }
    for(j=0;j<ratPoint[numb].NAscans;j++){
      n=j;
      if((ratPoint[numb].thata[n]-dimage->Arange[2])<0.00001&&\
        (ratPoint[numb].thata[n]-dimage->Arange[2])>-0.00001){
        Astart=j;
      }
      if((ratPoint[numb].thata[n]-dimage->Arange[3])<0.00001&&\
        (ratPoint[numb].thata[n]-dimage->Arange[3])>-0.00001){
        Astop=j+1;
        break;
      }
    }
  }else{
    Astart=0;
    Astop=ratPoint[numb].NAscans;
    Zstart=0;
    Zstop=ratPoint[numb].NZscans;
  }
  indices[0]=Zstart;
  indices[1]=Zstop;
  indices[2]=Astart;
  indices[3]=Astop;

  return(indices);
} /*angleToIndices*/

/*##############################################*/
/*write a list of feature types*/

void labelFeatureTypes(RatControl *ratPoint,int numb)
{
  ratPoint[numb].nFeaTypes=3;
  if(!(ratPoint[numb].feaTypes=(char **)calloc(ratPoint[numb].nFeaTypes,sizeof(char *)))){
    fprintf(stderr,"error in feature type list allocation\n");
    exit(1);
  }
  ratPoint[numb].feaTypes[0]=challoc((uint64_t)sizeof("ground "),"ground label",0);
  ratPoint[numb].feaTypes[1]=challoc((uint64_t)sizeof("soil "),"soil label",0);
  ratPoint[numb].feaTypes[2]=challoc((uint64_t)sizeof("echoes "),"echo label",0);
  strcpy(ratPoint[numb].feaTypes[0],"ground");
  strcpy(ratPoint[numb].feaTypes[1],"soil");
  strcpy(ratPoint[numb].feaTypes[2],"echoes");

  return;
}/*labelFeatureTypes*/

/*##############################################*/
/*force all feature pointers to null*/

void allocateFeautreSpace(RatControl *ratPoint,RatImage *dimage,int numb,int fplace)
{
  int band=0;

  for(band=0;band<dimage->nBands;band++){
    ratPoint[numb].ratFeat[fplace][band].pos=NULL;
    ratPoint[numb].ratFeat[fplace][band].max=NULL;
    ratPoint[numb].ratFeat[fplace][band].lead=NULL;
    ratPoint[numb].ratFeat[fplace][band].trail=NULL;
    ratPoint[numb].ratFeat[fplace][band].energy=NULL;
    ratPoint[numb].ratFeat[fplace][band].certainty=NULL;
  }

  return;
}/*allocateFeautreSpace*/

/*############################################################################*/
/*clear out all arrays used for feature extcraction*/

void clearFeatures(RatImage *dimage,RatControl *ratPoint,int numb,int beams)
{
  int i=0,j=0,band=0;

  if(ratPoint[numb].ratFeat){
    for(i=0;i<beams;i++){
      if(ratPoint[numb].ratFeat[i]){
        for(band=0;band<dimage->nBands;band++){
          TIDY(ratPoint[numb].ratFeat[i][band].max);
          TIDY(ratPoint[numb].ratFeat[i][band].lead);
          TIDY(ratPoint[numb].ratFeat[i][band].trail);
          TIDY(ratPoint[numb].ratFeat[i][band].energy);
          if(ratPoint[numb].ratFeat[i][band].certainty){
            for(j=0;j<ratPoint[numb].ratFeat[i][band].nFeat;j++)TIDY(ratPoint[numb].ratFeat[i][band].certainty[j]);
            free(ratPoint[numb].ratFeat[i][band].certainty);
            ratPoint[numb].ratFeat[i][band].certainty=NULL;
          }
          if(ratPoint[numb].ratFeat[i][band].pos){
            for(j=0;j<ratPoint[numb].ratFeat[i][band].nFeat;j++)TIDY(ratPoint[numb].ratFeat[i][band].pos[j]);
            free(ratPoint[numb].ratFeat[i][band].pos);
            ratPoint[numb].ratFeat[i][band].pos=NULL;
          }
        }
        free(ratPoint[numb].ratFeat[i]);
        ratPoint[numb].ratFeat[i]=NULL;
      }
    }
    free(ratPoint[numb].ratFeat);
    ratPoint[numb].ratFeat=NULL;
  }
  if(ratPoint[numb].feaTypes){
    for(j=0;j<ratPoint[numb].nFeaTypes;j++)TIDY(ratPoint[numb].feaTypes[j]);
    free(ratPoint[numb].feaTypes);
    ratPoint[numb].feaTypes=NULL;
  }

  return;
}/*clearFeatures*/

/*#######################################################################*/
/*crop a waveform to remove excess zeroes*/

void cropWaveform(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int bin=0,band=0,n=0,seg=0,mat=0;
  int place=0,nplace=0,Lplace=0;
  int start=0,stop=0,length=0;
  double *refl=NULL;
  float *material=NULL;

  if(dimage->crop==NULL){
    printf("No bounds specified for crop\n");
    return;
  }

  if(dimage->crop[0]>dimage->crop[1]){
    fprintf(stderr,"Upper bound is closer than rear bound, %f %f\n",dimage->crop[1],dimage->crop[1]);
    exit(1);
  }

  start=(int)((dimage->crop[0]-ratPoint[numb].min_R)/ratPoint[numb].bin_L);
  stop=(int)((dimage->crop[1]-ratPoint[numb].min_R)/ratPoint[numb].bin_L);
  if(start<0||start>=ratPoint[numb].bins){fprintf(stderr,"Bad start bin for crop %d\n",start);exit(1);}
  if(stop<0||stop>=ratPoint[numb].bins){fprintf(stderr,"Bad stop bin for crop %d\n",stop);exit(1);}
  length=stop-start;
  dimage->newpoint->bins=length;

  if(!(refl=(double *)calloc(length*ratPoint[numb].nBands*(ratPoint[numb].n+1)*\
      (ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs),sizeof(double)))){
    fprintf(stderr,"error in visible image buffer allocation number 1.\n");
    exit(1);
  }
  if(!(material=(float *)calloc(length*ratPoint[numb].nMat*2*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs),sizeof(float)))){
    fprintf(stderr,"error in visible image buffer allocation number 1.\n");
    exit(1);
  }

  for(Lplace=0;Lplace<ratPoint[numb].NAscans*ratPoint[numb].NZscans;Lplace++){
    for(seg=0;seg<ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs;seg++){
      for(bin=start;bin<=stop;bin++){
        for(band=0;band<dimage->nBands;band++){
          for(n=0;n<ratPoint[numb].n+1;n++){
            place=findReflIndex(dimage,ratPoint,numb,bin,band,n);
            nplace=findReflIndex(dimage,ratPoint,numb,bin-start,band,n);
            refl[nplace]=ratPoint[numb].ratRes[Lplace].refl[place];
          }
        }
        for(mat=0;band<ratPoint[numb].nMat;band++){
          for(n=0;n<2;n++)material[(bin-start)*ratPoint[numb].nMat*2+mat*2+n]=\
		  ratPoint[numb].ratRes[Lplace].material[bin*ratPoint[numb].nMat*2+mat*2+n];
        }
      }
    }
    TIDY(ratPoint[numb].ratRes[Lplace].refl);
    TIDY(ratPoint[numb].ratRes[Lplace].material);
    ratPoint[numb].ratRes[Lplace].refl=dalloc(length*ratPoint[numb].nBands*(ratPoint[numb].n+1),"cropped refl",Lplace);
    ratPoint[numb].ratRes[Lplace].material=falloc(length*ratPoint[numb].nMat*2,"cropped material",Lplace);
    for(seg=0;seg<ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs;seg++){
      for(bin=0;bin<dimage->newpoint->bins;bin++){
        for(band=0;band<dimage->nBands;band++){
          for(n=0;n<ratPoint[numb].n+1;n++){
            place=findReflIndex(dimage,ratPoint,numb,bin,band,n);
            ratPoint[numb].ratRes[Lplace].refl[place]=refl[nplace];
          }
	  for(mat=0;mat<ratPoint[numb].nMat;mat++){
            for(n=0;n<2;n++){
	      ratPoint[numb].ratRes[Lplace].material[seg*length*ratPoint[numb].nMat*2+bin*ratPoint[numb].nMat*2+mat*2+n]=material[seg*length*ratPoint[numb].nMat*2+bin*ratPoint[numb].nMat*2+mat*2+n];
	    }
	  }
        }
      }
    }
  }
  TIDY(refl);
  TIDY(material);

  ratPoint[numb].bins=length;
  ratPoint[numb].min_R=dimage->crop[0];
  ratPoint[numb].max_R=dimage->crop[1];
  
  return;
}

/*####################################################################################*/
/*convert a waveform into a Geiger mode return*/

void GeigerMode(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace)
{
  int bin=0,band=0;
  int place=0;
  double thresh=0;

  for(band=0;band<dimage->nBands;band++){
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      if(!(dimage->firstReturn))thresh=dimage->Geiger*(double)rand()/(double)RAND_MAX;
      else                    thresh=0.0;
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      /*printf("thresh %f %f\n",thresh,ratPoint[numb].ratRes[Lplace].refl[place]);*/
      if(ratPoint[numb].ratRes[Lplace].refl[place]>thresh){
        if(!(dimage->discreteInt))ratPoint[numb].ratRes[Lplace].refl[place]=1.0;   /*otherwise leave as it is*/
        /*ratPoint[numb].ratRes[Lplace].geiger=(float)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R;*/ 
        break;
      }else ratPoint[numb].ratRes[Lplace].refl[place]=0.0;
    }
    for(bin++;bin<ratPoint[numb].bins;bin++){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      ratPoint[numb].ratRes[Lplace].refl[place]=0.0;
    }
  }

  return;
}/*GeigerMode*/

/*##########################################################################################*/
/*add multiple Geiger returns to form a pseudo-waveform*/

void addGeiger(RatControl *ratPoint,RatImage *dimage,int numb)
{
  int i=0,place=0,Lplace=0;
  int bin=0,band=0;
  int reflLength=0;
  double *refl=NULL;
  float *material=NULL;

  int materialLength=0;
  int n=0,q=0,j=0,k=0;
  char namen[200];
  FILE *output=NULL;


  reflLength=ratPoint[numb].bins*ratPoint[numb].nBands*(ratPoint[numb].n+1);
  materialLength=ratPoint[numb].bins*ratPoint[numb].nMat*2;

  if(dimage->scanN==1)sprintf(namen,"%s.%d.%d.lidar",dimage->output,\
    (int)(dimage->Arange[0]*180/M_PI),(int)(dimage->Arange[1]*180/M_PI));
  else sprintf(namen,"%s.%d.%d.%d.%d.%d.lidar",dimage->output,(int)ratPoint[numb].from[0],\
    (int)ratPoint[numb].from[1],(int)ratPoint[numb].from[2],\
    (int)(dimage->Arange[0]*180/M_PI),(int)(dimage->Arange[1]*180/M_PI));
  if((output=fopen(namen,"w"))==NULL){
    fprintf(stderr,"Error opening ascii results file %s\n",namen);
    exit(1);
  }
  if(dimage->intent)printf("Writting data to an ascii file %s\n",namen);
  fprintf(output,"# This is an echi.rat data file\n# The numbers on top are the zenith and azimuth angles\
 in degrees\n# The blocks of numbers are range, wavelength, refl, refl of each order of interaction.\n");

  refl=dalloc(reflLength,"pseudo waveform transfer",numb);
  material=falloc(materialLength,"pseudo waveform transfer",numb);
  for(Lplace=0;Lplace<ratPoint[numb].NZscans*ratPoint[numb].NAscans;Lplace++){
    for(place=0;place<reflLength;place++)refl[place]=0.0;
    for(i=0;i<dimage->addGeiger;i++){
      ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
      for(band=0;band<dimage->nBands;band++){
        for(bin=0;bin<ratPoint[numb].bins;bin++){
          place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
          if(ratPoint[numb].ratRes[Lplace].refl[place]>0.0){
            /*printf("%d %d %f\n",i,bin,ratPoint[numb].ratRes[Lplace].refl[place]);*/
            refl[place]+=1.0;
            break;
          }/*return check*/
        }/*bin loop*/
      }/*band loop*/
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    }/*Geiger loop*/

    ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
    for(place=0;place<reflLength;place++)ratPoint[numb].ratRes[Lplace].refl[place]=refl[place];


    /*write out the results*/
    n=Lplace;
    fprintf(output,"\n# %g %g\n\n",ratPoint[numb].theta[n]*180.0/M_PI,ratPoint[numb].thata[n]*180.0/M_PI);
    for(i=0;i<ratPoint[numb].bins;i++){
      fprintf(output,"%f ",(double)i*ratPoint[numb].bin_L+ratPoint[numb].min_R);
      for(q=0;q<dimage->withinSegs+dimage->withoutSegs;q++){
        fprintf(output," Seg%d ",q);
        for(j=0;j<dimage->nBands;j++){
          band=dimage->band[numb][j];
          fprintf(output,"%g %g ",ratPoint[numb].wavelength[band],\
            refl[q*reflLength+(i*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)]);
          for(k=0;k<ratPoint[numb].n;k++){
            fprintf(output,"%g ",refl[q*reflLength+(i*ratPoint[numb].nBands+band)*(ratPoint[numb].n+1)+k+1]);
          }
        }
        fprintf(output,"  mat ");
        for(j=0;j<2*ratPoint[numb].nMat;j++){
          fprintf(output,"%g ",ratPoint[numb].ratRes[n].material[q*materialLength+i*ratPoint[numb].nMat*2+j]);
        }
      }
      fprintf(output,"%g ",ratPoint[numb].ratRes[n].sky);
      fprintf(output,"\n");
    }
    fprintf(output,"\n");
    ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
  }/*beam loop*/

  if(output){
    fclose(output);
    output=NULL;
  }
  if(dimage->intent)printf("Ascii data written\n");

  TIDY(refl);

  return;
}/*addGeiger*/

/*###################################################################################################*/
/*calculate ratio of two bands, for ground identification*/

void divideBands(RatControl *ratPoint,RatImage *dimage,int numb)
{
  int bin=0,Lplace=0;
  int band1=0,band2=0;
  double *maxA=NULL;       /*maximum ratio and amplitude*/
  double *thresh=NULL;
  double *ratio=NULL;
  double *meanoise=NULL,*stdevnoise=NULL;
  double maxSignal(RatImage *,RatControl *,int,int,int);
  void windowedStats(RatImage *,RatControl *,double *,int,int,int);
  void findEdge(RatImage *,RatControl *,int,int,double *);
  void findGround(RatImage *,RatControl *,int,int,double *);
  char namen[200];
  FILE *opoo=NULL;

  if(dimage->nBands!=2){
    fprintf(stderr,"Two wavebands are needed for band division, not %d\n",dimage->nBands);
    exit(1);
  }

  /*if dimage->groundout has not been defined set here*/
  if(!dimage->groundout){
    dimage->groundout=challoc((uint64_t)strlen("groundEstimate")+1,"ground etimate name",numb);
    strcpy(dimage->groundout,"groundEstimate");
  }

  if(dimage->ratiout){
    if(dimage->scanN==1)sprintf(namen,"%s.%g.%g.ratio",dimage->output,\
      ratPoint[numb].wavelength[dimage->band[numb][0]],ratPoint[numb].wavelength[dimage->band[numb][1]]);
    else sprintf(namen,"%s.%d.%d.%d.%g.%g.ratio",dimage->output,(int)\
      (ratPoint[numb].from[0]),(int)(ratPoint[numb].from[1]),(int)(ratPoint[numb].from[2]),\
      ratPoint[numb].wavelength[dimage->band[numb][0]],ratPoint[numb].wavelength[dimage->band[numb][1]]);
    if((opoo=fopen(namen,"w"))==NULL){
      fprintf(stderr,"Error opening ratio results file %s",namen);
      exit(1);
    }
    fprintf(opoo,"# This shows the ratio between %g and %g\n",ratPoint[numb].wavelength[dimage->band[numb][0]],ratPoint[numb].wavelength[dimage->band[numb][1]]);
    fprintf(opoo,"# 1 range\n# 2 ratio\n# 3 band1\n# 4 band2\n");

    printf("Printing ratio of %g to %g to %s\n",ratPoint[numb].wavelength[dimage->band[numb][0]],ratPoint[numb].wavelength[dimage->band[numb][1]],namen);
  }
  
  Lplace=0;   /*I'm not sure how I want to use this in future, one band at a time for now*/

  ratio=dalloc(ratPoint[numb].bins,"reflectance ratio",numb);

  ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);

  meanoise=dimage->meanoise;
  stdevnoise=dimage->stdevnoise;


  /*calculate ratio*/
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    band1=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    band2=findReflIndex(dimage,ratPoint,numb,bin,1,0);


    if(ratPoint[numb].ratRes[Lplace].refl[band1]>0.0&&ratPoint[numb].ratRes[Lplace].refl[band2]>0.0){
      ratio[bin]=ratPoint[numb].ratRes[Lplace].refl[band1]/(ratPoint[numb].ratRes[Lplace].refl[band2]+ratPoint[numb].ratRes[Lplace].refl[band1]);
    }else{
      ratio[bin]=0.0;
    }
    /*  For estimating ground position from maximum ratio
    #if(ratio[bin]>max){
    # max=ratio[bin];
    #  groundBin=bin;
    #}*/
    if(dimage->ratiout)fprintf(opoo,"%g %g %g %g\n",(double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R,ratio[bin],\
     ratPoint[numb].ratRes[Lplace].refl[band1],ratPoint[numb].ratRes[Lplace].refl[band2]);
  }

  if(opoo){
    fclose(opoo);
    opoo=NULL;
  }


/* Maximum of ratio finds ground position in absence of noise. Useless with 
#  sprintf(namen,"%s.ground",dimage->groundout);
#  if((groundEst=fopen(namen,"a+"))==NULL){
#    printf("Error opening ratio results file %s",namen);
#    exit(1);
#  }
#  fprintf(groundEst,"Ground is probably at %f for %s\n",(double)groundBin*ratPoint[numb].bin_L+ratPoint[numb].min_R,dimage->output);
#  if(groundEst){
#    fclose(groundEst);
#    groundEst=NULL;
#  }
*/


  /*have a go at finding the windowed statistics of the ratio*/
  if(dimage->windowstat)windowedStats(dimage,ratPoint,ratio,ratPoint[numb].bins,numb,Lplace);


  /*look for the edge*/
  /*if(dimage->spectralGround)findGround(dimage,ratPoint,numb,Lplace,ratio);*/
  if(dimage->spectralGround)findEdge(dimage,ratPoint,numb,Lplace,ratio);

  TIDY(ratio);
  TIDY(meanoise);
  TIDY(stdevnoise);
  TIDY(thresh);
  TIDY(maxA);

  return;
}/*divideBands*/


/*###################################################################################################*/
/*Look for an the ground, called from divideBands*/

void findGround(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace,double *jimlad)
{
  int bin=0,steps=0;
  int slength=0,length=0;
  int start=0,end=0;      /*start and end of return*/
  int wstart=0,wend=0;    /*start and end of smoothed signal ignoring wings*/
  int grart=0,grend=0;    /*start and end of the ground*/
  int upperBound=0;
  double lengthThresh=0;            /*this sets maximum position accuracy*/
  double *smoother=NULL;
  double *smoothed=NULL,*workspace=NULL;
  double *grad=NULL,*Laplace=NULL,*thirdif=NULL;
  double minLap=0,maxLap=0;     /*min amd max Laplacian values*/
  double max=0,test=0,ground=0;
  double maxGrad=0,lastMax=0;
  double thresh=0;
  int band1=0,band2=0;
  void fourier(double *,unsigned long,int);
  int cont=0;
  char contin=0,found=0,nextfound=0;

  FILE *ratioGround=NULL;
  char namen[200];





  grad=dalloc(ratPoint[numb].bins,"gradient",numb);
  Laplace=dalloc(ratPoint[numb].bins,"Laplacian",numb);
  thirdif=dalloc(ratPoint[numb].bins,"third differential",numb);

  /*set initial length to be half length of return*/
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    if(jimlad[bin]>0.0){
      slength=-1.0*bin;
      break;
    }
  }
  lengthThresh=1;

  /*select a suitable length, sampled at pulse resolution*/
  length=ratPoint[numb].bins;

 test=log((double)length)/log(2.0)-(int)(log((double)length)/log(2.0)+0.5);
  if(test>THRESHOLD||test<-THRESHOLD){
    printf("Padding from %d to ",length);
    length=pow(2.0,(float)((int)(log((double)ratPoint[numb].bins)/log(2.0)+0.5)+1));
  }
  printf(" %d for edge detection...\n",length);


  smoother=dalloc(2*(length+1),"edge detection smoothing",numb);
  smoothed=dalloc(2*(length+1),"smoothed edge detection",numb);
  workspace=dalloc(2*(length+1),"edge detection workspace",numb);

  /*set the threshold to avoid small number division in ratio*/
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    band1=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    band2=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if(ratPoint[numb].ratRes[Lplace].refl[band1]>thresh)thresh=ratPoint[numb].ratRes[Lplace].refl[band1];
    if(ratPoint[numb].ratRes[Lplace].refl[band2]>thresh)thresh=ratPoint[numb].ratRes[Lplace].refl[band2];
  }
  thresh*=0.05;  /*5% of maximum, refined a little*/
  cont=0;
  contin=0;
  while(!contin||!cont){
    /*make sure this leaves some signal*/
    cont=0;
    contin=0;
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      band1=findReflIndex(dimage,ratPoint,numb,bin,0,0);
      band2=findReflIndex(dimage,ratPoint,numb,bin,1,0);
      if(ratPoint[numb].ratRes[Lplace].refl[band1]>thresh)contin=1;
      if(ratPoint[numb].ratRes[Lplace].refl[band2]>thresh)cont=1;
    }
    if(!contin||!cont){  /*If the threshold leaves no signal reduce the threshold*/
      thresh/=1.1;
      if(thresh<0.000000000001)goto EDGEEND;     /*if the threshold gets really small and no signal is left don;t bother smoothing*/
    }
  }


  for(bin=1;bin<ratPoint[numb].bins-1;bin++){
    workspace[2*bin-1]=jimlad[bin]; /*real*/
    workspace[2*bin]=0.0;           /*imaginary*/
  }
  for(;bin<length;bin++){     /*pad with zeroes*/
    workspace[2*bin-1]=0.0;
    workspace[2*bin]=0.0;
  }
  fourier(workspace,length,1);


  
  /*now find the bounds of the useful signal to avoid edge effects*/
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    band1=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    band2=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if(ratPoint[numb].ratRes[Lplace].refl[band1]>thresh&&ratPoint[numb].ratRes[Lplace].refl[band2]>thresh){
      start=bin;
      break;
    }
  }
  if(start==0)start=1;
  for(bin=ratPoint[numb].bins-1;bin>start;bin--){
    band1=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    band2=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if(ratPoint[numb].ratRes[Lplace].refl[band1]>thresh&&ratPoint[numb].ratRes[Lplace].refl[band2]>thresh){
      end=bin;
      break;
    }
  }
  /*smooth reducing width until there is only one step*/
  contin=1;
  cont=0;
  slength=3000;
  do{

   /*smooth signal THIS SHOULD ALL REPLACED BY THE EXPLICIT FREQUENCY DOMAIN GAUSSIAN RATHER THAN FFTing*/
    for(bin=1;bin<=length;bin++){
      smoother[2*bin-1]=gaussian((double)bin*ratPoint[numb].bin_L,(double)slength/4.0,0.0)+gaussian((double)bin*ratPoint[numb].bin_L,(double)slength/4.0,(double)length*ratPoint[numb].bin_L);  /*real*/
      smoother[2*bin]=0.0;                                                                                                       /*imaginary*/
    }
    fourier(smoother,length,1);
    
    for(bin=1;bin<=length;bin++){
      smoothed[2*bin-1]=workspace[2*bin-1]*smoother[2*bin-1]-workspace[2*bin]*smoother[2*bin];
      smoothed[2*bin]=workspace[2*bin-1]*smoother[2*bin]+workspace[2*bin]*smoother[2*bin-1];
    }
    fourier(smoothed,length,-1);
    for(bin=1;bin<=2*length;bin++)smoothed[bin]/=(double)length;

    /*### remember "smoothed" is a complex array ###*/

    /*then calculate gradient, Laplacian and third differential*/
    steps=0;
    for(bin=2;bin<ratPoint[numb].bins;bin++){
      grad[bin]=smoothed[2*bin-1]*smoothed[2*bin-1]+smoothed[2*bin]*smoothed[2*bin]-(smoothed[2*(bin-1)-1]*smoothed[2*(bin-1)-1]+smoothed[2*(bin-1)]*smoothed[2*(bin-1)]);
      if(bin>3)Laplace[bin]=grad[bin]-grad[bin-1];
      if(bin>4)thirdif[bin]=Laplace[bin]-Laplace[bin-1];
      fprintf(stdout,"%g %g %g %g %g\n",((double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R)/1000.0,smoothed[2*bin-1]*smoothed[2*bin-1]+smoothed[2*bin]*smoothed[2*bin],grad[bin],Laplace[bin],thirdif[bin]);
    }/*bin loops*/


    /*find signal bounds to aid search*/
    for(bin=ratPoint[numb].bins-1;bin>=1;bin--){
      if(smoothed[2*bin-1]>0.0){
        upperBound=bin;
        break;
      }
    }


    /*find the wings*/
    /*leading wing*/
    thresh=0.0;
    for(bin=start;bin<ratPoint[numb].bins;bin++){
      if(grad[bin]>thresh)thresh=grad[bin];
      if((thirdif[bin]*thirdif[bin+1]<0.0)&&(thirdif[bin]<0.0))break;
    } 
    /*wait for the steep gradient to settle down*/
    lastMax=thresh;
    thresh*=0.01;
    for(;bin<ratPoint[numb].bins;bin++){
      /*keep a track of of maxima*/
      if((Laplace[bin]*Laplace[bin-1]<0.0)&&(grad[bin]<0.0)){
        maxGrad=grad[bin];
	if(maxGrad>lastMax){
	  wstart=bin;
	  break;
	}else lastMax=maxGrad;
      }
    }
    /*trailing wing*/
    minLap=10000.0;
    maxLap=-10000.0;
    maxGrad=10000; /*here maxGrad is being used as a minimum*/
    for(bin=upperBound;bin>wstart;bin--){
      if(grad[bin]<maxGrad){
        maxGrad=grad[bin];
        wend=bin;
      }
      if(Laplace[bin]>maxLap){
        maxLap=Laplace[bin];
        grend=bin;
      }


      /*if we are to use the Laplacian use below, but the gradient will b better behaved and with the blurring probably in a better position*/
      /*if(Laplace[bin]<minLap){
        minLap=Laplace[bin];
        wend=bin;
      }*/
      /*printf("%d %f %f\n",i,minLaplace,range[minLapPoint]);*/
    }
    lastMax=maxGrad*maxGrad;
    for(bin=wend-1;bin>wstart;bin--){
      if(Laplace[bin]*Laplace[bin-1]<0.0){
        maxGrad=grad[bin]*grad[bin];
	if(maxGrad>lastMax){
	  wend=bin;
	  break;
	}else lastMax=maxGrad;
      }
    }
    fprintf(stderr,"Looking from %f to %f\n",(double)wstart*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)wend*ratPoint[numb].bin_L+ratPoint[numb].min_R);


    
    /*Estimate the ground start. Look for groundStart between the wing and ground end*/
    maxLap=0;
    for(bin=wstart;bin<wend;bin++){
      /*loolk for the greatest increase in gradient, giving weight to protracted increases*/
      //if(Laplace[bin]>0.0){  /*here the weighting function should go, if required*/
        if(Laplace[bin]>maxLap){
          maxLap=Laplace[bin];
          grart=bin;
        }
      //}
    }

    /*count features between the ground bounds to determine correct amount of smoothing*/
    for(bin=grart;bin<grend;bin++){


    }
 
    if(steps>1){      /*increase smoothing*/
      slength++;  /*=(int)((double)slength*1.1);*/
    }else if(steps<=1){ /*decrease smoothing*/
      slength--;  /*=(int)((double)slength/1.1);*/
      if(!slength)slength=1;
      contin=0;      /*we are near the correct size*/
    } 
  
    if(nextfound&&!contin&&steps>0){ /*yoyoing*/
      found=1;
      goto EDGEEND;
    }else if(!contin&&steps>1){  /*we have found it, do once more*/
      /*slength++;*/
      nextfound=1;
    }else if(slength==1&&!steps){ /*there are no features to find*/
      goto EDGEEND;
    }else if(slength==1&&steps==1){
      found=1;
      goto EDGEEND;
    }
    if(nextfound)found=1;
    
    cont++;

  }while(0); // slength>0&&(!found||(contin||(steps!=1&&slength>lengthThresh))));

  EDGEEND:


  sprintf(namen,"%s.groundedge",dimage->groundout);
  if((ratioGround=fopen(namen,"a+"))==NULL){
    fprintf(stderr,"Error opening windowed stats file %s",namen);
    exit(1);
  }
  fprintf(ratioGround,"The ground lies between %f and %f\n",(double)grart*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)grend*ratPoint[numb].bin_L+ratPoint[numb].min_R);

  if(ratioGround){
    fclose(ratioGround);
    ratioGround=NULL;
  }

  
  exit(0);
  
  if(found){
    /*now have a look for maximum gradient*/
    max=0;
    for(bin=wstart;bin<=wend;bin++){
      /*# if(smoothed[2*bin-1]>max){*/
      if(grad[bin]>max){
        max=grad[bin];
        ground=(double)(bin-1)*ratPoint[numb].bin_L+ratPoint[numb].min_R;   /*this will be start of gorund*/
      }
    }
    ground=((double)end*ratPoint[numb].bin_L+ratPoint[numb].min_R+ground)/2.0; /*take signal end as end of ground*/

    if(dimage->groundout==NULL){
      dimage->groundout=challoc((uint64_t)strlen("groundEstimate")+1,"ground etimate name",numb);
      strcpy(dimage->groundout,"groundEstimate");
    }

    sprintf(namen,"%s.groundedge",dimage->groundout);
    if((ratioGround=fopen(namen,"a+"))==NULL){
      fprintf(stderr,"Error opening windowed stats file %s",namen);
      exit(1);
    }

    fprintf(ratioGround,"Thy ground is at %f for %s\n",ground,dimage->output);

    if(ratioGround){
      fclose(ratioGround);
      ratioGround=NULL;
    }
    if(dimage->ratiout){
      sprintf(namen,"%s.windowStatsgroundedge",dimage->output);
      if((ratioGround=fopen(namen,"w"))==NULL){
        fprintf(stderr,"Error opening windowed stats file %s",namen);
        exit(1);
      }

      fprintf(ratioGround,"# 1 range\n# 2 ratio\n# 3 smoothed\n# 4 gradient\n# 5 Laplacian\n# 6 third differential\n");
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        fprintf(ratioGround,"%f %f %f %f %f %f\n",(double)bin*ratPoint[numb].bin_L+\
          ratPoint[numb].min_R,jimlad[bin],smoothed[2*bin-1],grad[bin],Laplace[bin],thirdif[bin]);
      }
    }
    if(ratioGround){
      fclose(ratioGround);
      ratioGround=NULL;
    }


  }else{ /*we have failed*/
    if(dimage->groundout==NULL){
      dimage->groundout=challoc((uint64_t)strlen("groundEstimate")+1,"ground etimate name",numb);
      strcpy(dimage->groundout,"groundEstimate");
    }

    sprintf(namen,"%s.groundedge",dimage->groundout);
    if((ratioGround=fopen(namen,"a+"))==NULL){
      fprintf(stderr,"Error opening windowed stats file %s",namen);
      exit(1);
    }

    fprintf(ratioGround,"# COuld not find ground for %s\n",dimage->output);

    if(ratioGround){
      fclose(ratioGround);
      ratioGround=NULL;
    }
  }

  TIDY(workspace);
  TIDY(smoothed);
  TIDY(smoother);
  TIDY(grad);
  TIDY(Laplace);
  TIDY(thirdif);
  return;
}/*findGround*/


/*###################################################################################################*/
/*Look for an edge, called from divideBands*/

void findEdge(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace,double *jimlad)
{
  int i=0,j=0;  
  int bin=0,steps=0;
  int slength=0,length=0;
  int start=0,end=0;      /*start and end of return*/
  int wstart=0,wend=0;    /*start and end of smoothed signal ignoring wings*/
  double lengthThresh=0;            /*this sets maximum position accuracy*/
  double *smoother=NULL;
  double *smoothed=NULL,*workspace=NULL;
  double *grad=NULL,*Laplace=NULL,*thirdif=NULL;
  double max=0,test=0,ground=0;
  double thresh=0;
  int band1=0,band2=0;
  void fourier(double *,unsigned long,int);
  int cont=0;
  char contin=0,found=0,nextfound=0;

  FILE *ratioGround=NULL;
  char namen[200];


  grad=dalloc(ratPoint[numb].bins,"gradient",numb);
  Laplace=dalloc(ratPoint[numb].bins,"Laplacian",numb);
  thirdif=dalloc(ratPoint[numb].bins,"third differential",numb);

  /*set initial length to be half length of return*/
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    if(jimlad[bin]>0.0){
      slength=-1.0*bin;
      break;
    }
  }
  for(bin=ratPoint[numb].bins-1;bin>=0;bin--){
    if(jimlad[bin]>0.0){
      slength+=bin;
      break;
    }
  }
  lengthThresh=1;

  /*select a suitable length, sampled at pulse resolution*/
  length=ratPoint[numb].bins;
  test=log((double)length)/log(2.0)-(int)(log((double)length)/log(2.0)+0.5);
  if(test>THRESHOLD||test<-THRESHOLD){
    printf("Padding from %d to ",length);
    length=pow(2.0,(float)((int)(log((double)ratPoint[numb].bins)/log(2.0)+0.5)+1));
  }
  printf(" %d for edge detection...\n",length);


  smoother=dalloc(2*(length+1),"edge detection smoothing",numb);
  smoothed=dalloc(2*(length+1),"smoothed edge detection",numb);
  workspace=dalloc(2*(length+1),"edge detection workspace",numb);

  /*set the threshold to avoid small number division in ratio*/
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    band1=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    band2=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if(ratPoint[numb].ratRes[Lplace].refl[band1]>thresh)thresh=ratPoint[numb].ratRes[Lplace].refl[band1];
    if(ratPoint[numb].ratRes[Lplace].refl[band2]>thresh)thresh=ratPoint[numb].ratRes[Lplace].refl[band2];
  }
  thresh*=0.05;  /*5% of maximum, refined a little*/
  cont=0;
  contin=0;
  while(!contin||!cont){
    /*make sure this leaves some signal*/
    cont=0;
    contin=0;
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      band1=findReflIndex(dimage,ratPoint,numb,bin,0,0);
      band2=findReflIndex(dimage,ratPoint,numb,bin,1,0);
      if(ratPoint[numb].ratRes[Lplace].refl[band1]>thresh)contin=1;
      if(ratPoint[numb].ratRes[Lplace].refl[band2]>thresh)cont=1;
    }
    if(!contin||!cont){  /*If the threshold leaves no signal reduce the threshold*/
      thresh/=1.1;
      if(thresh<0.000000000001)goto EDGEEND;     /*if the threshold gets really small and no signal is left don;t bother smoothing*/
    }
  }


  for(bin=1;bin<ratPoint[numb].bins-1;bin++){
    /*band1=findReflIndex(dimage,ratPoint,numb,bin,0,0);*/
    /*band2=findReflIndex(dimage,ratPoint,numb,bin,1,0);*/
    workspace[2*bin-1]=jimlad[bin]; /*real*/
    workspace[2*bin]=0.0;           /*imaginary*/
  }
  for(;bin<length;bin++){     /*pad with zeroes*/
    workspace[2*bin-1]=0.0;
    workspace[2*bin]=0.0;
  }
  fourier(workspace,length,1);



  /*now find the bounds of the useful signal to avoid edge effects*/
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    band1=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    band2=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if(ratPoint[numb].ratRes[Lplace].refl[band1]>thresh&&ratPoint[numb].ratRes[Lplace].refl[band2]>thresh){
      start=bin;
      break;
    }
  }
  if(start==0)start=1;
  for(bin=ratPoint[numb].bins-1;bin>start;bin--){
    band1=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    band2=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if(ratPoint[numb].ratRes[Lplace].refl[band1]>thresh&&ratPoint[numb].ratRes[Lplace].refl[band2]>thresh){
      end=bin;
      break;
    }
  }
  /*smooth reducing width until there is only one step*/

  /*FILE *smooth=NULL;*/

  contin=1;
  cont=0;
  do{
    /*#sprintf(namen,"%s.%d.smoothEdge",dimage->groundout,cont);
    #if((smooth=fopen(namen,"w"))==NULL){
    #  printf("Error opening windowed stats file %s",namen);
    #  exit(1);
    #}*/

   /*smooth signal THIS SHOULD ALL REPLACED BY THE EXPLICIT FREQUENCY DOMAIN GAUSSIAN RATHER THAN FFTing*/
    for(bin=1;bin<=length;bin++){
      smoother[2*bin-1]=gaussian((double)bin,(double)slength/4.0,0.0)+gaussian((double)bin,(double)slength/4.0,(double)length);  /*real*/
      smoother[2*bin]=0.0;                                                                                                       /*imaginary*/
    }
    fourier(smoother,length,1);

    for(bin=1;bin<=length;bin++){
      smoothed[2*bin-1]=workspace[2*bin-1]*smoother[2*bin-1]-workspace[2*bin]*smoother[2*bin];
      smoothed[2*bin]=workspace[2*bin-1]*smoother[2*bin]+workspace[2*bin]*smoother[2*bin-1];
    }
    fourier(smoothed,length,-1);
    for(bin=1;bin<=2*length;bin++)smoothed[bin]/=(double)length;

    /*#for(bin=1;bin<=2*ratPoint[numb].bins;bin+=2)fprintf(smooth,"%e %0.60f\n",(double)bin/2.0*ratPoint[numb].bin_L+ratPoint[numb].min_R,smoothed[bin]);
    #if(smooth){
    #  fclose(smooth);
    #  smooth=NULL;
    #}*/

    /*### remember "smoothed" is a complex array ###*/

    /*then calculate gradient, Laplacian and third differential*/
    steps=0;
    for(bin=1;bin<ratPoint[numb].bins;bin++){

      grad[bin]=smoothed[2*(bin+1)-1]-smoothed[2*bin-1];
      if(bin<ratPoint[numb].bins-1){
        Laplace[bin]=smoothed[2*(bin+2)-1]+smoothed[2*bin-1]-2.0*smoothed[2*(bin+1)-1];
      }
      if(bin>2&&bin<ratPoint[numb].bins-1){
        thirdif[bin]=Laplace[bin]-Laplace[bin-1];
      }
    }/*bin loops*/

    /*then look for turning points for bounds without wings */
    wstart=wend=0;
    for(bin=start;bin<ratPoint[numb].bins;bin++){
      if((thirdif[bin]*thirdif[bin+1]<0.0)&&thirdif[bin]<0.0){
        wstart=bin;
        break;
      }
    }
    for(bin=end;bin>wstart+1;bin--){
      if((thirdif[bin-1]*thirdif[bin]<0.0)&&thirdif[bin]>0.0){
        wend=bin;
        break;
      }
    }
    /*check the wave bounds*/

    for(bin=wstart;bin<=wend;bin++){
      if(thirdif[bin]*thirdif[bin-1]<0.0&&thirdif[bin-1]>0.0){
        steps++;
      }
    }


    printf("# searching for edge slength %d steps %d contin %d cont %d between %d and %d\n",(int)slength,steps,contin,cont,start+slength/2,end-slength/2);
    if(steps>1){      /*increase smoothing*/
      slength++;  /*=(int)((double)slength*1.1);*/
    }else if(steps<=1){ /*decrease smoothing*/
      slength--;  /*=(int)((double)slength/1.1);*/
      if(!slength)slength=1;
      contin=0;      /*we are near the correct size*/
    }

    if(nextfound&&!contin&&steps>0){ /*yoyoing*/
      found=1;
      goto EDGEEND;
    }else if(!contin&&steps>1){  /*we have found it, do once more*/
      /*slength++;*/
      nextfound=1;
    }else if(slength==1&&!steps){ /*there are no features to find*/
      goto EDGEEND;
    }else if(slength==1&&steps==1){
      found=1;
      goto EDGEEND;
    }
    if(nextfound)found=1;

    cont++;

  }while(slength>0&&(!found||(contin||(steps!=1&&slength>lengthThresh))));

  EDGEEND:



  if(found){
    /*now have a look for maximum gradient*/
    max=0;
    for(bin=wstart;bin<=wend;bin++){
      /*# if(smoothed[2*bin-1]>max){*/
      if(Laplace[bin]>max){
        max=Laplace[bin];
        ground=(double)(bin-1)*ratPoint[numb].bin_L+ratPoint[numb].min_R;   /*this will be start of gorund*/
      }
    }
    /*ground=((double)end*ratPoint[numb].bin_L+ratPoint[numb].min_R+ground)/2.0; take signal end as end of ground*/

    /*if(dimage->groundout==NULL){
      dimage->groundout=challoc((uint64_t)strlen("groundEstimate")+1,"ground etimate name",numb);
      strcpy(dimage->groundout,"groundEstimate");
    }*/

    /*sprintf(namen,"%s.groundedge",dimage->groundout);
    if((ratioGround=fopen(namen,"a+"))==NULL){
      fprintf(stderr,"Error opening windowed stats file %s",namen);
      exit(1);
    }

    fprintf(ratioGround,"Thy ground is at %f for %s\n",ground,dimage->output);

    if(ratioGround){
      fclose(ratioGround);
      ratioGround=NULL;
    }
    if(dimage->ratiout){
      sprintf(namen,"%s.windowStatsgroundedge",dimage->output);
      if((ratioGround=fopen(namen,"w"))==NULL){
        fprintf(stderr,"Error opening windowed stats file %s",namen);
        exit(1);
      }

      fprintf(ratioGround,"# 1 range\n# 2 ratio\n# 3 smoothed\n# 4 gradient\n# 5 Laplacian\n# 6 third differential\n");
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        fprintf(ratioGround,"%f %f %f %f %f %f\n",(double)bin*ratPoint[numb].bin_L+\
	  ratPoint[numb].min_R,jimlad[bin],smoothed[2*bin-1],grad[bin],Laplace[bin],thirdif[bin]);
      }
    }
    if(ratioGround){
      fclose(ratioGround);
      ratioGround=NULL;
    }*/

    if(dimage->ratiout){  /*[print out the final smoothed ratio and derivatives*/ 
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        j=findReflIndex(dimage,ratPoint,numb,bin,0,0);
        i=findReflIndex(dimage,ratPoint,numb,bin,1,0);
        fprintf(stdout,"%f %g %g %g %g %g %g\n",((double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R)/1000.0,ratPoint[numb].ratRes[Lplace].refl[j],ratPoint[numb].ratRes[Lplace].refl[i],smoothed[2*bin-1],grad[bin],Laplace[bin],thirdif[bin]);
      }
    }

    fprintf(stdout,"Ground starts at %f end at %f\n",ground,(double)end*ratPoint[numb].bin_L+ratPoint[numb].min_R);

  }else{ /*we have failed*/
    if(dimage->groundout==NULL){
      dimage->groundout=challoc((uint64_t)strlen("groundEstimate")+1,"ground etimate name",numb);
      strcpy(dimage->groundout,"groundEstimate");
    }

    sprintf(namen,"%s.groundedge",dimage->groundout);
    if((ratioGround=fopen(namen,"a+"))==NULL){
      fprintf(stderr,"Error opening windowed stats file %s",namen);
      exit(1);
    }

    fprintf(ratioGround,"# COuld not find ground for %s\n",dimage->output);

    if(ratioGround){
      fclose(ratioGround);
      ratioGround=NULL;
    }
  }

  TIDY(workspace);
  TIDY(smoothed);
  TIDY(smoother);
  TIDY(grad);
  TIDY(Laplace);
  TIDY(thirdif);

  return;
}/*findEdge*/


/*###################################################################################################*/
/*The new (as of 31st March 2009) spectral ground finding method*/

void spectralGround(RatControl *ratPoint,RatImage *dimage,int numb)
{
  int bin=0,band=0,Lplace=0,place=0;
  int length=0,i=0,j=0;
  int start=0,end=0;    /*signal extent*/
  int wstart=0,wend=0;  /*wings of the spectral ratio*/
  int grend=0,grart=0;
  int bins=0,padding=0;   /*to close up the gaps in the transfer arrays*/
  int *binMap=NULL,contN=0;
  float initialW=0;
  double *refl1=NULL,*refl2=NULL;
  double *ratio=NULL;
  double *weight=NULL,test=0;
  double *grad=NULL,*Laplace=NULL,*thirdif=NULL;
  double maxGrad=0,minGrad=0;
  double *nom=NULL,*denom=NULL;
  double *nomSmoo=NULL,*denomSmoo=NULL;
  double *smoothing=NULL,energy=0;
  double numerator1=0,numerator2=0;
  double denominator1=0,denominator2=0;
  double x=0,y=0,mu=0,sigma=0;
  double maxSmooth=0,minSmooth=0;
  double rat1=0,rat2=0,thresh=0;
  double LapThresh=0;
  double thresh1=0,thresh2=0;
  double energy1=0,energy2=0;
  double eThresh1=0,eThresh2=0;                    /*energy thresholds*/
  double mean1=0,mean2=0;                          /*mean intensities for noise tracking*/
  double k=0,a=0,c=0;                              /*weighting function parameters*/
  double lastMax=0;
  double lengthThresh=0,ground=0;
  int slength=0,sThresh=0;
  int steps=0;
  char contin=1,cont=0,nextfound=0,found=0;
  void fourier(double *,unsigned long,int);

  double *medianDouble(double *,int,int);
  double *medTemp=NULL;    /*for median filtering*/

  /*These numbers are arbitrary for now as a proof of concept*/
  maxSmooth=30.0*ratPoint[numb].bin_L;
  minSmooth=12.0*ratPoint[numb].bin_L;

  
  if(dimage->nBands!=2){
    fprintf(stderr,"-spectralGround requires 2 bands, not %d\n",dimage->nBands);
    exit(1);
  }

  lengthThresh=ratPoint[numb].bin_L;
  initialW=9000.0;  /*initial smoothing width*/
  Lplace=0;
  ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);



  /*determine the signal's extent to decide on smoothing function bounds*/
  thresh1=thresh2=-1.0;
  x=ratPoint[numb].min_R;
  mean1=mean2=0.0;
  while(x<dimage->noiseRange){
    bin=(int)((x-ratPoint[numb].min_R)/ratPoint[numb].bin_L);
    place=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    i=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>thresh1)thresh1=ratPoint[numb].ratRes[Lplace].refl[place];
    if(ratPoint[numb].ratRes[Lplace].refl[i]>thresh2)thresh2=ratPoint[numb].ratRes[Lplace].refl[i];
    mean1+=ratPoint[numb].ratRes[Lplace].refl[place];
    mean2+=ratPoint[numb].ratRes[Lplace].refl[i];
    contN++;
    x+=ratPoint[numb].bin_L;
  }
  if(contN>0){
    mean1/=(double)contN;
    mean2/=(double)contN;
  }


  if((thresh1<0.0)||(thresh2<0.0)){
    fprintf(stderr,"The thresholds haven't been calculated.\nIs your emptyrange of %f less than the minimum range of %f?\n",dimage->noiseRange,ratPoint[numb].min_R);
    exit(1);
  }

  /*Find the useful signal bounds*/
  /*find the waveform energies above the thresold*/
  start=end=-1;
  energy1=energy2=0.0;
  contN=0;
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    place=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    i=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>thresh1)energy1+=ratPoint[numb].ratRes[Lplace].refl[place]-thresh1;
    if(ratPoint[numb].ratRes[Lplace].refl[i]>thresh2)energy2+=ratPoint[numb].ratRes[Lplace].refl[i]-thresh2;
  }
  eThresh1=energy1*0.01;
  eThresh2=energy2*0.01;
  energy1=energy2=0.0;
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    place=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    i=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>thresh1)energy1+=ratPoint[numb].ratRes[Lplace].refl[place]-thresh1;
    if(ratPoint[numb].ratRes[Lplace].refl[i]>thresh2)energy2+=ratPoint[numb].ratRes[Lplace].refl[i]-thresh2;
    if((energy1>eThresh1)&&(energy2>eThresh2)){
      start=bin;
      break;
    }
  }
  energy1=energy2=0.0;

  /*find the ground from the raw waveforms*/
  grend=-1;
  for(bin=ratPoint[numb].bins-1;bin>start;bin--){
    place=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    i=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>thresh1)energy1+=ratPoint[numb].ratRes[Lplace].refl[place]-thresh1;
    if(ratPoint[numb].ratRes[Lplace].refl[i]>thresh2)energy2+=ratPoint[numb].ratRes[Lplace].refl[i]-thresh2;
    if((energy2>eThresh2)&&(grend<0)){      /*We've found some start*/
      for(j=bin;j<ratPoint[numb].bins;j++){ /*noise track*/
        place=findReflIndex(dimage,ratPoint,numb,j,1,0);
        if(ratPoint[numb].ratRes[Lplace].refl[place]<=mean2){
          grend=j;   /*ground end, set from stronger signal and only once*/
          break;
        }
      }
    }
    if((energy1>eThresh1)&&(energy2>eThresh2)){
      end=bin;
      break;
    }
  }
  if((start<0)||(end<0)){
    fprintf(stderr,"Arrgh, waveform bounds are incorrect, even before smoothing %f %f indices %d %d\n",(double)start*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)end*ratPoint[numb].bin_L+ratPoint[numb].min_R,start,end);
    exit(1);
  }


  /*Hereabouts take out all the blank bits in the middle. Leave enough on the ends to prevent wrapping around and things*/
  padding=start-(int)((dimage->noiseRange-ratPoint[numb].min_R)/ratPoint[numb].bin_L)+(int)(15000.0/ratPoint[numb].bin_L); /*to prevent wrap around in the Fourier domain*/
  bins=end-start+2*padding;
  refl1=dalloc(bins,"Collapsed wave for band 1",numb);
  refl2=dalloc(bins,"Collapsed wave for band 2",numb);
  binMap=ialloc(bins,"Collapsed wave bin map",numb);
  for(j=0;j<padding;j++){
    refl1[j]=refl2[j]=0.0;
    binMap[j]=start-(padding-j);
  }
  for(bin=start;bin<=end;bin++){
    place=findReflIndex(dimage,ratPoint,numb,bin,0,0);
    i=findReflIndex(dimage,ratPoint,numb,bin,1,0);
    if((ratPoint[numb].ratRes[Lplace].refl[place]>0.0)&&(ratPoint[numb].ratRes[Lplace].refl[i]>0.0)){
      refl1[j]=ratPoint[numb].ratRes[Lplace].refl[place];
      refl2[j]=ratPoint[numb].ratRes[Lplace].refl[i];
      binMap[j]=bin;
      j++;
    }
  }
  start=padding;
  i=end;  /*the old end*/
  //end=j;
  for(;j<bins;j++){                    /*to prevent wrap around in the Fourier domain*/
    refl1[j]=refl2[j]=0.0;
    binMap[j]=j-end+i;
  }


  slength=(double)(end-start)/2.0;
  sThresh=(double)(end-start)*3.0;


  fprintf(stdout,"Smoothing with %f %f\n",(double)slength*ratPoint[numb].bin_L,(double)sThresh*ratPoint[numb].bin_L);

  /*apply median filter to raw waveforms*/
  if(dimage->medRatio>0.0){
    medTemp=medianDouble(refl1,(int)(dimage->medRatio/(float)ratPoint[numb].bin_L+0.5),bins);
    TIDY(refl1);
    refl1=medTemp;
    medTemp=NULL;
    medTemp=medianDouble(refl2,(int)(dimage->medRatio/(float)ratPoint[numb].bin_L+0.5),bins);
    TIDY(refl2);
    refl2=medTemp;
    medTemp=NULL;
  }

  /*smooth to get the weights for later smoothing*/
  /*select a suitable length, sampled at pulse resolution*/
  length=bins;
  test=log((double)length)/log(2.0)-(int)(log((double)length)/log(2.0)+0.5);
  if(test>THRESHOLD||test<-THRESHOLD){
    printf("Padding from %d to ",length);
    length=pow(2.0,(float)((int)(log((double)bins)/log(2.0)+0.5)+1));
  }
  printf(" %d for edge detection...\n",length);



  /*load two arrays into complex arrays*/
  nom=dalloc(2*length+1,"Numerator array",numb);
  denom=dalloc(2*length+1,"Denominator array",numb);
  band=0;
  for(bin=1;bin<bins-1;bin++){
    nom[2*bin-1]=refl1[bin-1];
    nom[2*bin]=0.0;           /*imaginary*/
  }
  for(;bin<length;bin++){     /*pad with zeroes*/
    nom[2*bin-1]=0.0;
    nom[2*bin]=0.0;
  }
  band=1;
  for(bin=1;bin<bins-1;bin++){
    denom[2*bin-1]=refl2[bin-1];
    denom[2*bin]=0.0;           /*imaginary*/
  }
  for(;bin<length;bin++){     /*pad with zeroes*/
    denom[2*bin-1]=0.0;
    denom[2*bin]=0.0;
  }



  /*Fourier transform the two*/
  fourier(nom,length,1);
  fourier(denom,length,1);

  grad=dalloc(bins,"Spectral ratio gradient",numb);
  Laplace=dalloc(bins,"Spectral ratio Laplace",numb);
  thirdif=dalloc(bins,"Spectral ratio third differential",numb);
  ratio=dalloc(bins,"Spectral ratio waveform",numb);
  smoothing=dalloc(2*length+1,"edge detection smoothing",numb);
  nomSmoo=dalloc(2*length+1,"Smoothed numerator array",numb);
  denomSmoo=dalloc(2*length+1,"Smoothed denominator array",numb);



  /*Here find the maximum gradient within the wings as a starting point*/
  contin=1;
  cont=0;
  do{
   /*smooth signal THIS SHOULD ALL REPLACED BY THE EXPLICIT SPATIAL DOMAIN GAUSSIAN RATHER THAN FFTing*/
    for(bin=1;bin<=length;bin++){
      smoothing[2*bin-1]=gaussian((double)bin,(double)slength/4.0,0.0)+gaussian((double)bin,(double)slength/4.0,(double)length);  /*real*/
      smoothing[2*bin]=0.0;                                                                                                       /*imaginary*/
    }
    fourier(smoothing,length,1);

    for(bin=1;bin<=length;bin++){
      nomSmoo[2*bin-1]=nom[2*bin-1]*smoothing[2*bin-1]-nom[2*bin]*smoothing[2*bin];
      nomSmoo[2*bin]=nom[2*bin-1]*smoothing[2*bin]+nom[2*bin]*smoothing[2*bin-1]; 
      denomSmoo[2*bin-1]=denom[2*bin-1]*smoothing[2*bin-1]-denom[2*bin]*smoothing[2*bin];
      denomSmoo[2*bin]=denom[2*bin-1]*smoothing[2*bin]+denom[2*bin]*smoothing[2*bin-1];
    }
    fourier(nomSmoo,length,-1);
    fourier(denomSmoo,length,-1);
    for(bin=0;bin<bins;bin++){
      nomSmoo[bin]=sqrt(nomSmoo[2*(bin+1)-1]*nomSmoo[2*(bin+1)-1]+nomSmoo[2*(bin+1)]*nomSmoo[2*(bin+1)])/(double)length;
      denomSmoo[bin]=sqrt(denomSmoo[2*(bin+1)-1]*denomSmoo[2*(bin+1)-1]+denomSmoo[2*(bin+1)]*denomSmoo[2*(bin+1)])/(double)length;
    }

    /*now denoise to remove the long tails and aid wing finding*/
    /*calculate a noise thresold from the known emptyrange*/
    thresh1=thresh2=-1.0;
    bin=0;
    while(bin<padding){
      if(nomSmoo[bin]>thresh1)thresh1=nomSmoo[bin];
      if(denomSmoo[bin]>thresh2)thresh2=denomSmoo[bin];
      bin++;
    } 
    if((thresh1<0.0)||(thresh2<0.0)){
      fprintf(stderr,"The thresholds haven't been calculated.\nIs your emptyrange of %f less than the minimum range of %f?\nMaybe the padding needs reasessing.\n",dimage->noiseRange,ratPoint[numb].min_R);
      exit(1);
    }   

    /*Find the useful signal bounds*/
    /*find the waveform energies above the thresold*/
    start=-1; //end=-1;
    start=-1;
    energy1=energy2=0.0;
    for(bin=0;bin<bins;bin++){
      if(nomSmoo[bin]>thresh1)energy1+=nomSmoo[bin]-thresh1;
      if(denomSmoo[bin]>thresh2)energy2+=denomSmoo[bin]-thresh2;
    }
    eThresh1=energy1*0.01;
    eThresh2=energy2*0.01;
    energy1=energy2=0.0;
    for(bin=0;bin<bins;bin++){
      if(nomSmoo[bin]>thresh1)energy1+=nomSmoo[bin]-thresh1;
      if(denomSmoo[bin]>thresh2)energy2+=denomSmoo[bin]-thresh2;
      if((energy1>eThresh1)&&(energy2>eThresh2)){
        start=bin;
        break;
      }
    }
    energy1=energy2=0.0;
    for(bin=bins-1;bin>start;bin--){
      if(nomSmoo[bin]>thresh1)energy1+=nomSmoo[bin]-thresh1;
      if(denomSmoo[bin]>thresh2)energy2+=denomSmoo[bin]-thresh2;
      if((energy1>eThresh1)&&(energy2>eThresh2)){
        //end=bin;
        break;
      }
    }
    if((start<0)||(end<0)){
      fprintf(stderr,"Arrgh, waveform bounds are incorrect %f %f indices %d %d\n",(double)binMap[start]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[end]*ratPoint[numb].bin_L+ratPoint[numb].min_R,start,end);
      exit(1);
    }


    /*Calculate ratio*/
    for(bin=0;bin<(start+3);bin++)ratio[bin]=grad[bin]=Laplace[bin]=thirdif[bin]=0.0;
    for(bin=end+1;bin<bins;bin++)ratio[bin]=grad[bin]=Laplace[bin]=thirdif[bin]=0.0;
    for(bin=start;bin<=end;bin++){
      /*a threshold check could go here but we might not need it*/
      if(denomSmoo[bin]>0.0)ratio[bin]=nomSmoo[bin]/denomSmoo[bin];
      else                  ratio[bin]=0.0;
    }

    /*Calculate derivatives*/
    for(bin=start;bin<=end;bin++){
      if(bin>(1+start))grad[bin]=ratio[bin]-ratio[bin-1];
      if(bin>(2+start))Laplace[bin]=grad[bin]-grad[bin-1];
      if(bin>(3+start))thirdif[bin]=Laplace[bin]-Laplace[bin-1];
    }

    /*then look for turning points for bounds without wings */
    wstart=wend=0;
    for(bin=start;bin<bins;bin++){
      if((thirdif[bin]*thirdif[bin+1]<0.0)&&thirdif[bin]<0.0){
        wstart=bin;
        break;
      }
    }
    for(bin=end;bin>wstart+1;bin--){
      if((thirdif[bin-1]*thirdif[bin]<0.0)&&thirdif[bin]>0.0){
        wend=bin;
        break;
      }
    }
    /*check the wave bounds*/


    steps=0;
    for(bin=wstart;bin<=wend;bin++){
      //if(Laplace[bin]*Laplace[bin-1]<0.0&&Laplace[bin-1]>0.0){ /*a single maximum of the first differential*/
      if(thirdif[bin]*thirdif[bin-1]<0.0&&thirdif[bin-1]>0.0){ /*a single maximum of the second differential*/
        steps++;
      }
    }



    fprintf(stdout,"# searching for sufficient smoothing edge slength %d steps %d contin %d cont %d",slength,steps,contin,cont);
    fprintf(stdout," between %f and %f %d %d\n",(double)binMap[start]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[end]*ratPoint[numb].bin_L+ratPoint[numb].min_R,start,end);
    if(steps>1){      /*increase smoothing*/
      slength++;  /*=(int)((double)slength*1.1);*/
    }else if(steps<=1){ /*decrease smoothing*/
      slength--;  /*=(int)((double)slength/1.1);*/
      if(!slength)slength=1;
      contin=0;      /*we are near the correct size*/
    }

    if(nextfound&&(!contin)&&(steps>0)){ /*yoyoing*/
      found=1;
      goto GROUNDEND;
    }else if((!contin)&&(steps>1)){  /*we have found it, do once more*/
      /*slength++;*/
      nextfound=1;
    }else if((slength==1)&&(!steps)){ /*there are no features to find*/
      goto GROUNDEND;
    }else if((slength==1)&&(steps==1)){
      found=1;
      goto GROUNDEND;
    }
    if(nextfound)found=1;

    cont++;
    if(slength>=sThresh)fprintf(stderr,"Hit upper limit\n");
    else if(slength<=0)fprintf(stderr,"Hit lower limit\n");
  }while((slength>1)&&(!found||(contin||((steps!=1)&&(slength>lengthThresh))))&&(slength<sThresh));

  GROUNDEND:



  if(found){
    /*now have a look for maximum gradient*/
    maxGrad=0;
    for(bin=wstart;bin<=wend;bin++){
      /*# if(smoothed[2*bin-1]>max){*/
      if(grad[bin]>maxGrad){
        maxGrad=grad[bin];
        ground=bin-1+slength/4.0; /*this will be somewhere within the ground return*/
      }
    }
    goto WEIGHTYSMOOTH;
  }else{ /*we have failed*/
    fprintf(stdout,"# Could not sufficiently smooth %s\n",dimage->output);
    fprintf(stdout,"# Left with %d steps\n",steps);
    exit(1);
  }
  WEIGHTYSMOOTH: /*the weighted smoothing*/


  /*fprintf(stderr,"Smoothing by %d\n",slength);*/

  /*slength is a good indicator of the required smoothing. Mn and max can be based on this*/
  maxSmooth=(double)slength*ratPoint[numb].bin_L;
  minSmooth=2000.0;

  
  /*create the smoothing function, this is to get the weighting*/
  for(bin=1;bin<=length;bin++){
    smoothing[2*bin-1]=gaussian((double)bin*ratPoint[numb].bin_L,(double)initialW/4.0,0.0)\
      +gaussian((double)bin*ratPoint[numb].bin_L,(double)initialW/4.0,(double)length*ratPoint[numb].bin_L);  /*real*/
    smoothing[2*bin]=0.0;
  }
  fourier(smoothing,length,1);

  for(bin=1;bin<=length;bin++){
    denomSmoo[2*bin-1]=denom[2*bin-1]*smoothing[2*bin-1]-denom[2*bin]*smoothing[2*bin];
    denomSmoo[2*bin]=denom[2*bin-1]*smoothing[2*bin]+denom[2*bin]*smoothing[2*bin-1];
    nomSmoo[2*bin-1]=nom[2*bin-1]*smoothing[2*bin-1]-nom[2*bin]*smoothing[2*bin];
    nomSmoo[2*bin]=nom[2*bin-1]*smoothing[2*bin]+nom[2*bin]*smoothing[2*bin-1];
  }
  fourier(nomSmoo,length,-1);
  fourier(denomSmoo,length,-1);
  thresh=0.0;
  for(bin=1;bin<=2*length;bin++){
    denomSmoo[bin]/=(double)length; 
    nomSmoo[bin]/=(double)length;
    if(denomSmoo[bin]>thresh)thresh=denomSmoo[bin];
  }
  thresh*=0.1; /*the threshold can be quite high as it doesn't truncate the signal, only the range of gradients*/

  /*calculate gradient of the ratio*/
  weight=dalloc(bins,"Smoothing weight map",numb);
  maxGrad=0.0;
  for(bin=2;bin<bins;bin++){
    denominator1=sqrt(denomSmoo[2*bin-1]*denomSmoo[2*bin-1]+denomSmoo[2*bin]*denomSmoo[2*bin]);
    numerator1=sqrt(nomSmoo[2*bin-1]*nomSmoo[2*bin-1]+nomSmoo[2*bin]*nomSmoo[2*bin]);
    denominator2=sqrt(denomSmoo[2*(bin-1)-1]*denomSmoo[2*(bin-1)-1]+denomSmoo[2*(bin-1)]*denomSmoo[2*(bin-1)]);
    numerator2=sqrt(nomSmoo[2*(bin-1)-1]*nomSmoo[2*(bin-1)-1]+nomSmoo[2*(bin-1)]*nomSmoo[2*(bin-1)]);
    if(denominator1>thresh)rat1=numerator1/denominator1;
    else                   rat1=0.0;
    if(denominator2>thresh)rat2=numerator2/denominator2; /*subtract thresh to prevent the big step and start*/ 
    else                   rat2=0.0;
    if(((denominator1<=thresh)&&(numerator1>thresh)&&(denominator2<=thresh)&&(numerator2>thresh))||\
      ((rat2==0.0)&&(rat1>0.0))||((rat1==0.0)&&(rat2>0.0)))weight[bin]=-1.0;
    else                                                   weight[bin]=(rat1-rat2)*(rat1-rat2);
    if(weight[bin]>maxGrad)maxGrad=weight[bin];
    //fprintf(stdout,"%f %g %g %g %g %g\n",(double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R,numerator1,denominator1,weight[bin],rat1,rat2);
  }
  /*set all weights with small gradients to the maximum to prevent daft spikes*/
  for(bin=1;bin<bins;bin++)if(weight[bin]<0.0)weight[bin]=maxGrad;



  /*here is the weighted smoothing and ground finding section*/
  do{

    /*parameterise the smoohting function*/
    minGrad=0.0;
    a=(10*minGrad-maxGrad)/9.0; 
    k=(minGrad-maxGrad)/((minSmooth-maxSmooth)*(maxGrad-a)*(minGrad-a));
    c=minSmooth-1.0/(k*(maxGrad-a)); 


    /*Now smooth the original waveforms, weighted by the gradient of the ratio of the smoothed waveforms*/
    for(bin=1;bin<bins;bin++){
      nom[bin]=0.0;
      denom[bin]=0.0;
      energy=0.0;
      /*step from this point, calculating the overlap of the Gaussian*/
      if(dimage->weighty)sigma=1/(k*(weight[bin]-a))+c;
      else               sigma=2000.0;
      /*(maxGrad-weight[bin])/maxGrad*(maxSmooth-minSmooth)+minSmooth;*/
      /*fprintf(stderr,"%f %f %f %f\n",(double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R,weight[bin],maxGrad,sigma);*/
      mu=(double)bin*ratPoint[numb].bin_L;
      x=mu;
      i=bin;
      thresh=gaussian(x,sigma/4.0,mu)*THRESHOLD;

      if(refl1[i]>0.0){
        if(bin<(bins-1)){
          do{ /*upstream smoothing*/
            y=gaussian(x,sigma/4.0,mu);
            nom[bin]+=y*refl1[i]; //*ratPoint[numb].bin_L;
            denom[bin]+=y*refl2[i]; //*ratPoint[numb].bin_L;
            energy+=y; //*ratPoint[numb].bin_L;
    
            x+=ratPoint[numb].bin_L;
            i++;
          }while((y>=thresh)&&(i<bins));
        }
        if(bin){  /*to avoid stepping off the edge of the waveform*/
          x=mu-ratPoint[numb].bin_L;
          i=bin-1;
          do{  /*downstream smoothing*/
            y=gaussian(x,sigma/4.0,mu);
            nom[bin]+=y*refl1[i]; //*ratPoint[numb].bin_L;
            denom[bin]+=y*refl2[i]; //*ratPoint[numb].bin_L;
            energy+=y; //*ratPoint[numb].bin_L;
  
            x-=ratPoint[numb].bin_L;
            i--;
          }while((y>=thresh)&&(i>=0));
        }
        if(energy>0.0){
          nom[bin]/=energy;
          denom[bin]/=energy;
        }else nom[bin]=denom[bin]=0.0;
      }else nom[bin]=denom[bin]=0.0;
    }

    /*now denoise to remove the long tails and aid wing finding*/
    /*calculate a noise thresold from the known emptyrange*/
    thresh1=thresh2=-1.0;
    bin=0;
    while(bin<padding){
      if(nom[bin]>thresh1)thresh1=nom[bin];
      if(denom[bin]>thresh2)thresh2=denom[bin];
      bin++;
    }
    if((thresh1<0.0)||(thresh2<0.0)){
      fprintf(stderr,"The thresholds haven't been calculated.\nIs your emptyrange of %f less than the minimum range of %f?\n",dimage->noiseRange,ratPoint[numb].min_R);
      exit(1);
    }

    /*find the waveform energies above the thresold*/
    start=-1; //end=-1;
    energy1=energy2=0.0;
    for(bin=0;bin<bins;bin++){
      if(nom[bin]>thresh1)energy1+=nom[bin]-thresh1;
      if(denom[bin]>thresh2)energy2+=denom[bin]-thresh2;
    }
    eThresh1=energy1*0.01;
    eThresh2=energy2*0.01;
    energy1=energy2=0.0;
    for(bin=0;bin<bins;bin++){
      if(nom[bin]>thresh1)energy1+=nom[bin]-thresh1;
      if(denom[bin]>thresh2)energy2+=denom[bin]-thresh2;
      if(energy1>eThresh1){
	start=bin;
	break;
      }
      if(energy2>eThresh2){
	start=bin;
	break;
      }
    }
    energy1=energy2=0.0;
    for(bin=bins-1;bin>start;bin--){
      if(nom[bin]>thresh1)energy1+=nom[bin]-thresh1;
      if(denom[bin]>thresh2)energy2+=denom[bin]-thresh2;
      if(energy1>eThresh1){
        //end=bin;
        break;
      }
      if(energy2>eThresh2){
        //end=bin;
        break;
      }
    }
    if((start<0)||(end<0)){ /*check that some bounds have been found*/
      fprintf(stderr,"Damnit!!!\nThe ends are nigh!\n");
      exit(1);
    }else{
      //fprintf(stdout,"\n\nLooking between %f and %f\n\n",(double)start*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)end*ratPoint[numb].bin_L+ratPoint[numb].min_R);
    }

    /*Now we have the right smoothing calculate ratio and derivatives*/
    for(bin=0;bin<start;bin++)ratio[bin]=grad[bin]=Laplace[bin]=thirdif[bin]=0.0;
    for(bin=end+1;bin<bins;bin++)ratio[bin]=grad[bin]=Laplace[bin]=thirdif[bin]=0.0;
    for(bin=start;bin<=end;bin++){
      /*a threshopld check could go here but we might not need it*/
      if(denom[bin]>0.0)ratio[bin]=nom[bin]/denom[bin];
      else              ratio[bin]=0.0;
      if(bin>1)grad[bin]=ratio[bin]-ratio[bin-1];
      if(bin>2)Laplace[bin]=grad[bin]-grad[bin-1];
      if(bin>3)thirdif[bin]=Laplace[bin]-Laplace[bin-1];
    }
    //for(bin=0;bin<ratPoint[numb].bins;bin++)fprintf(stdout,"%f %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",((double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R)/1000.0,nom[bin],denom[bin],ratio[bin],grad[bin],Laplace[bin],thirdif[bin],weight[bin],1/(k*(weight[bin]-a)),a,k,minSmooth,maxSmooth,minGrad,maxGrad);


    /*for(bin=0;bin<bins;bin++){
      fprintf(stdout,"%f %f %f %f %f\n",(double)binMap[bin]*ratPoint[numb].bin_L+ratPoint[numb].min_R,ratio[bin],grad[bin],nomSmoo[bin],denomSmoo[bin]);
    }
    exit(0);*/

    

    grart=-1;  /*set to nonesense values*/

    /*find the wings*/
    /*leading wing*/
    thresh=0.0;
    //wstart=wend=-1;
    /*for(bin=0;bin<bins;bin++){*/
    for(bin=start;bin<bins;bin++){
      if(thirdif[bin]>thresh)thresh=thirdif[bin]; 
      /*if(grad[bin]>thresh)thresh=grad[bin];*/
      //if((thirdif[bin]*thirdif[bin+1]<0.0)&&(thirdif[bin]<=0.0))break;
      if((thirdif[bin]*thirdif[bin-1]<0.0)&&(thirdif[bin]<0.0))break;
    }
    /*wait for the steep gradient to settle down*/
    lastMax=thresh;
    thresh*=0.01;
    for(;bin<bins;bin++){
      /*keep a track of of maxima*/
      if((Laplace[bin]*Laplace[bin-1]<0.0)&&(grad[bin]<0.0)){

        maxGrad=grad[bin];
        if(maxGrad>lastMax){
          //wstart=bin;
          break;
        }else lastMax=maxGrad;
      }
    }
    /*trailing wing*/
    /*step along from "end" until the gradient flattens off from the large negative at the tail*/
    maxGrad=100000; /*here maxGrad is being used as a minimum*/
    for(bin=end;bin>wstart;bin--){
      if(grad[bin]<maxGrad)maxGrad=grad[bin];
      //fprintf(stderr,"%f maxGrad %f\n",(double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R,maxGrad);
      if(grad[bin-1]>grad[bin]){ /*it's crossed the minimum*/
	maxGrad*=0.01;  /*use as a threshold*/
        for(;bin>wstart;bin--){
          if(grad[bin]>maxGrad){
	    wend=bin;
            break;
	  }
	}
	if(wend>0)break;
      }
      if(wend>0)break;
    }

    /*bit of a hack*/
    //wstart=start;



    /*determine whether the smoothing is sufficient*/

    /*somehow*/

    if((wstart<0)||(wend<0)){ /*then increease the minimum smoothing and start again*/
      minSmooth/=1.1; 
      fprintf(stdout,"Changing smoothing to %f between %f %f\n",minSmooth,(double)binMap[wstart]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[wend]*ratPoint[numb].bin_L+ratPoint[numb].min_R);
    }else{


      /*from the heavily smoothed ground estimage step along to find the actual ground*/
      grart=-1;
      for(bin=ground;bin>wstart;bin--){     /*step from the maximum smoothed gradient*/
        if(Laplace[bin]>Laplace[bin-1]){    /*we're on the rise, satrt looking for a maximum*/
          for(;bin>wstart;bin--){
            if((thirdif[bin]*thirdif[bin-1]<0.0)&&(thirdif[bin]<0.0)&&(Laplace[bin]>0.0)){
              grart=bin;
              break;
            }
          }
        }
        if(grart>0)break;
      }
      if(grart<0.0){
        fprintf(stderr,"Sod it ground start is negative %d from wing %f\n",grart,(double)binMap[wstart]*ratPoint[numb].bin_L+ratPoint[numb].min_R);
        exit(1);
      }


      /*find a threshold for the gradient*/      
      LapThresh=-1.0;
      for(bin=wstart;bin<wend;bin++){
        if(Laplace[bin]>LapThresh)LapThresh=Laplace[bin];
      }
      LapThresh*=0.001;


      /*determine whether the smoothing is sufficient*/
      steps=0; /*count the number of significant maxima of the second derivative*/
      /*for(bin=wstart+1;bin<wend;bin++){ 
        if((thirdif[bin]*thirdif[bin-1]<0.0)&&(thirdif[bin]<0.0)&&(Laplace[bin]>LapThresh))steps++;
      }
      fprintf(stderr,"Start %f end %f\nwstart %f wend %f\nSmoothing %f %f\nSteps %d grad %f\n",(double)start*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)end*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)wstart*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)wend*ratPoint[numb].bin_L+ratPoint[numb].min_R,minSmooth,maxSmooth,steps,maxGrad);
*/

      steps=1; /*hack around for now*/
      
      if(steps>2){ /*increase smoothing*/
        contin=1;
        minSmooth*=1.1;
        maxSmooth*=1.1;
      }else if(steps<1){ /*decrease smoothing*/
        contin=1;
        minSmooth/=1.1;
        maxSmooth/=1.1;
      }else if((steps==1)||(steps==2)){ /*it's just right*/
        contin=0;
      }else{
        fprintf(stderr,"Something is wrong with your code\n");
        exit(1);
      }


    }
    if(maxSmooth<((start-end)*ratPoint[numb].bin_L)){
      fprintf(stderr,"Smoothing too long now\n");
      exit(1);
    }
    if(minSmooth<1.0){
      fprintf(stderr,"Smoothing too small now\n");
      exit(1);
    }
  }while(contin);


  if(dimage->ratiout){  /*print out the final ratio and derivatives*/ 
    for(bin=0;bin<bins;bin++)fprintf(stdout,"%f %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",((double)binMap[bin]*ratPoint[numb].bin_L+ratPoint[numb].min_R)/1000.0,nom[bin],denom[bin],ratio[bin],grad[bin],Laplace[bin],thirdif[bin],weight[bin],1/(k*(weight[bin]-a)),a,k,minSmooth,maxSmooth,minGrad,maxGrad); 
  }


  if((grart<0)||(grend<0)){
    fprintf(stderr,"Noooooooo, ground %f %f\nSearched between %f and %f\nAnd wings %f %f\n",(double)binMap[grart]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[grend]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[start]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[end]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[wstart]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[wend]*ratPoint[numb].bin_L+ratPoint[numb].min_R);
    minSmooth/=1.1;
    fprintf(stdout,"Smoothing by %f\n",minSmooth);
    exit(1);
  }



  fprintf(stdout,"Ground starts at %f end at %f\n",(double)binMap[grart]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[grend]*ratPoint[numb].bin_L+ratPoint[numb].min_R);
  fprintf(stdout,"Having searched between bounds %f and %f and wings %f and %f\n",(double)binMap[start]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[end]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[wstart]*ratPoint[numb].bin_L+ratPoint[numb].min_R,(double)binMap[wend]*ratPoint[numb].bin_L+ratPoint[numb].min_R);

  TIDY(ratio);  
  TIDY(grad);
  TIDY(Laplace);
  TIDY(thirdif);
  TIDY(nom);
  TIDY(denom);
  TIDY(nomSmoo);
  TIDY(denomSmoo);
  TIDY(smoothing);
  TIDY(weight);
  TIDY(refl1);
  TIDY(refl2);
  TIDY(binMap);

  //ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
  
  return;
}/*spectralGround*/


/*###################################################################################################*/
/*calculate windowed variance of an array of doubles*/

void windowedStats(RatImage *dimage,RatControl *ratPoint,double *jimlad,int length,int numb,int Lplace)
{
  int i=0,j=0,place=0,scale=0;
  int nsteps=0,stepsize=0;
  int nscale=0;         /*number of scales*/
  int minsize=0,maxsize=0;
  int *winsize=NULL;
  double **variance=NULL; /*variance at different scales and positions*/
  double **window=NULL;  /*window shapes at each scale*/
  double **mean=NULL;
  double sigma=0,cont=0;
  double estimateWindowsize(int);
  FILE *opoo=NULL;
  char namen[200];

  nscale=100;
  if(length<200)minsize=1;
  else minsize=(int)((float)length/200.0+0.5);
  maxsize=(int)((float)length/2.0+0.5);
  if(minsize<2)minsize=2;

  winsize=ialloc(nscale,"variance window length",0);
  if(!(window=(double **)calloc(nscale,sizeof(double *)))){
    fprintf(stderr,"error allocation modified");
    exit(1);
  }
  if(!(mean=(double **)calloc(nscale,sizeof(double *)))){
    fprintf(stderr,"error allocation modified");
    exit(1);
  }
  if(!(variance=(double **)calloc(nscale,sizeof(double *)))){
    fprintf(stderr,"error allocation modified");
    exit(1);
  }


  for(scale=0;scale<nscale;scale++){

    sprintf(namen,"%s.%d.window",dimage->output,scale);
    if((opoo=fopen(namen,"w"))==NULL){
      fprintf(stderr,"Error opening windowed stats file %s",namen);
      exit(1);
    }


    winsize[scale]=minsize+(int)((float)((scale+1)*(maxsize-minsize))/(float)nscale);
    stepsize=1.0;     /*winsize[scale]/10;*/
    if(!stepsize)stepsize=1;

    /*printf("scale %d length %d minsize %d maxsize %d winsize %d stepsize %d\n",scale,length,minsize,maxsize,winsize[scale],stepsize);*/


    /*set the windowing function. caluclate sigma if we are to use a gaussian*/
    sigma=estimateWindowsize(winsize[scale]);
    window[scale]=dalloc(winsize[scale],"variance window function",scale);
    for(j=0;j<winsize[scale];j++){
      window[scale][j]=gaussian((double)j,sigma,(double)winsize[scale]/2.0);
    }

    nsteps=(int)((float)length/(float)stepsize);

    mean[scale]=dalloc(nsteps,"windowed mean",scale);
    variance[scale]=dalloc(nsteps,"windowed variance",scale);

    for(i=0;i<nsteps;i++){

      /*calculate mean*/
      cont=0;
      mean[scale][i]=0.0;
      for(j=0;j<winsize[scale];j++){
        place=i*stepsize-(int)((float)winsize[scale]/2.0)+j;
        if(place>=0&&place<length){
          mean[scale][i]+=jimlad[place]*window[scale][j];
          cont+=window[scale][j];
        }
      }
      if(cont>0.0) mean[scale][i]/=cont;
      else         mean[scale][i]=0.0;

      /*and the variance*/
      cont=0.0;
      variance[scale][i]=0.0;
      for(j=0;j<winsize[scale];j++){
        place=i*stepsize-(int)((float)winsize[scale]/2.0)+j;
        if(place>=0&&place<length){
          variance[scale][i]=(mean[scale][i]-jimlad[place])*(mean[scale][i]-jimlad[place])*window[scale][j];
          cont+=window[scale][j];
        }
      }
      if(cont>0.0)variance[scale][i]/=cont;
      else        variance[scale][i]=0.0;
      fprintf(opoo,"%f scale %d window %d mean %f variance %f cont %f stepsize %d winsize %d\n",(double)(i*stepsize)*ratPoint[numb].bin_L+ratPoint[numb].min_R,scale,i,mean[scale][i],variance[scale][i],cont,winsize[scale],nsteps);
    }
    if(opoo){
      fclose(opoo);
      opoo=NULL;
    }
  }/*window scale loop*/



  /*do whatever analysis we want on the windowed stats*/




  if(window){
    for(i=0;i<nscale;i++)TIDY(window[i]);
    free(window);
    window=NULL;
  }
  TIDY(winsize);
  if(mean){
    for(i=0;i<nscale;i++)TIDY(mean[i]);
    free(mean);
    mean=NULL;
  }
  if(variance){
    for(i=0;i<nscale;i++)TIDY(variance[i]);
    free(variance);
    variance=NULL;
  }
  return;
}/*windowedStats*/

/*##################################################################################################*/
/*find sigma needed to go below a threshold within a certain distance*/

double estimateWindowsize(int length)
{
  double sigma=0,E=0,Eold=0;
  double thresh=0,step=0,x=0;
  char turned=0;

  thresh=0.000001;
  x=(double)length/2.0;
  step=1.0;
  sigma=1.0;

  Eold=gaussian(x,sigma,0);
  do{
    E=gaussian(x,sigma,0);

    /*#if(E>thresh&&Eold<thresh){
      #step*=-0.5;
      #turned=1;
    #}*/
    sigma+=step;
  }while(E<thresh||turned);

  /*take one step back*/
  return(sigma-step);
}/*estimateWindowsize*/

/*###################################################################################################*/
/*estimate noise level from known "blank" area of waveform*/

void noiseEstimate(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace)
{
  int band=0,bin=0,blankBin=0,noiseBins=0,place=0;
  double *meanoise=NULL,*stdevnoise=NULL;
  double ndevs=0,*max=NULL;
  void signalStart(RatImage *,RatControl *,int,int,double *,double *,double *,double);

  ndevs=dimage->ndevs;   /*set as 3.0 by default*/

  /*predict noise level if part of the signal is known to be blank*/
  TIDY(dimage->meanoise);
  TIDY(dimage->stdevnoise);
  meanoise=dalloc(dimage->nBands,"mean noise",numb);
  stdevnoise=dalloc(dimage->nBands,"standard deviation of noise",numb);
  max=dalloc(dimage->nBands,"maximum signal",numb);
  dimage->noithresh=dalloc(dimage->nBands,"noise threshold",numb);


  /*start by looking at the extra background noise array*/
  noiseBins=dimage->noiseBins;
  if(dimage->noise==0.0)noiseBins=0;  /*as "->backnoise" is unnasigned if noise==0.0*/

/*  noiseBins=0; put this back in to get unrealisitic but qorkable old days */

  blankBin=(int)(((double)dimage->noiseRange-ratPoint[numb].min_R)/ratPoint[numb].bin_L+0.5);
  if(blankBin<0)blankBin=0;
  
  for(band=0;band<dimage->nBands;band++){ 
    meanoise[band]=0.0;
    stdevnoise[band]=0.0;
    max[band]=0.0;
    for(bin=0;bin<noiseBins;bin++){
      meanoise[band]+=dimage->backnoise[band][bin];
      if(dimage->backnoise[band][bin]>max[band])max[band]=dimage->backnoise[band][bin];
    }

    /*look into signal if we have some overrun*/
    for(bin=0;bin<blankBin;bin++){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      meanoise[band]+=ratPoint[numb].ratRes[Lplace].refl[place];
      if(ratPoint[numb].ratRes[Lplace].refl[place]>max[band])max[band]=ratPoint[numb].ratRes[Lplace].refl[place];
    }
    meanoise[band]/=(double)(noiseBins+blankBin);

    /*calculate standard deviation*/
    for(bin=0;bin<noiseBins;bin++){
      stdevnoise[band]+=(dimage->backnoise[band][bin]-meanoise[band])*(dimage->backnoise[band][bin]-meanoise[band]);
    }
    for(bin=0;bin<blankBin;bin++){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      stdevnoise[band]+=(ratPoint[numb].ratRes[Lplace].refl[place]-meanoise[band])*(ratPoint[numb].ratRes[Lplace].refl[place]-meanoise[band]);
    }
    stdevnoise[band]=sqrt(stdevnoise[band]/(double)(noiseBins+blankBin));

  }/*band loop*/

  /*Set threshold, ensuring the blank portion is blank*/
  for(band=0;band<dimage->nBands;band++){
    if(dimage->noise>0.0){
      dimage->noithresh[band]=meanoise[band]+ndevs*stdevnoise[band];
      if(dimage->noithresh[band]<max[band])dimage->noithresh[band]=max[band];
    }else if(dimage->noise==0.0){
      dimage->noithresh[band]=0.0;
      meanoise[band]=0.0;
      stdevnoise[band]=0.0;
    }else{
      fprintf(stderr,"What's going on here then?\n");
      exit(1);
    }
  }

  if(dimage->sigstart)signalStart(dimage,ratPoint,numb,Lplace,meanoise,stdevnoise,max,ndevs);

  dimage->meanoise=meanoise;
  dimage->stdevnoise=stdevnoise;

  TIDY(max);

  return;
}/*noiseEstimate*/

/*################################################################################################*/
/*estimate signal start*/

void signalStart(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace,double *meanoise,double *stdevnoise,double *max,double ndevs)
{
  int band=0,bin=0,place=0,mat=0;
  double thresh=0,unbiased=0,biased=0;
  double energy=0,maxE=0;
  int maxband=0;
  char brEak=0;

  /*use most energetic band to calculate signal start*/
  for(band=0;band<dimage->nBands;band++){
    energy=0.0;
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      energy+=ratPoint[numb].ratRes[Lplace].refl[place];
    }
    if(energy>maxE){
      maxE=energy;
      maxband=band;
    }
  }
  band=maxband;


    /*##for(band=0;band<dimage->nBands;band++){*/
  thresh=dimage->noithresh[band];
  energy=0.0;
  maxE=0.0;     /*calculate total energy without DC bias*/
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>thresh)maxE+=ratPoint[numb].ratRes[Lplace].refl[place];
  }
  for(bin=0;bin<ratPoint[numb].bins;bin++){
    place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>thresh){
      energy+=ratPoint[numb].ratRes[Lplace].refl[place]-thresh;

      if(energy>=maxE*0.01){  /*then we've had 1% of total energy*/
        if(dimage->unTracked)biased=((double)bin+0.5)*ratPoint[numb].bin_L+ratPoint[numb].min_R+dimage->conlength/4.0;
        for(;bin>=0;bin--){
          place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
          if((ratPoint[numb].ratRes[Lplace].refl[place]==0.0) || (ratPoint[numb].ratRes[Lplace].refl[place]<meanoise[band])){
            /*then the signal has gone to zero and the last point is the start*/
            unbiased=((double)bin+0.5)*ratPoint[numb].bin_L+ratPoint[numb].min_R+dimage->conlength/4.0;
            fprintf(stdout,"Signal starts at %f for band %d\n",unbiased,band);
            if(dimage->unTracked)fprintf(stdout,"Untracked start at %f for threshold %g\n",biased,dimage->noithresh[band]);
            break;
          }else if((ratPoint[numb].ratRes[Lplace].refl[place]==meanoise[band])&&\
                  ((meanoise[band]>0.0)||(dimage->conlength==0.0))){
           /*very unlikely*/
            unbiased=((double)bin+0.5)*ratPoint[numb].bin_L+ratPoint[numb].min_R+dimage->conlength/4.0;
            printf("Signal starts at %f for band %d\n",unbiased,band);
            break;
          }
        }
        break;
      }/*energy threshold*/
    }/*intensity threshold*/
  }/*bin loop*/
    /*##}*//*band loop*/


  /*the truth, if we're doing that*/
  brEak=0;
  if(dimage->unTracked){
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      for(mat=0;mat<ratPoint[numb].nMat;mat++){
        place=findMaterialIndex(dimage,ratPoint,numb,bin,mat,0,0);
        if(ratPoint[numb].ratRes[Lplace].material[place]>0.0){
          fprintf(stdout,"True start at %f\n",((double)bin+0.5)*ratPoint[numb].bin_L+ratPoint[numb].min_R);
          brEak=1;
          break;
        }
      }
      if(brEak)break;
    }
  }/*truth finding check*/


  /*#################################################################################*/
  /*this part is a quick bit to see which method for measuring signal start is better*/
/*#  actStart=-1.0;
# for(bin=0;bin<ratPoint[numb].bins;bin++){
#   for(mat=0;mat<ratPoint[numb].nMat;mat++){
#     if(ratPoint[numb].ratRes[Lplace].material[bin*ratPoint[numb].nMat*2+mat*2]>0.0){
#       actStart=(double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R;
#       break;
#     }
#   }
#   if(actStart>-1.0)break;
# }
# if(dimage->groundout==NULL){
#   dimage->groundout=challoc((uint64_t)strlen("groundEstimate")+1,"ground etimate name",numb);
#   strcpy(dimage->groundout,"groundEstimate");
# }
# sprintf(namen,"%s.toptest",dimage->groundout);
# if((opoo=fopen(namen,"a+"))==NULL){
#   fprintf(stderr,"Error opening %s\n",namen);
#   exit(1);
# }
# fprintf(opoo,"%f %f %f %f %s ",actStart,biased,unbiased,dimage->noise,dimage->output);
# if(max[0]>meanoise[0]+ndevs*stdevnoise[0])fprintf(opoo,"max");
# else fprintf(opoo,"thresh");
#fprintf(opoo,"\n");
#if(opoo){fclose(opoo);opoo=NULL;}
#*/
  /* End of the quick test*/
  /*####################################################################################*/

  return;
}/*signalStart*/


/*###################################################################################################*/

void removeNoise(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace)
{
  int bin=0,band=0,place=0;
  double *thresh=NULL;
  double *energy=NULL,*scale=NULL,factor=0;
  double *meanoise=NULL,*stdevnoise=NULL;

  if(dimage->intent)printf("Removing noise...\n");

  energy=dalloc(dimage->nBands,"energy for noise",Lplace);

  /*find energies to preserve spectral ratios*/
  for(band=0;band<dimage->nBands;band++){
    energy[band]=0.0;
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      energy[band]+=ratPoint[numb].ratRes[Lplace].refl[place];
    }
  }
  meanoise=dimage->meanoise;
  stdevnoise=dimage->stdevnoise;

  thresh=dalloc(dimage->nBands,"noise threshold",numb);

  /*Calculate scale factors preserving spectral ratio*/
  if(dimage->nBands==1){  /*no spectral ratio to preserve*/
    band=0;
    thresh[band]=dimage->noithresh[band];
  }else if(dimage->nBands==2){
    for(band=0;band<dimage->nBands;band++){
      thresh[band]=dimage->noithresh[band];
    }
    factor=energy[0]*thresh[1]/(energy[1]*thresh[0]);
    scale=dalloc(2,"noise threshold scale factor",Lplace);
    scale[0]=2.0*factor/(factor+1.0);
    scale[1]=2.0/(factor+1.0);

    thresh[0]*=scale[0];
    thresh[1]*=scale[1];

    printf("factor %f %f %f thresh %f %f energy %f %f\n",factor,scale[0],scale[1],thresh[0],thresh[1],energy[0],energy[1]);

  }else if(dimage->nBands>2){
    fprintf(stderr,"Bugger, not sure how to remove noise for %d bands yet\n",dimage->nBands);
    exit(1);
  }else{
    fprintf(stderr,"\nWe should not have %d bands\n\n",dimage->nBands);
    exit(1);
  }

  for(band=0;band<dimage->nBands;band++){
    if(thresh[band]>0.0){
      for(bin=0;bin<ratPoint[numb].bins;bin++){
        place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
        ratPoint[numb].ratRes[Lplace].refl[place]-=thresh[band];
        if(ratPoint[numb].ratRes[Lplace].refl[place]<0.0)ratPoint[numb].ratRes[Lplace].refl[place]=0.0;
      }
    }
  }

  meanoise=NULL;
  stdevnoise=NULL;
  TIDY(energy);
  TIDY(scale);
  TIDY(thresh);

  return;
}/*removeNoise*/

/*###################################################################################################*/
/*Calculate maximum amplitude of a band*/

double maxSignal(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace,int band)
{
  int bin=0,place=0;
  double max=0.0;

  for(bin=0;bin<ratPoint[numb].bins;bin++){
    place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
    if(ratPoint[numb].ratRes[Lplace].refl[place]>max)max=ratPoint[numb].ratRes[Lplace].refl[place];
  }

  return(max);
}/*maxSignal*/

/*###################################################################################################*/
/*perform some maths on waveforms*/

void modifyWaves(RatControl *ratPoint,RatImage *dimage,int numb)
{
  int i=0;
  int Lplace=0;
  void saveNewWave(RatControl *,RatImage *,int);

  Lplace=0;   /*for now, perhaps add some choice later*/

  ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);
/*  if(dimage->bandRatio[0]>0.0){
#    for(band=0;band<ratPoint[numb].nBands;band++){
#      if(dimage->ratioBand[0]==ratPoint[numb].wavelength[band]){
#        band1=band;
#        found1=1;
#      }else if(dimage->ratioBand[1]==ratPoint[numb].wavelength[band]){
#        band2=band;
#        found2=1;
#      }
#    }
#    if(!found1||!found2){
#      fprintf(stderr,"a band's not been found %d %f %d %f\n",found1,dimage->ratioBand[0],found2,dimage->ratioBand[1]);
#      exit(1);
#    }
#
#    for(bin=0;bin<ratoint[numb].bins;bin++){
#      for(n=0;n<=ratPoint[numb].n;n++){
#        place1=findReflIndex(dimage,ratPoint,numb,bin,band1,n);
#        place2=findReflIndex(dimage,ratPoint,numb,bin,band2,n);
#        jimplace=findReflIndex(dimage,ratPoint,numb,bin,0,n);
#        if(ratPoint[numb].ratRes[Lplace].refl[place2]>0.0){
#          jimlad[jimplace]=ratPoint[numb].ratRes[Lplace].refl[place1]/ratPoint[numb].ratRes[Lplace].refl[place2];
#        }else jimlad[jimplace]=0.0;
#      }
#    }
#    ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
*/

    /*set up the new header*/
    if(dimage->newpoint==NULL){
      if(!(dimage->newpoint=(RatHead *)calloc(1,sizeof(RatHead)))){
        fprintf(stderr,"error allocation modified");
        exit(2);
      }
      dimage->newpoint->bins=ratPoint[numb].bins;
      dimage->newpoint->NZscans=ratPoint[numb].NZscans;
      dimage->newpoint->NAscans=ratPoint[numb].NAscans;
      dimage->newpoint->n=ratPoint[numb].n;
      dimage->newpoint->nBands=1;
      dimage->newpoint->nMat=ratPoint[numb].nMat;
      dimage->newpoint->withinSegs=ratPoint[numb].withinSegs;
      dimage->newpoint->withoutSegs=ratPoint[numb].withoutSegs;
      dimage->newpoint->EpLength=ratPoint[numb].EpLength[0];
      for(i=0;i<3;i++){
        dimage->newpoint->from[i]=ratPoint[numb].from[i];
      }
      dimage->newpoint->div=ratPoint[numb].div;
      dimage->newpoint->divSt=ratPoint[numb].divSt;
      dimage->newpoint->iZen=ratPoint[numb].iZen;
      dimage->newpoint->fZen=ratPoint[numb].fZen;
      dimage->newpoint->iAz=ratPoint[numb].iAz;
      dimage->newpoint->fAz=ratPoint[numb].fAz;
      dimage->newpoint->wavelength=dalloc(1,"new file wavelength",numb);
      dimage->newpoint->wavelength[0]=-1.0;
      dimage->newpoint->Plength=ratPoint[numb].Plength[0];
      dimage->newpoint->Pres=ratPoint[numb].Pres;
    }

/*
#  }else{
#
 # }
*/

  saveNewWave(ratPoint,dimage,numb);

  return;
}/*modifyWaves*/

/*########################################################################################*/
/*save the solar irradiance for each wavelength read from an ascii file*/

void solarIrradiance(RatImage *dimage,RatControl *ratPoint,int numb)
{
  int band=0,i=0;
  int fileLines=0;
  float *wave=NULL,*irradiance=NULL;
  float diff=0,mindiff=0,lastdiff=0;         /*all differences are squared to save square rooting to get modulus*/
  char namen[200],line[200],guff1[100],guff2[100];
  char found=0;
  FILE *data=NULL;


  /*read in the ascii file*/
  sprintf(namen,"solarIrradiance.dat");
  if((data=fopen(namen,"r"))==NULL){
    fprintf(stderr,"Error opening %s",namen);
    exit(1);
  }

  fileLines=170;  /*make sure this is longer than the solar irradiance file (less comments)*/
  wave=falloc(fileLines,"solar irradiance wavelength",0);
  irradiance=falloc(fileLines,"that solar irradiance",0);
  i=0;
  while(fgets(line,200,data)!=NULL){
    if(strncasecmp(&(line[0]),"#",1)){
      if(sscanf(line,"%s %s",guff1,guff2)!=2) {
        fprintf(stderr,"Badly formatted file %s, need %d lines\n",namen,2);
        exit(1);
      }
      wave[i]=atof(guff1);
      irradiance[i]=atof(guff2);
      i++;
      if(i==fileLines){
        fprintf(stderr,"Not enough space has been set aside to hold the solar irradiance file. Does it have more than %d lines (less comments)?\n",fileLines);
        exit(1);
      }
    }
  } 
  fileLines=i;   /*set to the actual number in the file*/

  if(data){
    fclose(data);
    data=NULL;
  }

  dimage->solarI[numb]=falloc(dimage->nBands,"inner solar irradiance",numb);

  /*save closest wavelength values*/
  for(band=0;band<dimage->nBands;band++){
    mindiff=100000000.0;
    lastdiff=10000000000.0;
    found=0;
    for(i=0;i<fileLines;i++){
      diff=(ratPoint[numb].wavelength[dimage->band[numb][band]]-wave[i])*(ratPoint[numb].wavelength[dimage->band[numb][band]]-wave[i]);
      if(diff<mindiff){
        mindiff=diff;
        dimage->solarI[numb][band]=irradiance[i];
        found=1;
      }else if(diff>lastdiff){
        break;
      }
      lastdiff=diff;
    }
    if(!found){
      fprintf(stderr,"Can't find match for %f in %s\n",ratPoint[numb].wavelength[dimage->band[numb][band]],namen);
      exit(1);
    }
  }

  TIDY(wave);
  TIDY(irradiance);
  return;
}/*solarIrradiance*/

/*########################################################################################*/
/*save a modified wave form*/

void saveNewWave(RatControl *ratPoint,RatImage *dimage,int numb)
{
  int Lplace=0,tScans=0;
  int arrLength=0,marrLength=0;
  int *compLeng=NULL,*header1=NULL;
  double *header2=NULL;
  int *coords=NULL;
  double *jimlad=NULL;
  float *matlad=NULL;
  char intLidar=0,namen[100];
  int *intHeader(RatImage *,RatControl *,char,int);
  double *doHeader(RatControl *,RatImage *,char,int);
  FILE *dopoo=NULL;
  int encodeRefl(RatControl *,int,int,double *,int);
  int encodeMaterial(RatControl *,int,int,float *,int);

  if((dimage->Plength==0&&!dimage->sdecon)&&dimage->coarsen==1.0){
    printf("The option \"-save\" is redundant, you're not deconvolving\n");
    return;
  }

  /*check that the original data file will not be overwritten*/
  if(!strncasecmp(dimage->output,dimage->input,sizeof(dimage->output))){
    fprintf(stderr,"You are trying to overwrite the original data file %s, use -output\n",dimage->input);
    exit(1);
  }
  sprintf(namen,"%s.data",dimage->output);
  if((dopoo=fopen(namen,"wb"))==NULL){
    fprintf(stderr,"Error opening deconvolved data file\n");
    exit(1);
  }
  printf("Writting out modified waveform to %s\n",namen);
  /*if(namen)free(namen);*/

  tScans=ratPoint[numb].NAscans*ratPoint[numb].NZscans;

  if(!(coords=(int *)calloc(3*tScans,sizeof(int)))){
    fprintf(stderr,"error in deconvolved data coordinate array allocation.\n");
    exit(1);
  }
  if(!(compLeng=(int *)calloc(2*tScans,sizeof(int)))){
    fprintf(stderr,"error in deconvolved compression length array allocation.\n");
    exit(1);
  }
  if(!dimage->deconOrders){
    arrLength=ratPoint[numb].bins*ratPoint[numb].nBands*(ratPoint[numb].n+1)*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs);
    marrLength=(ratPoint->bins*ratPoint->nMat*2+1)*(ratPoint->withinSegs+ratPoint->withoutSegs);
  }else{
    arrLength=ratPoint[numb].bins*ratPoint[numb].nBands*3*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs);
    marrLength=(ratPoint->bins*ratPoint->nMat*2+1)*(ratPoint->withinSegs+ratPoint->withoutSegs);
  }

  /*arrange the int header*/
  if(!ratPoint[numb].encoded){fprintf(stderr,"nooooo\n");exit(1);}       /*this format will not work yet*/
  else if(ratPoint[numb].encoded&&ratPoint[numb].headerLength==12){
    intLidar=1;
  }else if(ratPoint[numb].encoded&&ratPoint[numb].headerLength==9){
    intLidar=0;
  }

  /*set up the headers*/
  header1=intHeader(dimage,ratPoint,intLidar,numb);
  header2=doHeader(ratPoint,dimage,intLidar,numb);

  if(dimage->byteord){
    header1=intSwap(header1,ratPoint[numb].headerLength);
    header2=doSwap(header2,ratPoint[numb].header2Length);
  }

  for(Lplace=0;Lplace<tScans;Lplace++){

    printf("Deconvolving beam %d of %d\n",Lplace+1,tScans);

    ratPoint=resultsArrange(dimage,ratPoint,numb,Lplace,0);

    if(!(jimlad=(double *)calloc(arrLength+2,sizeof(double)))){
      fprintf(stderr,"error in binary results allocation.\n");
      exit(1);
    }

    compLeng[2*Lplace]=encodeRefl(ratPoint,numb,Lplace,jimlad,arrLength);

    if(!(matlad=(float *)calloc(marrLength+1,sizeof(float)))){
      fprintf(stderr,"error in binary material results allocation.\n");
      exit(1);
    }
    compLeng[2*Lplace+1]=encodeMaterial(ratPoint,numb,Lplace,matlad,marrLength);

    /*now work out the compression map*/
    if(Lplace>0){
      coords[Lplace*3]=(int)(coords[3*(Lplace-1)+2]+(int)sizeof(float));
    }else if(Lplace==0){
      coords[Lplace*3]=(int)(ratPoint[numb].headerLength*sizeof(int)+ratPoint[numb].header2Length*sizeof(double)+\
        ratPoint->NZscans*ratPoint->NAscans*3*sizeof(int));
    }
    coords[3*Lplace+1]=(int)(coords[3*Lplace]+compLeng[2*Lplace]*(int)sizeof(double));
    coords[3*Lplace+2]=(int)(coords[3*Lplace+1]+compLeng[2*Lplace+1]*(int)sizeof(float));

    /*write the data to the new file*/
    if(dimage->byteord){   /*if the computer is little endian swap bytes before writting*/
      jimlad=doSwap(jimlad,compLeng[2*Lplace]);
      matlad=floSwap(matlad,compLeng[2*Lplace+1]);
    }
    if(fseek(dopoo,coords[3*Lplace],SEEK_SET)){fprintf(stderr,"fseek error for deconvolved refl %d to %d\n",Lplace,coords[3*Lplace]);exit(1);}
    if(fwrite(&(jimlad[0]),sizeof(double),compLeng[2*Lplace],dopoo)!=compLeng[2*Lplace]){
      fprintf(stderr,"Lidar data not written %d\n",Lplace);
      exit(1);
    }
    if(fseek(dopoo,coords[3*Lplace+1],SEEK_SET)){fprintf(stderr,"fseek error for matlad %d to %d\n",Lplace,coords[3*Lplace+1]);exit(1);}
    if(fwrite(&(matlad[0]),sizeof(float),compLeng[2*Lplace+1],dopoo)!=compLeng[2*Lplace+1]){
      fprintf(stderr,"Material data not written %d\n",Lplace);
      exit(1);
    }

    ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    TIDY(jimlad);
    TIDY(matlad);
  }/*beam step*/

  /*then write out the headers and maps to end*/
  if(dimage->byteord)coords=intSwap(coords,3*tScans);
  if(fseek(dopoo,(long int)(ratPoint[numb].headerLength*sizeof(int)+ratPoint[numb].header2Length*sizeof(double)),SEEK_SET)){
    fprintf(stderr,"fseek error for compression header\n");exit(1);}
  if(fwrite(&(coords[0]),sizeof(int),3*tScans,dopoo)!=3*tScans){
    fprintf(stderr,"error writting compression map\n");
    exit(1);
  }
  if(fseek(dopoo,(long int)(ratPoint[numb].headerLength*sizeof(int)),SEEK_SET)){fprintf(stderr,"fseek error for double header\n");exit(1);}
  if(fwrite(&(header2[0]),sizeof(double),ratPoint[numb].header2Length,dopoo)!=ratPoint[numb].header2Length){
    fprintf(stderr,"Data double header not written\n");
    exit(1);
  }
  if(fseek(dopoo,0,SEEK_SET)){fprintf(stderr,"fseek error for int header\n");exit(1);}
  if(fwrite(&(header1[0]),sizeof(int),ratPoint[numb].headerLength,dopoo)!=ratPoint[numb].headerLength){
    fprintf(stderr,"Data int header not written\n");
    exit(1);
  }

  TIDY(header1);
  TIDY(header2);
  TIDY(coords);

  if(dopoo){
    fclose(dopoo);
    dopoo=NULL;
  }
  return;
}/*saveNewWave*/

/*That's the end of the analysis, apart from the voxel and convolution bits*/
/*#########################################################################*/
