#define _ARARAT_
#define _MAIN_
#include "prat.h"
#ifndef i386
#include <unistd.h>
#endif
#ifdef DEBUGGER_
#include <mcheck.h>
#endif
extern	void	write_hips_image(),reset_local_z_to_local_max_height(),read_default_materials();
extern WavebandBag *globalWavebandbag;
extern FlagBag *globalFlagbag;
extern FILE **globalDatafile;
PixelStats globalPixelStats;

int globalArgc=0;
char **globalArgv=NULL; 
FILE	*write_data_buffer;

void 	signal_interrupt(sig)
int	sig;
{
/*
*	allow kill -10 to clean kill
*	writing out data in buffer before exiting
*/
#ifndef __alpha
  if(sig==SIGUSR1){
    fprintf(stderr,"drat:\tsignal interrupt %d for process %d received forcing clean exit\n",sig,getpid());
    /* write out current processing to write_data_buffer */
    writeInfo(&globalPixelStats,globalDatafile,globalFlagbag,globalMaterialbag,globalWavebandbag);
    exit(sig);
  }else if(sig==SIGUSR2){
    fprintf(stderr,"drat:\tsignal interrupt %d for process %d received forcing dump of results so far\n",sig,getpid());
    writeInfo(&globalPixelStats,globalDatafile,globalFlagbag,globalMaterialbag,globalWavebandbag);
  }
#endif
  return;
}

triplet	sort_contents_of_bbox();
void	*calloc(),init_header(),update_header(),mmap_read_hips_image(),get_skymap_wavelengths(),initialise_wavefront_storage(),parse_prat_wavefront_data(),read_spectral_file(),precalculate_camera_characteristics(),update_desc(),scan_image();
int	mmap_write_hips_image_no_free(),sscan_int(),sscan_double(),atoi();

/*
 *
 */
void	useage()
{
  fprintf(stderr,"drat [-help] [-verbose] [-skymap skymap.hips] [-random random_seed] [-a min_no_of_rays_per_pixel max_pixel_variance]\n[-o generic_op_image_filename] [-blocksize blocksize] [-sun sun_x sun_y sun_z] \n[-m rtd [-direct direct_solar_intensity] [-filename data_filename] data_filename\n");
  return;
}

/*
 *	group name storage area
 */

void	initialise_group_storage(group_Ptr,no_of_groups,max_len)
     Group	*group_Ptr;
     int	no_of_groups,max_len;
{
  int	i;
  
  if((group_Ptr->group_bboxes = (BBox **)calloc(no_of_groups,sizeof(BBox *)))==0)error1("drat:\terror in BBox allocation");
  if((group_Ptr->group_names = (char **)calloc(no_of_groups,sizeof(char *)))==0)error1("drat:\terror in char * allocation");
  for(i=0;i<no_of_groups;i++)
    if(((group_Ptr->group_names)[i] = (char *)calloc(max_len,sizeof(char)))==0)error1("drat:\terror in char allocation");
  
  group_Ptr->no_of_groups=0;
  return;
  
}

/*
 *	storage area
 */

int	initialise_contents_storage(contents,no_of_bboxes,no_of_triangles,no_of_beziers,no_of_spheres,no_of_cylinders,no_of_clones,no_of_dems)
     Contents	*contents;
     int	no_of_bboxes,no_of_triangles,no_of_beziers,no_of_spheres,no_of_cylinders,no_of_clones,no_of_dems;
{
  if(no_of_bboxes!=0)if((contents->bbox 	= (BBox *)calloc(no_of_bboxes,sizeof(BBox)))==0)
    error1("drat: error in contents box memory storage 1");
  if(no_of_triangles!=0)if((contents->triangle	= (Facet *)calloc(no_of_triangles,sizeof(Facet)))==0)error1("drat: error in contents box memory storage 2");
  if(no_of_beziers!=0)if((contents->bezier	= (Bezier4 *)calloc(no_of_beziers,sizeof(Bezier4)))==0)error1("drat: error in contents box memory storage 3");
  if(no_of_spheres!=0)if((contents->sphere	= (Sphere *)calloc(no_of_spheres,sizeof(Sphere)))==0)error1("drat: error in contents box memory storage 4");
  if(no_of_cylinders!=0)if((contents->cylinder	= (Cylinder *)calloc(no_of_cylinders,sizeof(Cylinder)))==0)error1("drat: error in contents box memory storage 5");
  if(no_of_clones!=0)if((contents->clone	= (Clones *)calloc(no_of_clones,sizeof(Clones)))==0)error1("drat: error in contents box memory storage 5");
  if(no_of_dems!=0)if((contents->dem	= (Dem *)calloc(no_of_dems,sizeof(Dem)))==0)error1("drat: error in contents box memory storage 6");
  contents->no_of_bboxes=0;
  contents->no_of_triangles=0;
  contents->no_of_beziers=0;
  contents->no_of_spheres=0;
  contents->no_of_cylinders=0;	
  contents->no_of_clones=0;	
  contents->no_of_dems=0;	
  return(1);
}

int setMaterialUseage(FlagBag *flagbag,MaterialBag *materialbag,char *name)
{
  FILE	*openFileForWrite();
  if(!(materialbag->material_list->useage=(MaterialUseage *)calloc(1,sizeof(MaterialUseage)))){
    fprintf(stderr,"error in core allocation\n");
    exit(0);
  }
  if(!(materialbag->material_list->useage->filename=(char *)calloc(strlen(name)+1+strlen(".materialUse"),sizeof(char)))){
    fprintf(stderr,"error in core allocation\n");
    exit(0);
  }
				/* copy filename */
  sprintf(materialbag->material_list->useage->filename,"%s.materialUse",name);

  if(!(materialbag->material_list->useage->materials=(char **)calloc(PRAT_MAX_MATERIALS,sizeof(char *)))){
    fprintf(stderr,"error in core allocation\n");
    exit(0);
  }
  if(!(materialbag->material_list->useage->names=(char **)calloc(PRAT_MAX_MATERIALS,sizeof(char *)))){
    fprintf(stderr,"error in core allocation\n");
    exit(0);
  }
  return(1);
}

int sortIllumination(int *no_of_sun_wavelength_samples,FlagBag *flagbag,IlluminationBag *illumination,char *filename){
  char buffer[1024];
  FILE *fp=NULL;
  double fdum,fdum2,sunValue;
  int i,expand_filename();
  triplet *sunDirection,normalise(triplet);

  /* need to pass an allocated string */
  if(!filename)return(0);

  sunDirection=illumination->sun_Ptr;
  *sunDirection=normalise(*sunDirection);
  if(!strlen(filename)){
    sunValue = 1./MAX(0.00000001,sunDirection->z);
    if(illumination->sky_flag)*no_of_sun_wavelength_samples=illumination->sky_data_Ptr->hd.num_frame;
    else *no_of_sun_wavelength_samples=1;

    for(i=0;i<*no_of_sun_wavelength_samples;i++){
      illumination->sun_magnitude[i]=sunValue;
    }
    strcpy(illumination->direct_file,"default solar illumination");
  }else{
    if(strcpy(illumination->direct_file,filename)==0)error1("parser:\terror in specifying -direct option");
    flagbag->direct_flag=1;
    if(!expand_filename(&(illumination->direct_file),"DIRECT_ILLUMINATION",FALSE))error2("drat:\terror opening direct irradaince file",illumination->direct_file);
    if((fp=fopen(illumination->direct_file,"r"))==NULL)error2("parser:\terror in specifying -direct option - cannot open file",illumination->direct_file);
    *no_of_sun_wavelength_samples=0;
    while( (fgets(buffer,1024-1,fp))!=NULL){
      if((sscanf(buffer,"%lf %lf",&fdum,&fdum2))==2){
	illumination->sun_magnitude[*no_of_sun_wavelength_samples]=fdum2/3.14159265358979323846;
	/*
	 *	direct irradiance values scaled by PI
	 *	so that op DN is RADIANCE
	 */
	(*no_of_sun_wavelength_samples)++;
      }
    }
    if(illumination->sky_flag) /*  error check */
      if(*no_of_sun_wavelength_samples != illumination->sky_data_Ptr->hd.num_frame)error1("parser:\tinconsistent no of wavelength samples between -skymap data and -direct data\n\t\t(the number of wavelength samples in the direct illumination file must equal the number of frames in the skymap data)");
  }
  return(1);
}

/*
 *	command line parser
 */
int	checkargs(argc,argv,flagbag,wavebandbag,materialbag,image_characteristics,illumination)
     int     argc;
     char    **argv;
     IlluminationBag	*illumination;
     FlagBag	*flagbag;
     WavebandBag *wavebandbag;
     MaterialBag *materialbag;
     Image_characteristics	*image_characteristics;
     
{
  int	to_flag=0,i,filespec=0,quit=0,no_of_sun_wavelength_samples=0;
  char	err[100],sky_imagemap[1000],**Ptr,*p;
  int	expand_filename();

  p=&sky_imagemap[0];
  Ptr=&p;
  /*
   *	defaults
   */
  materialbag->samples->max_diffuse_sampling_depth=0;
  materialbag->samples->no_of_diffuse_reflectance_samples=0;
  materialbag->samples->no_of_diffuse_transmittance_samples=0;
  materialbag->samples->no_of_shadow_rays=1;
  materialbag->samples->no_of_time_bins=0;
  materialbag->samples->startBinCount=0;
  materialbag->samples->endBinCount=0;
  materialbag->samples->binStep=0;
  
  *illumination->sun_Ptr=vector_copy2(0.0,0.0,1.0);
  /* need this for core dumper */
  globalRestart = flagbag->restart_Ptr;
  if(!(flagbag->restart_Ptr=(Restart *)calloc(1,sizeof(Restart))))error1("checkargs:\terror allocating Restart data");
  flagbag->restart_Ptr->restart=0;
  flagbag->restart_Ptr->from=0;
  flagbag->restart_Ptr->to=576;
  flagbag->restart_Ptr->from_sample=0;
  flagbag->restart_Ptr->to_sample=768;
  flagbag->restart_Ptr->recover=0;
  flagbag->ray_length_tol=RAY_LENGTH_TOL;
  flagbag->parallel=0;
  flagbag->matUseage=0;
  flagbag->orthographic=0;
  flagbag->vertexStore=512;
  flagbag->blackSky=0; 
  flagbag->photonSpread=1;
  flagbag->lidar=0;
  flagbag->sz=-1e20;
  init_header(&(image_characteristics->hd),"drat - Lewis' Plant RAy Tracer"," ",1,"today",576,768,32,0,PFFLOAT," ");
  update_header(&(image_characteristics->hd),argc,argv);
  
  strcpy(wavebandbag->op_image_file,"drat");
  wavebandbag->sensor_wavebands->no_of_wavebands=0;
  flagbag->pixel_variance->no_of_rays_per_pixel=1;
  flagbag->pixel_variance->variance=0.0;
  flagbag->explicit_lambertian=0;
  flagbag->max_ray_depth=1;
  flagbag->inverse_direction=0;
  flagbag->PhaseFunction=0;
  flagbag->ellipse=0;
  flagbag->cameraFile=NULL;
  flagbag->distanceTol=0.;
  flagbag->angleTol=0.;
  flagbag->sizeTol=0.;
  flagbag->float_flag=1;
  flagbag->rtRatio=1.0;

  for(i=1;i<argc;i++)
    if(*argv[i]=='-'){
      switch(*(argv[i]+1)){
      case 'i':	/* invert primary ray direction */
	if(strcmp(argv[i],"-inverse")==0){
	  flagbag->inverse_direction=1;
	}
	break;
      case 'e':	
	if(strcmp(argv[i],"-ellipse")==0)flagbag->ellipse=1;
	break;
      case 'm':	/* max rtd */
	  sscan_int(argv,argc,&i,(int)FATAL,&(flagbag->max_ray_depth));
	  materialbag->samples->max_diffuse_sampling_depth=MAX(flagbag->max_ray_depth,materialbag->samples->max_diffuse_sampling_depth);
	break;
      case 'l':
	flagbag->local=1;break;
      case 'a':
	sscan_int(argv,argc,&i,(int)FATAL,&(flagbag->pixel_variance->no_of_rays_per_pixel));
	sscan_double(argv,argc,&i,(int)NON_FATAL,&(flagbag->pixel_variance->variance));
	break;
      case 'r':
	if(strcmp(argv[i],"-ranging")==0 || strcmp(argv[i],"-rangeVertical")==0){
	  /* [-ranging start end dBin] */
	  if(strcmp(argv[i],"-ranging")==0)
	    flagbag->lidar=1;
	  else
	    flagbag->lidar=0;	    
	  materialbag->samples->startBinCount=atof(argv[++i]);
	  materialbag->samples->endBinCount=atof(argv[++i]);
	  materialbag->samples->binStep=atof(argv[++i]);
	  if(materialbag->samples->startBinCount>materialbag->samples->endBinCount||materialbag->samples->binStep<=0.0){
	    fprintf(stderr,"illegal parameters (%f %f %f) for option -ranging: [-ranging start end dBin]\n", materialbag->samples->startBinCount, materialbag->samples->endBinCount, materialbag->samples->binStep);
	    exit(1);
	  }
	  materialbag->samples->no_of_time_bins=2+(int)(0.5+(materialbag->samples->endBinCount-materialbag->samples->startBinCount)/materialbag->samples->binStep);
	  materialbag->samples->endBinCount=materialbag->samples->startBinCount + (materialbag->samples->no_of_time_bins-2)*materialbag->samples->binStep;
	}else if(strcmp(argv[i],"-rtRatio")==0){
	  flagbag->rtRatio=atof(argv[++i]);
	}else if(strcmp(argv[i],"-recover")==0){
	  flagbag->restart_Ptr->recover=1;
	}else if(strcmp(argv[i],"-restart")==0){
	  flagbag->restart_Ptr->restart=1;
	}else sscan_int(argv,argc,&i,(int)FATAL,&(flagbag->random_seed));
	break;
      case 'o':
	if(strcmp(argv[i],"-orthographic")==0)flagbag->orthographic=1;
	else if(strcmp(argv[i],"-objectTol")==0){
	  flagbag->angleTol=atof(argv[++i]);
	  flagbag->distanceTol=atof(argv[++i]);
	  flagbag->sizeTol=atof(argv[++i]);
	}else 
	  strcpy(wavebandbag->op_image_file,argv[++i]);break;
      case 'b':
	if(strncmp(argv[i],"-black",3)==0){
		flagbag->blackSky=1;
	}else{
		if(!sscan_int(argv,argc,&i,(int)NON_FATAL,&(flagbag->blocksize)))error1("parser:\terror in specifying -blocksize option");
		/* sometime get an error if blocksize not multiples of 3 */
		flagbag->blocksize-=(flagbag->blocksize)%6;
	}
	break;
      case 's':		 		/* image size */
	if(strcmp(argv[i],"-s")==0){
	  sscan_int(argv,argc,&i,FATAL,&(image_characteristics->hd.rows));
	  sscan_int(argv,argc,&i,NON_FATAL,&(image_characteristics->hd.cols));
	  if(!to_flag){
	    flagbag->restart_Ptr->to=image_characteristics->hd.rows;
	    flagbag->restart_Ptr->to_sample=image_characteristics->hd.cols;
	  }
	}else if(strcmp(argv[i],"-sz")==0){
	  flagbag->sz=atof(argv[++i]);
	}else if(strcmp(argv[i],"-storeVertex")==0){
	  flagbag->vertexStore=atoi(argv[++i]);
	}else if(strcmp(argv[i],"-sun")==0){
	  sscan_double(argv,argc,&i,FATAL,&(illumination->sun_Ptr->x));
	  sscan_double(argv,argc,&i,FATAL,&(illumination->sun_Ptr->y));
	  sscan_double(argv,argc,&i,FATAL,&(illumination->sun_Ptr->z));
	  *(illumination->sun_Ptr)=normalise(*(illumination->sun_Ptr));
	  if(flagbag->verbose)fprintf(stderr,"sun:\t%f %f %f\n",illumination->sun_Ptr->x,illumination->sun_Ptr->y,illumination->sun_Ptr->z);
	}else if(strncmp(argv[i],"-sensor_wavebands",3)==0){
	  while( !quit && *argv[i+1] != '-' && i != (argc-2)){
	    if(i==argc-2)quit=1;
	    wavebandbag->sensor_filenames[wavebandbag->sensor_wavebands->no_of_wavebands]=c_allocate(2000);
	    strcpy(wavebandbag->sensor_filenames[(wavebandbag->sensor_wavebands->no_of_wavebands)],argv[++i]);
	    if(!expand_filename(&(wavebandbag->sensor_filenames[(wavebandbag->sensor_wavebands->no_of_wavebands)]),"RSRLIB",FALSE))error2("drat:\terror opening sensor relative spectral response file",wavebandbag->sensor_filenames[(wavebandbag->sensor_wavebands->no_of_wavebands)]);
	    (wavebandbag->sensor_wavebands->no_of_wavebands)++;
	  }
	  wavebandbag->rsr_flag=1;
	}else if(strcmp(argv[i],"-sun_fov")==0){
	  sscan_double(argv,argc,&i,FATAL,&(illumination->sun_fov));
	  illumination->sun_fov = (illumination->sun_fov)/3.14159265358979323846;	/* normalise for d_sun_elevation */
	}else if(strcmp(argv[i],"-skymap")==0){
	  if(strcpy(sky_imagemap,argv[++i])==0){
	    useage();
	    error1("error in specifying -skymap option");
	  }
	  illumination->sky_flag=1;
	  if(!expand_filename(Ptr,"SKY_ILLUMINATION",FALSE))error2("drat:\terror opening skymap file",sky_imagemap);
	  mmap_read_hips_image(sky_imagemap,&(illumination->sky_data_Ptr->hd),&(illumination->sky_data_Ptr->data));
	  get_skymap_wavelengths(0,&(illumination->sky_data_Ptr->hd),(wavebandbag->lambda_min_Ptr),(wavebandbag->lambda_width_Ptr));
	  if(flagbag->direct_flag) /*  error check */
	    if(no_of_sun_wavelength_samples != illumination->sky_data_Ptr->hd.num_frame)error1("parser:\tinconsistent no of wavelength samples between -skymap data and -direct data\n\t\t(the number of wavelength samples in the direct illumination file must equal the number of frames in the skymap data)");				
	}
	break;
      case 'd':	/* -direct */
	i+=sortIllumination(&no_of_sun_wavelength_samples,flagbag,illumination,argv[i+1]);
	break;
      case 't':				
	if(strcmp(argv[i],"-test_frame")==0){   /* write a single frame */
	  flagbag->image_op=0;
	  if(*(argv[i+1])=='-' || i==argc-1) flagbag->test_image_op=0;
	  else sscan_int(argv,argc,&i,FATAL,&(flagbag->test_image_op));
	  if(flagbag->test_image_op==0)flagbag->test_image_op= -1;
	}else if(strcmp(argv[i],"-to")==0){     /* to */
	  sscan_int(argv,argc,&i,FATAL,&(flagbag->restart_Ptr->to));
	  sscan_int(argv,argc,&i,NON_FATAL,&(flagbag->restart_Ptr->to_sample));to_flag=1;
	}else if(strcmp(argv[i],"-tolerance")==0){  /* ray length tol */
	  sscan_double(argv,argc,&i,FATAL,&(flagbag->ray_length_tol));
	}
	break;
      case 'h':				/* -help */
	useage();exit(1);break;
      case 'v':				/* verbose */
	flagbag->verbose=1;
	if(i+1<argc){
	  if(atoi(argv[i+1])!=0){flagbag->data_verbose=1;i++;}
	  if(argv[i+1][0]=='0')i++;
	}
	break;
      case 'n':				
	if(strcmp(argv[i],"-no_image")==0){     /* no image op */
	  flagbag->image_op=0;
	}else
	  flagbag->normal=1;               /* use normals */
	break;
      case 'f':				/* ip wavefront filename */ 
	if(strcmp(argv[i],"-filename")==0){
	  if(strcpy(wavebandbag->wavefront_file,argv[i+1])==0)
	    error1("parser:\terror in specifying -filemane option");
	  filespec=1;i++;
        }else if(strcmp(argv[i],"-forward")==0){   /* forward ray tracing */
	  flagbag->reverse=0;
	}else if(strcmp(argv[i],"-from")==0){   /* from */
	  sscan_int(argv,argc,&i,FATAL,&(flagbag->restart_Ptr->from));
	  sscan_int(argv,argc,&i,NON_FATAL,&(flagbag->restart_Ptr->from_sample));
	}else if(strcmp(argv[i],"-float")==0){  /* image format to float */
	  flagbag->float_flag=1;
	  image_characteristics->hd.pixel_format=PFFLOAT;
	  image_characteristics->hd.bits_per_pixel=32;
	}
	break;
      case 'c':   /* camera input file  - else on stdin */
	if(!(flagbag->cameraFile=(CameraFile *)calloc(1,sizeof(CameraFile)))){
	  fprintf(stderr,"prat parser:\terror in core allocation\n");
	  exit(0);
	}
	flagbag->cameraFile->name=argv[++i];
	if(!(flagbag->cameraFile->fp=fopen(flagbag->cameraFile->name,"r"))){
	  fprintf(stderr,"prat parser:\terror opening camera file %s\n",flagbag->cameraFile->name);
	  exit(0);
	}
	flagbag->cameraFile->image_characteristics=image_characteristics;
	break;
      case 'p':
        flagbag->photonSpread=atoi(argv[++i]);
        break;
      default:				/* fall through to error */
	sprintf(err,"parser:\tunrecognised option %s in parser",argv[i]);
	useage();
	error1(err);
      }
    }else{
      if(i!=argc-1){
	sprintf(err,"parser:\tunrecognised option %s in parser",argv[i]);
	error1(err);
      }
    }
  /* material useage */
    flagbag->matUseage=setMaterialUseage(flagbag,materialbag,wavebandbag->op_image_file);
  if(!filespec)
    if(strcpy(wavebandbag->wavefront_file,argv[argc-1])==0)
      error1("parser:\terror in specifying input file as final command-line option");
  
  if(!expand_filename(&(wavebandbag->wavefront_file),"ARARAT_OBJECT",TRUE))error2("drat:\terror opening drat wavefront format object file",wavebandbag->wavefront_file);
  if(flagbag->vertexStore){
    flagbag->blocksize=flagbag->vertexStore+1;
  }
  if(flagbag->verbose){
    fprintf(stderr,"%s:\n",argv[0]);
    fprintf(stderr,"\tVERBOSE flag on (-v option)\n");
    fprintf(stderr,"\tinput file %s specified\n",wavebandbag->wavefront_file);
  }
  if(!strlen(illumination->direct_file))
    sortIllumination(&no_of_sun_wavelength_samples,flagbag,illumination,illumination->direct_file);
  return(1);
}

void	initialise_reflectance_storage(samples,no_of_wavebands)
     Samples	*samples;
     int	no_of_wavebands;
{
  if((samples->result=(double *)calloc(no_of_wavebands*samples->no_of_time_bins,sizeof(double)))==0)error1("initialise_reflectance_storage:\terror allocating memory");
  if((samples->wavelength=(int *)calloc(no_of_wavebands,sizeof(int)))==0)error1("initialise_reflectance_storage:\terror allocating memory");
  return;
}

/*
 *	initialise data files
 */

FILE	**initialise_data_files(flagbag,image_characteristics,wavebandbag,op_image_file,lidar)
     FlagBag	*flagbag;
     WavebandBag *wavebandbag;
     Image_characteristics *image_characteristics;
     char	*op_image_file;int lidar;
{
  FILE	**datafp;
  char	dataname[1000];
  
  /* should change this for the ranging option at some point - would be nice to
  ** get an image 
  */
  if(flagbag->test_image_op)image_characteristics->hd.num_frame=1;
  else if(flagbag->image_op)image_characteristics->hd.num_frame=wavebandbag->sensor_wavebands->no_of_wavebands;

  if(lidar)(image_characteristics->hd.num_frame)++;
 
  if(!flagbag->reverse)image_characteristics->hd.num_frame*=2; 

  datafp=(FILE **)v_allocate(5,sizeof(FILE *));
  
  if(!flagbag->parallel){
    sprintf(dataname,"%s.data",op_image_file);
    if(!flagbag->restart_Ptr->recover){
      if( (datafp[0] = fopen(dataname,"w"))==NULL){
	fprintf(stderr,"drat:\terror opening file %s\n",dataname);
	exit(-1);
      }
    }
    sprintf(dataname,"%s.direct_contributions",op_image_file);
    if( (datafp[1] = fopen(dataname,"w"))==NULL){
      fprintf(stderr,"drat:\terror opening file %s\n",dataname);
      exit(-1);
    }
    sprintf(dataname,"%s.diffuse_contributions",op_image_file);
    if( (datafp[2] = fopen(dataname,"w"))==NULL){
      fprintf(stderr,"drat:\terror opening file %s\n",dataname);
      exit(-1);
    }
    sprintf(dataname,"%s.directIrrad_contributions",op_image_file);
    if( (datafp[3] = fopen(dataname,"w"))==NULL){
      fprintf(stderr,"drat:\terror opening file %s\n",dataname);
      exit(-1);
    }
    sprintf(dataname,"%s.diffuseIrrad_contributions",op_image_file);
    if( (datafp[4] = fopen(dataname,"w"))==NULL){
      fprintf(stderr,"drat:\terror opening file %s\n",dataname);
      exit(-1);
    }
  }else{
    datafp[0]=datafp[1]=datafp[2]=datafp[3]=datafp[4]=stdout;
  }
  write_data_buffer=datafp[0];
  
  return(datafp);
}

/*
 *	initialise data flags
 */

void	initialise_flagbag(flagbag,restart,pixel_variance)
     FlagBag	*flagbag;
     Restart	*restart;
     PixelVarianceLimits	*pixel_variance;
{
  flagbag->reverse=1;
  flagbag->nGFunctionSamples=16;
  flagbag->restart_Ptr= restart;
  flagbag->verbose=0;
  flagbag->test_image_op=0;
  flagbag->fixed_wavelength=0;
  flagbag->explicit_lambertian=0;
  flagbag->local=0;
  flagbag->normal=0;
  flagbag->image_op=1;
  flagbag->data_verbose=0;
  flagbag->float_flag=0;
  flagbag->direct_flag=0;
  flagbag->blocksize=3336;
  flagbag->random_seed=1;
  flagbag->joint_probability=0;
  flagbag->pixel_variance= pixel_variance;
  flagbag->local_height=0;
  
  return;
}

void	initialise_flagbag_storage(flagbag,materialbag,sensor_wavebands)
     FlagBag	*flagbag;
     MaterialBag *materialbag;
     Sensor_Wavebands	*sensor_wavebands;
{
  return;
}

void	initialise_materialbag_storage(flagbag,materialbag,wavebandbag)
     FlagBag	*flagbag;
     MaterialBag *materialbag;
     WavebandBag *wavebandbag;
{
  return;
}



/*
 *	initialise skybag
 */

void	initialise_skybag(skybag,sky_data,sun_magnitude,solar,direct_file)
     IlluminationBag	*skybag;
     Image_characteristics *sky_data;
     char *direct_file;
     triplet *solar;
     double	*sun_magnitude;
{
  skybag->sky_data_Ptr=sky_data;
  skybag->sun_magnitude= sun_magnitude;
  skybag->sky_black=1;
  skybag->sun_Ptr= solar;
  skybag->sky_flag=0;
  skybag->sun_fov=0.0;
  skybag->theta_sun=0.0;
  skybag->phi_gap=0.0;
  skybag->direct_file= direct_file;
  
  return;
}

/*
 *	initialise waveband bag
 */

void	initialise_wavebandbag(wavebandbag,sensor_filenames,rsr_filename,op_image_file,wavefront_file,sensor_wavebands,lambda_min,lambda_width)
     WavebandBag 	*wavebandbag;
     char		**sensor_filenames;
     char		*wavefront_file;
     double		*lambda_min;
     double		*lambda_width;
     char		*op_image_file;
     char		**rsr_filename;
     Sensor_Wavebands	*sensor_wavebands;
{
  *lambda_min=0.0;
  *lambda_width=1.0;
  
  wavebandbag->sensor_filenames=sensor_filenames;
  wavebandbag->wavefront_file= wavefront_file;
  wavebandbag->lambda_min_Ptr= lambda_min;
  wavebandbag->lambda_width_Ptr= lambda_width;
  wavebandbag->op_image_file=op_image_file;
  wavebandbag->rsr_filename=rsr_filename;
  wavebandbag->rsr_flag=0;
  wavebandbag->theta_gap=1;
  wavebandbag->sensor_wavebands= sensor_wavebands;
  
  return;
}

void	initialise_materialbag(materialbag,material_table,material_list,samples)
     MaterialBag *materialbag;
     Material_table	*material_table;
     Material_List	*material_list;
     Samples		*samples;
{
  material_list->useage=NULL;
  materialbag->materials= material_table;
  materialbag->material_list= material_list;
  materialbag->samples= samples;
  
  return;
}

void	initialise_image_characteristics(image_characteristics,op_image_file)
     Image_characteristics	*image_characteristics;
     char			*op_image_file;
{
  image_characteristics->imagename=op_image_file;
  
  return;
}

/*
 *	main
 */
#ifdef PFAT 
/* prat globals */
extern MaterialBag *Materialbag;
extern FlagBag *Flagbag;
extern BBox *Bbox;
extern Ray *Rray;
extern ObjectList *Oobjectlist;
/* end of prat globals */

int pratMain(argc,argv,sunPosition,format)
     triplet *sunPosition;
     int *format;
     
#else /* PFAT */
     
     int	main(argc,argv)
     
#endif  /* PFAT */
     int     argc;
     char    **argv;
{
#ifdef PFAT
  static Ray	ray;
  static ObjectList        objectlist;
#endif /* PFAT */
  IlluminationBag	skybag;
  static FlagBag	flagbag;
  WavebandBag wavebandbag;
  static MaterialBag materialbag;
  static Image_characteristics sky_data,image_characteristics;
  Camera_ip camera_ip;
  Camera_op camera_op;
  Group	group;
  char 	*getenv(),op_image_file[2000],imagename[2000],direct_file[2000];
  int 	lidar,max_number_of_groups,timer,i,mmap_flag,get_mmap_flag(),read_camera_description(),level;
  triplet solar;
  double	sun_magnitude[1024];
  VeRtIcEs vertices,normals,locals;
  FILE	*fp,**datafp,*openFileForRead();
  static BBox	bbox;
  char		**material_names;
  Matrix4		load_identity_matrix4(),m_inv_reverse,m_inverse_fwd;
  PixelVarianceLimits	pixel_variance;
  Restart		restart;
  char		*rsr_filename[50],wavefront_file[2000],*sensor_filenames[50];
  double		lambda_min,lambda_width;
  Material_table	*material_table,*current_mtl;
  Material_List	material_list;
  Samples		samples;
  Sensor_Wavebands	sensor_wavebands;
  BigBag		bigbag;
  void pload_identity_matrix4(); 
#ifdef DEBUGGER_
 mtrace(); 
#endif
  max_number_of_groups=50000;
  timer=0;
  level=0;
  material_table=NULL;
  current_mtl=NULL;
  PRAT_MAX_MATERIALS=DEFAULT_PRAT_MAX_MATERIALS;
  strcpy(direct_file,"");
  /*
   *	signal interrupt on kill -8 pid
   */
  signal(SIGUSR1,signal_interrupt);
  signal(SIGUSR2,signal_interrupt);
  signal(SIGSTOP,signal_interrupt);
  globalArgc=argc;globalArgv=argv;
  
  if(getenv("PRAT_MAX_MATERIALS")){
    PRAT_MAX_MATERIALS = atoi(getenv("PRAT_MAX_MATERIALS"));
  }
  if(!(material_names=(char **)calloc(PRAT_MAX_MATERIALS,sizeof(char *))) || !(material_table=(Material_table *)calloc(PRAT_MAX_MATERIALS,sizeof(Material_table)))){
    fprintf(stderr,"error in core allocation\n");
    exit(1);
  }
  /*
   *	various initialisations
   */
  bigbag.flagbag= &flagbag;
  
  initialise_flagbag(bigbag.flagbag,&restart,&pixel_variance);
  
  initialise_skybag(&skybag,&sky_data,&sun_magnitude[0],&solar,&direct_file[0]);
  
  initialise_wavebandbag(&wavebandbag,sensor_filenames,rsr_filename,&op_image_file[0],wavefront_file,&sensor_wavebands,&lambda_min,&lambda_width);
  
  mmap_flag=get_mmap_flag(OP);
  
  /*
   *	setup
   */
  current_mtl= material_table;
  pload_identity_matrix4(&m_inv_reverse);
  pload_identity_matrix4(&m_inverse_fwd);
  if(getenv("MAX_GROUPS")){
    max_number_of_groups=MAX(max_number_of_groups,atoi(getenv("MAX_GROUPS")));
  }
  
  initialise_group_storage(&group,max_number_of_groups,100); 
  for(i=0;i<PRAT_MAX_MATERIALS;i++)material_names[i]=c_allocate(200);
  if(!(material_list.material=(Standard_Material_List *)calloc(PRAT_MAX_MATERIALS,sizeof(Standard_Material_List)))){
    fprintf(stderr,"error in core allocation\n");
    exit(0);
  }
  materialbag.material_names=material_names;
  initialise_materialbag(&materialbag,current_mtl,&material_list,&samples);
  
  initialise_image_characteristics(&image_characteristics,op_image_file);
  
  /*
   *	command line parser
   */
  
  checkargs(argc,argv,&flagbag,&wavebandbag,&materialbag,&image_characteristics,&skybag); 
#ifdef PFAT
  format[0]=image_characteristics.hd.rows;
  format[1]=image_characteristics.hd.cols;
  Materialbag= &materialbag;
#endif /* PFAT */
  
  /*
   *	only need to do this for the serial version
   */
  
  /*
   *	seed random number generator
   */
  seed_randomise(flagbag.random_seed);
  
  /*
   *	read sensor rsr data   
   */
  if ( ! (flagbag.joint_probability) )read_spectral_file(flagbag.verbose,wavebandbag.sensor_filenames,wavebandbag.rsr_flag,&flagbag.fixed_wavelength,wavebandbag.sensor_wavebands );
  /*
   *	initialise storage area(s)
   */
  initialise_wavefront_storage(flagbag.blocksize,&vertices);
  /*	initialise_materialbag_storage(&flagbag,&materialbag,&wavebandbag);*/
  initialise_flagbag_storage(&flagbag,&materialbag,&sensor_wavebands);
  
  /*
   *	open object file
   */
  fp=openFileForRead(&(wavebandbag.wavefront_file),"ARARAT_OBJECT",FATAL);
  
  /*
   *	print verbose header for object format
   */
  if(flagbag.data_verbose)fprintf(stderr,"{GLOBAL");
  
  /*
   *	read default materials
   */
  read_default_materials(flagbag.data_verbose,materialbag.material_list,materialbag.materials,material_names);
  
  /*
   *	read object file
   */
  parse_prat_wavefront_data(flagbag.data_verbose,&bbox,&bbox,fp,&level,&group,&current_mtl,&vertices,&normals,&locals,flagbag.normal,flagbag.local,&m_inv_reverse,&m_inverse_fwd,material_names,materialbag.material_list,materialbag.materials,flagbag.vertexStore,flagbag.angleTol,flagbag.distanceTol,flagbag.sizeTol);
  
#ifndef PFAT
  initialise_materialbag_storage(&flagbag,&materialbag,&wavebandbag);
  /*
   *	initialise reflectance data storage
   */
  initialise_reflectance_storage(materialbag.samples,wavebandbag.sensor_wavebands->no_of_wavebands);
  
  if(materialbag.samples->binStep)lidar=TRUE;else lidar=FALSE;
  /*
   *	initialise data output files   
   */
  datafp=initialise_data_files(&flagbag,&image_characteristics,&wavebandbag,op_image_file,lidar);
  
  /*
   *	read camera information
   */
  while(read_camera_description(flagbag.cameraFile,&(flagbag.local_height),&camera_ip,flagbag.restart_Ptr)){
    if(flagbag.local_height)reset_local_z_to_local_max_height(materialbag,&(camera_ip.look_at),&(camera_ip.camera_position),&flagbag,&bbox);
    /*
     *	calculate camera parameters
     */
    precalculate_camera_characteristics(flagbag.verbose,&camera_ip,&camera_op,&image_characteristics,&flagbag);
    
    sprintf(imagename,"%s.frame%06d",image_characteristics.imagename,camera_ip.frame);
    
    /*
     *	open set of frames -> mmap
     */
    if(flagbag.image_op||flagbag.test_image_op)
      image_characteristics.fd=mmap_write_hips_image_no_free(imagename,&image_characteristics.hd,&(image_characteristics.data),argc,argv,flagbag.restart_Ptr->restart);
    
    bigbag.illumination= &skybag; 
    bigbag.flagbag= &flagbag;
    bigbag.wavebandbag= &wavebandbag;
    bigbag.materialbag= &materialbag;
    
    /*
     *	process image frame
     */

    scan_image(argc,argv,timer,&bigbag,&bbox,&camera_op,&image_characteristics,datafp);

    timer++;
    
    if((flagbag.image_op||flagbag.test_image_op)&& !mmap_flag)write_hips_image(imagename,&image_characteristics);
    /*
     *	close image frame
     */
    if(flagbag.image_op||flagbag.test_image_op)
#ifdef MMAP
      /*			if(mmap_flag)
				if(munmap(image_characteristics.fd))error1("drat:\terror closing (un-memory-mapping) current image frame");
				else*/
#endif /*  MMAP*/
      if(close(image_characteristics.fd))error1("drat:\terror closing current image frame");
    flagbag.local_height=0;
    if(flagbag.parallel){
      fprintf(stdout,"pixel_processed_ok ");fflush(stdout);
    }
  }
#else /* PFAT */
  Flagbag=&flagbag;
  Bbox=&bbox;
  Rray=&ray;
  Oobjectlist=&objectlist;
  *sunPosition=solar;
#endif /* PFAT */
  return(0);
}
