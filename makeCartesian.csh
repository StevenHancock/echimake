#!/bin/csh -f


############################
# Convert echiMake output  #
# from polar top Cartesian #
# April 2016               #
############################


set bin="$HOME/src/echiMake"
@ makeName=1

while ($#argv>0)
  switch("$argv[1]")

  case -input
    set input="$argv[2]"
  shift argv;shift argv
  breaksw

  case -output
    set output="$argv[2]"
    @ makeName=0
  shift argv;shift argv
  breaksw

  #A help file#
  case -help
    echo " "
    echo "-input name;      input filename"
    echo "-output name;     output filename"
    echo " "
    exit;

  default:
    echo "Unknown argument $argv[1]"
    echo "Who knows"
    echo "Type echi.rat -help"
    exit;

  endsw
end

if( $makeName )then
  set output="$input:r.cart"
endif

gawk -f $bin/makeCartesian.awk < $input > $output
echo "Written to $output"

