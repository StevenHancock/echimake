/*######################################################################################*/
/*#This is a program for reading the output of starat, an echidna simulating ray tracer#*/
/*#It is split into three. This, analMake.c and voxMake.c. This part comtains the      #*/
/*#controlling functions and bits.                                                     #*/
/*# 25th October 2006              Steven Hancock, Geography, UCL                      #*/
/*######################################################################################*/
#include "echiMake.h"
#include "tools.c"

int main(int argc,char **argv)
{
  RatControl *ratPoint=NULL;
  RatControl *setUpcontrol(RatImage *);
  RatControl *forceNull(RatControl *,RatImage *);
  RatControl *postSetUp(RatControl *,RatImage *);
  RatImage *dimage=NULL,*sortHemisphere(RatControl *,int,RatImage *);
  RatImage *imagePrepare(RatControl *,int,RatImage *,int,char **);
  RatImage *panorama(RatControl *,RatImage *,int,int,char **);
  RatImage *readCommands(int,char **,RatImage *);
  RatImage *createVoxels(RatControl *,RatImage *);
  RatImage *analyseVoxels(RatControl *,RatImage *);
  RatImage *checkData(RatControl *,int,RatImage *);
  RatImage *selectBands(RatImage *,RatControl *);
  void asciiWrite(RatControl *,int,RatImage *);
  void totalFlux(RatControl *,int,RatImage *);
  void heightProfile(RatControl *,int,RatImage *);
  void scatterFraction(RatImage *,RatControl *,int);
  char byteOrder();
  void tidyUpArrays(RatImage *,RatControl *);
  void setPulseLength(RatImage *);
  void saveDeconvolved(RatImage *,RatControl *,int);
  void decomposeGauss(RatImage *,RatControl *,int);
  void printdataHeader(RatControl *,RatImage *,int);
  void analyseBeam(RatImage *,RatControl *,int);
  void beamStats(RatImage *,RatControl *);
  void modifyWaves(RatControl *,RatImage *,int);
  void divideBands(RatControl *,RatImage *,int);
  void spectralGround(RatControl *,RatImage *,int);
  void addGeiger(RatControl *,RatImage *,int);
  void trainGauss(RatControl *,RatImage *,int);
  void invertAscope(RatImage *,RatControl *);
  void phaseHeterogneity(RatImage *,RatControl *,int);
  void trueGap(RatImage *,RatControl *,int);
  void juppGap(RatImage *,RatControl *,int);
  int i=0;  /*loop variable*/

  /*allocate main structure*/
  if(!(dimage=(RatImage *)calloc(1,sizeof(RatImage)))){
    fprintf(stderr,"error in initial image allocation.\n");
    exit(1);
  }
/*#  if(sizeof(long int)!=4){
  #  printf("long integers are the wrong length on this system\n");
  #  exit(1);
  #}*/

  /*read the command line*/
  dimage=readCommands(argc,argv,dimage);

  ratPoint=setUpcontrol(dimage);

  dimage->byteord=byteOrder(dimage);
  if(dimage->input){
    for(i=0;i<dimage->scanN;i++){
      ratPoint=resultsArrange(dimage,ratPoint,i,-1,0);
    }
  }
  dimage=selectBands(dimage,ratPoint);
  ratPoint=forceNull(ratPoint,dimage);
  ratPoint=postSetUp(ratPoint,dimage);

  /*if(dimage->Plength>0)setPulseLength(dimage);*/

  if(dimage->teast){
    for(i=0;i<dimage->scanN;i++)if(dimage->data[i]){fclose(dimage->data[i]);dimage->data[i]=NULL;}
    return(0);
  }


  /*Now do whatever analysis you need to*/

  for(i=0;i<dimage->scanN;i++){

    if(dimage->checkD)dimage=checkData(ratPoint,i,dimage);

    if(dimage->printHeader)printdataHeader(ratPoint,dimage,i);
    
    if(dimage->addGeiger)addGeiger(ratPoint,dimage,i);

    /*if(dimage->waveMaths)modifyWaves(ratPoint,dimage,i);*/
    /*if(dimage->coarsen>1.0)modifyWaves(ratPoint,dimage,i);*/

    if(dimage->dividewave)divideBands(ratPoint,dimage,i);

    if(dimage->spectralGround2)spectralGround(ratPoint,dimage,i);

    /*if(dimage->nGauss)decomposeGauss(dimage,ratPoint,i);*/

    if(dimage->outdecon)saveDeconvolved(dimage,ratPoint,i);

    if(dimage->ascii)asciiWrite(ratPoint,i,dimage);

    if(dimage->totalFlux)totalFlux(ratPoint,i,dimage);

    if(dimage->Hres)heightProfile(ratPoint,i,dimage);

    if(dimage->scatFract)scatterFraction(dimage,ratPoint,i);

    if(dimage->GaussFit)trainGauss(ratPoint,dimage,i);

    if(dimage->Athresh>0&&!dimage->GaussFit)invertAscope(dimage,ratPoint);

    if(dimage->features)analyseBeam(dimage,ratPoint,i);

    if(dimage->mzoom||dimage->czoom||dimage->matzoom){
      dimage=imagePrepare(ratPoint,i,dimage,argc,argv);
      dimage=sortHemisphere(ratPoint,i,dimage);
    }

    if(dimage->pzoom)dimage=panorama(ratPoint,dimage,i,argc,argv);

    if(dimage->phaseHet)phaseHeterogneity(dimage,ratPoint,i);

    if(dimage->trueGap)trueGap(dimage,ratPoint,i);

    if(dimage->juppGap)juppGap(dimage,ratPoint,i);

    if(dimage->idealFile)break;  /*if one of the files is the ideal don't analyse*/
  }

  if(dimage->stats)beamStats(dimage,ratPoint);

  if(dimage->voxRes)dimage=createVoxels(ratPoint,dimage);  /*create and analyse voxels*/

  if(dimage->Vfile)dimage=analyseVoxels(ratPoint,dimage); /*if the voxels already exist*/

  /*end of analysis*/
  tidyUpArrays(dimage,ratPoint);

  return(0);
}
/*the end of MAIN*/
/*############################################################################*/



/*######################################################################################################*/
/*read the binary file into the arrays*/

RatControl *resultsArrange(RatImage *dimage,RatControl *ratPoint,int numb,int time,char mode)
{
  int i=0,j=0;
  int reflLength=0,matLength=0;
  double theta=0,thata=0;
  double *thetas=NULL,*thatas=NULL; /*for transferrring angles*/
  int *header1=NULL;
  char reflsize=0;  /*to deal with both floats and doubles*/
  FILE *data=NULL;
  uint64_t *compheader=NULL;  /*compression map*/
  double *jimlad=NULL;
  float *matlad=NULL;
  int mn=0,m=0; /*integers for uncompressing and coarsening*/
  char refl=0,material=0,angle=0;
  void stateIntent(RatImage *,RatControl *,int,int);
  void coarsenRange(RatImage *,RatControl *,int,int);
  void combineSplithead(RatImage *,RatControl *,int,int,char,char);
  void deconvolveFFT(RatControl *,RatImage *,int,int);
  void convolveLaplace(RatControl *,RatImage *,int,int);
  void convolveFFT(RatControl *,RatImage *,int,int,double);
  void maxDeconvolvedAmp(RatImage *,RatControl *,int,int);
  void addNoise(RatImage *,RatControl *,int,int);
  void atmosphericAttenuation(RatImage *,RatControl *,int,int);
  void solarIrradiance(RatImage *,RatControl *,int);
  void noiseEstimate(RatImage *,RatControl *,int,int);
  void removeNoise(RatImage *,RatControl *,int,int);
  void GeigerMode(RatImage *,RatControl *,int,int);
  void photonCount(RatImage *,RatControl *,int,int);
  void lowPassFilter(RatImage *,RatControl *,int,int);
  void readIntHeader(int *,FILE *,RatControl *,int,RatImage *);
  void readFloHeader(int,FILE *,RatControl *,int,RatImage *);
  int format=0;    /*data format*/
  RatControl *resultsAscii(RatImage *,RatControl *,int,int,char);

  if(numb>dimage->scanN){
    fprintf(stderr,"Trying to access a scan that isn't there. %d of %d\n",numb,dimage->scanN);
    exit(3);
  }
  if(time>ratPoint[numb].NZscans*ratPoint[numb].NAscans){
    fprintf(stderr,"Trying to access a beam that isn't there. %d of %d\n",time,ratPoint[numb].NZscans*ratPoint[numb].NAscans);
    exit(3);
  }
 
  data=dimage->data[numb];
  reflsize=sizeof(double);
  reflLength=(ratPoint[numb].bins*dimage->coarsen+ratPoint[numb].coarsenRemain)*\
	      ratPoint[numb].nBands*(ratPoint[numb].n+1)*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs);
  matLength=(ratPoint[numb].bins*dimage->coarsen+ratPoint[numb].coarsenRemain)*\
             ratPoint[numb].nMat*2*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs);
  
  if(dimage->asciiIn){
    ratPoint=resultsAscii(dimage,ratPoint,numb,time,0);
    return(ratPoint);
  }


  if(time>=0){

    if(mode==0){        /*read everything*/
      refl=1;
      material=1;
      angle=1;
    }else if(mode==1){  /*read refl and angles*/
      refl=1;
      material=0;
      angle=1;
    }else if(mode==2){  /*read only angles*/
      refl=0;
      material=0;
      angle=1;
    }else if(mode==3){  /*read material and angles*/
      refl=0;
      material=1;
      angle=1;
    }else if(mode==4){  /*read material and refl*/
      refl=1;
      material=1;
      angle=0;
    }else{              /*there has been a problem*/
      fprintf(stderr,"There is no mode %d\n",mode);
      exit(2);
    }
    if(dimage->voxRes==0)angle=0;    /*if we are not doing the voxels then all the angles have already been read*/
    else angle=0;     /*a hash to be going on with. It was a RAM saving step, but now we have more swap*/

    if(ratPoint[numb].ratRes[time].refl)refl=0;     /*make sure we really need to do this*/
    if(ratPoint[numb].ratRes[time].material)material=0;
    if(!angle&&!refl&&!material)return(ratPoint);

    if(angle&&(dimage->voxRes>0.0)){
      if(!(thetas=(double *)calloc(dimage->angles,sizeof(double)))){
        fprintf(stderr,"error in angle transfer allocation.\n");
        exit(1);
      }
      if(!(thatas=(double *)calloc(dimage->angles,sizeof(double)))){
        fprintf(stderr,"error in angle transfer allocation.\n");  
        exit(1);
      }
      for(i=0;i<dimage->angles;i++){
        thetas[i]=ratPoint[numb].theta[i];
        thatas[i]=ratPoint[numb].thata[i];
      }
      TIDY(ratPoint[numb].theta);
      TIDY(ratPoint[numb].thata);
      dimage->angles++;
      if(!(ratPoint[numb].theta=(double *)calloc(dimage->angles,sizeof(double)))){
        fprintf(stderr,"error in angle transfer allocation.\n");  
        exit(1);
      }
      if(!(ratPoint[numb].thata=(double *)calloc(dimage->angles,sizeof(double)))){
        fprintf(stderr,"error in angle transfer allocation.\n");
        exit(1);
      }
      for(i=0;i<dimage->angles-1;i++){
        ratPoint[numb].theta[i]=thetas[i];
        ratPoint[numb].thata[i]=thatas[i];
      }
      if(fseek(data,(long)ratPoint[numb].ratRes[time].coords[0],SEEK_SET)){\
        fprintf(stderr,"fseek error 1 %d %u\n",i,ratPoint[numb].ratRes[i].coords[0]);exit(1);}
      if(fread(&(theta),reflsize,1,data)!=1){fprintf(stderr,"error reading angle\n");exit(1);}
      if(dimage->byteord)theta=doOneSwap(theta);
      if(fseek(data,(long)(ratPoint[numb].ratRes[time].coords[0]+sizeof(double)),SEEK_SET)){\
        fprintf(stderr,"fseek error 2\n");exit(1);}
      if(fread(&(thata),reflsize,1,data)!=1){fprintf(stderr,"error reading angle\n");exit(1);}
      if(dimage->byteord)thata=doOneSwap(thata);
      ratPoint[numb].theta[dimage->angles-1]=theta;
      ratPoint[numb].thata[dimage->angles-1]=thata; 

      TIDY(thetas);
      TIDY(thatas);
    }  /*end of angle reading bit*/


    if(material){
      if(ratPoint[numb].nMat){  /*to make sure we're not trying to allocate nothing*/
        if(!(ratPoint[numb].ratRes[time].material=(float *)calloc(matLength+1,sizeof(float)))){
          fprintf(stderr,"error in material results allocation.\nAllocating %d\n",matLength+1);  /*we have added one to material size for uncompression*/
          exit(1);
        }
      }
      if(ratPoint[numb].ratRes[time].compLeng[1]){  /*to prevent allocation of empty bits*/
        if(!(matlad=(float *)calloc(ratPoint[numb].ratRes[time].compLeng[1],sizeof(float)))){
          fprintf(stderr,"error in material uncompressor allocation.\n");
          exit(1);
        }
        if(fseek(data,(long)ratPoint[numb].ratRes[time].coords[1],SEEK_SET)){
          fprintf(stderr,"fseek error 4\n");
	  exit(1);
	}
        if(fread(&(matlad[0]),sizeof(float),(size_t)(ratPoint[numb].ratRes[time].compLeng[1]),data)!=\
                                            (size_t)(ratPoint[numb].ratRes[time].compLeng[1])){
          fprintf(stderr,"error reading material data\n");exit(1);}
        if(dimage->byteord)matlad=floSwap(matlad,(uint64_t)ratPoint[numb].ratRes[time].compLeng[1]);
      }
    }

    if(refl){
	    
      if(!(ratPoint[numb].ratRes[time].refl=(double *)calloc(reflLength,sizeof(double)))){
        if(dimage->voxRes){                    /*if we run oput of RAM empty it and reread the data file*/
          ratPoint=clearAllArrays(ratPoint,dimage);
          ratPoint=resultsArrange(dimage,ratPoint,numb,time,mode);
        }else{
          fprintf(stderr,"error allocating the results %d.\n",time);
          exit(1);
        }
      }
      if(!(jimlad=(double *)calloc(ratPoint[numb].ratRes[time].compLeng[0],sizeof(double)))){
        if(dimage->voxRes){
          ratPoint=clearAllArrays(ratPoint,dimage);
          ratPoint=resultsArrange(dimage,ratPoint,numb,time,mode);
        }else{
          fprintf(stderr,"error in uncompressor allocation allocating %d.\n",ratPoint[numb].ratRes[time].compLeng[0]);
          exit(1);
        }
      }
      if(fseek(data,(long)ratPoint[numb].ratRes[time].coords[0],SEEK_SET)){
        fprintf(stderr,"fseek error 3, seeking %u\n",ratPoint[numb].ratRes[time].coords[0]);
        exit(1);
      }
      if(fread(&(jimlad[0]),sizeof(double),(size_t)(ratPoint[numb].ratRes[time].compLeng[0]),data)!=\
                                           (size_t)(ratPoint[numb].ratRes[time].compLeng[0])){
        if(fseek(data,(long)ratPoint[numb].ratRes[time].coords[0],SEEK_SET)){
          fprintf(stderr,"fseek error 3, seeking %u\n",ratPoint[numb].ratRes[time].coords[0]);exit(1);}
        fprintf(stderr,"error reading radiant flux data, read %d when trying  to read %d from %u\n",\
	  (int)fread(&(jimlad[0]),sizeof(double),(size_t)(ratPoint[numb].ratRes[time].compLeng[0]),data),\
	  ratPoint[numb].ratRes[time].compLeng[0],ratPoint[numb].ratRes[time].coords[0]);exit(1);}
      if(ratPoint[numb].ratRes[time].compLeng[0]){   /*byte swap if needed*/
        if(dimage->byteord)jimlad=doSwap(jimlad,(uint64_t)ratPoint[numb].ratRes[time].compLeng[0]);
      }/*byteswap test*/
    }
    if(ratPoint[numb].encoded){
      if(refl&&ratPoint[numb].ratRes[time].compLeng[0]){
        /*now uncompress to put data into ratRes*/
        mn=0;
        /*0 and 1 are the angles, which are uncompressed already*/
        for(i=2;i<ratPoint[numb].ratRes[time].compLeng[0];i++){
          if(jimlad[i]>=0.0){
            ratPoint[numb].ratRes[time].refl[(i-2)+mn]=jimlad[i];
          }else if(jimlad[i]<0){
            m=0;
            while(m<(int)(-1.0*jimlad[i])){
              ratPoint[numb].ratRes[time].refl[(i-2)+mn]=0;
              m++;
              if(m<(int)(-1.0*jimlad[i]))mn++;
            }
          }
        }
      }else if(!ratPoint[numb].ratRes[time].compLeng[0]){  /*fill with blank space*/
        for(i=0;i<reflLength;i++){
          ratPoint[numb].ratRes[time].refl[i]=0.0;
        }
      }
      if(material&&ratPoint[numb].ratRes[time].compLeng[1]){
        mn=0;
        for(i=0;i<ratPoint[numb].ratRes[time].compLeng[1];i++){
          if(matlad[i]>=0){
            if(dimage->badMat){  /*normalise materials for waveband multiplication*/
              ratPoint[numb].ratRes[time].material[i+mn]=matlad[i]/(float)ratPoint[numb].nBands;
            }else{   /*all data files have badly calibrated material info*/
              ratPoint[numb].ratRes[time].material[i+mn]=matlad[i];
            }
          }else if(matlad[i]<0){
            m=0;
            while(m<(int)(-1.0*matlad[i])){
              ratPoint[numb].ratRes[time].material[i+mn]=0.0;
              m++;
              if(m<(int)(-1.0*matlad[i]))mn++;
            }
          }
        } /*for ease we have temporarily put sky information into the material array*/
        ratPoint[numb].ratRes[time].sky=ratPoint[numb].ratRes[time].material[matLength];
        if(dimage->badMat)ratPoint[numb].ratRes[time].sky*=(float)ratPoint[numb].nBands;
      }else if(material&&!ratPoint[numb].ratRes[time].compLeng[1]){

        for(i=0;i<matLength;i++)ratPoint[numb].ratRes[time].material[i]=0.0;
      }

    }
    TIDY(jimlad);
    TIDY(matlad);

    if(refl){ /*modify the signal if wanted*/

      if(dimage->coarsen>1.0)coarsenRange(dimage,ratPoint,numb,time);

      /*recombine split sensor heads if desired*/
      if(dimage->withinSegs!=ratPoint[numb].withinSegs||dimage->withoutSegs!=ratPoint[numb].withoutSegs)\
        combineSplithead(dimage,ratPoint,numb,time,refl,material);

      /*modify the signal if desired*/
      if(dimage->noise!=0.0)addNoise(dimage,ratPoint,numb,time);
      if(dimage->trans)atmosphericAttenuation(dimage,ratPoint,numb,time);
      if(dimage->photon)photonCount(dimage,ratPoint,numb,time);
      if(dimage->clip!=0.0)lowPassFilter(dimage,ratPoint,numb,time);

      if(dimage->conlength>0.0)convolveFFT(ratPoint,dimage,numb,time,(double)dimage->conlength);
      if(dimage->stopGold)maxDeconvolvedAmp(dimage,ratPoint,numb,time);
      if(dimage->noisestats||dimage->denoise)noiseEstimate(dimage,ratPoint,numb,time);
      if(dimage->Geiger>0.0)GeigerMode(dimage,ratPoint,numb,time);
      if(dimage->denoise && dimage->noise>0.0){
        removeNoise(dimage,ratPoint,numb,time);
        if(dimage->noilength>0.0)convolveFFT(ratPoint,dimage,numb,time,(double)dimage->noilength);
      }
      if(dimage->Plength!=0||dimage->sdecon)deconvolveFFT(ratPoint,dimage,numb,time);
      else if(dimage->deconLaplace)convolveLaplace(ratPoint,dimage,numb,time);
      if(dimage->postlength>0.0)convolveFFT(ratPoint,dimage,numb,time,(double)dimage->postlength);
    }/*only modify signal if it has been read anew*/

  }else if(time<0){    /*########read header information###############*/
    readIntHeader(&format,data,ratPoint,numb,dimage);
    readFloHeader(format,data,ratPoint,numb,dimage);

    /*now read the compression map*/
    if(!(compheader=(uint64_t *)calloc((uint64_t)ratPoint[numb].NZscans*(uint64_t)ratPoint[numb].NAscans*3,sizeof(uint64_t)))){
      fprintf(stderr,"error in compression map array allocation.\n");
      exit(1);
    }
    if(fseek(data,(long)(ratPoint[numb].headerLength*sizeof(int)+ratPoint[numb].header2Length*sizeof(double)),SEEK_SET))\
       {fprintf(stderr,"fseek error bewoon.\n");exit(1);}
    if((fread(&(compheader[0]),sizeof(uint64_t),(size_t)(3*(uint64_t)ratPoint[numb].NZscans*(uint64_t)ratPoint[numb].NAscans),data))!=\
                                                (size_t)(3*(uint64_t)ratPoint[numb].NZscans*(uint64_t)ratPoint[numb].NAscans)){
      fprintf(stderr,"error reading the compression map.\n");
      exit(1);
    }
//for(i=0;i<3*ratPoint[numb].NZscans*ratPoint[numb].NAscans;i++)fprintf(stdout,"%d %d\n",i,compheader[i]);
    if(dimage->byteord)compheader=uint64Swap(compheader,3*(uint64_t)ratPoint[numb].NZscans*(uint64_t)ratPoint[numb].NAscans);
    for(i=0;i<ratPoint[numb].NZscans*ratPoint[numb].NAscans;i++){
      ratPoint[numb].ratRes[i].coords[0]=compheader[3*i];
      ratPoint[numb].ratRes[i].coords[1]=compheader[3*i+1];
      ratPoint[numb].ratRes[i].coords[2]=compheader[3*i+2];
//fprintf(stdout,"beam %d Map %d %d %d\n",i,(int)ratPoint[numb].ratRes[i].coords[0],(int)ratPoint[numb].ratRes[i].coords[1],(int)ratPoint[numb].ratRes[i].coords[2]);
      ratPoint[numb].ratRes[i].compLeng[0]=(int)((ratPoint[numb].ratRes[i].coords[1]-ratPoint[numb].ratRes[i].coords[0])/sizeof(double));
      if((ratPoint[numb].ratRes[i].coords[1]-ratPoint[numb].ratRes[i].coords[0])%sizeof(double)){
        fprintf(stderr,"Rounding issue in the refl length\n");
	fprintf(stderr," remainder %d for beam %d\n",(int)((ratPoint[numb].ratRes[i].coords[1]-\
	  ratPoint[numb].ratRes[i].coords[0])%sizeof(double)),i);
        exit(1);
      }
      ratPoint[numb].ratRes[i].compLeng[1]=(int)((ratPoint[numb].ratRes[i].coords[2]-ratPoint[numb].ratRes[i].coords[1])/sizeof(float));
//fprintf(stdout,"lengths %d %d %d\n",i,ratPoint[numb].ratRes[i].compLeng[0],ratPoint[numb].ratRes[i].compLeng[1]);
      if((ratPoint[numb].ratRes[i].coords[2]-ratPoint[numb].ratRes[i].coords[1])%sizeof(float)){
        fprintf(stderr,"Rounding issue in the material length\n");
        exit(1);
      }
      for(j=0;j<3;j++){
        if(ratPoint[numb].ratRes[i].coords[j]<((uint64_t)ratPoint[numb].headerLength*sizeof(int)+(uint64_t)ratPoint[numb].header2Length*sizeof(double)+\
          3*(uint64_t)ratPoint[numb].NAscans*(uint64_t)ratPoint[numb].NZscans*sizeof(int))){
          fprintf(stderr,"error, problem with your coords beam %d coord %d header size %d coordinate %u\n",i,j,\
            (int)(ratPoint[numb].headerLength*sizeof(int)+ratPoint[numb].header2Length*sizeof(double)+\
            3*ratPoint[numb].NAscans*ratPoint[numb].NZscans*sizeof(int)),\
            ratPoint[numb].ratRes[i].coords[j]);
          exit(2);
        }
      }
    }
    TIDY(compheader);

    if(!dimage->teast){
      if(dimage->intent)stateIntent(dimage,ratPoint,numb,format);

      TIDY(header1);

      /*if we are to coarsen the range resolution do it here*/
      if(dimage->coarsen>1.0){
        ratPoint[numb].coarsenRemain=ratPoint[numb].bins%dimage->coarsen;
        ratPoint[numb].bins=(int)(ratPoint[numb].bins/dimage->coarsen);       /*round down*/
        ratPoint[numb].bin_L*=(double)dimage->coarsen;
      }/*coarsen*/

  /*    if(!dimage->voxRes){  */  /*### this shouldbe taken out if the RAM saving bit is to be used*/
        TIDY(ratPoint[numb].theta);
        ratPoint[numb].theta=NULL;
        if(!(ratPoint[numb].theta=(double *)calloc(ratPoint[numb].NAscans*ratPoint[numb].NZscans,sizeof(double)))){
          fprintf(stderr,"error in data pointer allocation.\n");
          exit(1);
        }
        if(!(ratPoint[numb].thata=(double *)calloc(ratPoint[numb].NAscans*ratPoint[numb].NZscans,sizeof(double)))){
          fprintf(stderr,"error in data pointer allocation.\n");
          exit(1);
        }
        /*for now read in the zenith and aziumth angels for all the beams*/
        if(dimage->intent)printf("Reading angles from data file\n");
        for(i=0;i<ratPoint[numb].NZscans*ratPoint[numb].NAscans;i++){
          if(fseek(data,(long)ratPoint[numb].ratRes[i].coords[0],SEEK_SET)){\
            fprintf(stderr,"fseek error 1 %d %u\n",i,ratPoint[numb].ratRes[i].coords[0]);exit(1);}
          if(fread(&(theta),reflsize,1,data)!=1){fprintf(stderr,"error reading angle %d from %u\n",i,ratPoint[numb].ratRes[i].coords[0]);exit(1);}
          if(dimage->byteord)theta=doOneSwap(theta);
          if(fseek(data,(long)(ratPoint[numb].ratRes[i].coords[0]+sizeof(double)),SEEK_SET)){\
            fprintf(stderr,"fseek error 2\n");exit(1);}
          if(fread(&(thata),reflsize,1,data)!=1){fprintf(stderr,"error reading angle %d from %u\n",i,(ratPoint[numb].ratRes[i].coords[0]+sizeof(double)));exit(1);}
          if(dimage->byteord)thata=doOneSwap(thata);
          if(!dimage->checkD){  /*do not correct if we are checking for errors*/
            if(theta<=2*M_PI&&theta>=-2*M_PI)ratPoint[numb].theta[i]=theta;
            else{
              printf("Dodgy theta angle value %d is %f\n",i,theta);
              if(i>0)ratPoint[numb].theta[i]=ratPoint[numb].theta[i-1];
              else ratPoint[numb].theta[i]=0;
            }
            if(thata<=2*M_PI&&thata>=-2*M_PI)ratPoint[numb].thata[i]=thata;
            else{
              printf("Dodgy thata angle value %d is %f\n",i,thata);
              if(i>0)ratPoint[numb].thata[i]=ratPoint[numb].thata[i-1];
              else ratPoint[numb].thata[i]=0;
            }
          }else{
            ratPoint[numb].theta[i]=theta;
            ratPoint[numb].thata[i]=thata;
          }
        }
      /*}  */
    }/*is it a test*/

    ratPoint[numb].pulse=fFalloc(ratPoint[numb].pBands,"pulse",0);
    if(ratPoint[numb].EpLength[0]>0){ /*then read the pulse in*/
      ratPoint[numb].pulse[0]=falloc(ratPoint[numb].EpLength[0],"pulse",0);
      if(fseek(data,(long)(ratPoint[numb].ratRes[ratPoint[numb].NAscans*ratPoint[numb].NZscans-1].coords[2]+\
	  sizeof(double)),SEEK_SET)){\
        fprintf(stderr,"fseek error to %d\n",ratPoint[numb].ratRes[ratPoint[numb].NAscans*\
	  ratPoint[numb].NZscans-1].coords[2]);exit(1);}
      if(fread(&(ratPoint[numb].pulse[0][0]),sizeof(float),ratPoint[numb].EpLength[0],data)!=ratPoint[numb].EpLength[0]){
        fprintf(stderr,"error reading angle %d",i);
        exit(1);
      }
      if(dimage->byteord)ratPoint[numb].pulse[0]=floSwap(&(ratPoint[numb].pulse[0][0]),(uint64_t)ratPoint[numb].EpLength[0]);

    }else if((dimage->Plength<0)&&ratPoint[numb].EpLength==0){
      fprintf(stderr,"The file contains no pulse shape, can't use \"-deconvolve -ve\"\n");
      exit(1);
    }/*pulse reading*/

  }  /*header reading bit*/
  return(ratPoint);
}/*resultsArrange*/

/*###################################################################################*/
/*resate the message of intent*/

void stateIntent(RatImage *dimage,RatControl *ratPoint,int numb,int format)
{
  int i=0;

  if(!ratPoint[numb].Plength){
    fprintf(stderr,"Oh no\n");
    exit(1);
  }
  if(!ratPoint[numb].EpLength){
    fprintf(stderr,"Oh no\n");
    exit(1);
  }

  /*##############message of intent#########################################################*/
  /*Print out the control variables to make sure we are reading what we thought we were*/
  printf("\nTo make sure this is the correct data file I will read the parameters of the scan.\n");
  printf("The data label is %d\n",format);
  printf("Rays were traced from %g %g %g\nOver the angles %g to %g zenith \nand %g to %g azimuth\
  \nWith %d wavebands of wavelength",ratPoint[numb].from[0],ratPoint[numb].from[1],ratPoint[numb].from[2],\
  ratPoint[numb].iZen*180/M_PI,ratPoint[numb].fZen*180/M_PI,ratPoint[numb].iAz*180/M_PI,\
  ratPoint[numb].fAz*180/M_PI,ratPoint[numb].nBands);
  for(i=0;i<ratPoint[numb].nBands;i++) fprintf(stdout," %g",ratPoint[numb].wavelength[i]);
  printf("\nWith a beam divergence of %g and FOV of %g\nThere will be %d beams in groups of %d and %d\
  \nThe lidar is measuring two way distance from %g to %g in steps of %g\n",ratPoint[numb].Ldiv*180/M_PI,\
  ratPoint[numb].div*180/M_PI,ratPoint[numb].NZscans*ratPoint[numb].NAscans,ratPoint[numb].NZscans,\
  ratPoint[numb].NAscans,ratPoint[numb].min_R,ratPoint[numb].max_R,ratPoint[numb].bin_L);
  printf("The sensor was split into %d within and %d without segments\n",ratPoint[numb].withinSegs,\
  ratPoint[numb].withoutSegs);
  printf("Resampled to %d and %d segments\n",dimage->withinSegs,dimage->withoutSegs);
  printf("Each ray was limited to %d interactions\n",ratPoint[numb].n);
  printf("The object had %d materials\n",ratPoint[numb].nMat);
  if(ratPoint[numb].Plength[0]>0)printf("The simulation used a pulse of length %g sampled at %g\n",\
  ratPoint[numb].Plength[0],ratPoint[numb].Pres);
  if(dimage->Plength>0.0)printf("Deconvolving a pulse of length %f at a resolution of %f\n",dimage->Plength,dimage->Pres);
  if(dimage->coarsen>1)printf("Coarsening from resolution %f to %f\n",ratPoint[numb].bin_L,\
    ratPoint[numb].bin_L/(double)dimage->coarsen); 
  fprintf(stdout,"Squint angle of %g\n",ratPoint[numb].squint*180.0/M_PI);
  printf("\n");
  /*############intent has been restated#####################################################*/

  return;
}/*stateIntent*/

/*#####################################################################*/
/*Coarsen range resolution*/

void coarsenRange(RatImage *dimage,RatControl *ratPoint,int numb,int time)
{
  int i=0,j=0,m=0,n=0;
  int oplace=0,place=0;
  double *jimlad=NULL;
  float *matlad=NULL;
  int reflLength=0,matLength=0;

  reflLength=ratPoint[numb].bins*ratPoint[numb].nBands*(ratPoint[numb].n+1)*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs);
  matLength=ratPoint[numb].bins*ratPoint[numb].nMat*2*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs);

  jimlad=dalloc(reflLength,"coarsen transfer",time);
  matlad=falloc(matLength,"coarsen material transfer",time);
  for(i=0;i<ratPoint[numb].bins;i++){
    for(j=0;j<dimage->coarsen;j++){
      for(m=0;m<ratPoint[numb].nBands;m++){
        for(n=0;n<ratPoint[numb].n;n++){
          place=((i*dimage->coarsen+j)*ratPoint[numb].nBands+m)*(ratPoint[numb].n+1)+n;
          oplace=i*ratPoint[numb].nBands*(ratPoint[numb].n+1)+m*(ratPoint[numb].n+1)+n;

          if((place>=reflLength*dimage->coarsen)||(place<0)){
            fprintf(stderr,"balls\n");
            exit(1);
          }
          if((oplace>=reflLength)||(oplace<0)){
            fprintf(stderr,"balls 2\n");
            exit(1);
          }
          jimlad[oplace]+=ratPoint[numb].ratRes[time].refl[place];
        }
      }
      for(m=0;m<ratPoint[numb].nMat;m++){
        place=i*ratPoint->nMat*2+m*2;
        if((place+1>=matLength)||(place<0)){
          fprintf(stderr,"noooooooooo %d %d\n",place,ratPoint[numb].bins*ratPoint[numb].nMat*\
	    2*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs));
          fprintf(stderr,"bins %d nMat %d segs %d\n",ratPoint[numb].bins,\
	    ratPoint[numb].nMat,(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs));
          exit(1);
        }
        oplace=(i*dimage->coarsen+j)*ratPoint->nMat*2+m*2;
        if((oplace+1>matLength*dimage->coarsen)||(oplace<0)){
          fprintf(stderr,"noooooooooo 2\n");
          exit(1);
        }
        matlad[place]+=ratPoint[numb].ratRes[time].material[oplace];
        matlad[place+1]+=ratPoint[numb].ratRes[time].material[oplace+1];
      }
    }/*coarsening loop*/
  }/*bin loop*/

  ratPoint=clearArrays(ratPoint,numb,time,dimage);
  ratPoint[numb].ratRes[time].refl=jimlad;
  jimlad=NULL;
  ratPoint[numb].ratRes[time].material=matlad;
  matlad=NULL;
  
  return;
}/*coarsenRange*/

/*#######################################################*/
/*run length encode an the refl array*/

int encodeRefl(RatControl *ratPoint,int numb,int Lplace,double *jimlad,int arrLength)
{
  int j=0,jc=0,zeroN=0;
  char zeroI=0;

  jimlad[0]=ratPoint[numb].theta[Lplace];
  jimlad[1]=ratPoint[numb].thata[Lplace];
  jc=2;
  zeroN=0;
  zeroI=0;
  for(j=0;j<arrLength;j++){
    if(ratPoint[numb].ratRes[Lplace].refl[j]==0){
      zeroN++;  /*output -1*zeroes instead of a line of zeroes*/
      if(!zeroI)zeroI=1;
    }else if(ratPoint[numb].ratRes[Lplace].refl[j]>0){
      if(zeroI){
        jimlad[jc]=(double)(-1*zeroN);
        zeroI=0;  /*reset zero counter*/
        zeroN=0;
        jc++;
      }
      jimlad[jc]=ratPoint[numb].ratRes[Lplace].refl[j];
      jc++;
    }
  }
  if(zeroI){
    jimlad[jc]=(double)(-1*zeroN);
    jc++;
  }

  if(jc<0||jc>arrLength+2){
    fprintf(stderr,"What's going on with compressed length %d max compressed length %d\n",jc,arrLength+2);
    exit(1);
  }

  return(jc);
}/*encodeRefl*/

/*#######################################################*/
/*run length encode the material array*/

int encodeMaterial(RatControl *ratPoint,int numb,int Lplace,float *matlad,int marrLength)
{
  int j=0,jc=0,zeroN=0;
  char zeroI=0;

  for(j=0;j<marrLength;j++){
    if(ratPoint[numb].ratRes[Lplace].material[j]==0){
      zeroN++;  /*output -1*zeroes instead of a line of zeroes*/
      if(!zeroI)zeroI=1;
    }else if(ratPoint[numb].ratRes[Lplace].material[j]>0){
      if(zeroI){
        matlad[jc]=(float)(-1*zeroN);
        zeroI=0; /*reset zero counter*/
        zeroN=0;
        jc++;
      }
      matlad[jc]=ratPoint[numb].ratRes[Lplace].material[j];
      jc++;
    }
  }
  /*we must do an extra step for the sky*/
  if(ratPoint[numb].ratRes[Lplace].sky==0){
    zeroN++;
    if(!zeroI)zeroI=1;
  }else if(ratPoint[numb].ratRes[Lplace].sky>0){
    if(zeroI){
      matlad[jc]=(float)(-1*zeroN);
      jc++;
    }
    matlad[jc]=ratPoint[numb].ratRes[Lplace].sky;
    zeroN=0;
    zeroI=0;
    jc++;
  }
  if(zeroI){
    matlad[jc]=-1*zeroN;
    jc++;
  }
  return(jc);
}/*encodeMaterial*/

/*#######################################################*/
/*calculate reflectance array index*/

int findReflIndex(RatImage *dimage,RatControl *ratPoint,int numb,int bin,int band,int n)
{
  int place=0;

  /*check the position is in the array*/
  if(numb<0||numb>=dimage->scanN){fprintf(stderr,"bad scan %d of %d\n",numb,dimage->scanN);exit(1);}
  if(bin<0||bin>=ratPoint[numb].bins){
    fprintf(stderr,"bad bin %d of %d\n",bin,ratPoint[numb].bins);
    exit(1);
  }
  if(dimage->band[numb][band]<0||dimage->band[numb][band]>=ratPoint[numb].nBands){
    fprintf(stderr,"bad band %d of %d\n",dimage->band[numb][band],ratPoint[numb].nBands);
    exit(1);
  }
  if(n<0||n>ratPoint[numb].n){fprintf(stderr,"bad scatter depth %d of %d\n",n,ratPoint[numb].n);exit(1);}

  place=(bin*ratPoint[numb].nBands+dimage->band[numb][band])*(ratPoint[numb].n+1)+n;

  if(place<0||place>ratPoint[numb].bins*ratPoint[numb].nBands*(ratPoint[numb].n+1)){
    fprintf(stderr,"bad reflectance place %d of %d\n",place,\
      ratPoint[numb].bins*ratPoint[numb].nBands*(ratPoint[numb].n+1));
    exit(1);
  }

  return(place);
} /*findReflIndex*/

/*#######################################################*/
/*caluclate material array index*/

int findMaterialIndex(RatImage *dimage,RatControl *ratPoint,int numb,int bin,int mat,int d,int seg)
{
  int place=0;

  if(numb<0||numb>=dimage->scanN){fprintf(stderr,"bad scan %d of %d\n",numb,dimage->scanN);exit(1);}
  if(bin<0||bin>=ratPoint[numb].bins){
    fprintf(stderr,"bad bin for mat %d of %d\n",bin,ratPoint[numb].bins);
    exit(1);
  }
  if(mat<0||mat>=ratPoint[numb].nMat){
    fprintf(stderr,"bad material number %d of %d\n",mat,ratPoint[numb].nMat);
    exit(1);
  }
  if(d<0||d>=2){fprintf(stderr,"Bad direct/indirect indicator, %d of %d\n",d,2);exit(1);}
  if(seg<0||seg>=ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs){\
    fprintf(stderr,"bad seg, %d of %d\n",seg,ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs);exit(1);}

  place=seg*ratPoint[numb].bins*ratPoint[numb].nMat*2+bin*ratPoint[numb].nMat*2+mat*2+d;
  if(place<0||place>ratPoint[numb].bins*ratPoint[numb].nMat*2*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs)){
    fprintf(stderr,"bad material place %d of %d\n",place,ratPoint[numb].bins*ratPoint[numb].nMat*2*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs));
    exit(1);
  }

  return(place);
}/*findMaterialIndex*/

/*#######################################################*/
/*select bands to use*/

RatImage *selectBands(RatImage *dimage,RatControl *ratPoint)
{
  int numb=0,i=0,j=0;
  float *wavelength=NULL;
  void solarIrradiance(RatImage *,RatControl *,int);


  if(dimage->scanN<1){  /*if we are not analysing scan data*/
    dimage->nBands=0;
    return(dimage);
  }

  if(dimage->nBands<0){
    if(dimage->scanN>1){
      for(numb=1;numb<dimage->scanN;numb++){
        if(ratPoint[0].nBands!=ratPoint[numb].nBands){
          fprintf(stderr,"These files have different numbers of wavebands, you must use \"-bands\"\n");
          exit(1);
        }
      }
    }
    dimage->nBands=ratPoint[0].nBands;
  }

  if(dimage->nBands!=ratPoint[0].nBands){
    if(!(wavelength=(float *)calloc(dimage->nBands,sizeof(float)))){
      fprintf(stderr,"error in wavelength transfer allocation.\n");
      exit(1);
    }
    for(i=0;i<dimage->nBands;i++){
      wavelength[i]=(float)dimage->band[0][i];
    }
  }
  if(dimage->band){
    TIDY(dimage->band[0]);
    free(dimage->band);
    dimage->band=NULL;
  }
  if(!(dimage->band=(int **)calloc(dimage->scanN,sizeof(int *)))){
    fprintf(stderr,"error in waveband allocation.\n");
    exit(1);
  }

  for(numb=0;numb<dimage->scanN;numb++){
    if(!(dimage->band[numb]=(int *)calloc(dimage->nBands,sizeof(int)))){
      fprintf(stderr,"error in waveband allocation.\n");
      exit(1);
    }
    if(dimage->nBands==ratPoint[numb].nBands){
      for(i=0;i<dimage->nBands;i++)dimage->band[numb][i]=i;
    }else if(dimage->nBands<ratPoint[numb].nBands){
      for(j=0;j<ratPoint[numb].nBands;j++){
        for(i=0;i<dimage->nBands;i++){
          if(ratPoint[numb].wavelength[j]-wavelength[i]<=0.001&&ratPoint[numb].wavelength[j]-wavelength[i]>=-0.001){
            dimage->band[numb][i]=j;
          }
        }
      }
    }else{
      fprintf(stderr,"This file contains less bands than you want to use %d and %d\n",\
        ratPoint[numb].nBands,dimage->nBands);
      exit(1);
    }
    /*read in solar irradiance spectrum if noised*/
    if(dimage->noise>0.0)solarIrradiance(dimage,ratPoint,numb);
  }

  /*printf("for %d scans and %d bands\n",dimage->scanN,dimage->nBands);
  #printf("band 1 %f\n",ratPoint[0].wavelength[dimage->band[0][0]]);*/

  TIDY(wavelength);
  return(dimage);
}/*selectBands*/

/*########################################################*/
/*recombine split sensor heads*/

void combineSplithead(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace,char refl,char material)
{
  int i=0,j=0,k=0;
  int reflLength=0,materialLength=0;
  double *jimlad=NULL;
  float *matlad=NULL;

  if(refl){
    reflLength=ratPoint[numb].bins*ratPoint[numb].nBands*(ratPoint[numb].n+1);
    if(!(jimlad=(double *)calloc((ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs)*reflLength,sizeof(double)))){
      fprintf(stderr,"error in refl head combiner allocation.\n");
      exit(1);
    }
    for(i=0;i<(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs)*reflLength;i++){
      jimlad[i]=ratPoint[numb].ratRes[Lplace].refl[i];
    }
    TIDY(ratPoint[numb].ratRes[Lplace].refl);
    if(!(ratPoint[numb].ratRes[Lplace].refl=(double *)calloc((dimage->withinSegs+dimage->withoutSegs)*reflLength,sizeof(double)))){
      fprintf(stderr,"error in refl head combiner allocation 2.\n");
      exit(1);
    }
    for(k=0;k<reflLength;k++){
      for(i=0;i<ratPoint[numb].withinSegs;i++){
        j=(int)(i*dimage->withinSegs/ratPoint[numb].withinSegs);
        ratPoint[numb].ratRes[Lplace].refl[j*reflLength+k]+=jimlad[i*reflLength+k];
      }
      for(i=ratPoint[numb].withinSegs;i<ratPoint[numb].withoutSegs+ratPoint[numb].withinSegs;i++){
        j=(int)(dimage->withinSegs+i*dimage->withoutSegs/ratPoint[numb].withoutSegs);
        ratPoint[numb].ratRes[Lplace].refl[j*reflLength+k]+=jimlad[i*reflLength+k];
      }
    }
    TIDY(jimlad);
  }/*end of refl combination*/

  if(material){
    materialLength=ratPoint[numb].bins*ratPoint[numb].nMat*2;
    if(!(matlad=(float *)calloc((ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs)*materialLength,sizeof(float)))){
      fprintf(stderr,"error in refl head combiner allocation.\n");
      exit(1);
    }

    for(i=0;i<(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs)*materialLength;i++){
      matlad[i]=ratPoint[numb].ratRes[Lplace].material[i];
    }
    TIDY(ratPoint[numb].ratRes[Lplace].material);
    if(!(ratPoint[numb].ratRes[Lplace].material=(float *)calloc((dimage->withinSegs+dimage->withoutSegs)*materialLength,sizeof(float)))){
      fprintf(stderr,"error in refl head combiner allocation 2.\n");
      exit(1);
    }
    for(k=0;k<materialLength;k++){
      for(i=0;i<ratPoint[numb].withinSegs;i++){
        j=(int)(i*dimage->withinSegs/ratPoint[numb].withinSegs);
        ratPoint[numb].ratRes[Lplace].material[j*reflLength+k]+=matlad[i*reflLength+k];
      }
      for(i=ratPoint[numb].withinSegs;i<ratPoint[numb].withoutSegs+ratPoint[numb].withinSegs;i++){
        j=(int)(dimage->withinSegs+i*dimage->withoutSegs/ratPoint[numb].withoutSegs);
        ratPoint[numb].ratRes[Lplace].material[j*reflLength+k]+=matlad[i*reflLength+k];
      }
    }
    TIDY(matlad);
  }/*end of material combination*/
  return;
}/*combineSplithead*/

/*########################################################*/
/*to return the data label of the wrong architecture*/
int swappedBytes(int numb,int nBytes)
{
  int i=0;
  union{
    char buff[4];
    int x;
  }ibuff,obuff;

  if(sizeof(int)!=4){
    fprintf(stderr,"Oh dear oh dear oh dear. INtegers are not 4 bytes long\n");
    exit(1);
  }

  ibuff.x=numb;
  for(i=0;i<nBytes;i++){
    obuff.buff[i]=ibuff.buff[nBytes-1-i];
  }
  return(obuff.x);
}

/*############################################################################################################*/
/*clear out the results arrays*/

RatControl *clearArrays(RatControl *ratPoint,int numb,int Lplace,RatImage *dimage)
{

  if(!dimage->memorymap)return(ratPoint);  /*don't clear until the end*/

  TIDY(ratPoint[numb].ratRes[Lplace].refl);
  TIDY(ratPoint[numb].ratRes[Lplace].material);
/*  for(numb=0;numb<1;numb++){ ##################An over the top hash!########
#    for(Lplace=0;Lplace<ratPoint[numb].NZscans*ratPoint[numb].NAscans;Lplace++){
#      if(ratPoint[numb].ratRes[Lplace].refl){
#printf("Something is not null!\n");
#        free(ratPoint[numb].ratRes[Lplace].refl);
#        ratPoint[numb].ratRes[Lplace].refl=NULL;
#      }
#      if(ratPoint[numb].ratRes[Lplace].material){
#printf("Something is not null!\n");
#        free(ratPoint[numb].ratRes[Lplace].material);
#        ratPoint[numb].ratRes[Lplace].material=NULL;
#      }
#    }
#  }  
############*REMOVE BEFORE USE#####################*/
  return(ratPoint);
}

/*#################################################################################################*/
/*to clear all the arrays of we run out of RAM*/

RatControl *clearAllArrays(RatControl *ratPoint,RatImage *dimage)
{
  int numb=0,Lplace=0;

  for(numb=0;numb<dimage->scanN;numb++){
    for(Lplace=0;Lplace<ratPoint[numb].NAscans*ratPoint[numb].NZscans;Lplace++){
      ratPoint=clearArrays(ratPoint,numb,Lplace,dimage);
    }
  }

  return(ratPoint);
}

/*#############################################################################################*/
/*force all pointers to NULL at start*/

RatControl *forceNull(RatControl *ratPoint,RatImage *dimage)
{
  int numb=0,Lplace=0;

  for(numb=0;numb<dimage->scanN;numb++){
    for(Lplace=0;Lplace<ratPoint[numb].NAscans*ratPoint[numb].NZscans;Lplace++){
      ratPoint[numb].ratRes[Lplace].refl=NULL;
      ratPoint[numb].ratRes[Lplace].material=NULL;
    }
  }

  return(ratPoint);
}

/*########################################################################################*/
/*clear out all the arrays at the program's end*/

void tidyUpArrays(RatImage *dimage,RatControl *ratPoint)
{
  int i=0,j=0;

  /*end of analysis*/
  if(dimage->data){
    ratPoint=clearAllArrays(ratPoint,dimage);
    for(i=0;i<dimage->scanN;i++){
      TIDY(ratPoint[i].wavelength);
      dimage->memorymap=1;
      TIDY(ratPoint[i].ratRes);
      TIDY(ratPoint[i].theta);
      TIDY(ratPoint[i].thata);
      if(dimage->data[i]!=NULL){
        fclose(dimage->data[i]);  /*close input files*/
        dimage->data[i]=NULL;
      }
      TTIDY((void **)ratPoint[i].pulse,ratPoint[i].pBands);
      TIDY(ratPoint[i].EpLength);
      if(ratPoint[i].maxAmp){
        for(j=0;j<ratPoint[i].NAscans*ratPoint[i].NZscans;j++)TIDY(ratPoint[i].maxAmp[j]);
        free(ratPoint[i].maxAmp);
        ratPoint[i].maxAmp=NULL;
      }
    }
  } /*make sure ratPoint is allocated*/

  TIDY(dimage->output);
  TIDY(dimage->acAe);
  TIDY(dimage->voxRat);
  TIDY(dimage->data);
  TIDY(dimage->crop);
  TIDY(dimage->meanoise);
  TIDY(dimage->noithresh);
  TIDY(dimage->stdevnoise);
  TIDY(dimage->mLUT);
  TIDY(dimage->thetaLUT);
  if(dimage->phaseLUT){
    for(i=0;i<dimage->NthetaLUT;i++)TIDY(dimage->phaseLUT[i]);
    free(dimage->phaseLUT);
    dimage->phaseLUT=NULL;
  }
  TIDY(ratPoint);
  if(dimage->band){
    for(i=0;i<dimage->scanN;i++){
      TIDY(dimage->band[i]);
    }
    free(dimage->band);
    dimage->band=NULL;
  }
  TIDY(dimage->trans);
  if(dimage->solarI){
    for(i=0;i<dimage->scanN;i++)TIDY(dimage->solarI[i]);
    free(dimage->solarI);
    dimage->solarI=NULL;
  }
  /*free all estimates*/
  if(dimage->backnoise){
    for(i=0;i<dimage->nBands;i++)TIDY(dimage->backnoise[i]);
    free(dimage->backnoise);
    dimage->backnoise=NULL;
  }

  if(dimage){
    free(dimage);
    dimage=NULL;
  }

  return;
}/*tidyUpArrays*/


/*#################################################################################################*/
/*reset the angle arrays*/

/*RatImage *clearAngles(RatImage *dimage)
#{
#  if(dimage->theta){
#    free(dimage->theta);
#    dimage->theta=NULL;
#  }
#  if(dimage->thata){
#    free(dimage->thata);
#    dimage->thata=NULL;
#  }
#  dimage->angles=0;
#  return(dimage);
#}
*/

/*############################################################################################################*/
/*read the command line*/

RatImage *readCommands(int argc,char **argv,RatImage *dimage)
{
  int i=0,j=0;
  int x=0,y=0,z=0; /*coordinates for file names*/
  char namen[200];
  void usage(char **);
  void checkArguments(int,int,int,char *);

  /*defaults*/
  dimage->memorymap=1;    /*empty arrays after every calculation. Unsuitable for random noise*/
  dimage->intent=1;       /*print out the statement of intent*/
  dimage->ascii=0;
  dimage->asciiSave=0;    /*print out everything, even blank space*/
  dimage->asciiIn=0;      /*read binary data file*/
  dimage->totalFlux=0;
  dimage->stats=0;
  dimage->rawStats=0;
  dimage->jusAng=0;
  dimage->printHeader=0;
  dimage->mzoom=0;
  dimage->czoom=0;
  dimage->pzoom=0;
  dimage->matzoom=0;
  dimage->gain=1000;
  dimage->view=1;
  dimage->teast=0;
  dimage->scatFract=0;
  dimage->voxRes=0;
  dimage->linearVoxels=0;  /*use the more complex LAD model for invetrsion*/
  dimage->species=1;       /*broadleaf, non shadowing*/
  dimage->trueGap=0;       /*don't calculate the true gap fraction*/
  dimage->juppGap=0;       /*don't do Jupp, 2009's gap fraction method*/
  dimage->standRadius=0.0; /*do the whole hemisphere, not cylindrical plot*/
  dimage->errThresh=0.00001;
  dimage->vzoom=0;
  dimage->zenN=1;
  dimage->Vfile=0;
  dimage->coarsen=1;
  dimage->checkD=0;
  dimage->vrml=0;
  dimage->fixclump=0;     /*don't correct for clumpng*/
  dimage->createMap=0;    /*don't make a voxel map, unless it's needed*/
  dimage->checkMap=0;     /* don't print out an ascii voxel map*/
  dimage->withinSegs=0;   /*act as if the sensor heads weren't split*/
  dimage->withoutSegs=1;  /*act as if the sensor heads weren't split*/
  dimage->nBands=-1;
  for(i=0;i<3;i++){
    dimage->nRGB[i]=0;
    dimage->matRGB[i]=NULL;
    dimage->voxCentre[i]=0;
  }
  dimage->data=NULL;
  dimage->gnucoords=0;     /*don't print put voxel coordinated for gnuplot*/
  dimage->Vascii=0;        /*don't print out ascii voxel files, except at inversion*/
  dimage->Vflat=0;         /*don't compress all voxel layers into a single image*/
  dimage->Vlog=1;          /*print out voxels with each iteration*/
  dimage->plants=NULL;     /*will become "plants.matlib" if not reset*/
  dimage->Athresh=0.0;     /*do not try and invert tree height*/
  dimage->noiseRange=0.0;  /*don't use empty signal to predict noise*/
  dimage->denoise=0;       /*don't subtract noise*/
  dimage->meanoise=NULL;   /*prevent segmentation faults*/
  dimage->stdevnoise=NULL; /*same as above*/
  dimage->sigstart=0;      /*don't look for the signal start. Always noise tracked*/
  dimage->ndevs=3.0;       /*use three standard deviations for the signal start*/
  dimage->unTracked=0;     /*don;t look for start without noise tracking, in addition to noise tracking*/
  dimage->noisestats=0;    /*don't calculate noise statistics*/
  dimage->windowstat=0;    /*don't calculate windowed stats*/
  dimage->features=0;      /*do not look for waveform features*/
  dimage->LUTlegend=0;     /*no legend*/
  dimage->Plength=0;       /*no pusle deconvolution*/
  dimage->conlength=0.0;   /*no pre deconvolution smoothing*/
  dimage->postlength=0.0;  /*no post deconvolution smoothing*/
  dimage->noilength=0.0;   /*no post de-noising smoothing*/
  dimage->outdecon=0;      /*don't save deconvolved data*/
  dimage->convergeCheck=0; /*don't check for convergence*/
  dimage->pulse=NULL;
  dimage->Pres=1.0;
  dimage->diters=100;      /*100 iterations of Gold's method for deconvolution*/
  dimage->sdecon=0;        /*don't deconvolve using a pulse contained in the data file*/
  dimage->stopGold=0;      /*don't use maximum amplitude to stop deconvolution*/
  dimage->jansson=0;       /*use Gold's method, not Jansson's if decnvolving*/
  dimage->deconLaplace=0;  /*don't deconvolve with a Laplacian, mutually exclusive with above*/
  dimage->deconOrders=0;   /*don't deconvolve multiple scattering*/
  dimage->Geiger=0.0;      /*don't convert waveform to Geiger mode APD*/
  dimage->addGeiger=0;     /*don't combine multiple Geiger samples files*/
  dimage->discreteInt=0;   /*if it's discrete return, din't record the intensity (Geiger mode)*/
  dimage->firstReturn=0;   /*don't just output the first return*/
  dimage->funk=gaussian;   /*pulse shape*/
  dimage->nGauss=0;        /*don't use Powell's method to perform Gaussian decomposition*/
  dimage->noise=0.0;       /*Gaussian noise to add*/
  dimage->seed=0;          /*don't bother random seeding*/
  dimage->crop=NULL;       /*don't crop the waveform*/
  dimage->dividewave=0;    /*don't divide two waves*/
  dimage->spectralGround=0;/*don't try to find the ground with spectral methods*/
  dimage->spectralGround2=0;/*don't try to find the ground with new spectral methods*/
  dimage->weighty=1;       /*use the weighting function when smoothing spectral ratios*/
  dimage->ratiout=0;       /*don't output ratios*/
  dimage->waveMaths=0;     /*don't do any wave maths*/
  dimage->anyAdjustable=1; /*check gaps at least once*/
  dimage->phaseHet=0;      /*don't test for heterogeneoty of phase function*/
  dimage->idealFile=0;     /*there is no ideal file, analyse all inputs*/
  dimage->trans=NULL;      /*don't worry about atmspheric transittance*/
  strcpy(dimage->input," ");
  strcpy(dimage->Vinput," "); 

  for (i=1;i<argc;i++){
    if (*argv[i]=='-'){
      if (!strncasecmp(argv[i],"-input",6)){
        checkArguments(1,i,argc,"-input");
        strcpy(dimage->input,argv[++i]);
        dimage->scanN=1;
        if(!(dimage->data=(FILE **)calloc(dimage->scanN,sizeof(FILE *)))){
          fprintf(stderr,"error in data pointer allocation.\n");
          exit(1);
        }
        sprintf(namen,"%s.data",dimage->input);
        if((dimage->data[0]=fopen(namen,"rb"))==NULL){
          fprintf(stderr,"Error opening results file %s\n",namen);
          exit(1);
        }
      }else if (!strncasecmp(argv[i],"-Ainput",11)){
        checkArguments(1,i,argc,"-Asciiinput");
        strcpy(dimage->input,argv[++i]);
        dimage->scanN=1;
        if(!(dimage->data=(FILE **)calloc(dimage->scanN,sizeof(FILE *)))){
          fprintf(stderr,"error in data pointer allocation.\n");
          exit(1);
        }
        sprintf(namen,"%s",dimage->input);
        if((dimage->data[0]=fopen(namen,"r"))==NULL){
          fprintf(stderr,"Error opening results file %s\n",namen);
          exit(1);
        }
        dimage->asciiIn=1;
      }else if(!strncasecmp(argv[i],"-Minput",7)){
        checkArguments(4,i,argc,"-Minput");
        strcpy(dimage->input,argv[++i]);
      /*  while(++i<argc&&*argv[i]!='-'){  */
        while(++i<argc){
          dimage->scanN++;
        }
        if(! (dimage->scanN%3) ) dimage->scanN=dimage->scanN/3;
        else {fprintf(stderr,"you have entered a bad number of coordinates\n");exit(1);}
        if(!(dimage->data=(FILE **)calloc(dimage->scanN,sizeof(FILE *)))){
          fprintf(stderr,"error in data pointer allocation.\n");
          exit(1);
        }
        for(j=i-dimage->scanN*3;j<i;j=j+3){
          if(j+2>argc){fprintf(stderr,"read too far %d of %d\n",j+2,argc);exit(1);}
          x=atoi(argv[j]);
          y=atoi(argv[j+1]);
          z=atoi(argv[j+2]);
          /*now open the file*/
          sprintf(namen,"%s.%d.%d.%d.data",dimage->input,x,y,z);
          if((j-i)/3+dimage->scanN>dimage->scanN){fprintf(stderr,"Oh dear\n");exit(3);}
          if((dimage->data[(j-i)/3+dimage->scanN]=fopen(namen,"rb"))==NULL){
            fprintf(stderr,"Error opening results file %s\n",namen);
            exit(1);
          }
        }
        i--;  /*take a step back afterwards*/
      }else if(!strncasecmp(argv[i],"-pulseInput",11)){
        checkArguments(2,i,argc,"-pulseInput");
	dimage->scanN=2;
        if(!(dimage->data=(FILE **)calloc(dimage->scanN,sizeof(FILE *)))){
          fprintf(stderr,"error in data pointer allocation.\n");
          exit(1);
        }
	/*pulsed file*/
        strcpy(dimage->input,argv[++i]);
	sprintf(namen,"%s.data",dimage->input);
        if((dimage->data[0]=fopen(namen,"rb"))==NULL){
          fprintf(stderr,"Error opening results file %s\n",namen);
          exit(1);
        }
	/*the ideal file*/
        sprintf(namen,"%s.data",argv[++i]);
        if((dimage->data[1]=fopen(namen,"rb"))==NULL){
          fprintf(stderr,"Error opening results file %s\n",namen);
          exit(1);
        }
        dimage->idealFile=1;   /*there is an ideal file, don't analyse only compare*/
      }else if(!strncasecmp(argv[i],"-mzoom",6)){
        checkArguments(1,i,argc,"-mzoom");
        if(i+1>=argc){
          fprintf(stderr,"error in number of arguments for -mzoom option: 1 required\n");
          exit(1);
        }
        dimage->mzoom=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-czoom",6)){
        checkArguments(1,i,argc,"-czoom");
        if(i+1>=argc){
          fprintf(stderr,"error in number of arguments for -czoom option: 1 required\n");
          exit(1);
        }
        dimage->czoom=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-pzoom",6)){
        checkArguments(1,i,argc,"-pzoom");
        if(i+1>=argc){
          fprintf(stderr,"error in number of arguments for -czoom option: 1 required\n");
          exit(1);
        }
        dimage->pzoom=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-matzoom",8)){ /*this is hard, we don't know numbers*/
        checkArguments(1,i,argc,"-matzoom");
        if(i+1 >= argc){
          fprintf(stderr,"error in number of arguments for -matzoom option: many required\n");
          exit(1);
        }
        dimage->matzoom=atoi(argv[++i]);
        while(strncasecmp(argv[++i],"R",1)&&i<argc);
        while(strncasecmp(argv[++i],"G",1)&&i<argc) dimage->nRGB[0]++;
        if(!(dimage->matRGB[0]=(int *)calloc(dimage->nRGB[0],sizeof(int)))){
          fprintf(stderr,"error in initial image allocation.\n");
          exit(1);
        }
        for(j=i-dimage->nRGB[0];j<i;j++){
          dimage->matRGB[0][j-i+dimage->nRGB[0]]=atoi(argv[j]);
        }
        while(strncasecmp(argv[++i],"B",1)&&i<argc) dimage->nRGB[1]++;
        if(!(dimage->matRGB[1]=(int *)calloc(dimage->nRGB[1],sizeof(int)))){
          fprintf(stderr,"error in initial image allocation.\n");
          exit(1);
        }
        for(j=i-dimage->nRGB[1];j<i;j++){
          dimage->matRGB[1][j-i+dimage->nRGB[1]]=atoi(argv[j]);
        }
        while(++i<argc&&*argv[i]!='-')dimage->nRGB[2]++;
        if(!(dimage->matRGB[2]=(int *)calloc(dimage->nRGB[2],sizeof(int)))){
          fprintf(stderr,"error in initial image allocation.\n");
          exit(1);
        }
        for(j=i-dimage->nRGB[2];j<i;j++){
          dimage->matRGB[2][j-i+dimage->nRGB[2]]=atoi(argv[j]);
        }
        i--;  /*take a step back afterwards, as we won't know where the end is*/
      }else if(!strncasecmp(argv[i],"-split",6)){
        checkArguments(2,i,argc,"-split");
        dimage->withinSegs=atoi(argv[++i]);
        dimage->withoutSegs=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-coarsen",8)){
        checkArguments(1,i,argc,"-coarsen");
        dimage->coarsen=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-gain",5)){
        checkArguments(1,i,argc,"-gain");
        dimage->gain=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-blurry",7))dimage->view=0;
      else if(!strncasecmp(argv[i],"-ascii",6)){
        checkArguments(4,i,argc,"-ascii");
        dimage->ascii=1;
        for(j=0;j<4;j++){
         /* if(i+j >= argc){
            fprintf(stderr,"error in number of arguments for -ascii option: 4 required\n");
            exit(1);
          }
         */
          dimage->Arange[j]=(double)(atof(argv[++i])*M_PI/180);
        }
      }else if(!strncasecmp(argv[i],"-filledAscii",12))dimage->asciiSave=1;
      else if(!strncasecmp(argv[i],"-reflectance",12)){
        checkArguments(4,i,argc,"-reflectance");
        dimage->totalFlux=1;
        for(j=0;j<4;j++){
          dimage->Arange[j]=(double)(atof(argv[++i])*M_PI/180.0);
        }
      }else if(!strncasecmp(argv[i],"-stats",6))dimage->stats=1;
      else if(!strncasecmp(argv[i],"-rawStats",9))dimage->rawStats=1;
      else if(!strncasecmp(argv[i],"-noBinStats",11))dimage->stats=2;
      else if(!strncasecmp(argv[i],"-joyless",8))dimage->intent=0;
      else if(!strncasecmp(argv[i],"-legend",7))dimage->LUTlegend=1;
      else if(!strncasecmp(argv[i],"-printHeaders",13))dimage->printHeader=1;
      else if(!strncasecmp(argv[i],"-onlyAngles",11))dimage->jusAng=1;
      else if(!strncasecmp(argv[i],"-up",3))dimage->point=0;
      else if(!strncasecmp(argv[i],"-scatter",8))dimage->scatFract=1;
      else if(!strncasecmp(argv[i],"-maxHeight",10)){
        checkArguments(3,i,argc,"-maxHeight");
  /*      for(j=0;j<2;j++)dimage->Hpos[j]=atof(argv[++i]); */
        for(j=0;j<2;j++)dimage->Hdim[j]=atoi(argv[++i]);
        dimage->Hres=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-trueGap",8)){   /*measure the true gap fraction of an Echidna scan*/
        checkArguments(1,i,argc,"-trueGap");
        dimage->trueGap=1;
        dimage->tGapres=atof(argv[++i])*M_PI/180.0;  /*in radians*/
      }else if(!strncasecmp(argv[i],"-juppGap",8)){  /*do Jupp, 2009 gap fraction method*/
        dimage->juppGap=1;
      }else if(!strncasecmp(argv[i],"-standRadius",12)){
        checkArguments(1,i,argc,"-standRadius");
        dimage->standRadius=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-voxel",6)){
        checkArguments(7,i,argc,"-voxel");
        dimage->voxRes=atof(argv[++i]);
        for(j=0;j<3;j++){
/*          if(i+1+j >= argc){
            fprintf(stderr,"error in number of arguments for -voxel option: 7 required\n");
            exit(1);
          }
   */
          dimage->voxCentre[j]=atof(argv[++i]);
        }
        for(j=0;j<3;j++){
        /*  if(i+1+j >= argc){
            fprintf(stderr,"error in number of arguments for -voxel option: 7 required\n");
            exit(1);
          }
        */
          dimage->voxScene[j]=atof(argv[++i]);
        }
      }else if(!strncasecmp(argv[i],"-linearLAD",10)){
        dimage->linearVoxels=1;
      }else if(!strncasecmp(argv[i],"-broadleaf",10)){  /*use combined phase and G functions*/
        dimage->species=1;
      }else if(!strncasecmp(argv[i],"-coniferous",11)){ /*use constant phase factor*/
        dimage->species=0;
      }else if(!strncasecmp(argv[i],"-vzoom",6)){
        checkArguments(1,i,argc,"-vzoom");
        dimage->vzoom=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-vnumb",6)){
        checkArguments(1,i,argc,"-vnumb");
        dimage->zenN=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-Vinput",7)){
        checkArguments(1,i,argc,"-Vinput");
        dimage->Vfile=1;
        strcpy(dimage->Vinput,argv[++i]);
      }else if(!strncasecmp(argv[i],"-output",7)){
        checkArguments(1,i,argc,"-output");
        if(!(dimage->output=(char *)calloc(strlen(argv[++i])+1,sizeof(char)))){
          fprintf(stderr,"error in initial image allocation.\n");
          exit(1);
        }
        strcpy(dimage->output,argv[i]);  /*the ++i is above*/
      }else if(!strncasecmp(argv[i],"-bands",6)){
        checkArguments(1,i,argc,"-bands");
        dimage->nBands=atof(argv[++i]);
	checkArguments(dimage->nBands,i,argc,"-bands, number of wavelengths");
        if(!(dimage->band=(int **)calloc(1,sizeof(int *)))){
          fprintf(stderr,"error in inversion waveband allocation.\n");
          exit(1);
        }
        if(!(dimage->band[0]=(int *)calloc(dimage->nBands,sizeof(int)))){
          fprintf(stderr,"error in inversion waveband allocation.\n");
          exit(1);
        }
        for(j=0;j<dimage->nBands;j++){
          dimage->band[0][j]=atoi(argv[++i]);
        }
      }else if(!strncasecmp(argv[i],"-Ascope",7)){
        checkArguments(1,i,argc,"-Ascope");
        if(dimage->Athresh!=0.0){fprintf(stderr,"Can't use -features and -Ascope together yet\n");exit(1);}
        dimage->Athresh=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-emptyRange",11)){
        checkArguments(1,i,argc,"-emptyRange");
        dimage->noiseRange=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-denoise",8)){
        dimage->denoise=1;
      }else if(!strncasecmp(argv[i],"-sigstart",9)){
        dimage->sigstart=1;
        dimage->noisestats=1;
      }else if(!strncasecmp(argv[i],"-starthresh",11)){
        checkArguments(1,i,argc,"-starthresh");
        dimage->ndevs=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-trackless",10)){
        dimage->unTracked=1;
      }else if(!strncasecmp(argv[i],"-windowstat",11)){
        dimage->windowstat=1;
      }else if(!strncasecmp(argv[i],"-GaussEstimate",14)){
        checkArguments(1,i,argc,"-GaussEstimate");
        dimage->GaussFit=1;
        if(dimage->Athresh!=0.0){fprintf(stderr,"Can't use -features and -Ascope together yet\n");exit(1);}
        dimage->Athresh=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-features",9)){
        dimage->features=1;
        checkArguments(4,i,argc,"-features");
        for(j=0;j<4;j++){
          dimage->Arange[j]=(double)(atof(argv[++i])*M_PI/180);
        }
      }else if(!strncasecmp(argv[i],"-Vascii",7))dimage->Vascii=1;
      else if(!strncasecmp(argv[i],"-Vflat",6))dimage->Vflat=1;
      else if(!strncasecmp(argv[i],"-noVlog",7))dimage->Vlog=0;
      else if(!strncasecmp(argv[i],"-vthresh",8)){
        checkArguments(1,i,argc,"-vthresh");
        dimage->errThresh=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-crop",5)){
        checkArguments(2,i,argc,"-crop");
        if(!(dimage->crop=(double *)calloc(2,sizeof(double)))){
          fprintf(stderr,"error in crop bound allocation.\n");
          exit(1);
        }
        for(j=0;j<2;j++)dimage->crop[j]=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-materials",10)){
        checkArguments(1,i,argc,"materials");
        if(!(dimage->plants=(char *)calloc(strlen(argv[++i])+1,sizeof(char)))){
          fprintf(stderr,"error in crop bound allocation.\n");
          exit(1);
        }
        strcpy(dimage->plants,argv[i]);
      }else if(!strncasecmp(argv[i],"-Geiger",7)){
        checkArguments(1,i,argc,"-Geiger");
        dimage->Geiger=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-firstReturn",12)){
        dimage->firstReturn=1;
      }else if(!strncasecmp(argv[i],"-discreteInt",12)){
        dimage->discreteInt=1;
      }else if(!strncasecmp(argv[i],"-pulseRes",9)){
        checkArguments(1,i,argc,"-pulseRes");
        dimage->Pres=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-convolve",9)){
        checkArguments(1,i,argc,"-convolve");
        dimage->conlength=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-postconvolve",13)){
        checkArguments(1,i,argc,"-postconvolve");
        dimage->postlength=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-postnoiseconvolve",18)){
        checkArguments(1,i,argc,"-postnoiseconvolve");
        dimage->noilength=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-pestconvolve",13)){
        checkArguments(1,i,argc,"-pestconvolve");
        dimage->pestlength=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-iterations",11)){
        checkArguments(1,i,argc,"-iterations");
        dimage->diters=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-noise",6)){
        checkArguments(1,i,argc,"-noise");
        dimage->noise=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-converge",9)){
        dimage->convergeCheck=1;
      }else if(!strncasecmp(argv[i],"-jansson",8)){
        dimage->jansson=1;
        dimage->stopGold=1;
      }else if(!strncasecmp(argv[i],"-clip",5)){
        checkArguments(1,i,argc,"-clip");
        dimage->clip=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-deconvolve",11)){
        checkArguments(1,i,argc,"-deconvolve");
        dimage->Plength=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-seed",5)){
        checkArguments(1,i,argc,"-seed");
        dimage->seed=atoi(argv[++i]);
        srand(dimage->seed);
      }else if(!strncasecmp(argv[i],"-selfdeconvolve",15))dimage->sdecon=1;
      else if(!strncasecmp(argv[i],"-stopGold",9))dimage->stopGold=1;
      else if(!strncasecmp(argv[i],"-Laplacedecon",13))dimage->deconLaplace=1;
      else if(!strncasecmp(argv[i],"-scatdecon",10))dimage->deconOrders=1;
      else if(!strncasecmp(argv[i],"-addGeiger",10)){
        checkArguments(1,i,argc,"-addGeiger");
        dimage->addGeiger=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-ratio",6)){
        checkArguments(2,i,argc,"-ratio");
        dimage->ratioBand[0]=atof(argv[++i]);
        dimage->ratioBand[1]=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-trans",6)){
        if(dimage->nBands==-1){fprintf(stderr,"Need to specify \"-bands\" before \"-trans\"\n");exit(1);}
        checkArguments(dimage->nBands,i,argc,"-trans");
        dimage->trans=falloc(dimage->nBands,"atmospheric transmittance",0);
        for(j=0;j<dimage->nBands;j++)dimage->trans[j]=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-fitGauss",9)){
        checkArguments(1,i,argc,"-fitGauss");
        dimage->nGauss=atoi(argv[++i]);
      }else if(!strncasecmp(argv[i],"-groundout",10)){
        checkArguments(1,i,argc,"-groundout");
        dimage->groundout=challoc((uint64_t)strlen(argv[++i])+1,"ground estimate filename",0);
        strcpy(dimage->groundout,argv[i]);  /*the ++i is above*/
      }else if(!strncasecmp(argv[i],"-groundedge",11)){
        dimage->dividewave=1;
        dimage->spectralGround=1;
      }else if(!strncasecmp(argv[i],"-dividebands",12)){
        dimage->dividewave=1;
	dimage->ratiout=1;
      }else if(!strncasecmp(argv[i],"-spectralGround",15)){
        dimage->spectralGround2=1;
      }else if(!strncasecmp(argv[i],"-medRatio",9)){
        checkArguments(1,i,argc,"-medRatio");
        dimage->medRatio=atof(argv[++i]);
      }else if(!strncasecmp(argv[i],"-noWeight",9)){
        dimage->weighty=0;
      }else if(!strncasecmp(argv[i],"-diagnoseGround",15)){   /*print out the smoothed function*/
        dimage->ratiout=1;
      }else if(!strncasecmp(argv[i],"-noclear",8))dimage->memorymap=0;
      else if(!strncasecmp(argv[i],"-save",5))dimage->outdecon=1;
      else if(!strncasecmp(argv[i],"-down",5))dimage->point=-1*M_PI;
      else if(!strncasecmp(argv[i],"-vrml",5))dimage->vrml=1;
      else if(!strncasecmp(argv[i],"-fixclumping",12))dimage->fixclump=1;
      else if(!strncasecmp(argv[i],"-header",7))dimage->teast=1;
      else if(!strncasecmp(argv[i],"-checkData",10))dimage->checkD=1;
      else if(!strncasecmp(argv[i],"-gnucoords",10))dimage->gnucoords=1;
      else if(!strncasecmp(argv[i],"-phaseHet",9))dimage->phaseHet=1;
      else if(!strncasecmp(argv[i],"-u",2)||!strncasecmp(argv[i],"-help",5)){
        usage(argv);
        exit(0);
      }else{
        fprintf(stderr,"%s: unknown argument on command line: %s\nTry echiMake -help\n",argv[0],argv[i]);   /* Program_name = argv[0] */
        exit(1);
      }
    }
  }
  if(i!=argc){
    fprintf(stderr,"Something is wrong\n");
    exit(1);
  }
  if(!dimage->coarsen)dimage->coarsen=1.0;

  /*check for conflicting commands and missing commands*/ 
  if(dimage->output==NULL){   /*if we don't define output name use input as root*/
    if(strncasecmp(dimage->input," ",1)){
      if(!(dimage->output=(char *)calloc(strlen(dimage->input)+1,sizeof(char)))){
        fprintf(stderr,"error in initial image allocation.\n");
        exit(1);
      }
      strcpy(dimage->output,dimage->input);
    }else if(strncasecmp(dimage->Vinput," ",1)){
      if(!(dimage->output=(char *)calloc(strlen(dimage->Vinput)+1,sizeof(char)))){
        fprintf(stderr,"error in initial image allocation.\n");
        exit(1);
      }
      strcpy(dimage->output,dimage->Vinput); 
    }
  }
  if(dimage->firstReturn&&(dimage->noise>0.0)){
    fprintf(stderr,"Currently we cannot do first return lidar in the presence of noise.\nUse -Geiger with an appropriate threshold\n");
    exit(1);
  }

  return(dimage);
}/*readCommands*/



void usage(char **argv){
  fprintf(stderr, "usage: %s [options]\n", argv[0]);
  fprintf(stderr, "where the options are:\n\n");
  fprintf(stderr, "-input file\n-Minput file_name x y z for as many files as you want. MUST BE LAST COMMAND\n-Ainput file_name; input an ascii file, two columns, one range t'other radiant flux.\n-pulseInput pulsed ideal; input two data files, only the first will be analysed, second for comparison\n-joyless; do not print out the message of intent\n-Vinput name; Input a binary voxel file for analysis\n-output; to change output file root\n-vnumb; number of view zeniths between 0 and 90\n-pzoom beams/pixel\n\n");

  fprintf(stderr,"DATA MODIFIERS\n-bands, number of bands, wavelengths in nanometers; for inversion\n-coarsen n;       coarsens range resolution by a factor of n\n-split withinSegs withoutSegs; to recombine split sensor heads\n-trans ...; atmospheric transmittance for all bands to be used. Must specify \"-bands\" first\n\n");
  
  fprintf(stderr,"PICTURES\n-mzoom monchrome_image_size\n-czoom colour_image_size\n-matzoom material_image_size R red_material G green_material B blue_material\n-vzoom n m; voxel image, n pixels per voxel\n-gain brightness\nblurry; whether we use FOV or beam width(default)\n\n");
  
  fprintf (stderr,"SYSTEM BITS\n-noclear; don't emtpy the memory until the end. Necessary for noise\n-seed seed; seed random number generator\n\n");

/*ascii outputs*/
fprintf(stderr,"OUTPUTS\n-ascii iZen fZen iAz fAz; lidar data written as ascii between angle bounds chosen\n-filledAscii; only print filled bins. Needs \"-ascii\"\n-reflectance iZen fZen iAz fAz; lidar data written as ascii between angle bounds chosen\n-legend; ONLY WITH ASCII, outputs a legend for the ascii file\n-stats; prints out mean radiances and their standard deviations, needs more than one scan file\n-rawStats; prints out reflectance values from which the stats were calculated\n-noBinStats; does not print out statistics for each bin\n-printHeaders; prints headers to screen\n-onlyAngles; just print out angles to ascii file\n-dividebands; divide two bands, chosen with \"-band\" option, output to \".ratio\"\n-windowstat; calculate windowed statisitics of the ratio from \"-dividebands\" option\n\n");

/*data modification*/
fprintf(stderr,"RECORDING MODE\n-Geiger threshold; convert waveforms into Geiger mode returns\n-firstReturn; convert to first return. Doesn't work with noise, use -Geiger instead\n-discreteInt; record the intensity of a discrete return\n-noise photon_number; add Gaussian noise as if a certain number of photons were in the signal\n-addgeiger number_of_runs; add up multiple Geiger returns to a single waveform\n-spectralGround; the latest multi-spectral edge finding methiod. Prints to stdout\n-noWeight; don't use the weighting function when smoothing\n-diagnoseGround; prints out the smoothed ratio used to find ground, whichever method is used.\n\nThese are older and not necessarily the best methods, kept for sentimentalityand to learn from\n-ratio band1 band2; calculate the ration of two bands\n-medRatio width;   apply a median filter to the spectral ratio before analysis\n-groundedge; estimate ground position from spectral ratio of two bands with \"-band\" option, output to \".groundedge\"\n-groundout filename; filename to output ground position estimate from \"-dividebands\"\n\n");

/*deconvolution*/
fprintf(stderr,"DECONVOLUTION\n-deconvolve pulse_length; to deconvolve a for a finite pulse length\n-selfdeconvolve; deconvolves with a pulse stored in the data file\n-jansson; deconvolve with jansson's methods, using max ampkitude as upper bound\n-stopGold; use maximum amplitude to stop iterating Gold's method\n-converge; check for convergence with pulse length\n-Laplacedecon; deconvolve by convolution with the Laplacian of the pulse. Mutually exclusive with selfdeconvolve which takes priority\n-scatdecon; deconvole multiple scattering into a single array\n-save; save deconvolved or convolved data to a new file ending in \".decon.data\"\n-convolve, pulse_length; convolve waveform with a pulse after noise is added before any processing\n-postnoiseconvolve length; smooth signal after denoising, before deconvolution.\n-postconvolve pulse_length; convolve waveform after deconvolution\n-pestconvolve length; smooth signal for estimating Gaussian parameters. Not passed to other functions\n-pulseRes resolution; for sub unit pulse shape resolution\n-iterations; number of iterations for Gold's deconvolution method. Default is 100\n-clip, fraction; clip off all features with a frequency greater than a fraction of the pulse\n-crop, start stop; Crop a waveform between two bounds\n-fitGauss nGauss; perform Gaussian decomposition with n Gaussians\n\n");

/*waveform decomposition*/
fprintf(stderr,"DECOMPOSITION\n-GaussEstimate threshold; estimate intiial parameters of Gaussians\n-features; attempt to extract features from a wavefrom\n\n");

/*noise*/
fprintf(stderr,"NOISE\n-emptyRange range; range known to be empty used for calculating noise\n-denoise; use empty range estimate to subtract noise\n-sigstart; use noise statistics to estimate signal start\n-starthresh thresh;   set noise threshold, number of standard deviations\n-trackless;     find start without noise tracking as well\n\n");

/*voxels*/
fprintf(stderr,"VOXELS\n-Vascii; output voxels as an ascii file\n-Vlog; print out voxels with each iteration\n-onlyAngles; ascii output only shows angles\n-scatter; outputs contyributions from direct and scattered light\n-Ascope threshold; simple height inversion for single beam\n-voxel voxel_size x y z (central cords) a b c (scene dimensions); outputs voxel representation.\n-vthresh; threshold for least squares (one float)\n-materials, filename; specify plants.matlib file to get albedos\n-maxHeight x_size y_size resolution; outputs an ascii of maximum outputs\n-header; does not read anything except headers\n-checkData; check data for errors and output to *.errorlog\n-gnucoords; output coordinates for gnuplot\n-vrml; outputs voxels as a vrml file\n-fixclumping; attempts to correct for sub-bin clumping\n-Vflat; when drawing voxels compress all into a horizontal layer\n-trueGap zen_res; measure the true gap fraction with a zenith resolution\n-juppGap;  Jupp et al (2009)'s gap fraction method\n-standRadius r;  determine the gap fraction of only a cynlidrical plot\n-linearLAD;  use simple linear LAD model during inversion\n-coniferous;   use a constant phase factor\n-broadleaf;   use a combined phase and G functions\n\n");

  fprintf(stderr,"BITS AND PIECES\n-phaseHet; calculate hetereogeneity of phase function\n");

  return;
}

/*#########################################################*/
/*set up the control structure*/

RatControl *setUpcontrol(RatImage *dimage)
{
  int numb=0;
  RatControl *ratPoint=NULL;

  if(!(ratPoint=(RatControl *)calloc(dimage->scanN,sizeof(RatControl)))){
    fprintf(stderr,"error allocating structure memory\n");
    exit(1);
  }
  for(numb=0;numb<dimage->scanN;numb++){
    ratPoint[numb].EpLength=0;
  }

  /*space to hold solar irradiance values*/
  if(dimage->noise>0.0){
    if(!(dimage->solarI=(float **)calloc(dimage->scanN,sizeof(float *)))){
      fprintf(stderr,"This error in solar irradiance allocation, allocating %d.\n",dimage->scanN);
      exit(1);
    }
  }

  return(ratPoint);  
}/*setUpcontrol*/

/*##########################################################*/
/*Now the headers have beenm read carry on setting up*/

RatControl *postSetUp(RatControl *ratPoint,RatImage *dimage)
{
  int numb=0,Lplace=0,band=0;

  if(dimage->sdecon||(dimage->Plength!=0.0)){
    for(numb=0;numb<dimage->scanN;numb++){
      if(!(ratPoint[numb].maxAmp=(double **)calloc(ratPoint[numb].NAscans*ratPoint[numb].NZscans,sizeof(double *)))){
        printf("error in deconvolution criterion allocation.\n");
        exit(1);
      }
      if(!dimage->stopGold){
        for(Lplace=0;Lplace<ratPoint[numb].NAscans*ratPoint[numb].NZscans;Lplace++){
          ratPoint[numb].maxAmp[Lplace]=dalloc(dimage->nBands,"convergence criterion",Lplace);
          for(band=0;band<dimage->nBands;band++)ratPoint[numb].maxAmp[Lplace][band]=-1.0;
        }
      }
    }
  }/*do this if we are to deconvolve*/

  return(ratPoint);
}/*postSetUp*/


/*##########################################################*/
/*apply atmosheric attenuation*/

void atmosphericAttenuation(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace)
{
  int bin=0,band=0,place=0;

  for(band=0;band<dimage->nBands;band++){
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      ratPoint[numb].ratRes[Lplace].refl[place]*=(double)(dimage->trans[band]);
    }
  }

  return;
}/*atmosphericAttenuation*/


/*##########################################################*/
/*add Gaussian noise to total radiant flux*/

void addNoise(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace)
{
  int bin=0,band=0,place=0;
  double sigma=0;
  double photonEnergy=0;
  double energy=0,photonScale=0;
  double background=0,backEnergy=0,backPhotons=0;
  double backnoise=0,noise=0;
  double Esun=0,Tatm=0,Ar=0,Tfov=0,QE=0,Bwidth=0,NEP=0,F=0,cosZen=0;
  double GaussNoise();
  /*int noiseBins=0;*/

  Tatm=0.8;
  cosZen=cos(0.0*M_PI/180.0);
  Ar=M_PI*0.25;  /*0.5m radius, squared*/
  Tfov=0.0002;   /*200 micro radians*/
  QE=0.5;
  Bwidth=1.0; /*in nm*/
  NEP=0.00000000000001000;
  F=2.0;

  if(dimage->seed){
    srand(dimage->seed);
    srand(rand()+dimage->seed);
  }

  if(dimage->noiseRange>0.0){
    if(!(dimage->backnoise=(double **)calloc(dimage->nBands,sizeof(double *)))){
      fprintf(stderr,"allocation error for background noise array\n");
      exit(1);
    }

  }

  for(band=0;band<dimage->nBands;band++){
    Esun=dimage->solarI[numb][band];  /*in W.m-2.srad-1.nm-1.*/

    /*calculate total energy for rescaling to photon number*/
    energy=0.0;
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      energy+=ratPoint[numb].ratRes[Lplace].refl[place];
    }
    if(energy<=0.0){
      fprintf(stderr,"Issue with the noise\n"); 
      exit(1);
    }
    photonScale=dimage->noise/energy;   /*factor to multiply signal to to get photon count*/
    photonEnergy=1.9865/(pow(10.0,16.0)*ratPoint[numb].wavelength[dimage->band[numb][band]]);
    backEnergy=energy*Esun*Tfov*Tfov/4.0*Ar*cosZen*Tatm*Bwidth;
    backPhotons=backEnergy/photonEnergy;                  /*this is flux as we have Watts rather than Joules*/
    backnoise=(QE*backPhotons+(QE/photonEnergy)*(QE/photonEnergy)*NEP*NEP/(2.0*F))*ratPoint[numb].bin_L/299800000000.0;

    for(bin=0;bin<ratPoint[numb].bins;bin++){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      /*photon statistic noise*/
      sigma=ratPoint[numb].ratRes[Lplace].refl[place]*photonScale;
      /*background photon numbers*/
      background=backnoise*GaussNoise()*F;
/*
#printf("backEnergy %e, backPhotons %e, backnoise %f, background %f, refl %f, refl photons %f\n",backEnergy*ratPoint[numb].bin_L/299800000000,backPhotons*ratPoint[numb].bin_L/299800000000,backnoise,background,ratPoint[numb].ratRes[Lplace].refl[place],ratPoint[numb].ratRes[Lplace].refl[place]*photonScale);
*/
      if((double)rand()/(double)RAND_MAX<0.5){
        ratPoint[numb].ratRes[Lplace].refl[place]+=sqrt(sigma*GaussNoise()+background)/photonScale;
      }else{
        noise=sigma*GaussNoise()-background;
        if(noise>0.0)ratPoint[numb].ratRes[Lplace].refl[place]-=sqrt(noise)/photonScale;
        else         ratPoint[numb].ratRes[Lplace].refl[place]+=sqrt(-1.0*noise)/photonScale;
        if(ratPoint[numb].ratRes[Lplace].refl[place]<0.0)ratPoint[numb].ratRes[Lplace].refl[place]=0.0;
      }
    }/*bin loop*/

    /*create an array of background noise*/
    if(dimage->noiseRange>0.0){
      if(dimage->noiseRange>ratPoint[numb].min_R){
        dimage->noiseBins=(int)((dimage->noiseRange-ratPoint[numb].min_R)/ratPoint[numb].bin_L);
      }else{
        dimage->noiseBins=(int)(ratPoint[numb].min_R/ratPoint[numb].bin_L);
      }

      dimage->backnoise[band]=dalloc(dimage->noiseBins,"background noise",numb);
      for(bin=0;bin<dimage->noiseBins;bin++){
        dimage->backnoise[band][bin]=sqrt(backnoise*GaussNoise()*F)/photonScale;
        /*printf("bin %d %f %f\n",bin,dimage->backnoise[band][bin],GaussNoise());*/
      }
    }

  }

  return;
}/*addNoise*/

/*########################################################################################*/
/*return a fraction of a sigma (multiplied externally for efficiency) for Gaussian noise*/

double GaussNoise()
{
  /*BEWARE, I had this as sqrt(2.0*log(...)), but am, not sure why that should be so. Not sure which is right*/
  double noise=0,max=0;

  /*For some reason RAND_MAX is negative on growler. Does it wrap around? YES FOOL. it gets too big for itself*/
  if(RAND_MAX>0)max=(double)RAND_MAX;
  else          max=-1*RAND_MAX;

  noise=sqrt(log(1.0/((double)(rand()+1)/(max+1))));


  /*implement this later, currently taken care of in "addNoise"*/
/*  if((float)rand()/(float)RAND_MAX<0.5)noise*=-1.0;*/
	
  return(noise);

}/*GaussNoise*/

/*#################################################################*/
/*Read in Lewis' ascii files*/

RatControl *resultsAscii(RatImage *dimage,RatControl *ratPoint,int numb,int time,char mode)
{
  int i=0,place=0,Lplace=0;
  char line[200];
  double *range=NULL,*flux=NULL;
  double r=0,f=0;
  char tempR[200],tempF[200];
  double *markDo(int,double *,double);
  void stateIntent(RatImage *,RatControl *,int,int);
  void deconvolveFFT(RatControl *,RatImage *,int,int);
  void convolveFFT(RatControl *,RatImage *,int,int,double);
  void addNoise(RatImage *,RatControl *,int,int);
  void removeNoise(RatImage *,RatControl *,int,int);
  void GeigerMode(RatImage *,RatControl *,int,int);
  void lowPassFilter(RatImage *,RatControl *,int,int);
  void coarsenRange(RatImage *,RatControl *,int,int);
  void combineSplithead(RatImage *,RatControl *,int,int,char,char);
  void convolveLaplace(RatControl *,RatImage *,int,int);
  void solarIrradiance(RatImage *,RatControl *,int);
  void noiseEstimate(RatImage *,RatControl *,int,int);
  void photonCount(RatImage *,RatControl *,int,int);
  RatControl *resultsAscii(RatImage *,RatControl *,int,int,char);


  if(time>=0){

    if(fseek(dimage->data[0],0,SEEK_SET)){
      fprintf(stderr,"fseek error 1 %d %u\n",i,ratPoint[numb].ratRes[i].coords[0]);
      exit(1);
    }


    while(fgets(line,200,dimage->data[0])!=NULL){
      if(sscanf(line,"%s %s",tempR,tempF)!=2) {
        fprintf(stderr, "Badly formatted material file\n");
        exit(1);
      }
      r=atof(tempR);
      f=atof(tempF);
      range=markDo(i,range,r);
      flux=markDo(i,flux,f);
      i++;
    }


    /*and hammer into structures*/
    if(!(ratPoint[numb].ratRes[time].refl=(double *)calloc(ratPoint[numb].bins*ratPoint[numb].nBands*\
      (ratPoint[numb].n+1)*dimage->coarsen*(ratPoint[numb].withinSegs+ratPoint[numb].withoutSegs),sizeof(double)))){
      fprintf(stderr,"allocation error\n");
      exit(1);
    }
    for(i=0;i<ratPoint[numb].bins;i++){
      place=findReflIndex(dimage,ratPoint,numb,i,0,0);
      ratPoint[numb].ratRes[Lplace].refl[place]=flux[i];
    }

    /*modify the signal if desired*/
    if(dimage->noise!=0.0)addNoise(dimage,ratPoint,numb,time);
    if(dimage->photon)photonCount(dimage,ratPoint,numb,time);
    if(dimage->clip!=0.0)lowPassFilter(dimage,ratPoint,numb,time);
    if(dimage->conlength>0.0)convolveFFT(ratPoint,dimage,numb,time,(double)dimage->conlength);
    if(dimage->noisestats||dimage->denoise)noiseEstimate(dimage,ratPoint,numb,time);
    if(dimage->denoise)removeNoise(dimage,ratPoint,numb,time);
    if(dimage->Plength!=0||dimage->sdecon)deconvolveFFT(ratPoint,dimage,numb,time);
    else if(dimage->deconLaplace)convolveLaplace(ratPoint,dimage,numb,time);
    if(dimage->postlength>0.0)convolveFFT(ratPoint,dimage,numb,time,(double)dimage->postlength);
    if(dimage->Geiger>0.0)GeigerMode(dimage,ratPoint,numb,time);

  }else{
    if(fseek(dimage->data[0],0,SEEK_SET)){
      fprintf(stderr,"fseek error 1 %d %u\n",i,ratPoint[numb].ratRes[i].coords[0]);
      exit(1);
    }


    while(fgets(line,200,dimage->data[0])!=NULL){
      if(sscanf(line,"%s %s",tempR,tempF)!=2) {
        fprintf(stderr, "Badly formatted material file\n");
        exit(1);
      }
      r=atof(tempR);
      f=atof(tempF);
      range=markDo(i,range,r);
      flux=markDo(i,flux,f);
      i++;
    }

    ratPoint[numb].min_R=range[0];
    ratPoint[numb].max_R=range[i-1];
    ratPoint[numb].bin_L=range[2]-range[1];

    ratPoint[numb].bins=i;
    ratPoint[numb].n=0;
    ratPoint[numb].nBands=1;
    ratPoint[numb].withoutSegs=1;
    ratPoint[numb].withinSegs=0;
    ratPoint[numb].NZscans=1;
    ratPoint[numb].NAscans=1;
    ratPoint[numb].iZen=-180;
    ratPoint[numb].fZen=-180;
    ratPoint[numb].iAz=0.0;
    ratPoint[numb].fAz=0.0;

    ratPoint[numb].theta=dalloc(1,"theta",numb);
    ratPoint[numb].thata=dalloc(1,"thata",numb);

    ratPoint[numb].theta[0]=-180.0*M_PI/180.0;
    ratPoint[numb].thata[0]=0.0;

    if(!(ratPoint[numb].wavelength=(double *)calloc(ratPoint[numb].nBands,sizeof(double)))){
      fprintf(stderr,"error in wavelength array allocation %d.\n",ratPoint[numb].nBands);
      exit(1);
    }
    ratPoint[numb].wavelength[0]=0.0;

    if(dimage->intent)stateIntent(dimage,ratPoint,numb,0);

    if(!(ratPoint[numb].ratRes=(RatResults *)calloc(ratPoint[numb].NZscans*ratPoint[numb].NAscans,sizeof(RatResults)))){
      fprintf(stderr,"error allocating the results.\n");
      exit(1);
    }
  }

  TIDY(range);
  TIDY(flux);

  return(ratPoint);
}/*resultsAscii*/

/*#############################################################*/
/*convert from reflectance to photon count*/

void photonCount(RatImage *dimage,RatControl *ratPoint,int numb,int Lplace)
{
  int bin=0,band=0,place=0;
  double energy=0,effIns=0,atmos=0;
  double telArea=0,range=0;
  double photonOut=0;
  double photons=0;

  energy=0.1;
  telArea=1.0;
  atmos=0.1;
  effIns=0.1;

  /*convert laser energy and wavelength to photon count*/

  for(band=0;band<dimage->nBands;band++){
    photonOut=energy*ratPoint[numb].wavelength[dimage->band[numb][band]]*pow(10,16)/1.986445213;
    photons=(double)photonOut*effIns*telArea*atmos;
    for(bin=0;bin<ratPoint[numb].bins;bin++){
      place=findReflIndex(dimage,ratPoint,numb,bin,band,0);
      range=(double)bin*ratPoint[numb].bin_L+ratPoint[numb].min_R;
      ratPoint[numb].ratRes[Lplace].refl[place]=(double)(unsigned long int)(photons*ratPoint[numb].ratRes[Lplace].refl[place]/(range*range/1000000));
    }
  }
  return;
}/*photonCount*/


/*#############################################################*/
/*read integer header*/

void readIntHeader(int *format,FILE *data,RatControl *ratPoint,int numb,RatImage *dimage)
{
  int *header1=NULL;
  void readIntForm18(RatControl *,FILE *);   /*read int header, format 18*/
  void readFloForm18(RatControl *,FILE *);   /*read decimal header, format 18*/


  header1=ialloc(1,"int header",0);

  /*first read the format type*/
  if(fseek(data,(long)0,SEEK_SET)){fprintf(stderr,"error seeking to the file start\n");exit(1);}
  if(fread(&(header1[0]),sizeof(int),1,data)!=1){
    fprintf(stderr,"error reading the header.\n");
    exit(1);
  }

  /*only run length encoded files are no supported*/
  ratPoint[numb].encoded=1;



  if(dimage->byteord)header1=intSwap(header1,1);
  *format=header1[0];
  if(header1[0]!=11){  /*check format*/
    if(header1[0]==swappedBytes(10,sizeof(int))||header1[0]==swappedBytes(12,sizeof(int))){
      fprintf(stderr,"The data file was created on the wrong architecture.\n");
      exit(1);
    }else if(header1[0]==10){
      fprintf(stderr,"This is the old format without a map data file.\nUse echiMakeNoComp\n");
      exit(1);
    }else if(header1[0]==12){  /*run length encoded file*/
      dimage->badMat=1;
      ratPoint[numb].headerLength=10;
    }else if(header1[0]==13){  /*encoded with number of head splits in*/
      ratPoint[numb].headerLength=12;
      dimage->badMat=1;
    }else if(header1[0]==14){  /*new, but un encoded file*/
      fprintf(stderr,"This is a new format non-endoded file and won't yet work, sorry\n");
      exit(1);
    }else if(header1[0]==15){  /*new, encoded with double lidar parameters*/
      ratPoint[numb].headerLength=9;
      dimage->badMat=1;
    }else if(header1[0]==16){  /*int lidar with a pulse*/
      ratPoint[numb].headerLength=13;
    }else if(header1[0]==17){  /*decimal lidar with a pulse*/
      ratPoint[numb].headerLength=10;
    }else if(header1[0]==18){  /*SALCA style with all extras*/
      readIntForm18(&(ratPoint[numb]),data);
    }else{
      fprintf(stderr,"This file does not have the correct label %d.\n",header1[0]);
      exit(1);
    }
  }

  if(header1[0]!=18){
    /*now read the data*/
    TIDY(header1);
    header1=ialloc(ratPoint[numb].headerLength,"int header",0);
    if(fseek(data,(long)0,SEEK_SET)){fprintf(stderr,"error seeking to the file start\n");exit(1);}
    if(fread(&(header1[0]),sizeof(int),ratPoint[numb].headerLength,data)!=ratPoint[numb].headerLength){
      fprintf(stderr,"error reading the header.\n");
      exit(1);
    }
    if(dimage->byteord)header1=intSwap(header1,(uint64_t)ratPoint[numb].headerLength);
    if(header1[0]<15){   /*lidar variables are ints*/
      ratPoint[numb].min_R=(double)header1[1];
      ratPoint[numb].max_R=(double)header1[2];
      ratPoint[numb].bin_L=(double)header1[3];
      ratPoint[numb].bins=header1[4];
      ratPoint[numb].NZscans=header1[5];
      ratPoint[numb].NAscans=header1[6];
      ratPoint[numb].n=header1[7];
      ratPoint[numb].nBands=header1[8];
      ratPoint[numb].nMat=header1[9];
      if(ratPoint[numb].headerLength==12){
        ratPoint[numb].withinSegs=header1[10];
        ratPoint[numb].withoutSegs=header1[11];
      }else if(ratPoint[numb].headerLength==10){
        ratPoint[numb].withinSegs=0;
        ratPoint[numb].withoutSegs=1;
      }else{
        fprintf(stderr,"The header length, %d is unexpected\n",ratPoint[numb].headerLength);
        exit(1);
      }
      ratPoint[numb].header2Length=10+ratPoint[numb].nBands;
    }else if(header1[0]==15){  /*lidar variables are doubles*/
    }else if(header1[0]==15){  /*lidar variables are doubles*/
      ratPoint[numb].bins=header1[1];
      ratPoint[numb].NZscans=header1[2];
      ratPoint[numb].NAscans=header1[3];
      ratPoint[numb].n=header1[4];
      ratPoint[numb].nBands=header1[5];
      ratPoint[numb].nMat=header1[6];
      ratPoint[numb].withinSegs=header1[7];
      ratPoint[numb].withoutSegs=header1[8];
      ratPoint[numb].header2Length=13+ratPoint[numb].nBands;
    }else if(header1[0]==16){
      ratPoint[numb].min_R=(double)header1[1];
      ratPoint[numb].max_R=(double)header1[2];
      ratPoint[numb].bin_L=(double)header1[3];
      ratPoint[numb].bins=header1[4];
      ratPoint[numb].NZscans=header1[5];
      ratPoint[numb].NAscans=header1[6];
      ratPoint[numb].n=header1[7];
      ratPoint[numb].nBands=header1[8];
      ratPoint[numb].nMat=header1[9];
      ratPoint[numb].withinSegs=header1[10];
      ratPoint[numb].withoutSegs=header1[11];
      ratPoint[numb].pBands=1;
      ratPoint[numb].EpLength=ialloc(ratPoint[numb].pBands,"pulse length",0);
      ratPoint[numb].EpLength[0]=header1[12];
      ratPoint[numb].header2Length=12+ratPoint[numb].nBands;
    }else if(header1[0]==17){
      ratPoint[numb].bins=header1[1];
      ratPoint[numb].NZscans=header1[2];
      ratPoint[numb].NAscans=header1[3];
      ratPoint[numb].n=header1[4];
      ratPoint[numb].nBands=header1[5];
      ratPoint[numb].nMat=header1[6];
      ratPoint[numb].withinSegs=header1[7];
      ratPoint[numb].withoutSegs=header1[8];
      ratPoint[numb].pBands=1;
      ratPoint[numb].EpLength=ialloc(ratPoint[numb].pBands,"pulse length",0);
      ratPoint[numb].EpLength[0]=header1[9];
      ratPoint[numb].header2Length=15+ratPoint[numb].nBands;
    }
  }/*not SALCA. Tody the aboive eventually*/

  /*check sensor head splitting*/
  if(dimage->withinSegs>ratPoint[numb].withinSegs||dimage->withoutSegs>ratPoint[numb].withoutSegs){
    fprintf(stderr,"The scan %d is only split into %d and %d and cannot be split into %d and %d\n",numb,\
      ratPoint[numb].withinSegs,ratPoint[numb].withoutSegs,dimage->withinSegs,dimage->withoutSegs);
    exit(1);
  }

  TIDY(header1);

  return;
}/*readIntHeader*/


/*#####################################################################################*/
/*read int header, SALCA format*/

void readIntForm18(RatControl *ratPoint,FILE *data)
{
  int i=0;
  int *header1=NULL;

  /*we should be pointing at the second integer of the file*/
  if(fread(&ratPoint->headerLength,sizeof(int),1,data)!=1){
    fprintf(stderr,"error reading the header.\n");
    exit(1);
  }
  if(fread(&ratPoint->header2Length,sizeof(int),1,data)!=1){
    fprintf(stderr,"error reading the header.\n");
    exit(1);
  }
  if(fseek(data,0,SEEK_SET)){  /*rewind to file start*/
    fprintf(stderr,"fseek error\n");
    exit(1);
  }
  header1=ialloc(ratPoint->headerLength,"int header",0);
  if(fread(&(header1[0]),sizeof(int),ratPoint->headerLength,data)!=ratPoint->headerLength){
    fprintf(stderr,"error reading the header.\n");
    exit(1);
  }
  if((header1[1]!=ratPoint->headerLength)||(header1[2]!=ratPoint->header2Length)){
    fprintf(stderr,"Was?\n");
    exit(1);
  }
  ratPoint->bins=header1[3];
  ratPoint->NZscans=header1[4];
  ratPoint->NAscans=header1[5];
  ratPoint->n=header1[6];
  ratPoint->nBands=header1[7];
  ratPoint->nMat=header1[8];
  ratPoint->withinSegs=header1[9];
  ratPoint->withoutSegs=header1[10];
  ratPoint->pBands=header1[11];
  TIDY(ratPoint->EpLength);
  ratPoint->EpLength=ialloc(ratPoint->pBands,"pulse bins",0);
  for(i=0;i<ratPoint->pBands;i++)ratPoint->EpLength[i]=header1[12+i];
  TIDY(header1);
  return;
}/*readIntForm18*/


/*#####################################################################################*/
/*read decimal header, SALCA format*/

void readFloForm18(RatControl *ratPoint,FILE *data)
{
  int i=0;
  double *header2=NULL;

  header2=dalloc(ratPoint->header2Length,"decimal header",0);
  if(fread(&(header2[0]),sizeof(double),ratPoint->header2Length,data)!=ratPoint->header2Length){
    fprintf(stderr,"error reading the decimal header.\n");
    exit(1);
  }

  ratPoint->min_R=header2[0];
  ratPoint->max_R=header2[1];
  ratPoint->bin_L=header2[2];
  for(i=0;i<3;i++)ratPoint->from[i]=header2[i+3];
  ratPoint->Ldiv=header2[6];
  ratPoint->div=header2[7];
  ratPoint->divSt=header2[8];
  ratPoint->iZen=header2[9];
  ratPoint->fZen=header2[10];
  ratPoint->iAz=header2[11];
  ratPoint->fAz=header2[12];
  ratPoint->squint=header2[13];
  for(i=14;i<14+ratPoint->nBands;i++)ratPoint->wavelength[i-14]=header2[i];
  TIDY(ratPoint->Plength);
  ratPoint->Plength=falloc(ratPoint->pBands,"pulse length",0);
  for(i=0;i<ratPoint->pBands;i++)ratPoint->Plength[i]=header2[14+ratPoint->nBands+i];
  ratPoint->Pres=header2[14+ratPoint->nBands+ratPoint->pBands];

  return;
}/*readFloForm18*/


/*#####################################################################################*/
/*read the decimal header*/

void readFloHeader(int format,FILE *data,RatControl *ratPoint,int numb,RatImage *dimage)
{
  int i=0;
  double *header2=NULL;

  if(!(ratPoint[numb].wavelength=(double *)calloc(ratPoint[numb].nBands,sizeof(double)))){
    fprintf(stderr,"error in wavelength array allocation %d.\n",ratPoint[numb].nBands);
    exit(1);
  }
  if(!(ratPoint[numb].ratRes=(RatResults *)calloc(ratPoint[numb].NZscans*ratPoint[numb].NAscans,sizeof(RatResults)))){
    fprintf(stderr,"error allocating the results.\n");
    exit(1);
  }

  if(format==18){
    readFloForm18(&(ratPoint[numb]),data);
  }else{
    /*allocate space for the header array*/
    if(!(header2=(double *)calloc(ratPoint[numb].header2Length,sizeof(double)))){
      fprintf(stderr,"error in temporary header array allocation.\n");
      exit(1);
    }
    if(fseek(data,(long)(ratPoint[numb].headerLength*sizeof(int)),SEEK_SET)){fprintf(stderr,"fseek error\n");exit(1);}
    if((fread(&(header2[0]),sizeof(double),(size_t)ratPoint[numb].header2Length,data))!=(size_t)ratPoint[numb].header2Length){
      fprintf(stderr,"error reading the header.\n");
      exit(1);
    }
    if(dimage->byteord)header2=doSwap(header2,(uint64_t)ratPoint[numb].header2Length);
    /*set ratRes and ratPoint equal to the binary file(s?)*/
    if(format!=15&&format!=17){         /*integer lidar parameters*/
      for(i=0;i<3;i++){
        ratPoint[numb].from[i]=header2[i];
      }
      ratPoint[numb].Ldiv=header2[3];
      ratPoint[numb].div=header2[4];
      ratPoint[numb].divSt=header2[5];
      ratPoint[numb].iZen=header2[6];
      ratPoint[numb].fZen=header2[7];
      ratPoint[numb].iAz=header2[8];
      ratPoint[numb].fAz=header2[9];
      for(i=10;i<10+ratPoint[numb].nBands;i++){
        ratPoint[numb].wavelength[i-10]=header2[i];
      }
    }else if(format==15||format==17){   /*double lidar parameters*/
      ratPoint[numb].min_R=header2[0];
      ratPoint[numb].max_R=header2[1];
      ratPoint[numb].bin_L=header2[2];
      for(i=3;i<6;i++){
        ratPoint[numb].from[i-3]=header2[i];
      }
      ratPoint[numb].Ldiv=header2[6];
      ratPoint[numb].div=header2[7];
      ratPoint[numb].divSt=header2[8];
      ratPoint[numb].iZen=header2[9];
      ratPoint[numb].fZen=header2[10];
      ratPoint[numb].iAz=header2[11];
      ratPoint[numb].fAz=header2[12];
      for(i=13;i<13+ratPoint[numb].nBands;i++){
        ratPoint[numb].wavelength[i-13]=header2[i];
      }
    }
    if(format==16){        /*int lidar*/
      ratPoint[numb].pBands=1;
      ratPoint[numb].Plength=falloc(1,"pulse length",0);
      ratPoint[numb].Plength[0]=header2[10+ratPoint[numb].nBands];
      ratPoint[numb].Pres=header2[10+ratPoint[numb].nBands+1];
    }else if(format==17){  /*decimal lidar*/
      ratPoint[numb].pBands=1;
      ratPoint[numb].Plength=falloc(1,"pulse length",0);
      ratPoint[numb].Plength[0]=header2[13+ratPoint[numb].nBands];
      ratPoint[numb].Pres=header2[13+ratPoint[numb].nBands+1];
    }
    TIDY(header2);
  }/*SALCA/not SALCA test*/
  return;
}/*readFloHeader*/

/*that's the end */
/*######################################################*/
